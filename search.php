<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package underscores_sample
 */
global $wp_query;
get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) { ?>

			<header class="page-header">
				<h1 class="page-title"><?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'rcconnect_uk_2017' ), '<span>' . get_search_query() . '</span>' );
				?></h1>

			</header><!-- .page-header -->
			
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			if (  $wp_query->max_num_pages > 1 ) {
				echo '
					<div class="clearfix"></div>
					<div class="ajax-load-more mb-40">
						<a href="#" class="btn btn-default rc_loadmore_btn"><i class="fa fa-plus-circle"></i>Show More</a>
					</div>';
			}

		} else {

			get_template_part( 'template-parts/content', 'none' );

		} ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
