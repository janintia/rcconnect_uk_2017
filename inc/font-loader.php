<?php

function rc_uk_footer_font_loader() {
	?>
	<script type="text/javascript">
		WebFontConfig = {
			// google: { families: [ 'Lato:300,400,700', 'Merriweather:700i' ] },
			// google: { families: [ 'Merriweather:700i' ] },
			// google: { families: [ 'Lato:300,400,700', 'Noto+Serif:400i' ] },
			google: { families: [ 'Noto+Serif:400i' ] },
			custom: {
				families: ['Proxima Nova'],
				urls: ['<?php echo get_theme_file_uri( '/css/webfonts.css' ); ?>']
			}
		};
		
		(function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			'://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})(); 
		
	</script>
	<?php
}
add_action( 'wp_footer', 'rc_uk_footer_font_loader' );