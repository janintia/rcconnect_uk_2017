<?php

/**
 * Google Tag Manager
 *
 */
function rc_uk_header_header_gtm() {
	?>	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer', '<?php echo rc_google_tag_manager(); ?>');</script>
	<!-- End Google Tag Manager -->
		
	<!-- Start of Sleeknote signup and lead generation tool - www.sleeknote.com -->
	<script id='sleeknoteScript' type='text/javascript'>
		(function () {        var sleeknoteScriptTag = document.createElement('script');
			sleeknoteScriptTag.type = 'text/javascript';
			sleeknoteScriptTag.charset = 'utf-8';
			sleeknoteScriptTag.src = ('//sleeknotecustomerscripts.sleeknote.com/12549.js');
			var s = document.getElementById('sleeknoteScript'); 
		   s.parentNode.insertBefore(sleeknoteScriptTag, s); 
	   })();
	   </script>
	<!-- End of Sleeknote signup and lead generation tool - www.sleeknote.com -->
	<?php
}
add_action( 'wp_head', 'rc_uk_header_header_gtm' );

function rc_add_google_tag_manager_body() {
	?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo rc_google_tag_manager(); ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php
}
add_action( 'rcm_body_start', 'rc_add_google_tag_manager_body' );

function rc_uk_footer_omniture() {
	?>
	<!-- Omniture -->
	<script type="text/javascript">_satellite.pageBottom();</script>
	<?php
}
// add_action( 'wp_footer', 'rc_uk_footer_omniture' );

function rc_uk_footer_linkedin_insight() {
	?>
	<script type="text/javascript">
		_linkedin_partner_id = "593794";
		window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
		window._linkedin_data_partner_ids.push(_linkedin_partner_id);
	</script><script type="text/javascript">
		(function(){var s = document.getElementsByTagName("script")[0];
		var b = document.createElement("script");
		b.type = "text/javascript";b.async = true;
		b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		s.parentNode.insertBefore(b, s);})();
	</script>
	<noscript>
		<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=593794&fmt=gif" />
	</noscript>
	<?php
}
add_action( 'wp_footer', 'rc_uk_footer_linkedin_insight' );

/**
 * Header and Footer scripts
 *
 */
function rc_uk_header_scripts() {
	?>
	<!-- SiteCatalyst code version: H.27.5.--><!--defer async-->
	<script type="text/javascript"  src="//assets.adobedtm.com/7618e9029c8436bd1b1d926078af6c171547716a/satelliteLib-aed35a9669ef96a6ae2c7fe7c9d0155d4c489109.js"></script>
	<?php
}
// add_action( 'wp_head', 'rc_uk_header_scripts' );

function google_site_tag() {
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-36051195-4"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-36051195-4');
	</script>
	<?php
}

function rc_uk_footer_scripts() {
	?>
	<!--Marketo -->
	<script src="//munchkin.marketo.net/munchkin.js" type="text/javascript"></script>
	<script>mktoMunchkin("075-DTB-715");</script>

	<!-- Omniture -->
	<script type="text/javascript">_satellite.pageBottom();</script>

	<!-- CrazyEgg -->
	<script type="text/javascript">
		setTimeout(function(){var a=document.createElement("script");
		var b=document.getElementsByTagName("script")[0];
		a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0034/7442.js?"+Math.floor(new Date().getTime()/3600000);
		a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
	</script>
	<?php
}
// add_action( 'wp_footer', 'rc_uk_footer_scripts' );