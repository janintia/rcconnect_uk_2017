<?php 
function rc_domain_url( $url ){
    $pu = parse_url($url);
    return $pu["scheme"] . "://" . $pu["host"];
}

function rc_country() {    
    if( defined( "RC_BLOG_COUNTRY" ) ) {
        return RC_BLOG_COUNTRY;
    } else {
        return "UK";
    }
}

function rc_bmid_param() {
    global $post;

    $bmid = get_post_meta($post->ID, '_rc_uk_bmid_meta_key', true);

    if(!empty( $bmid )) {
        return "?BMID=" . $bmid;
    }

    return "?BMID=BLOGNEW" . rc_country();
}

function rc_gw_url() {
    return esc_url( rc_domain_url( home_url( '/' ) ) ) . rc_bmid_param();
}

function rc_google_tag_manager() {
    if( defined( "RC_BLOG_COUNTRY" ) ) {
        if( RC_BLOG_COUNTRY === 'FR' ) {
            return 'GTM-5BW8VNB';
        } elseif( RC_BLOG_COUNTRY === 'UK' ) {
            return 'GTM-TK5TW3H';
        }
    } else {
        return 'GTM-TK5TW3H';
    }
}

/**
 * Enqueue inline Javascript. @see wp_enqueue_script().
 * 
 * KNOWN BUG: Inline scripts cannot be enqueued before 
 *  any inline scripts it depends on, (unless they are
 *  placed in header, and the dependant in footer).
 * 
 * @param string      $handle    Identifying name for script
 * @param string      $src       The JavaScript code
 * @param array       $deps      (optional) Array of script names on which this script depends
 * @param bool        $in_footer (optional) Whether to enqueue the script before </head> or before </body> 
 * 
 * @return null
 */
function enqueue_inline_script( $handle, $js, $deps = array(), $in_footer = false ){
    // Callback for printing inline script.
    $cb = function()use( $handle, $js ){
        // Ensure script is only included once.
        if( wp_script_is( $handle, 'done' ) )
            return;
        // Print script & mark it as included.
        echo "<script type=\"text/javascript\" id=\"js-$handle\">\n$js\n</script>\n";
        global $wp_scripts;
        $wp_scripts->done[] = $handle;
    };
    // (`wp_print_scripts` is called in header and footer, but $cb has re-inclusion protection.)
    $hook = $in_footer ? 'wp_print_footer_scripts' : 'wp_print_scripts';

    // If no dependencies, simply hook into header or footer.
    if( empty($deps)){
        add_action( $hook, $cb );
        return;
    }

    // Delay printing script until all dependencies have been included.
    $cb_maybe = function()use( $deps, $in_footer, $cb, &$cb_maybe ){
        foreach( $deps as &$dep ){
            if( !wp_script_is( $dep, 'done' ) ){
                // Dependencies not included in head, try again in footer.
                if( ! $in_footer ){
                    add_action( 'wp_print_footer_scripts', $cb_maybe, 11 );
                }
                else{
                    // Dependencies were not included in `wp_head` or `wp_footer`.
                }
                return;
            }
        }
        call_user_func( $cb );
    };
    add_action( $hook, $cb_maybe, 0 );
}

// Modifies the main query in the Homepage.
// Dtermines whether a post is excluded from the Homepage by checking the post_meta '_efhp_meta_key' 
function rc_modify_main_query( $query ) {    
    if ( $query->is_home() && $query->is_main_query() ) { // Run only on the homepage
        $excluded_posts = rc_get_meta_values( '_efhp_meta_key', 'true' );
        $query->query_vars['post__not_in'] = $excluded_posts; // Exclude my featured category because I display that elsewhere
    }
}
add_action( 'pre_get_posts', 'rc_modify_main_query' );

// Gets all the values of a post meta from all the posts
function rc_get_meta_values( $key = '', $value = '', $type = 'post', $status = 'publish' ) {
    global $wpdb;

    if( empty( $key ) ) {
        return;
    }

    $meta_value_param = '';
    if( $value != '' ) {
        $format = "AND pm.meta_value = '%s'";
        $meta_value_param = sprintf($format, $value);
    }
    
    $r = $wpdb->get_col( $wpdb->prepare( "
        SELECT pm.post_id FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s' 
        AND p.post_status = '%s' 
        AND p.post_type = '%s'" . $meta_value_param, 
        $key, $status, $type ) );

    return $r;
}