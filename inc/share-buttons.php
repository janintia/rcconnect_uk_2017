<?php

function rc_uk_share_buttons() {
	$title = urlencode( get_the_title() );
	$permalink = urlencode( get_permalink() );
	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	?>
	<script>
		function fbShare(url, title, descr, image, winWidth, winHeight) {
			var winTop = (screen.height / 2) - (winHeight / 2);
			var winLeft = (screen.width / 2) - (winWidth / 2);
			window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
		}
	</script>
	<div class="w rc-uk-postshares">
		<ul id="rc-uk-shares" class="rc-uk-shares list-unstyled">
			<li><span>Share</span></li>
			<li>
				<a href="http://twitter.com/share?text=<?php echo esc_html( $title ); ?>&amp;url=<?php echo esc_url( $permalink ); ?>" title="Tweet" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" target="_parent">
					<i class="fa fa-twitter"></i>
				</a>
			</li>
			<li id="fb">
				<a href="javascript:fbShare('<?php echo esc_url( $permalink ); ?>', '<?php echo esc_html( $title ); ?>', 'Facebook share popup', '<?php echo esc_url( $featured_img_url ); ?>', 520, 350)"><i class="fa fa-facebook"></i></a>
			</li>
			<li id="googleplus">
				<a href="https://plus.google.com/share?url=<?php echo esc_url( $permalink ); ?>" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=400, height=470'); return false" title="Share on Google+" target="_parent">
					<i class="fa fa-google-plus"></i>
				</a>
			</li>
		</ul>
	</div>
	<?php
}
//add_action( 'rc_uk_after_site_main', 'rc_uk_share_buttons' );

function rc_uk_bctt_atts( $atts ) {
	
}
// add_filter( 'bctt_atts', 'rc_uk_bctt_atts' );

/**
 * Generate bitly short url
 *
 * @since    1.0.0
 */
function rc_generate_bitly_url( $url, $atts ) {	
	global $post;
	
	$bitlyUrl = get_post_meta( $post->ID, '_bctt_bitly_url_twitter', true );
	
	if ( $bitlyUrl ) {
	    	return $bitlyUrl;
			
	} else {
		//generate the URL
		$bitly = 'http://api.bit.ly/v3/shorten?login=o_3j80173mon&apiKey=R_5465cfb6487b46c49df32f65bafb8970&longUrl=' . urlencode( rc_social_share_bmid( $url, 'twitter' ) ) . '&format=txt';
		
		$response = wp_remote_get( $bitly,  array( 'timeout' => 15 ) );
		
		if ( ! is_wp_error( $response ) && isset( $response['response']['code'] ) && 200 === $response['response']['code'] ) {
			$short_url = trim( wp_remote_retrieve_body( $response ) );
			
			update_post_meta( $post->ID, '_bctt_bitly_url_twitter', $short_url );
			
			return $short_url;
		}
	}
	
	return false;
}

add_filter( 'bctturl', 'rc_generate_bitly_url', 10, 2 );
// Append BMID=BLOGNEWUK and Shorten URL of Better Click to Tweet