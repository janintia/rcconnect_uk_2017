<?php

class RC_UK_Video_Widget extends WP_Widget {
	
	public function __construct() {
		// Add Widget scripts
		add_action('admin_enqueue_scripts', array($this, 'scripts'));
		
		$widget_options = array( 
			'classname' => 'rc-uk-video-widget',
			'description' => 'A widget for displaying a YouTube video.',
		);
		
		parent::__construct( 'rc-uk-video-widget', 'RC UK - YouTube Video', $widget_options );
		
	}
	
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance[ 'title' ] );
		$youtube_url = apply_filters( 'rc_uk_vw_video_url', $instance[ 'youtube_url' ] );
		$image = ! empty( $instance['image'] ) ? $instance['image'] : '';
		$description = apply_filters( 'rc_uk_vw_desc', $instance[ 'description' ] );
		
		echo $args['before_widget']; 
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		?>
		<div class="video-img">
			<a href="#" class="play-btn" data-toggle="modal" data-target="#videoModal" data-thevideo="<?php echo esc_url( $youtube_url ); ?>">
				<span></span>
			</a>						
			<?php if($image): ?>
			  <img src="<?php echo site_url(). '/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif'; ?>" class="lazy lazy-hidden" data-lazy-type="image" data-src="<?php echo esc_url($image); ?>" alt="lazy loader">
		   <?php endif; ?>
		</div>		 
		<!--<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
			  <div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close X</button>
				<div>
				  <iframe width="100%" height="550" src="" sandbox="allow-scripts allow-same-origin allow-popups allow-presentation"></iframe>
				</div>
			  </div>
			</div>
		  </div>
		</div>-->
		<p class="desc mt-10"><?php echo esc_html( $description ); ?></p>
		<?php echo $args['after_widget'];
	}
	
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$youtube_url = ! empty( $instance['youtube_url'] ) ? $instance['youtube_url'] : '';
		$description = ! empty( $instance['description'] ) ? $instance['description'] : '';
		$image = ! empty( $instance['image'] ) ? $instance['image'] : '';
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'youtube_url' ); ?>">YouTube Video URL:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'youtube_url' ); ?>" name="<?php echo $this->get_field_name( 'youtube_url' ); ?>" value="<?php echo esc_attr( $youtube_url ); ?>" class="widefat" />
		</p>
		<p>
		  <label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" type="text" value="<?php echo esc_url( $image ); ?>" />
		  <button class="upload_image_button button button-primary">Upload Image</button>
	   </p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>">Description:</label>
			<textarea id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" class="widefat"><?php echo esc_attr( $description ); ?></textarea>
		</p><?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
		$instance[ 'youtube_url' ] = strip_tags( $new_instance[ 'youtube_url' ] );
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? $new_instance['image'] : '';
		$instance[ 'description' ] = strip_tags( $new_instance[ 'description' ] );
		return $instance;
	}
	
	public function scripts() {
	   wp_enqueue_script( 'media-upload' );
	   wp_enqueue_media();
	   wp_enqueue_script('rc-media-admin', get_template_directory_uri() . '/src/js/rc-media-admin.js', array('jquery'));
	}
}

function rc_uk_video_widget() { 
  register_widget( 'RC_UK_Video_Widget' );
}
add_action( 'widgets_init', 'rc_uk_video_widget' );