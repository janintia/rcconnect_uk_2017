<?php

class RC_UK_Newsletter_Subscribe extends WP_Widget {
	
	public function __construct() {
		
		$widget_options = array( 
			'classname' => 'rc-uk-newsletter-subscribe',
			'description' => 'A MailChip newsletter subscription widget.',
		);
		
		parent::__construct( 'rc-uk-newsletter-subscribe', 'RC UK - Newsletter Subscribe', $widget_options );
		
	}
	
	public function widget( $args, $instance ) {
		$label = apply_filters( 'widget_title', $instance[ 'label' ] );
		$description = apply_filters( 'rc_uk_newsletter_form_desc', $instance[ 'description' ] );
		
		echo $args['before_widget']; ?>

		<!-- Begin MailChimp Signup Form -->
		<form action="https://ringcentral.us17.list-manage.com/subscribe/post?u=38ab95e69ee2f500812a37d3b&amp;id=081e384d43" method="post" name="subscribe-widget-form" class="subscribe-widget-form">
			<div id="mc_embed_signup_scroll">
				<h4><?php echo esc_html( $label ); ?></h4>
				<p class="mb-20"><?php echo esc_html( $description ); ?></p>
				<div class="input-group">
					<input type="email" value="" name="EMAIL" class="email form-control" id="subscribe-email" placeholder="Type your email and hit enter" required>
					
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_38ab95e69ee2f500812a37d3b_081e384d43" tabindex="-1" value=""></div>
					
					<span class="input-group-btn">
						<button type="submit" class="btn btn-default" name="subscribe" id="mc-embedded-subscribe">
							<span class="subscribe-btn"></span>
							<span class="sr-only">Search</span>
						</button>
					</span>
				</div>
			</div>
		</form>
		<!--End mc_embed_signup-->
		
		<?php echo $args['after_widget'];
	}
	
	public function form( $instance ) {
		$label = ! empty( $instance['label'] ) ? $instance['label'] : '';
		$description = ! empty( $instance['description'] ) ? $instance['description'] : ''; ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'label' ); ?>">Label:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'label' ); ?>" name="<?php echo $this->get_field_name( 'label' ); ?>" value="<?php echo esc_attr( $label ); ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>">Description:</label>
			<textarea id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" class="widefat"><?php echo esc_attr( $description ); ?></textarea>
		</p><?php 

	}
	
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance[ 'label' ] = strip_tags( $new_instance[ 'label' ] );
		$instance[ 'description' ] = strip_tags( $new_instance[ 'description' ] );
		return $instance;
	}
}

function rc_uk_newsletter_subscribe() { 
  register_widget( 'RC_UK_Newsletter_Subscribe' );
}
add_action( 'widgets_init', 'rc_uk_newsletter_subscribe' );