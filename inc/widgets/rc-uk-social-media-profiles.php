<?php

class RC_UK_Social_Media_Profiles extends WP_Widget {
	
	public function __construct() {
		
		$widget_options = array( 
			'classname' => 'rc-uk-social-media-profiles',
			'description' => 'A widget for displaying social media profiles.',
		);
		
		parent::__construct( 'rc-uk-social-media-profiles', 'RC UK - Social Media Profiles', $widget_options );
		
	}
	
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance[ 'title' ] );
		$twitter = apply_filters( 'rc_uk_sm_profile_twitter', $instance[ 'twitter' ] );
		$google_plus = apply_filters( 'rc_uk_sm_profile_google_plus', $instance[ 'google_plus' ] );
		$linkedin = apply_filters( 'rc_uk_sm_profile_linkedin', $instance[ 'linkedin' ] );
		$youtube = apply_filters( 'rc_uk_sm_profile_youtube', $instance[ 'youtube' ] );
		
		echo $args['before_widget']; 
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		?>
		<ul class="social-media-icons list-unstyled list-inline">
			<?php if( ! empty( $twitter ) ) { ?>
			<li><a href="<?php echo esc_url( $twitter ); ?>"><i class="fa fa-twitter"></i></a></li><!--Twitter-->
			<?php } ?>
			<?php if( ! empty( $google_plus ) ) { ?>
			<li><a href="<?php echo esc_url( $google_plus ); ?>"><i class="fa fa-google-plus"></i></a></li><!--Google+-->
			<?php } ?>
			<?php if( ! empty( $linkedin ) ) { ?>
			<li><a href="<?php echo esc_url( $linkedin ); ?>"><i class="fa fa-linkedin"></i></a></li><!--LinkedIn-->
			<?php } ?>
			<?php if( ! empty( $youtube ) ) { ?>
			<li><a href="<?php echo esc_url( $youtube ); ?>"><i class="fa fa-youtube"></i></a></li><!--YouTube-->
			<?php } ?>
		</ul>
		
		<?php echo $args['after_widget'];
	}
	
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$twitter = ! empty( $instance['twitter'] ) ? $instance['twitter'] : '';
		$google_plus = ! empty( $instance['google_plus'] ) ? $instance['google_plus'] : '';
		$linkedin = ! empty( $instance['linkedin'] ) ? $instance['linkedin'] : '';
		$youtube = ! empty( $instance['youtube'] ) ? $instance['youtube'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" />
		</p><p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>">Twitter URL:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo esc_attr( $twitter ); ?>" class="widefat" />
		</p><p>
			<label for="<?php echo $this->get_field_id( 'google_plus' ); ?>">Google+ URL:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'google_plus' ); ?>" name="<?php echo $this->get_field_name( 'google_plus' ); ?>" value="<?php echo esc_attr( $google_plus ); ?>" class="widefat" />
		</p><p>
			<label for="<?php echo $this->get_field_id( 'linkedin' ); ?>">LinkedIn URL:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="<?php echo esc_attr( $linkedin ); ?>" class="widefat" />
		</p><p>
			<label for="<?php echo $this->get_field_id( 'youtube' ); ?>">YouTube URL:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="<?php echo esc_attr( $youtube ); ?>" class="widefat" />
		</p>
		<?php
	}
	
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
		$instance[ 'twitter' ] = strip_tags( $new_instance[ 'twitter' ] );
		$instance[ 'google_plus' ] = strip_tags( $new_instance[ 'google_plus' ] );
		$instance[ 'linkedin' ] = strip_tags( $new_instance[ 'linkedin' ] );
		$instance[ 'youtube' ] = strip_tags( $new_instance[ 'youtube' ] );
		return $instance;
	}
}

function rc_uk_social_media_profiles() { 
  register_widget( 'RC_UK_Social_Media_Profiles' );
}
add_action( 'widgets_init', 'rc_uk_social_media_profiles' );