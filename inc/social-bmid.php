<?php

function rc_social_share_bmid( $url, $social_media ) {
	global $post;

	$return = $url;
	$bmid = '';
	$cid = '&CID=SOCIAL';
	
	switch( $social_media ) {
		case 'twitter':
			$bmid = '?BMID=BLOGNTWUK' . $cid;
			break;
			
		case 'linkedin':
			$bmid = '?BMID=BLOGNLIUK' . $cid;		
			break;
			
		case 'google_plus':
			$bmid = '?BMID=BLOGNGPUK' . $cid;		
			break;
			
		case 'facebook':
			$bmid = '?BMID=BLOGNFBUK' . $cid;			
			break;
			
		default:
			$bmid = '';			
	}

    $bmid_key = get_post_meta($post->ID, '_rc_uk_bmid_meta_key', true);

    if(!empty( $bmid_key )) {
        $bmid = '?BMID='. $bmid_key . $cid;
    }

	$return = $url . $bmid;			

	return $return;
}


/**
 * Generate bitly short url for sharing buttons per social media channel
 *
 * @since    1.0.0
 */
function rc_bitly_url( $url, $post_id = 0, $channel ) {
	
	$bitlyUrl = get_post_meta( $post_id, '_rc_bitly_url_' . $channel, true );
	if ( $bitlyUrl ) {
		return $bitlyUrl;
	} else {
		//generate the URL
		$bitly = 'http://api.bit.ly/v3/shorten?login=o_3j80173mon&apiKey=R_5465cfb6487b46c49df32f65bafb8970&longUrl=' . urlencode( rc_social_share_bmid( $url, $channel ) ) . '&format=txt';
		
		$response = wp_remote_get( $bitly,  array( 'timeout' => 15 ) );
		if ( ! is_wp_error( $response ) && isset( $response['response']['code'] ) && 200 === $response['response']['code'] ) {
			$short_url = trim( wp_remote_retrieve_body( $response ) );
			update_post_meta( $post_id, '_rc_bitly_url_' . $channel, $short_url );
			return $short_url;
		}
	}
	return false;

}


function rc_other_target_urls( $target_urls_array ) {
	$url_referrer     = wp_get_referer();
	$url = explode( '?', $url_referrer );
	$post_id = url_to_postid( $url[0] );

	// URL with Twitter BMID
	$target_urls_array[0][] = $url[0] . '?BMID=BLOGNTWUK';
	
	// URL with LinkedIn BMID
	$target_urls_array[0][] = $url[0] . '?BMID=BLOGNLIUK';
	
	// Facebook
	$fb_bitly_url = get_post_meta( $post_id, '_heateor_sss_bitly_url_facebook', true );
	if( ! empty( $fb_bitly_url ) ) {
		$target_urls_array[0][] = $fb_bitly_url;
	}
	
	// Twitter
	$tw_bitly_url = get_post_meta( $post_id, '_heateor_sss_bitly_url_twitter', true );
	if( ! empty( $tw_bitly_url ) ) {
		$target_urls_array[0][] = $tw_bitly_url;
	}
	// LinkedIn
	$li_bitly_url = get_post_meta( $post_id, '_heateor_sss_bitly_url_linkedin', true );
	if( ! empty( $li_bitly_url ) ) {
		$target_urls_array[0][] = $li_bitly_url;
	}
	
	// Additional URLs
	$addl_urls = get_post_meta( $post_id, 'rc_bitly_urls', true );
	
	if( ! empty( $addl_urls ) ) {
		$addl_urls_array = explode( PHP_EOL, $addl_urls );
		
		if( ! empty( $addl_urls_array ) ) {
			foreach( $addl_urls_array as $addl_url ) {
				$target_urls_array[0][] = $addl_url;
			}
		}
	}
		
	return $target_urls_array;
}
add_filter( 'heateor_sss_target_share_urls', 'rc_other_target_urls' );

?>