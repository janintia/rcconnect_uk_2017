<?php
/**
 * Adds extra fields of information on a user profile */
add_action( 'show_user_profile', 'rc_uk_extra_user_profile_fields' );
add_action( 'edit_user_profile', 'rc_uk_extra_user_profile_fields' );

function rc_uk_extra_user_profile_fields( $user ) { 
?>
	<h3><?php _e("Extra profile information", "blank"); ?></h3>

	<table class="form-table">
		<tr>
			<th>
				<label for="author_company"><?php _e('Company', 'rcconnect_uk_2017'); ?>
			</label></th>
			<td>
				<input type="text" name="author_company" id="author_company" value="<?php echo esc_attr( get_the_author_meta( 'author_company', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your role.', 'rcconnect_uk_2017'); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="company_position"><?php _e('Position', 'rcconnect_uk_2017'); ?>
			</label></th>
			<td>
				<input type="text" name="company_position" id="company_position" value="<?php echo esc_attr( get_the_author_meta( 'company_position', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your position.', 'rcconnect_uk_2017'); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="author_role"><?php _e('Role', 'rcconnect_uk_2017'); ?>
			</label></th>
			<td>
				<input type="text" name="author_role" id="author_role" value="<?php echo esc_attr( get_the_author_meta( 'author_role', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your role.', 'rcconnect_uk_2017'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="short_description"><?php _e("Short Description"); ?></label></th>
			<td>
				<textarea row="6" type="text" name="short_description" id="short_description" class="regular-text"><?php echo esc_attr( get_the_author_meta( 'short_description', $user->ID ) ); ?></textarea><br />
				<span class="description"><?php _e("Please enter a short description about this author."); ?></span>
			</td>
		</tr>
		
	</table>
	<?php 
}
add_action( 'personal_options_update', 'rc_uk_save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'rc_uk_save_extra_user_profile_fields' );

function rc_uk_save_extra_user_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

	update_user_meta( $user_id, 'author_company', $_POST['author_company'] );
	update_user_meta( $user_id, 'company_position', $_POST['company_position'] );
	update_user_meta( $user_id, 'author_role', $_POST['author_role'] );
	update_user_meta( $user_id, 'short_description', $_POST['short_description'] );
}
?>