<?php
/**
 * RC Connect UK 2017: Customizer
 *
 * @package WordPress
 * @subpackage rc_connect_uk_2017
 * @since 1.0
 */

require_once get_parent_theme_file_path( 'inc/classes/post-dropdown-custom-control.php' );
	
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function rc_connect_uk_2017_customize_register( $wp_customize ) {
	
	$wp_customize->add_section( 'front_page_section' , array(
    		'title'      => esc_html__( 'Front Page', 'rcconnect_uk_2017' ),
            'capability'  => 'edit_theme_options', //Capability needed to tweak
			'priority'   => 30,
	) );
	
	for ( $count = 1; $count <= 3; $count++ ) {		
	
		$wp_customize->add_setting( 'post_dropdown_setting_' . $count, array(
			'default'       => '',
		) );
		
		$wp_customize->add_control( new Post_Dropdown_Custom_Control( $wp_customize, 'post_dropdown_setting_' . $count, array(
			'label'   => 'Hero Banner Post #' . $count,
			'section' => 'front_page_section',
			'settings'   => 'post_dropdown_setting_' . $count,
		) ) );
		
	}
}
add_action( 'customize_register', 'rc_connect_uk_2017_customize_register' );
