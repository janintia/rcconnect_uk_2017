<?php

if (defined('WPSEO_VERSION')) {
	
    function filter_wpseo_robots( $robots ) {
		
	    if ( is_paged() ) {
			
			// Do I need to exclude the homepage and front page?
			//if ( !is_home() && !is_front_page() ) {
		        return 'noindex,follow';	
            //}				
	    }
	    return $robots;
    }	
    add_filter( 'wpseo_robots', 'filter_wpseo_robots' );
	
	function filter_wpseo_metadesc( $s ) {
		global $wp_query;
		global $page;
		
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		
		! empty ( $page ) && 1 < $page && $paged = $page;
		
		$paged > 1 && $s .= ' | ' . sprintf( __( 'Page %s of %s' ), $paged, $wp_query->max_num_pages );
		
		return $s;
	}
	add_filter( 'wpseo_metadesc', 'filter_wpseo_metadesc', 100, 1 );
}

/* Disable image compression */
add_filter('jpeg_quality', function($arg){return 95;});


/* Disable Yoast SEO default schema */
function rc_disable_yoast_schema_data($data) {
	if( !is_home() && $data['@type'] === 'Organization' ) {
		$data = array();
	}
	return $data;
}
add_filter('wpseo_json_ld_output', 'rc_disable_yoast_schema_data', 10, 1);

function featured_images_RSS($content) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ){
		$content = '<p>' . get_the_post_thumbnail( $post->ID, 'large', array( 'style' => 'margin-bottom: 20px;' )) . '</p>' . $content;
	}
	return $content;
}
add_filter('the_excerpt_rss', 'featured_images_RSS');
add_filter('the_content_feed', 'featured_images_RSS');