<?php
/**
 * Replace URL of canonical tag of Yoast SEO
 *
 * This will replace the base URL in the canonical 
 * when the page is viewed from the origin domain (blog.corp)
 *
 * @author	Jan Intia
 * @since	2.1
 * @param	string	$canonical	The URL from Yoast.
 * @return  string	$canonical	Returns the URL for the canonical tag.
 * 
 */
function rc_replace_canonical_url( $canonical ) {
	
	// if requested from original server (blog.corp)
	if ( ( empty( $_SERVER['HTTP_X_FORWARDED_HOST'] ) ) &&
	( empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) ) { 
	
		if( rc_country() === 'UK' ) {
			return str_replace( get_site_url(), 'https://www.ringcentral.co.uk/blog', $canonical );
		} elseif( rc_country() == 'FR' ) {
			return str_replace( get_site_url(), 'https://www.ringcentral.fr/blog', $canonical );
		}
	
	} else {
		return $canonical;
		
	}		
}
add_filter('wpseo_canonical', 'rc_replace_canonical_url');


/**
 * Revert to original URL
 *
 * Uses the original URL of origin (blog.corp)
 *
 * @author	Jan Intia
 * @since	2.1
 * @param	string	$url	The URL to convert.
 * @param	object	$p		Post object.
 * @return  string	$url	Returns the URL.
 * 
 */
function rc_revert_canonical_url( $url, $p ) {
	
	// if requested from original server (blog.corp)
	if ( ( empty( $_SERVER['HTTP_X_FORWARDED_HOST'] ) ) &&
	( empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) ) { 
	
		if( rc_country() === 'UK' ) {
			return str_replace( 'https://www.ringcentral.co.uk/blog', get_site_url(), $url );
		} elseif( rc_country() == 'FR' ) {
			return str_replace( 'https://www.ringcentral.fr/blog', get_site_url(), $url );
		}
	} else {
		return $url;
		
	}		
}


/**
 * Change base URLs in sitemap_index.xml
 *
 * @author	Jan Intia
 * @since	2.1
 * 
 */
function rc_change_base_url_wpseo( $type ) { 	
	// root map xml
	if( 1 == $type ) {
		add_filter( 'home_url', 'rc_change_root_map_url', 10, 4 );
		
	} elseif ( post_type_exists( $type ) || $tax = get_taxonomy( $type ) || $type == 'author' ) {		
		add_filter( 'home_url', 'rc_change_root_map_url', 10, 4 );
		
		if( version_compare( WPSEO_VERSION, '5.2', '>=' ) ) {
		   
		} else {		   
			add_filter( 'wpseo_xml_sitemap_post_url', 'rc_revert_canonical_url', 10, 2 );
			add_filter( 'wpseo_sitemap_post_type_archive_link', 'rc_change_post_type_archive_map_url', 10, 2 );
			add_filter( 'wpseo_sitemap_entry', 'rc_change_post_type_map_url', 10, 3 );
		}
		
	}
	
	return $type;
};		 
add_filter( 'wpseo_build_sitemap_post_type', 'rc_change_base_url_wpseo', 10, 1 );

/**
 * Filter for changing the xml root map urls
 *
 * @author	Jan Intia
 * @since	2.1
 * 
 */
function rc_change_root_map_url( $url, $path, $orig_scheme, $blog_id ) {
	$url = rc_replace_canonical_url( $url );	
	
	return $url;
}

/**
 * Filter for changing the xml post type archive map urls
 *
 * @author	Jan Intia
 * @since	2.1
 * 
 */
function rc_change_post_type_archive_map_url( $url, $p ) {
	$url = rc_replace_canonical_url( $url );	
	
	return $url;
}

/**
 * Filter for changing the xml post type map urls
 *
 * @author	Jan Intia
 * @since	2.1
 * 
 */
function rc_change_post_type_map_url( $url, $post_s, $p ) {
	$url = rc_replace_canonical_url( $url );	
	
	return $url;
}

add_filter( 'wpseo_enable_xml_sitemap_transient_caching', '__return_false' );

/**
 * Sitemap Modification for Priority based on Post's date
 */
function rc_custom_post_xml_priority( $return, $type, $post) {
	if($type == 'post'){			
		$post_date = $post->post_date;
		$now = date('Y-m-d H:i:s');
		$monthdiff = month_diff($now, $post_date);
		if($monthdiff >=60){
			$return = '0.3';
		} elseif($monthdiff >= 24 && $monthdiff <= 59){
			$return = '0.4';
		} elseif($monthdiff >= 12 && $monthdiff <= 23){
			$return = '0.6';
		}else {
			$return = '0.7';
		}
					
	}
	
	if($type == 'page'){			
		$return = '0.5';	
	}
	return $return;
}
add_filter( 'wpseo_xml_sitemap_post_priority', 'rc_custom_post_xml_priority', 10, 3 );

function rc_my_custom_page_freq( $default, $url ) {
	if($url==site_url()){
		return 'daily';
	} else {
		return 'yearly';
	}		
}
add_filter( 'wpseo_sitemap_page_single_change_freq', 'rc_my_custom_page_freq', 10, 2 );

function rc_my_custom_post_freq( $default, $url ) {
	if($url==site_url()){
		return 'daily';
	} else {
		return 'monthly';
	}
	
}
add_filter( 'wpseo_sitemap_post_single_change_freq', 'rc_my_custom_post_freq', 10, 2 );

function rc_custom_term_priority( $url, $type, $term) {
	if ($type == 'term') {
		$url['pri'] = 0.5;
		$url['chf'] = 'monthly';
	}
	return $url;
}
add_filter( 'wpseo_sitemap_entry', 'rc_custom_term_priority', 10, 3 );	


/**
 * Add BMID URL parameter to Home URL
 *
 * This will add a ?BMID=BLOGNEWUK parameter to every links
 * pointing to ringcentral.co.uk domain pages
 * from the blog interface, single posts/article,
 * salesforce box integration banner.
 *
 * @author	Jan Intia
 * @since	2.1
 * @param	string	$url	The URL to add the param into.
 * @return  string			Returns the URL GET parameter with value.	
 * 
 */
function rcconnect_add_bmid( $url ) {
	
	// check if we're in a blog interface or an article
	if( ( 'post' === get_post_type() && is_single() ) || is_home() ) {
		$url = add_query_arg( array(
			'BMID' => 'BLOGNEW' . rc_country(),
		), $url );
	}
	
	return $url;
}


/**
 * Add Async to JS deferred by Autoptimize plugin
 *
 * @author	Jan Intia
 * @since	2.1
 * 
 */
function rc_ao_override_defer( $defer ) {
	return $defer . "async " ;
}
// add_filter( 'autoptimize_filter_js_defer', 'rc_ao_override_defer', 10,1 );