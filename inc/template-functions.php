<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package underscores_sample
 */
 
 
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since RC Connect UK 2017 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function rc_uk_blog_2017_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Read more<span class="screen-reader-text"> "%s"</span>', 'rcconnect_uk_2017' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'rc_uk_blog_2017_excerpt_more' );

/* Change Excerpt length */
function rc_uk_blog_2017_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'rc_uk_blog_2017_excerpt_length', 999 );


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function underscores_sample_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'underscores_sample_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function underscores_sample_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'underscores_sample_pingback_header' );

function rc_uk_get_category_info( $post_categories ) {
	$cats = array();
     
	foreach($post_categories as $c){
		$cat = get_category( $c );
		$cats[] = array( 
			'name' => $cat->name, 
			'slug' => $cat->slug,
			'url' => get_category_link( $cat->term_id ),
		);
	}
	return $cats;
}

/**
 * Disable WordPress sanitization to allow more than just $allowedtags from /wp-includes/kses.php
 */
remove_filter('pre_user_description', 'wp_filter_kses');
/**
 * Add sanitization for WordPress posts
 */
add_filter( 'pre_user_description', 'wp_filter_post_kses');

/**
 * Remove 'Category', 'Author', 'Tags' title text
 */
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>' ;
    }

    return $title;

});

/**
 * Load More Ajax Functionality
 */
function rc_uk_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
 
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			get_template_part( 'template-parts/post/content' );
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 
 
		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
 
 
 
add_action('wp_ajax_loadmore', 'rc_uk_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'rc_uk_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
        //return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
        return $output . ' xmlns:og="http://opengraphprotocol.org/schema/"';
    }
add_filter('language_attributes', 'add_opengraph_doctype');


/* Remove the "Category" text from the page title on category archive pages */
add_filter( 'get_the_archive_title', function ($title) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  }
  return $title;
});

add_filter('wpseo_breadcrumb_single_link' ,'rc_uk_remove_posts_link_from_breadcrumbs', 10 ,2);
function rc_uk_remove_posts_link_from_breadcrumbs($link_output, $link ){
 
    if( $link['text'] == 'Posts' ) {
        $link_output = '';
    }
 
    return $link_output;
}

function rc_uk_list_author_categories() {
	global $wpdb;
	
	$author = get_query_var('author');
	
	$categories = $wpdb->get_results("
		SELECT DISTINCT( terms.term_id ) as ID, terms.name
		FROM $wpdb->posts as posts
		LEFT JOIN $wpdb->term_relationships as relationships ON posts.ID = relationships.object_ID
		LEFT JOIN $wpdb->term_taxonomy as tax ON relationships.term_taxonomy_id = tax.term_taxonomy_id
		LEFT JOIN $wpdb->terms as terms ON tax.term_id = terms.term_id
		WHERE posts.post_status = 'publish' AND
			posts.post_author = '$author' AND
			tax.taxonomy = 'category'
		ORDER BY terms.name ASC
	");
	?>
	<ul class="list-unstyled list-inline list-tags mt-20">
		<?php foreach($categories as $category) : ?>
		<li>
			<a href="<?php echo get_category_link( $category->ID ); ?>" title="<?php echo $category->name ?>">
				<?php echo $category->name; ?>
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php
}

if( function_exists( 'mt_profile_img' ) ) {
		
	/**
	* mt_profile_img
	* 
	* Adds a profile image
	*
	@param $user_id INT - The user ID for the user to retrieve the image for
	@ param $args mixed
		size - string || array (see get_the_post_thumbnail)
		attr - string || array (see get_the_post_thumbnail)
		echo - bool (true or false) - whether to echo the image or return it
	*/
	function mt_profile_img_src( $user_id, $args = array() ) {
		$profile_post_id = absint( get_user_option( 'metronet_post_id', $user_id ) );
		
		if ( 0 === $profile_post_id || 'mt_pp' !== get_post_type( $profile_post_id ) ) {
			return false;
		}
		
		$defaults = array(
			'size' => 'thumbnail',
			'attr' => '',
			'echo' => true
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args ); //todo - get rid of evil extract
		
		$post_thumbnail_id = get_post_thumbnail_id( $profile_post_id );
		
		//Return false or echo nothing if there is no post thumbnail
		if( !$post_thumbnail_id ) {
			if ( $echo ) echo '';
			else return false;
			return;
		}
		
		//Implode Classes if set and array - dev note: edge case
		if ( is_array( $attr ) && isset( $attr[ 'class' ] ) ) {
			if ( is_array( $attr[ 'class' ] ) ) {
				$attr[ 'class' ] = implode( ' ', $attr[ 'class' ] );	
			}	
		}
		
		$post_thumbnail_src =  wp_get_attachment_image_src( $post_thumbnail_id, $size, false );
		$post_thumbnail = apply_filters( 'mpp_thumbnail_html', $post_thumbnail_src, $profile_post_id, $post_thumbnail_id, $user_id );
		if ( $echo ) {
			echo $post_thumbnail;
		} else {
			return $post_thumbnail;
		}
	} //end mt_profile_img
}

function rc_uk_is_amp_page() {
	if( !function_exists( 'is_amp_endpoint' ) ) {		
		return false;
	}
	
	return is_amp_endpoint();
}
add_action( 'wp', 'rc_uk_is_amp_page' );

/**
 * Remove 'hentry' from post_class()
 */
function rc_uk_remove_hentry( $class ) {
	$class = array_diff( $class, array( 'hentry' ) );	
	return $class;
}
add_filter( 'post_class', 'rc_uk_remove_hentry' );


/**
 * Add image sizes to wp media settings
 */
function display_custom_image_sizes( $sizes ) {
  global $_wp_additional_image_sizes;
  if ( empty($_wp_additional_image_sizes) )
    return $sizes;

  foreach ( $_wp_additional_image_sizes as $id => $data ) {
    if ( !isset($sizes[$id]) )
      $sizes[$id] = ucfirst( str_replace( '-', ' ', $id ) );
  }

  return $sizes;
}
add_filter( 'image_size_names_choose', 'display_custom_image_sizes' );
