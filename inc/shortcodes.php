<?php

function cta_banner_func( $atts ) {

    extract(shortcode_atts(array(
        "img" => '',
        "sub_heading" => '',
        "heading" => '',
        "body" => '',
        "btn_url" => '',
        "btn_text" => '',        
        "btn2_url" => '',
        "btn2_text" => '',
        "css_classes" => '',
	), $atts));

    ob_start();
	?>
	<div class="cta-banner <?php echo esc_attr( $css_classes ); ?>">
		<img src="<?php echo esc_url( $img ); ?>" alt="<?php echo $heading; ?>">
		<div class="cta-body">
			<span class="cta-sub-heading"><?php echo $sub_heading; ?></span>
			<h3 class="cta-heading"><?php echo $heading; ?></h3>
			<p><?php echo $body; ?></p>
			<a href="<?php echo esc_url( $btn_url ); ?>" class="btn btn-primary" 
            target="_blank" rel="noopener"><?php echo $btn_text; ?></a>
            
            <?php if( !empty( $btn2_url ) && !empty( $btn2_text ) ) { ?>
			<a href="<?php echo esc_url( $btn2_url ); ?>" class="btn btn-white" 
            target="_blank" rel="noopener"><?php echo $btn2_text; ?></a>
            <?php } ?>
		</div>
	</div>
	<?php

    $output_string = ob_get_contents();
    ob_end_clean();
    return $output_string;
}

add_shortcode( 'cta_banner', 'cta_banner_func' );
?>