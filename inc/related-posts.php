<?php

function rc_uk_rp4wp_thumbnail_size() {
	return 'rcconnect_uk_2017-related-posts';
}

add_filter( 'rp4wp_thumbnail_size', 'rc_uk_rp4wp_thumbnail_size' );

function rc_uk_rp4wp_get_children_link_args( $link_args, $parent_id ) {
	$defaults = $link_args;

	// change orderby to 'rand'
	$args = array(
		'orderby' => 'rand',
		'posts_per_page' => '3',
	);

	$args = wp_parse_args( $args, $defaults );

	return $args;
}
add_filter( 'rp4wp_get_children_link_args', 'rc_uk_rp4wp_get_children_link_args', 10, 2 );

function rc_uk_rp4wp_heading( $heading ) {
	return '<div class="widget-title">' . strip_tags( $heading ) . '</div>';
}
add_filter( 'rp4wp_heading', 'rc_uk_rp4wp_heading' );

// Disable related posts plugin on AMP pages
function rc_uk_append_content( $append ) {
	if( rc_uk_is_amp_page() ) {
		return false;
	}

	return $append;
}
add_filter( 'rp4wp_append_content', 'rc_uk_append_content' );


// function rcm_related_posts_heading( $heading ) {
	// if( rc_country() == 'FR' ) {
		// return 'Articles Similaires';
	// } else {
		// return '<span>' . $heading . '</span>';
	// }
// }
// add_filter( 'rp4wp_heading', 'rcm_related_posts_heading', 10, 1 );