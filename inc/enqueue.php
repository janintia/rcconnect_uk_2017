<?php


/**
 * Enqueue scripts and styles.
 */
function rc_uk_blog_2017_scripts() {
	
	global $wp_query; 
	
	// Theme stylesheet.
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	
	/**
	 * Load our IE-only stylesheet for all versions of IE:
	 * <!--[if IE]> ... <![endif]-->
	 */
	wp_enqueue_style( 'rc-uk-ie', get_theme_file_uri( '/css/ie.css' ), array( 'style' ) );
	wp_style_add_data( 'rc-uk-ie', 'conditional', 'IE' );

	// Custom Stylesheet
	wp_enqueue_style( 'custom-style', get_theme_file_uri( '/css/theme.min.css' ), array( 'style' ), '1.0' );
	
	/********** ***********/
	
	$hosted_dtm_url = '/js/tools-visitorapi-custom.js';
	$dtm_url = '//assets.adobedtm.com/7618e9029c8436bd1b1d926078af6c171547716a/satelliteLib-aed35a9669ef96a6ae2c7fe7c9d0155d4c489109.js';
	if( defined( "RC_BLOG_COUNTRY" ) ) {
		if( RC_BLOG_COUNTRY === 'FR' ) {
			$hosted_dtm_url = '/js/tools-visitorapi-custom-fr.js';
			$dtm_url = '//assets.adobedtm.com/7618e9029c8436bd1b1d926078af6c171547716a/satelliteLib-87dce09d1dee8bace50538b1b1eacb3b14533967.js';
		}
	}
	
	// RC tools, Site catalyst and Custom Omniture script
	// wp_enqueue_script( 'rc-tools-visitorapi-custom', get_theme_file_uri( $hosted_dtm_url ), array( 'jquery' ), '1.0.0', false );
	
	// wp_enqueue_script( 'site-catalyst', $dtm_url, array(), '3.0.0', false );

	// RC tools
	wp_enqueue_script( 'rc-tools', get_theme_file_uri( '/js/tools.js' ), array( 'jquery' ), '1.0.0', false );
	// Site Catalyst
	wp_enqueue_script( 'site-catalyst', $dtm_url, array( 'rc-tools' ), '3.0.0', false );
	
	//wp_enqueue_script( 'site-catalyst', get_theme_file_uri( '/js/visitor-api.js' ), array( 'rc-tools' ), '3.0.0', false ); 
	
	// Custom Omniture script
	//wp_enqueue_script( 'custom-js', get_theme_file_uri( '/js/custom.js' ), array( 'site-catalyst' ), '1.0.0', false );
	
	// Theme js
	wp_enqueue_script( 'script', get_theme_file_uri( '/js/theme.min.js' ), array( 'jquery' ), '1.0.0', true );
	
	// Localize the script with new data 
	$obj_array = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ), // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	);
	wp_localize_script( 'script', 'frontend_obj', $obj_array );
	
}
add_action( 'wp_enqueue_scripts', 'rc_uk_blog_2017_scripts' );