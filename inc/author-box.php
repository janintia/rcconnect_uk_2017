<?php
/**
 * Adds an author box at the end of the post content
*/

/* This code adds new profile fields to the user profile editor */
function rc_uk_modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['twitter'] = 'Twitter URL';
	$profile_fields['gplus'] = 'Google+ URL';
	$profile_fields['linkedin'] = 'Linkedin URL';

	// Remove old fields
	unset($profile_fields['aim']);
	unset($profile_fields['yim']);
	unset($profile_fields['jabber']);
	unset($profile_fields['googleplus']);
	unset($profile_fields['facebook']);

	return $profile_fields;
}
add_filter( 'user_contactmethods', 'rc_uk_modify_contact_methods' );

/* This code adds the author box to the end of your single posts */
function rc_uk_add_post_content( $content ) {
	global $post;
	
	$author_role = get_the_author_meta( 'author_role' ) ? get_the_author_meta( 'author_role' ) : esc_html__( 'Author', 'rcconnect_uk_2017' );
	
	// Author title 
	$author_title = '<div class="h4"><a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '"><span>'. get_the_author() .'</span></a></div><span class="h5">' . $author_role . '</span>';
	
	// Social media profiles
	$author_social = '<ul class="author_box_social list-unstyled">';		
	if( get_the_author_meta('twitter',get_query_var('author') ) ) {
		$author_social .= '<li><a href="' . esc_url( get_the_author_meta( 'twitter' ) ) . '" target="_blank"><i class="fa fa-twitter"></i></a></li>';
	}		
	if( get_the_author_meta('gplus',get_query_var('author') ) ) {
		$author_social .= '<li><a href="' . esc_url( get_the_author_meta( 'gplus' ) ) . '" target="_blank"><i class="fa fa-google-plus"></i></a></li>';
	}		
	if( get_the_author_meta('linkedin',get_query_var('author') ) ) {
		$author_social .= '<li><a href="' . esc_url( get_the_author_meta( 'linkedin' ) ) . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>';
	}
	$author_social .= '</ul>';
	
	// Gravatar
	// $author_gravatar = '<div class="author_box_gravatar">'. get_avatar( get_the_author_meta('email'), '150' ) . $author_social . '</div>';
	
	ob_start();
	if ( function_exists ( 'mt_profile_img' ) ) {
		$author_id = $post->post_author;
		mt_profile_img( $author_id, array(
			'size' => 'thumbnail',
			'echo' => true )
		);
	}
	$avatar = ob_get_clean();
	
	$author_gravatar = '<div class="author_box_gravatar">'. $avatar . $author_social . '</div>';
	
	// Author description
	$author_desc = get_the_author_meta( 'short_description', get_query_var( 'author' ) );
	
	if ( is_single() ) { 
	
		$content .= '
			<div class="author_box_wrap inset-column">' .  $author_gravatar . ' 
				<div class="author_box_text">' . $author_title . $author_desc . '</div>
		';
			
		$content .= '
				<div class="clearfix"></div>
			</div>
		';
	}
	
	return $content;
}

function rc_uk_adding_to_content() {
	if( !rc_uk_is_amp_page() ) {
		add_filter( 'the_content', 'rc_uk_add_post_content', 0 );
	}
}

add_action( 'wp', 'rc_uk_adding_to_content' );
?>