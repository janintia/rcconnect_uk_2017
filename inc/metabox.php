<?php

function rc_uk_add_custom_box()
{
    $screens = ['post'];
    foreach ($screens as $screen) {
        add_meta_box(
            'wporg_box_id',           // Unique ID
            'BMID Option',  // Box title
            'rc_uk_bmid_box_html',  // Content callback, must be of type callable
            $screen                   // Post type
        );
    }
}
add_action('add_meta_boxes', 'rc_uk_add_custom_box');


function rc_uk_bmid_box_html($post)
{
    $value = get_post_meta($post->ID, '_rc_uk_bmid_meta_key', true);
    ?>
    <label for="rc_uk_bmid_field"><?php esc_html_e( 'Enter the BMID parameter to use for this page. E.g: BLOGNEWUK, UKRETAIL', 'rcconnect_uk_2017' ) ?></label>
    <input name="rc_uk_bmid_field" type="text" id="rc_uk_bmid_field" class="postbox" value="<?php echo $value; ?>" />
    <?php
}

function rc_uk_save_postdata($post_id)
{
    if (array_key_exists('rc_uk_bmid_field', $_POST)) {
        update_post_meta(
            $post_id,
            '_rc_uk_bmid_meta_key',
            $_POST['rc_uk_bmid_field']
        );
    }
}
add_action('save_post', 'rc_uk_save_postdata');


/**
 * Exclude from Homepage metabox option
 *
 */
class ExcludeFromHP_Meta_Box
{
	
	 /**
     * Hook into the appropriate actions when the class is constructed.
     */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add' ) );
        add_action( 'save_post',      array( $this, 'save'         ) );
    }
 
 
    public function add()
    {
        $screens = ['post'];
        foreach ($screens as $screen) {
            add_meta_box(
                'efhp_box_id',          // Unique ID
                esc_html__( 'Exclude from Homepage' , 'rcconnect_uk_2017' ), // Box title
                //[self::class, 'html'],   // Content callback, must be of type callable
                array( $this, 'html' ),   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }
 
    public function save($post_id)
    {
        if (array_key_exists('efhp_field', $_POST)) {
            update_post_meta(
                $post_id,
                '_efhp_meta_key',
                $_POST['efhp_field']
            );
        }
    }
 
    public function html($post)
    {
        $value = get_post_meta($post->ID, '_efhp_meta_key', true);
        ?>
        <label for="efhp_field"><?php esc_html_e( 'Exclude this post from the Homepage', 'rcconnect_uk_2017' ) ?></label>
        <select name="efhp_field" id="efhp_field" class="postbox">
            <option value="false" <?php selected($value, 'false'); ?>><?php esc_html_e( 'False', 'rcconnect_uk_2017' ) ?></option>
            <option value="true" <?php selected($value, 'true'); ?>><?php esc_html_e( 'True', 'rcconnect_uk_2017' ) ?></option>
        </select>
        <?php
    }
}

/**
 * Calls the class on the post edit screen.
 */
function call_ExcludeFromHP_Meta_Box() {
    new ExcludeFromHP_Meta_Box();
}
 
if ( is_admin() ) {
    add_action( 'load-post.php',     'call_ExcludeFromHP_Meta_Box' );
    add_action( 'load-post-new.php', 'call_ExcludeFromHP_Meta_Box' );
}
 