<?php

/**
 * RC Connect UK 2017 only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rcconnect_uk_2017_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/rcconnect_uk_2017
	 * If you're building a theme based on RC Connect UK 2017, use a find and replace
	 * to change 'rcconnect_uk_2017' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'rcconnect_uk_2017' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
		

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'rcconnect_uk_2017-thumbnail-avatar', 100, 100, true );
	
	add_image_size( 'rcconnect_uk_2017-post-feature', 743, 493, true );
	
	add_image_size( 'rcconnect_uk_2017-post-feature-1000x664', 1000, 664, true );
	
	add_image_size( 'rcconnect_uk_2017-author-profile', 250, 250, true );
	
	add_image_size( 'rcconnect_uk_2017-related-posts', 250, 140, true );
	
	add_image_size( 'rcconnect_uk_2017-avatar', 40, 40, true );
	
	add_image_size( 'rcconnect_uk_2017-amp-avatar', 24, 24, true );
	
	add_image_size( 'rcconnect_uk_2017-twitter-summary-large', 876, 438, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 840;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'    => __( 'Primary Menu', 'rcconnect_uk_2017' ),
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 200,
		'height'      => 30,
		'flex-width'  => true,
		'flex-height'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );	
}
add_action( 'after_setup_theme', 'rcconnect_uk_2017_setup' );