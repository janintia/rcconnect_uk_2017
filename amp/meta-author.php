<?php $post_author = $this->get( 'post_author' ); ?>
<?php if ( $post_author ) : ?>
	<?php 
	$avatar_src = array();
	if ( function_exists ( 'mt_profile_img' ) ) {
		$author_id = $post_author->ID;
		$avatar_src = mt_profile_img_src( $author_id, array(
			'size' => 'rcconnect_uk_2017-amp-avatar',
			'echo' => false )
		);
	}	
	?>
	<?php // $author_avatar_url = get_avatar_url( $post_author->user_email, array( 'size' => 24 ) ); ?>
	<div class="amp-wp-meta amp-wp-byline">
		<?php if ( function_exists( 'get_avatar_url' ) && !empty( $avatar_src ) ) : ?>
			<amp-img src="<?php echo esc_url( $avatar_src[0] ); ?>" width="24" height="24" layout="fixed"></amp-img>
		<?php endif; ?>
		<span class="amp-wp-author author vcard"><?php echo esc_html( $post_author->display_name ); ?></span>
	</div>
<?php endif; ?>
