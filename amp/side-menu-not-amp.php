<a class="navButton" tabindex="0">&#9776;</a> <span class="navClose" tabindex="0">&#9776;</span>
<button class="navFrame">
	<nav id="main-sections-nav" class="sitenav">
		<ul id="sections-menu-off-canvas" class="side-nav">	
			<li class="main-nav"> <a class="main-nav-item" href="<?php echo get_site_url(); ?>/customer-stories">Customer Stories</a></li>
			<li class="main-nav"> <a class="main-nav-item" href="<?php echo get_site_url(); ?>/category/ringcentral-tips/">RingCentral Tips</a></li>
			<li class="main-nav"> <a class="main-nav-item" href="<?php echo get_site_url(); ?>/category/ringcentral-uk/">RingCentral UK</a></li>
			<li class="main-nav"> <a class="main-nav-item" href="<?php echo get_site_url(); ?>/business-news/">Business News</a></li>
			<li class="main-nav"> <a class="main-nav-item" href="<?php echo get_site_url(); ?>/category/developer-platform/">Developers</a></li>
			<li class="main-nav"> <a class="main-nav-item" href="<?php echo get_site_url(); ?>/contribute/">Contribute</a></li>
		</ul>
		<ul id="site-attribution-off-canvas-menu" class="side-nav site-attribution">
		<li class="main-nav">RingCentral Videos</li>
		<li class="main-nav"><amp-img src="https://www.ringcentral.com/blog/wp-content/themes/rcconnect_US_2013/images/youtube-link.png" width="131" height="72" layout="responsive" alt="RingCentral Videos"></li>
		</ul>
	</nav>
</button>
<button id="navscrim"></button>