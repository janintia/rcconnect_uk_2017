<!doctype html>
<html amp <?php language_attributes(); ?>>
<head>			 	
	<?php do_action( 'amp_post_template_head', $this ); ?>
	<?php $this->load_parts( array( 'header' ) ); ?>
	<style amp-custom>     
		<?php $this->load_parts( array( 'style' ) ); ?>
		<?php do_action( 'amp_post_template_css', $this ); ?>
    </style>	
</head>
<body class="<?php echo esc_attr( $this->get( 'body_class' ) ); ?>">

<?php $this->load_parts( array( 'nav', 'sidebar' ) ); ?>

<div class="content-container" amp-access="action != 3">
	<article class="amp-wp-article">

		<header class="amp-wp-article-header">
			<h1 class="amp-wp-title"><?php echo wp_kses_data( $this->get( 'post_title' ) ); ?></h1>
			<?php $this->load_parts( apply_filters( 'amp_post_article_header_meta', array( 'meta-author', 'meta-time' ) ) ); ?>
		</header>
		
		<?php $this->load_parts( array( 'featured-image' ) ); ?>
		
		<div class="amp-wp-article-content">
			<?php echo $this->get( 'post_amp_content' ); // amphtml content; no kses ?>
		</div>
		
		<div class="amp-wp-social-share">
		<?php $this->load_parts( array( 'social-share' ) ); ?>
		</div>
		<!-- Article Footer 
		<footer class="amp-wp-article-footer">
			
			<?php //$this->load_parts( apply_filters( 'amp_post_article_footer_meta', array( 'meta-taxonomy', 'meta-comments-link' ) ) ); ?>
		</footer>
		-->

	</article>
</div>
<?php $this->load_parts( array( 'footer' ) ); ?>
<?php do_action( 'amp_post_template_footer', $this ); ?>
</body>
</html>