<footer class="amp-wp-footer">	
		<p class="left">&copy; 1999-<?php echo date("Y"); ?> RingCentral, Inc. All rights reserved</p>
		<div class="right">
			<a href="#" id="top">
				<amp-img width='40' height='29' src="<?php echo esc_url( get_theme_file_uri( '/images/top.png' ) ); ?>" />
			</a>
		</div>	
</footer>

<!-- Google AMP Analytics for GA -->
<amp-analytics type="googleanalytics" id="googleanalytics1">
<script type="application/json">
{
  "vars": {
	"account": "UA-36051195-1"
  },
  "triggers": {
	"trackPageview": {
	  "on": "visible",
	  "request": "pageview"
	}
  }
}
</script>
</amp-analytics>
<!-- End of GA Analytics -->

<!-- Google AMP Analytics for Omniture -->
<amp-analytics type="adobeanalytics_nativeConfig">
	<script type="application/json">
	{
		"requests": {			
			"iframeMessage": "https://blog.corp.ringcentral.com/o/"
		},
		"vars": {
			"eventId": "pageview"
		},
		"extraUrlParams": {
			"pageName": "${ampdocUrl}"
		}
	}
	</script>
</amp-analytics>
<!-- End of Omniture Analytics -->
