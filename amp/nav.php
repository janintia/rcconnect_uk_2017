<header id="#top" class="amp-wp-header">
	<div>
		<a href="<?php echo esc_url( $this->get( 'home_url' ) ); ?>">
			<?php echo esc_html( $this->get( 'blog_name' ) ); ?>
		</a>
	</div>
</header>
<header class="amp-smallheader">
<div class="div-right"><a href="<?php echo esc_url( $this->get( 'home_url' ) ); ?>">Blog Home</a> | <a href="<?php echo rc_gw_url(); ?>" target="_blank">RingCentral <?php echo rc_country(); ?> Home</a></div>
</header>