(function($){
	"use strict"
	
	// Ajax Load More Pagination	
	function rc_loadmore_btn() {
		$('.rc_loadmore_btn').click( function(e){
			e.preventDefault();
			
			var button = $(this),
				button_container = button.closest( '.ajax-load-more' ),
				data = {
				'action': 'loadmore',
				'query': frontend_obj.posts, // that's how we get params from wp_localize_script() function
				'page' : frontend_obj.current_page
			};
	 
			$.ajax({
				url : frontend_obj.ajaxurl, // AJAX handler
				data : data,
				type : 'POST',
				beforeSend : function ( xhr ) {
					button.text('Loading...'); // change the button text, you can also add a preloader image
				},
				success : function( data ){
					if( data ) { 	
						var ajax_load_more = button.text( 'Show More' ).closest('.ajax-load-more');
						// remove the clearfix
						ajax_load_more.prev('.clearfix').remove();
						
						// insert new posts
						ajax_load_more.before(data).before( '<div class="clearfix"></div>' );; // insert new posts
						
						frontend_obj.current_page++;
	 
						if ( frontend_obj.current_page == frontend_obj.max_page ) 
							button.closest('.ajax-load-more').remove(); // if last page, remove the button
					} else {
						button.closest('.ajax-load-more').remove(); // if no data, remove the button as well
					}
				}
			});
		});
	}
	
	rc_loadmore_btn();
	
	function rc_video_embed() {
		if( jQuery().fitVids ) {
			$(".rc-responsive-embed").fitVids();
		}
	}
	rc_video_embed();
	
	// Sticky Sidebar
	function sticky_sidebar() {
		$('#primary, #secondary').theiaStickySidebar({
		  // Settings
		  additionalMarginTop: 30,
		  additionalMarginBottom: 60,
		  minWidth: 992,
		});
	}
	
	sticky_sidebar();
	
	$(document).ready( function() {

		function expand_searchbox() {
			var $header_search = $( '#header-search' ),
				$header_searchbox = $header_search.find( '#search-form' );
				
			$header_searchbox.on( 'focus', function() {
				var $this = $( this ),
				$form = $this.closest( 'form.search-form' );
				$form.addClass( 'focused' );
			});
			
			// On blur
			$header_searchbox.on( 'blur', function() {
				var $this = $( this ),
				$form = $this.closest( 'form.search-form' );
				$form.removeClass( 'focused' );
			});			
		}
		
		//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
		function autoPlayYouTubeModal(){
		  var trigger = $("body").find('[data-toggle="modal"]');
		  trigger.click(function() {
			var theModal = $(this).data( "target" ),
			videoSRC = $(this).attr( "data-theVideo" ), 
			videoSRCauto = videoSRC+"?autoplay=1" ;
			$(theModal+' iframe').attr('src', videoSRCauto);
			$(theModal+' button.close').click(function () {
				$(theModal+' iframe').attr('src', videoSRC);
			});   
		  });
		}
		autoPlayYouTubeModal();
		
		expand_searchbox();
		
	});
	
	$(window).load( function(){	 
		function addHeaderOffset() {
			
			var blaah = '';
			var blaah = $('div:contains(blaah)');
			var blahsibling = blaah.next('div');
			var spanBelowBody = '';
			var sleeknoteTopbarOffset =  0;
			var $navbar_el = $('.navbar-fixed-top');
			var $cookie_banner = $('.rc-cookie-banner')
			
			if( blahsibling ) {
				spanBelowBody = blahsibling.children('span').first();
				sleeknoteTopbarOffset = parseInt(spanBelowBody.css('height')) - 3;
				
				if( sleeknoteTopbarOffset > 0 ) {
					// add to navbar and cookie banner
					$navbar_el.css( 'marginTop', sleeknoteTopbarOffset );
					$cookie_banner.css( 'marginTop', sleeknoteTopbarOffset );
				} else {
					$navbar_el.css( 'marginTop', 0 );
					$cookie_banner.css( 'marginTop', 0 );
				}
			}
			
			setTimeout(addHeaderOffset, 500);
		}
		
		setTimeout(addHeaderOffset, 10);
	});
	
	// animated number
	var a = 0;
	$(window).scroll(function() {
		if($("#counter").length){
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function() {
				  var $this = $(this),
					countTo = $this.attr('data-count'),
					duration = $this.attr('data-duration') || 2000;
					
				  $({
					countNum: $this.text()
				  }).animate({
					  countNum: countTo
					},
					{
					  duration: parseInt(duration),
					  easing: 'swing',
					  step: function() {
						$this.text(Math.floor(this.countNum));
					  },
					  complete: function() {
						$this.text(this.countNum);
					  }

					});
				});
				a = 1;
			}
		}
	});
})(jQuery);

// Font FaceObserver
(function( w ){
	"use strict";
	
	// if the class is already set, we're good.
	if( w.document.documentElement.className.indexOf( "fonts-loaded" ) > -1 ){
		return;
	}
	var NotoSerif400i = new w.FontFaceObserver( "Noto Serif", {
		weight: 400,
		style: 'italic',
	});
	
	w.Promise.all([ NotoSerif400i.load() ]).then(function(){
		w.document.documentElement.className += " fonts-loaded";
	});
}( this ));
