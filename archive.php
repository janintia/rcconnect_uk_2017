<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package underscores_sample
 */
global $wp_query;
get_header(); ?>

	<div id="primary" class="col-md-12">
		<main id="main" class="site-main">
			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->
			<div class="row multi-columns-row">
				<?php
				if ( have_posts() ) { ?>

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/post/content' );

					endwhile;

					if (  $wp_query->max_num_pages > 1 ) {
						echo '
							<div class="clearfix"></div>
							<div class="ajax-load-more mb-40">
								<a href="#" class="btn btn-default rc_loadmore_btn"><i class="fa fa-plus-circle"></i>Show More</a>
							</div>';
					}
					

				} else {

					get_template_part( 'template-parts/content', 'none' );

				} ?>
			</div><!--.row multi-columns-row-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
