<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package underscores_sample
 */

get_header(); ?>

	<div id="primary" class="">
	
		<main id="main" class="site-main inset-column">

			<?php do_action( 'rc_uk_after_site_main' ); ?>
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">404<span><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'rcconnect_uk_2017' ); ?></span></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p class="text-center"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'rcconnect_uk_2017' ); ?></p>

					<?php

						the_widget( 'WP_Widget_Recent_Posts' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
