<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage RC_Connect_UK_2017
 * @since 1.0
 * @version 1.0
 */
global $wp_query; 
get_header(); ?>

<div id="primary" class="col-md-12">
	<div class="theiaStickySidebar">
		<main id="main" class="site-main" role="main">
			<div class="row multi-columns-row">
				<?php // Show the selected frontpage content.
				if ( have_posts() ) {
					while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/post/content' );
					endwhile;
					
					if (  $wp_query->max_num_pages > 1 ) {
						?>
							<div class="clearfix"></div>
							<div class="ajax-load-more mb-40">
								<a href="#" class="btn btn-default rc_loadmore_btn"><i class="fa fa-plus-circle"></i><?php esc_html_e( 'Show More', 'rcconnect_uk_2017' ); ?></a>
							</div>					
						<?php
					}
					
				} else { 
					get_template_part( 'template-parts/post/content', 'none' );
				} ?>
			</div>
		</main><!-- #main -->
	</div>
</div><!-- #primary -->

<?php 
// get_sidebar();
get_footer();
