<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage RC_Connect_UK_2017
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">		
		<link href="<?php echo esc_url( get_template_directory_uri() . '/images/favicon.ico' ); ?>" type="image/vnd.microsoft.icon" rel="shortcut icon">
		<?php if( is_page_template( 'template-home.php' ) ) { ?>
			<link rel="canonical" href="<?php echo esc_url( get_site_url() ); ?>"/>
		<?php } ?>
		<?php if( is_home() ) { ?> 
		<meta name="msvalidate.01" content="37393656AD5E2981BD03165CA6426AAE" />
		<?php } ?>
		<?php wp_head(); ?>
		
	</head>

<body <?php body_class( 'boxed blog-list' ); ?>>
	<?php do_action( 'rcm_body_start' ); ?>
	<div id="page" class="site">

		<header id="masthead" class="site-header">
			<?php get_template_part( 'template-parts/main', 'nav' ); ?> 
		</header><!-- #masthead -->
		
		<?php if ( is_front_page() && is_home() || is_page_template( 'template-home.php' ) ) { ?>
		<?php get_template_part( 'template-parts/featured', 'contents' ); ?> 
		<?php } ?>
		<?php $upload_dir = wp_upload_dir(); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="https://www.ringcentral.co.uk/lp/voice-of-retail-book?BMID=BLOGNEWUK">
						<img src="<?php echo $upload_dir['baseurl'] . '/doug-stephens-retailers-728x90.jpg'; ?>" width="1000" height="125" alt="Doug Stephens" style="margin: 20px auto 40px; display:block;" />
					</a>
				</div>
			</div>
		</div>
		<?php if( is_single() ) { ?>
		<div class="breadcrumb-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
					<?php
						if ( function_exists( 'yoast_breadcrumb' ) ) {
							yoast_breadcrumb( '<p class="breadcrumbs">', '</p>' );
						}
					?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="site-content-contain">
			<div id="content" class="site-content">
				<div class="container">
					<div class="row">
