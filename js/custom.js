function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setCookie(name, value, expires, path, domain, secure) {
		name = name.toLowerCase();
		if (!path) {
			path = '/';
		}
		if (typeof expires != 'number') {
			expires = '';
		} else {
			var expires_date = new Date((new Date()).getTime() + (expires * 24 * 60 * 60 * 1000));
			expires = ' expires=' + expires_date.toGMTString() + ';';
		}
		var d = (domain) ? ';domain=' + domain : '';
		var s = (secure) ? ";secure" : "";
		document.cookie = name + "=" + value + ";" + expires + " Path=" + path + d + s;
}

function get_domain_cookie() {
	var host = window.location.hostname;
	if(host == 'localhost') return host;
	var _domain = host.split('.');
	_domain.shift();
	if (_domain[0] == 'secure') {
		_domain.shift();
	}
	domain = '.' + _domain.join('.');
	return domain;
}