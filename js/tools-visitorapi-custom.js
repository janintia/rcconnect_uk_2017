(function($){
	
// Cookies Manager
var CookiesManager = {
	setCookie: function(name, value, expires, path, domain, secure) {
		name = name.toLowerCase();
		if (!path) {
			path = '/';
		}
		if (typeof expires != 'number') {
			expires = '';
		} else {
			var expires_date = new Date((new Date()).getTime() + (expires * 24 * 60 * 60 * 1000));
			expires = ' expires=' + expires_date.toGMTString() + ';';
		}
		var d = (domain) ? ';domain=' + domain : '';
		var s = (secure) ? ";secure" : "";
		document.cookie = name + "=" + value + ";" + expires + " Path=" + path + d + s;
	},
	getCookie: function(name) {
		var c = document.cookie;
		var matches = c.match(new RegExp("(?:^|; )" + name + "=([^;]*)", "i"));
		return matches ? decodeURIComponent(matches[1]) : null;
	},
	attachCookie: function(cookieName, str) {
		var value = CookiesManager.getCookie(cookieName);
		var re = new RegExp("(&|\\\?)" + cookieName + "=");
		if (value && !str.match(re)) {
			str += '&' + cookieName + '=' + value;
		}
		return str;
	},
	delCookie: function(name, path, domain) {
		document.cookie = name + "=" +
			((path) ? ";path=" + path : "") +
			((domain) ? ";domain=" + domain : "") +
			";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	},
}

var GetParamsManager = {
	is_set: function(varName) {
		var regexS = "[?&]" + varName + "=([^&#]*)";
		var regex = new RegExp(regexS, 'i');
		var tmpURL = window.location.href;
		var results = regex.exec(tmpURL);
		if (results == null)
			return null;
		else
			return decodeURIComponent(results[1]);
	},
	get2array: function() {
		var ret = {};
		if (window.location.search) {
			var temp = window.location.search.substr(1).split('&');
			var i = temp.length;
			while (i--)
				if (temp[i]) {
					try {
						var tmp = temp[i].replace(/\+/g, "%20").split('=');
						var key = decodeURIComponent(tmp[0]).toLowerCase();
						if (typeof ret[key] == 'undefined' && typeof tmp[1] != 'undefined')
							ret[key] = decodeURIComponent(tmp[1]);
					} catch (e) {
						// incorrect parameter is not saved in cookie
					}
				}
		}
		return ret;
	}
}

//GWTS-623
;
(function() {

	// php functions

	function serialize(mixed_value) {
		//		note: We feel the main purpose of this function should be to ease the transport of data between php & js
		//		note: Aiming for PHP-compatibility, we have to translate objects to arrays
		//	example 1: serialize(['Kevin', 'van', 'Zonneveld']);
		//	returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
		//	example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});
		//	returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'

		var val, key, okey,
			ktype = '',
			vals = '',
			count = 0,
			_utf8Size = function(str) {
				var size = 0,
					i = 0,
					l = str.length,
					code = '';
				for (i = 0; i < l; i++) {
					code = str.charCodeAt(i);
					if (code < 0x0080) {
						size += 1;
					} else if (code < 0x0800) {
						size += 2;
					} else {
						size += 3;
					}
				}
				return size;
			};
		_getType = function(inp) {
			var match, key, cons, types, type = typeof inp;

			if (type === 'object' && !inp) {
				return 'null';
			}
			if (type === 'object') {
				if (!inp.constructor) {
					return 'object';
				}
				cons = inp.constructor.toString();
				match = cons.match(/(\w+)\(/);
				if (match) {
					cons = match[1].toLowerCase();
				}
				types = ['boolean', 'number', 'string', 'array'];
				for (key in types) {
					if (cons == types[key]) {
						type = types[key];
						break;
					}
				}
			}
			return type;
		};
		type = _getType(mixed_value);

		switch (type) {
			case 'function':
				val = '';
				break;
			case 'boolean':
				val = 'b:' + (mixed_value ? '1' : '0');
				break;
			case 'number':
				val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
				break;
			case 'string':
				val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
				break;
			case 'array':
			case 'object':
				val = 'a';
				/*
				 if (type === 'object') {
				 var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
				 if (objname == undefined) {
				 return;
				 }
				 objname[1] = this.serialize(objname[1]);
				 val = 'O' + objname[1].substring(1, objname[1].length - 1);
				 }
				 */

				for (key in mixed_value) {
					if (mixed_value.hasOwnProperty(key)) {
						ktype = _getType(mixed_value[key]);
						if (ktype === 'function') {
							continue;
						}

						okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
						vals += serialize(okey) + serialize(mixed_value[key]);
						count++;
					}
				}
				val += ':' + count + ':{' + vals + '}';
				break;
			case 'undefined':
				// Fall-through
			default:
				// if the JS object has a property which contains a null value, the string cannot be unserialized by PHP
				val = 'N';
				break;
		}
		if (type !== 'object' && type !== 'array') {
			val += ';';
		}
		return val;
	}

	// Parses the string into variables
	function parse_str(str) {
		if (str.indexOf('?') != -1)
			str = str.replace('?', '');
		var separator = '=',
			glue = '&',
			query_array = str.split(glue),
			result = [];

		for (var x = 0; x < query_array.length; x++) {
			var tmp = query_array[x].split(separator);
			result[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
		}

		return result;
	}


	function parse_url(str, component) {
		//	note: Does not replace invalid characters with '_' as in PHP, nor does it return false with  
		//	note: Besides function name, is essentially the same as parseUri as well as our allowing
		//	note: an extra slash after the scheme/protocol (to allow file:/// as in PHP)
		//	example 1: parse_url('http://username:password@hostname/path?arg=value#anchor');
		//	returns 1: {scheme: 'http', host: 'hostname', user: 'username', pass: 'password', path: '/path', query: 'arg=value', fragment: 'anchor'}

		var query, key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port',
			'relative', 'path', 'directory', 'file', 'query', 'fragment'
		],
			ini = (this.php_js && this.php_js.ini) || {},
			mode = (ini['phpjs.parse_url.mode'] && ini['phpjs.parse_url.mode'].local_value) || 'php',
			parser = {
				php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-scheme to catch file:/// (should restrict this)
			};

		var m = parser[mode].exec(str),
			uri = {},
			i = 14;
		while (i--) {
			if (m[i]) {
				uri[key[i]] = m[i];
			}
		}

		if (component) {
			return uri[component.replace('PHP_URL_', '').toLowerCase()];
		}
		if (mode !== 'php') {
			var name = (ini['phpjs.parse_url.queryKey'] && ini['phpjs.parse_url.queryKey'].local_value) || 'queryKey';
			parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
			uri[name] = {};
			query = uri[key[12]] || '';
			query.replace(parser, function($0, $1, $2) {
				if ($1) {
					uri[name][$1] = $2;
				}
			});
		}
		delete uri.source;

		return uri;
	}

	function getQueryParams(qs) {
		qs = qs.split("+").join(" ");
		var params = {}, tokens,
			re = /[?&]?([^=]+)=([^&]*)/g;

		while (tokens = re.exec(qs)) {
			params[decodeURIComponent(tokens[1]).toLowerCase()] = decodeURIComponent(tokens[2]);
		}

		return params;
	}

	function checkOtm() {
		var partnerId = null;
		if (document.referrer)
			partnerId = document.referrer.match(/partnerid=([^\&]+)(&|$)/i);

		if (partnerId)
			return partnerId[1];

		partnerId = getQueryParams(window.location.search).partnerid;
		if (!partnerId) {
			if (CookiesManager.getCookie('partnerid'))
				partnerId = CookiesManager.getCookie('partnerid');
			else if (CookiesManager.getCookie('gw_partnerid'))
				partnerId = CookiesManager.getCookie('gw_partnerid');
		}

		return partnerId;
	}

	function keysToLowerCase(arr) {
		if (!Object.keys(arr).length)
			return;

		var result = [];

		for (var key in arr) {
			result[key.toLowerCase()] = arr[key];
		}

		return result;
	}

	function setOtm() {
		var partnerId = checkOtm(),
			referer = document.referrer,
			pid = '',
			bmid = '',
			sid = '',
			$_ref = '',
			resultCookie = {},
			get = keysToLowerCase(parse_str(window.location.search));

		if (!partnerId)
			return false;

		referer = parse_url(referer);

		if (!referer['query'])
			referer['query'] = '';
		$_ref = keysToLowerCase(parse_str(referer['query']));

		if ($_ref['pid'])
			pid = $_ref['pid'];
		if ($_ref['bmid'])
			bmid = $_ref['bmid'];
		//GW-16685
		if ($_ref['tduid'])
			sid = $_ref['tduid'];
		else if (CookiesManager.getCookie('gw_tduid'))
			sid = CookiesManager.getCookie('gw_tduid');
		else if ($_ref['sid'])
			sid = $_ref['sid'];

		if (!pid) {
			if (get['pid'])
				pid = get['pid'];
			else if (CookiesManager.getCookie('gw_pid'))
				pid = CookiesManager.getCookie('gw_pid');
		}

		if (!bmid) {
			if (get['bmid'])
				bmid = get['bmid'];
			else if (CookiesManager.getCookie('gw_bmid'))
				bmid = CookiesManager.getCookie('gw_bmid');
		}

		if (get['sid'])
			sid = get['sid'];

		resultCookie = {
			'partnerid': partnerId,
			'bmid': bmid,
			'pid': pid,
			'sid': sid
		};

		var domain = get_domain_cookie();

		CookiesManager.setCookie('gw_otm', encodeURIComponent(serialize(resultCookie)), '', "/", domain);
	}

	setOtm();

})();

function $_(id) {
	return document.getElementById(id);
}

function hash_to_string(hash)
{
	arr = [];
	for (key in hash) {
		try {
			arr.push(key + ':' + hash[key]);
		} catch (e) {
		}
	}
	return arr.join(',');
}



Function.prototype.rcBind = function(obj)
{
	var m = this;
	var a = arguments.length > 1 ? arguments[1] : {};
	return function(e)
	{
		if (arguments.length)
		{
			if (arguments[0].srcElement || arguments[0].target)
			{
				a['oEvent'] = arguments[0];
			}
			else
			{
				for (var key in arguments[0])
				{
					a[key] = arguments[0][key];
				}
			}

		}
		return m.call(obj, a);
	}
}

Function.prototype.rcBindAsIs = function(obj)
{
	var method = this;
	var arg = [];
	for (var i = 1; i < arguments.length; i++)
	{
		arg.push(arguments[i]);
	}
	return function()
	{
		if (arguments.length)
		{
			for (var i = 0; i < arguments.length; i++)
			{
				arg.push(arguments[i]);
			}
		}
		var res = null;
		//try
		//{
		res = method.apply(obj, arg);
		//}
		//catch(e){}
		if (arguments.length)
		{
			for (var i = 0; i < arguments.length; i++)
			{
				arg.pop();
			}
		}
		return res;
	}
}


function addEvent(obj, ev, func, useCapture)
{
	useCapture = !useCapture ? false : true;

	//alert(useCapture);

	if (ev == 'ondomready')
	{
		onDomReady(func);
		return;
	}
	if (ev != 'onunload') {
		window.addEvent(window, 'onunload', function() {
			window.removeEvent(obj, ev, func);
			func = null;
		})
	}

	if (window.addEventListener) {
		obj.addEventListener(ev.replace(/^on/i, ''), func, useCapture);
	} else if (window.attachEvent) {
		obj.attachEvent(ev, func);
	}
}

function onDomReady(callback)
{
	var _timer;


	var onDomReadyCallBack = function()
	{
		if (arguments.callee.done)
			return;

		arguments.callee.done = true;
		if (_timer)
		{
			clearInterval(_timer);
			_timer = null;
		}


		callback();

	};



	if (document.addEventListener)
	{
		document.addEventListener("DOMContentLoaded", onDomReadyCallBack, false);
	}


	if (document.attachEvent && document.documentElement.doScroll && window == window.top)
	{
		(
			function()
			{
				try
				{
					document.documentElement.doScroll("left");
				}
				catch (error)
				{
					setTimeout(arguments.callee, 0);
					return;
				}
				onDomReadyCallBack();
			}
		)();
	}




	if (/WebKit/i.test(navigator.userAgent))
	{
		_timer = setInterval(function()
		{
			if (/loaded|complete/.test(document.readyState))
			{
				onDomReadyCallBack();
			}
		}, 10);
	}

	addEvent(window, 'onload', onDomReadyCallBack);

}



function removeEvent(obj, ev, func) {
	if (window.addEventListener) {
		obj.removeEventListener(ev.replace(/^on/i, ''), func, false);
	} else if (window.detachEvent) {
		obj.detachEvent(ev, func);
	}
}
function in_array(needle, haystack, strict) {
	var found = false, key, strict = !!strict;

	for (key in haystack) {
		if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
			found = true;
			break;
		}
	}

	return found;
}
function count(mixed_var, mode) {
	var key, cnt = 0;

	if (mode == 'COUNT_RECURSIVE')
		mode = 1;
	if (mode != 1)
		mode = 0;

	for (key in mixed_var) {
		cnt++;
		if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
			cnt += count(mixed_var[key], 1);
		}
	}

	return cnt;
}
function is_string(mixed_var) {
	return (typeof (mixed_var) == 'string');
}
function is_numeric(mixed_var) {
	return !isNaN(mixed_var);
}

function is_dom(obj) {
	return obj && obj.style;
}

function is_oObj(obj) {
	return obj && obj.getView && obj.getView();
}
is_vObj = is_oObj;

function is_array(mixed_var) {
	return (mixed_var instanceof Array);
}


function is_object(mixed_var) {
	if (mixed_var instanceof Array) {
		return false;
	} else {
		return (mixed_var !== null) && (typeof (mixed_var) == 'object');
	}
}

function empty(mixed_var) {
	var is_object_empty = false;
	if (is_object(mixed_var)) {
		for (key in mixed_var) {
			return is_object_empty;
		}
		is_object_empty = true;
	}
	return (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || (is_array(
		mixed_var) && mixed_var.length === 0) || is_object_empty);
}

function is_obj(obj) {
	return obj && typeof (obj);
}

function strToJson(str)
{
	if (typeof str != 'string')
	{
		str = '';
	}

	var res = {};

	try
	{
		res = (new Function('return ' + str.replace(/^[^{]*|[^}]$/, '')))();
	}
	catch (e) {
	}
	;

	return res;
}


function getChilds(obj, nodeName, attr, style, arr) {
	if (!arr) {
		arr = Array();
	}
	if (obj.hasChildNodes()) {
		for (var i = 0; i < obj.childNodes.length; i++) {
			if (obj.childNodes[i].nodeName == '#text') {
				continue;
			}
			if (!nodeName || obj.childNodes[i].nodeName == nodeName) {
				if (!attr) {
					if (!style) {
						arr.push(obj.childNodes[i]);
					} else {
						if (obj.childNodes[i].style) {
							var flg = false;
							for (key in style) {
								var re = new RegExp(style[key])
								if (obj.childNodes[i].style[key].match(re)) {
									flg = true;
								} else {
									flg = false;
									break;
								}
							}
							if (flg) {
								arr.push(obj.childNodes[i]);
							}
						}
					}
				} else {
					var flg = false;
					for (key in attr) {
						if (obj.childNodes[i].getAttribute(key) == attr[key]) {
							flg = true;
						} else {
							flg = false;
							break;
						}
					}
					if (flg) {
						if (!style) {
							arr.push(obj.childNodes[i]);
						} else {
							if (obj.childNodes[i].style) {
								var flg = false;
								for (key in style) {
									if (obj.childNodes[i].style[key] == style[key]) {
										flg = true;
									} else {
										flg = false;
										break;
									}
								}
								if (flg) {
									arr.push(obj.childNodes[i]);
								}
							}
						}
					}
				}
			}
			getChilds(obj.childNodes[i], nodeName, attr, style, arr);
		}
	}
	return arr;
}
function errorMsg(method, msg) {
	alert("Error:\r\r\n\n    " + method + "                        \r\n    " + msg);
}
function noticeMsg(method, msg) {
	alert('Notice!!!:\r\r    ' + method + '                        \r    ' + msg + '!');
}

function returnFalse(event) {
	event.cancelBubble = true;
	return false;
}

function getNextBrother(obj) {
	if (!is_dom(obj)) {
		errorMsg('tools.js -> Window :: getNextBrother', 'Object in not DOM element');
		return;
	}
}




function parseXML(xml) {
	var arr = Array();
	if (!xml || !xml.firstChild) {
		return '';
	}

	var root = xml.nodeName == '#document' ? xml.firstChild : xml;
	if (root.nodeName == '#text') {
		root = root.nextSibling;
	}
	if (root.nodeName == 'xml') {
		root = root.nextSibling;
	}
	if (root) {
		for (var i = 0; i < root.childNodes.length; i++) {
			var item = root.childNodes[i];
			var nodeName = item.nodeName;
			var attributes = getAttributes(item);
			if (nodeName != '#text') {
				if (nodeName != "#comment") {
					switch (nodeName) {
						case 'length':
						case 'push':
							nodeName = '_' + nodeName;
							break;
					}
					var val = parseXML(item);
					if (!arr[nodeName]) {
						if (attributes) {
							var temp = Array();
							temp['attrs'] = attributes;
							temp['textContent'] = val;
							arr[nodeName] = temp;
						} else {
							arr[nodeName] = val;
						}
					} else {
						if (!is_array(arr[nodeName])) {
							var temp = arr[nodeName];
							arr[nodeName] = Array(temp);
						}
						if (arr[nodeName].length == 0) {
							var temp = arr[nodeName];
							arr[nodeName] = Array(temp);
						}
						if (attributes) {
							var temp = Array();
							temp['attrs'] = attributes;
							temp['textContent'] = val;
							arr[nodeName].push(temp);
						} else {
							arr[nodeName].push(val);
						}
					}
				}
			}
		}
		var flg = false;
		for (key in arr) {
			flg = true;
			break;
		}
		return flg ? arr : (root.text || root.textContent);
	}
	return null;
}

function getAttributes(node) {
	var arr = Array();
	if (node && node.attributes && node.attributes.length > 0) {
		for (var i = 0; i < node.attributes.length; i++) {
			var attr = node.attributes[i];
			arr[attr.name] = attr.value;
		}
		return arr;
	}
	return null;
}

function formatPhoneNumber(number) {
	return number.substring(0, 1) + ' (' + number.substring(1, 4) + ') ' + number.substring(4,
		7) + '-' + number.substring(7, 11);
}

function templater(str, hash)
{

	var leftQ = '<%';
	var rightQ = '%>';
	var re1 = new RegExp("((^|" + rightQ + ")[^\t]*)'", 'g');
	var re2 = new RegExp("\t=(.*?)\\" + rightQ, 'g');
	var my_str = "var a=[];with(hash){a.push('" + str
		.replace(/[\t\r\n]/g, " ")
		.split(leftQ)
		.join("\t")
		.replace(re1, "$1\r")
		.replace(re2, "',$1,'")
		.split("\t")
		.join("');")
		.split(rightQ)
		.join("a.push('")
		.split("\r")
		.join("\\'") + "');} return a.join('');";

	my_str = 'try{' + my_str + '}catch(e){throw e;}';

	return new Function("hash", my_str)(hash);
}

function getCssClassNamevalue(o, className)
{
	var sClasses = o.className.replace(/\n|\r|\t/g, ' ');
	var aClasses = sClasses.split(' ');


	var re = new RegExp('^' + className + '(\-|)([^\-]+|)', '');
	for (var i = 0; i < aClasses.length; i++)
	{
		var class_ = aClasses[i];
		if (res = class_.match(re))
		{
			return res[2];
		}
	}
	return null;
}
getCssClassNameValue = getCssClassNamevalue

function setCssClassName(o, className, value)
{

	if (getCssClassNameValue(o, className) == value)
	{
		return;
	}

	if (typeof value == 'undefined')
	{
		o.className = className;
	}
	else
	{
		var sClasses = o.className.replace(/\n|\r|\t/g, ' ');
		var aClasses = sClasses.split(' ');
		//var re       = new RegExp('^' + className + '\-[^\-]+','') ;
		var re = new RegExp('^' + className + '\-.+', '');
		for (var i = 0; i < aClasses.length; i++)
		{
			var class_ = aClasses[i];
			if (class_.match(re))
			{
				aClasses.splice(i, 1);
			}
		}

		if (value == '' && value != 0)
		{
			aClasses.push(className);
		}
		else
		{


			aClasses.push(className + '-' + value);
		}
		o.className = aClasses.join(' ');
		if (o.firstChild && o.firstChild.tagName == 'B')
		{
			o.firstChild.innerHTML = o.className;
		}
	}
	//alert(o.className)
}

function addClassName(o, className)
{
	var aClasses = o.className.replace(/\n|\r|\t/g, ' ').split(' ');
	for (var i = 0; i < aClasses.length; i++)
	{
		if (aClasses[i] == className)
		{
			return;
		}
	}
	o.className += ' ' + className;
}



function getElementsByClassName(classNameVal, parent, tag)
{
	var res = [];
	parent = parent || document;
	var nodes = parent.getElementsByTagName(tag || '*');
	var l = nodes.length;
	if (l)
	{
		var re = new RegExp("(^|\\s)" + classNameVal + "(\\s|$)");

		for (var i = 0; i < l; i++)
			if (re.test(nodes[i].className))
				res.push(nodes[i]);
	}
	return res;
}

function each(obj, callBack, space)
{
	if (!obj)
	{
		throw new Error('Each\nObject is undefined');
	}
	if (typeof callBack != 'function')
	{
		throw new Error('Each\nCallBack undefined or not a function');
	}

	space = space || obj;

	if (obj.length)
	{
		for (var i = 0, l = obj.length; i < l; i++) {
			if (callBack.call(space, obj[i], parseInt(i), obj) === false)
				return false;
		}
	}
	else
	{
		for (var key in obj) {
			if (obj.hasOwnProperty && obj.hasOwnProperty(key)) {
				if (callBack.call(space, obj[key], key, obj) === false)
					return false;
			}
		}
	}
	return true;
}

function getCurrentStyle(o, name)
{
	if (window.getComputedStyle)
	{
		return window.getComputedStyle(o, null)[name];
	} else if (o.currentStyle)
	{
		return o.currentStyle[name];
	}
	return  null;
}

function each_r(obj, callBack, space)
{
	if (!obj)
	{
		throw new Error('Each\nObject is undefined');
	}
	if (typeof callBack != 'function')
	{
		throw new Error('Each\nCallBack undefined or not a function');
	}

	space = space || obj;

	var res = [];

	if (obj.length)
	{
		for (var i = 0, l = obj.length; i < l; i++) {
			var ret_val = callBack.call(space, obj[i], i, obj);
			if (ret_val)
			{
				res.push(ret_val);
			}
		}
	}
	else
	{
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				var ret_val = callBack.call(space, obj[key], key, obj);
				if (ret_val)
				{
					res.push(ret_val);
				}
			}
		}
	}
	return res.length ? res : null;
}

function getCssPropValueBySelectorAndProp(selector, prop)
{
	var css = window.document.styleSheets;
	if (css.length)
	{
		var res =
			each_r(css,
				(
					function(selector, prop, cssGroup)
					{
						try
						{
							var fullRules = cssGroup.cssRules || cssGroup.rules
							if (fullRules)
							{
								var res =
									each_r(fullRules,
										(
											function(selector, prop, rule)
											{
												if (rule.selectorText == selector)
												{
													return rule.style[prop];
												}
												return null;
											}
										).rcBindAsIs(this, selector, prop)
										);
								return res ? res : null;

							}
						} catch (e) {
						}
					}
				).rcBindAsIs(this, selector, prop)
				);
		return res ? res[0] : null;
	}
}


function opacity(id, opacStart, opacEnd, millisec) {

	var speed = Math.round(millisec / 100);
	var timer = 0;


	if (opacStart > opacEnd) {
		for (i = opacStart; i >= opacEnd; i--) {
			setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
			timer++;
		}
	} else if (opacStart < opacEnd) {
		for (i = opacStart; i <= opacEnd; i++)
		{
			setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
			timer++;
		}
	}
}


function changeOpac(opacity, id) {
	var object = document.getElementById(id).style;
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
	object.filter = "alpha(opacity=" + opacity + ")";
}

function getParentByTagsName(arr, node)
{
	while (inArray(arr, node.nodeName) == -1)
	{
		if (node.nodeName == 'BODY')
		{
			return null;
		}
		node = node.parentNode;
	}
	return node;

}



function fireEvent(el, ev)
{
	if (document.createEventObject)
	{
		var E = document.createEventObject();
		el.fireEvent(ev, E);
		return true;
	}
	else if (document.createEvent)
	{
		var E = document.createEvent("MouseEvents");
		E.initEvent(ev.replace(/^on/, ''), true, false);
		el.dispatchEvent(E);

		var E = document.createEvent("HTMLEvents");
		E.initEvent(ev.replace(/^on/, ''), true, false);
		el.dispatchEvent(E);

		return true;
	}
	else
	{
		return false;
	}
}

function rand(min, max) {
	var argc = arguments.length;
	if (argc == 0) {
		min = 0;
		max = 2147483647;
	} else if (argc == 1) {
		throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
	}
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


function concatHash()
{
	var r = {};
	for (var i = 0; i < arguments.length; i++)
	{
		var parent = arguments[i];
		for (var key in parent)
		{
			r[key] = parent[key];
		}
	}
	return r;
}


function inArray(arr, val) {
	if (arr) {
		for (var i = 0, l = arr.length; i < l; i++) {
			if (arr[i] == val) {
				return i;
			}
		}
	}
	return -1;
}

/* 
 *	Check whether page has been opened from some search engine and
 *	in case of true result set according cookies of gw_pid and gw_engine_referrer
 */
if (typeof brand_prefix == "undefined") {
	var brand_prefix = "RC";
	var tmp_results1 = /m\.ringcentral\.com/.exec(location.host);
	var tmp_results2 = /\.m\.ringcentral\.com/.exec(location.host);
	var tmp_results3 = /\/gwmobile\//.exec(location.href);
	if (tmp_results1 != null || tmp_results2 != null || tmp_results3 != null) { /* GW-6841 */
		brand_prefix = "MOB";
	} else {
		var domain = get_domain_cookie();
		switch (domain) {
			case '.ringcentral.com':
				brand_prefix = 'RC';
				break;
			case '.ringcentral.ca':
				brand_prefix = 'CA';
				break;
			case '.ringcentral.co.uk':
				brand_prefix = 'UK';
				break;
			case '.extremefax.com':
				brand_prefix = 'XF';
				break;
		}
	}
}


function get_domain_cookie() {
	var host = window.location.hostname;
	if(host == 'localhost') return host;
	var _domain = host.split('.');
	_domain.shift();
	if (_domain[0] == 'secure') {
		_domain.shift();
	}
	domain = '.' + _domain.join('.');
	return domain;
}
var cookieSetHost = get_domain_cookie();

/**********start insert*/
/*block for set 'pid', 'bmid', 'rckw', 'rcmt', 'ad',  'kid', 'aid' 'kcid' vars*/
function ParamToCookieSet() {
	var domain = get_domain_cookie();//'.' + window.location.hostname.match(/[^.]+\.(co\.uk|[^.]+)$/)[0];
//    var arr_param = ['pid', 'bmid', 'rckw', 'rcmt', 'ad',  'kid', 'aid'];
	var arr_param_del = ['pid', 'bmid', 'rckw', 'rcmt', 'ad', 'kid', 'aid', 'tduid'];
	var arr_param = ['rckw', 'rcmt', 'ad', 'kid', 'kcid'];
	var arr_param_session = ['pid', 'bmid', 'aid', 'spid', 'tduid'];

	var pid = GetParamsManager.is_set('pid');
	var bmid = GetParamsManager.is_set('bmid');
	if ( !(bmid || /(^|; )gw_bmid=(.*?)(;|$)/i.exec(document.cookie)) ) {	
		['.ringcentral.com/aff/office/small-business-phone-system_c.html',
		'.ringcentral.ca/aff/office/small-business-phone-system.html',
		'.ringcentral.com/aff/fax25p_6.html',
		'.ringcentral.com/aff/contactcenter'].forEach( function( page ){
			if ( document.location.href.indexOf(page) !== -1 ) {
				bmid = 'AFF_GEN';
				CookiesManager.setCookie('gw_bmid', bmid, '', '/', domain);
			}
		});

	}

	var afn = GetParamsManager.is_set('afn');
//    var lcid = GetParamsManager.is_set('lcid');

	var tduid = GetParamsManager.is_set('tduid');
	var cjevent = GetParamsManager.is_set('cjevent');

	if (cjevent) {
		CookiesManager.setCookie('gw_cjevent', cjevent, 120, '/', domain);
	}

	if (tduid) {
		CookiesManager.setCookie('gw_tduid', tduid, '', '/', domain);
	}

	var partnerid = GetParamsManager.is_set('partnerid');
	if (partnerid) {
		CookiesManager.setCookie('gw_partnerid', partnerid, '', '/', domain);
	}
	var safid = GetParamsManager.is_set('safid');
	if (safid) {
		CookiesManager.setCookie('gw_safid', safid, '', '/', domain);
	}
	var kcid = GetParamsManager.is_set('kcid');
	if (kcid) {
		CookiesManager.setCookie('gw_kcid', kcid, '', '/', domain);
	}

	var rckw = GetParamsManager.is_set('rckw');
	if (rckw) {
		CookiesManager.setCookie('gw_rckw', rckw, '', '/', domain);
	}


	function getBmidFromReferrer(hostname) {
		var patterns = {
			GGLPLUS: ['(^|\\.)plus\\.google\\.'],
			FSRC_GOOGLE: ['(^|\\.)google\\.'],
			FSRC_BING: ['(^|\\.)bing\\.'],
			FSRC_YAHOO: ['(^|\\.)yahoo\\.'],
			FSRC_OTHERS: ['(^|\\.)aol\\.',
				'(^|\\.)yandex\\.',
				'(^|\\.)baidu\\.',
				'(^|\\.)duckduckgo\\.',
				'(^|\\.)dogpile\\.',
				'(^|\\.)ask\\.',
				'(^|\\.)myway\\.',
				'(^|\\.)msn\\.',
				'(^|\\.)naver\\.',
				'(^|\\.)sogou\\.'
			],
			FB_L: ['(^|\\.)facebook\\.'],
			LI_L: ['(^|\\.)linkedin\\.',
				'^lnkd\\.in'
			],
			T_L: ['(^|\\.)twitter\\.',
				'^t\\.co'
			],
			YT_L: ['(^|\\.)youtube\\.'],
			RCUS_OTHERREF: ['(^|\\.)pinteres\\.',
				'(^|\\.)reddit\\.',
				'(^|\\.)instagramm\\.',
				'(^|\\.)slideshare\\.',
				'(^|\\.)yelp\\.',
				'(^|\\.)on24\\.',
			],
			GGUSCONCOMPET: ['(^|\\.)doubleclick.net$',
				'(^|\\.)googlesyndication.com$'
			],
			RCUS_INTDOMAINS: [
				'(^|\\.)glip.com$',
				'(^|\\.)ringcentral\\.',
				'(^|\\.)extremefax.com$',
				'(^|\\.)salesforce.com$',
				'(^|\\.)ringdemo\\.',
				'(^|\\.)liveperson.net$',
				'(^|\\.)serverdata.net$',
			],
			RCUS_INPRODUCT: ['(^|\\.)mindbodyonline.com$',
				'(^|\\.)e-access.att.com$',
				'(^|\\.)mybodhi.com$',
				'(^|\\.)showroomtransport.com$',
				'(^|\\.)amridgeuniversity.blackboard.com$',
				'(^|\\.)holidiumlabs.com$',
				'(^|\\.)yammer.com$',
			],
			AFF_GEN: ['(^|\\.)shawngraham.me$',
				'(^|\\.)fitsmallbusiness.com$',
				'(^|\\.)fabsuite.com$',
				'(^|\\.)komando.com$',
				'(^|\\.)saas-reviews.com$',
				'(^|\\.)smallbiztrends.com$',
				'(^|\\.)appstechnews.com$',
				'(^|\\.)gmailfax.com$',
				'(^|\\.)howtogeek.com$',
				'(^|\\.)businessnewsdaily.com$',
				'(^|\\.)getapp.com$',
			],
		};

		var ret = '';
		if (typeof hostname === 'undefined') {
			if (!window.document.referrer)
				return ret;
			var parser = document.createElement('a');
			parser.href = window.document.referrer.toLowerCase();
			hostname = parser.hostname != window.location.hostname ? parser.hostname : '';
		}
		if (!hostname)
			return ret;

		Object.keys(patterns).forEach(function(key) {
			if (!ret) {
				patterns[key].forEach(function(reg) {
					if (!ret && hostname.match(reg)) {
						ret = key;
					}
				});
			}
			;
		});

		if (!ret)
			ret = 'RCUS_OTHERREF';
		return ret;
	}



	function DelPreviousParam() {
		for (var i = 0; i < arr_param_del.length; i++) {
			CookiesManager.delCookie('gw_' + arr_param_del[i], '/', domain);
		}
	}

	function SetParam() {/* set param 'rckw', 'rcmt', 'ad',  'kid', 'aid', 'kcid' */
		var param = GetParamsManager.get2array();
		for (var i = 1; i < arr_param.length; i++) {
			if (typeof param[arr_param[i]] != 'undefined' && param[arr_param[i]].toString().length > 0) {
				var name = 'gw_' + arr_param[i];
				CookiesManager.setCookie(name, param[arr_param[i]], 30, '/', domain);
			}
		}
		for (var i = 1; i < arr_param_session.length; i++) {
			if (typeof param[arr_param_session[i]] != 'undefined' && param[arr_param_session[i]].toString().length > 0) {
				var name = 'gw_' + arr_param_session[i];
				CookiesManager.setCookie(name, param[arr_param_session[i]], '', '/', domain);
			}
		}
	}

	function BeginSetParam() {
		DelPreviousParam();
		SetParam();
		if (afn && !(!!bmid)) {
			CookiesManager.setCookie('gw_bmid', escape(afn), '', '/', domain);
			if (!!pid) {
				CookiesManager.setCookie('gw_pid', escape(pid), '', '/', domain);
			}
		} else if (pid && !(!!bmid)) {
			CookiesManager.setCookie('gw_bmid', escape(pid), '', '/', domain);
		} else {
			CookiesManager.setCookie('gw_bmid', escape(bmid), '', '/', domain);
			if (!!pid) {
				CookiesManager.setCookie('gw_pid', escape(pid), '', '/', domain);
			}
		}
	}

	var rr_host, rr_query;
	if (!(!!pid || !!bmid)) { /*set pid from referrer*/
		var rr = GetParamsManager.is_set('gw_referrer');
		if (rr == 'null')
			rr = '';
		if (!rr)
			rr = window.document.referrer;
		if (!!rr) {
			var rr_host = '';
			try {
				rr_host = rr.match(/(http|https):\/\/(?:www\.)?([^\/]+)/)[2];
			} catch (e) {
				rr_host = decodeURIComponent(rr).match(/(http|https):\/\/(?:www\.)?([^\/]+)/)[2];
			}
			var rr_query = rr.split("?")[1] || "";
			/*  hook search */
			var re = /results\/Web\/([^\/]+)\//i;
			var found = rr.match(re);
			if (found != null) {
				rr_query += '&hook_query=' + found[1];
			}
//            pid = get_GW_PID(rr_host, rr_query);
			var referer = GetParamsManager.is_set('gw_referrer');
			if (
				null == referer
				|| 'null' == referer
			) {
				referer = undefined;
			}
			pid = getBmidFromReferrer(referer);
			pid = pid == "" ? null : pid;
		}
	}

	if (!!pid || !!bmid || !!afn) {
		BeginSetParam();/*chahge Param*/
//        var gw_rckw = get_GW_RCKW(rr_host, rr_query);
//		var gw_rckw = '';
//		if (gw_rckw != "") {
//			CookiesManager.setCookie("gw_rckw", escape(gw_rckw), '', '/', domain);
//		}
	}

	if (bmid == 'CJZLKR0808_01') {
		(function() {
			var matches = [];
			if (matches = /(\.[^\.]+\.(com|ca|co\.uk|ru|net))$/.exec(window.location.hostname)) {
				//e.g. .ringcentral.com, .ringcentral.co.uk
				CookiesManager.setCookie('trial30', 'true', '', '/', matches[0]);
			} else {
				CookiesManager.setCookie('trial30', 'true');
			}
		})();
	}
	//Set cookie CID
	bmid = CookiesManager.getCookie('gw_bmid');
	// GW-7906 GW-8690

	var setcid = false;

	if (GetParamsManager.is_set('cid')) {
		var tmp_array_key = ['PARTNER', 'RETARGET', 'REFERRAL', 'MEDIA', 'RESELLER', 'OTHER', 'SERVPROV', 'SOCIAL', 'LEADGEN', 'AFF', 'SEM'];
		var tmp_value_key = GetParamsManager.is_set('cid').toUpperCase();
		for (var p in tmp_array_key) {
			if (tmp_value_key == tmp_array_key[p]) {
				CookiesManager.setCookie('gw_cid', tmp_array_key[p], 30, '/', domain);
				setcid = true;
			}
		}
	}

	if (!setcid && !CookiesManager.getCookie('gw_cid')) {
		if (!CookiesManager.getCookie('gw_bmid') && !CookiesManager.getCookie('gw_partnerid')) {
			CookiesManager.setCookie('gw_cid', 'DIRECT', 30, '/', domain);
		} else if (GetParamsManager.is_set('rckw') && GetParamsManager.is_set('rcmt')) {
			CookiesManager.setCookie('gw_cid', 'SEM', 30, '/', domain);
		} else if (bmid && (bmid.substring(0, 2)).toUpperCase() == 'FS') {
//GW-17470
			CookiesManager.setCookie('gw_cid', 'SEO', 30, '/', domain);
		} else if (typeof (GetParamsManager.is_set('afn')) == 'string') {
			CookiesManager.setCookie('gw_cid', 'AFF', 30, '/', domain);
		}
	}


	/* block for set 'adgrpid', 'oid', 'product', 'afn', 'sid', 'office_phone', 'oppid' 'kcid' 'rckw' vars*/

	var domain = get_domain_cookie();//'.' + window.location.hostname.match(/[^.]+\.(co\.uk|[^.]+)$/)[0];
	var param = GetParamsManager.get2array();
	var get_param = ['adgrpid', 'oid', 'product', 'afn', 'sid', 'office_phone', 'oppid', 'kcid', 'rckw'];

	for (var p in get_param) {
		if (typeof param[get_param[p]] != 'undefined') {
			var name = get_param[p];
			if (name == 'adgrpid'
				|| name == 'afn'
				|| name == 'sid'
				|| name == 'kcid'
				|| name == 'rckw'
				|| name == 'oppid')
			{
				name = 'gw_' + name;
			}

			if (name == 'gw_oppid' && /https/i.test(window.location.protocol)) {
				CookiesManager.setCookie(name, param[get_param[p]], '', '/', domain, true);
			} else {
				CookiesManager.setCookie(name, param[get_param[p]], '', '/', domain);
			}

			if (name == 'product') {
				name = name + '_tags';
				var cValue = (param[get_param[p]].toLowerCase() == 'mobile') ? 'online' : param[get_param[p]];
				switch (cValue) {
					case 'office':
					case 'online':
					case 'fax':
						CookiesManager.setCookie(name, cValue, '', '/', domain);
						break;
				}
			}
		}
	}
	//GW-6315
	if (typeof param['siteid'] != 'undefined') {
		CookiesManager.setCookie('gw_sid', escape(param['siteid']), '', '/', domain);
	}
	//GW-5054
	if (typeof param['spid'] != 'undefined') {
		CookiesManager.setCookie('gw_spid', escape(param['spid']), '', '/', domain);
	}


}
;

ParamToCookieSet();

!function() {
	var params = [];

	$.each({
		'gw_bmid': 'BMID',
		'gw_kcid': 'kcid',
		'gw_pid': 'PID',
		'gw_aid': 'AID',
		'gw_rckw': 'RCKW'
	}, function(key, val) {
		var v = CookiesManager.getCookie(key);
		if (v)
			params.push(val + '=' + v);
	});

	if (params.length > 0) {
		var reg = /^(http:\/\/|https:\/\/|\/\/|)(marketo|go).ringcentral.com([^\?]+)(\??.*)$/;
		$(function() {
			$("a").attr('href', function(i, h) {
				if (h) {
					var t = h.match(reg);
					if (t) {
						if (t[4])
							if (t[4].length == 1)
								h += params.join('&');
							else
								h += '&' + params.join('&');
						else
							h += '?' + params.join('&');
						return h;
					}
				}
			})
		});
	}
}();

/**********end insert*/
var nead_cookie_afn = ['/cj/toll-free-numbers.asp', '/cj/toll-free-numbersb.asp', '/cj/toll-free-numbersb_sp.asp',
	'/cj/toll-free-numbers10p.html', '/cj/toll-free-numbers30b.asp', '/cj/toll-free-numbersZLKR30.asp',
	'/cj/virtual-phone-system.html', '/cj/virtual-phone-systemsf30.html',
	'/cj/virtual-pbx-new.asp', '/cj/faxandphone.html', '/cj/voicemail.asp', '/cj/vanitynumber.asp',
	'/cj/call-forwarding.asp', '/cj/digitalline-voip.html', '/cj/local-numbers/area-codes.html',
	'/cj/fax.asp', '/cj/fax30.asp', '/cj/fax.asp', '/cj/office-everywhere-phone-fax.asp'];



var ref_full = window.location.pathname;// + window.location.search;
if (inArray(nead_cookie_afn, ref_full) != -1) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'cj', '', '/', cookieSetHost);
}

if (ref_full.match(/^\/aff\//)) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'aff', '', '/', cookieSetHost);
}

if (ref_full.match(/^\/az\//)) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'az', '', '/', cookieSetHost);
}

if (CookiesManager.getCookie('gw_afn')) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', CookiesManager.getCookie('gw_afn'), '', '/', cookieSetHost);
	var hasAfnCookie = true;
}

var p = GetParamsManager.is_set('p');
if (p) {
	CookiesManager.setCookie('p', p);
}

var o = GetParamsManager.is_set('o');
if (o) {
	CookiesManager.setCookie('o', o, 1 / 64);
}

if (CookiesManager.getCookie('afn')) {
	var hasAfnCookie = true;
}


(function() {
	/* begin GW-536 */
	function getProductTag(url) {
		var _productTags = {
			'/fax/default.html': 'fax',
			'/fax/emailfax.html': 'fax',
			'/fax/default.asp': 'fax',
			'/fax/emailfax.asp': 'fax',
			'/office/business-phone-service.html': 'office',
			'/office/business-voip-phone.html': 'office',
			'/office/index.html': 'office',
			'/office/internet-business-phone-system.html': 'office',
			'/office/online-business-phone-system.html': 'office',
			'/office/phone-system.html': 'office',
			'/office/small-business-phone-system.html': 'office',
			'/office/virtual-business-phone-service.html': 'office',
			'/office/virtual-business-phone-system.html': 'office',
			'/office/virtual-pbx-phone-system.html': 'office',
			'/business-toll-free.dhtml': 'online',
			'/features/real-time-control/overview.html': 'online',
			'/lp/800-business-number.html': 'online',
			'/lp/800-business-number.asp': 'online',
			'/lp/800numbers.html': 'online',
			'/lp/800numbers.asp': 'online',
			'/lp/800numbers-yh-a.asp': 'online',
			'/lp/800service.html': 'online',
			'/lp/800service.asp': 'online',
			'/lp/866-business-number.html': 'online',
			'/lp/866-business-number.asp': 'online',
			'/lp/877-business-number.html': 'online',
			'/lp/877-business-number.asp': 'online',
			'/lp/auto-attendant.asp': 'online',
			'/lp/call-forwarding.html': 'online',
			'/lp/call-forwarding.asp': 'online',
			'/lp/callscreening.asp': 'online',
			'/lp/call-screening.html': 'online',
			'/lp/call-screening.asp': 'online',
			'/lp/digitalline-voip.html': 'online',
			'/lp/digitalline-voip-home.html': 'online',
			'/lp/local-numbers.html': 'online',
			'/lp/local-numbers.asp': 'online',
			'/lp/small-business-phone.html': 'online',
			'/lp/toll-free-numbers.html': 'online',
			'/lp/toll-free-numbers.asp': 'online',
			'/lp/toll-free-numbers.asp'                  : 'online',
				'/lp/toll-free-numbers-yh-a.asp': 'online',
			'/lp/unified-communications.asp': 'online',
			'/lp/vanitynumber.html': 'online',
			'/lp/vanitynumber.asp': 'online',
			'/lp/virtual-pbx.asp': 'online',
			'/lp/virtualphonenumber.asp': 'online',
			'/lp/virtual-phone-service.html': 'online',
			'/lp/virtual-phone-system.html': 'online',
			'/lp/voicemail.asp': 'online',
			'/lp/auto-attendant.html': 'online',
			'/lp/unified-communications.html': 'online',
			'/lp/virtualphonenumber.html': 'online',
			'/lp/virtual-pbx.html': 'online',
			'/lp/voicemail.html': 'online',
			'/lp/toll-free-numbers-yh-a.asp'  :  'online'


		};

		var _regex = new RegExp(
			'^\/features\/local-numbers\/area-code-(201|202|203|205|206|208|209|210|212|213|214|216|217|218|219|224|225|228|229|231|234|239|240|248|251|252|253|254|256|260|262|267|269|270|276|281|301|302|303|304|305|307|309|310|312|313|314|315|316|317|318|321|323|325|330|334|336|337|339|347|352|360|361|386|401|402|404|405|406|407|408|409|410|412|413|414|415|417|419|423|424|425|432|434|435|440|443|469|478|479|480|484|501|502|503|504|505|507|508|509|510|512|513|516|517|518|520|530|540|541|559|561|562|567|570|571|573|575|580|585|586|601|603|605|606|607|608|609|610|612|614|615|616|617|618|619|620|623|626|630|631|636|646|650|651|661|662|678|682|701|702|703|704|706|707|708|712|713|714|715|716|717|718|719|720|724|727|731|732|734|740|754|757|760|763|765|769|770|772|773|775|785|786|801|802|803|804|805|806|810|812|813|814|815|816|817|818|828|830|831|832|843|845|847|850|856|858|859|860|862|863|864|865|870|901|903|904|906|908|909|913|914|915|916|918|919|920|925|928|936|937|940|949|951|952|954|956|970|971|973|979|989)\.html',
			'i');

		if (typeof _productTags[url] == 'string') {
			return _productTags[url];
		} else if (_regex.test(url)) {
			return 'online';
		} else {
			return false;
		}
	}

	var _productTag = getProductTag(window.location.pathname);
	if (_productTag && !CookiesManager.getCookie('product_tags')) {
		CookiesManager.setCookie('product_tags', _productTag, 30, '/', cookieSetHost);
	}
})();

(function() {
	// gw-3616
	var tellapalId = GetParamsManager.is_set('tellapal.id');
	if (!!tellapalId) {
		CookiesManager.setCookie('tellapal_id', tellapalId, '', '/', cookieSetHost);
	}

// gw-4291
	var today = new Date();
	if (CookiesManager.getCookie('gw_new_visitor') == null) {
		CookiesManager.setCookie('gw_new_visitor', today.getTime(), 30, '/', cookieSetHost);
	} else {
		var gw_new_visitor = CookiesManager.getCookie('gw_new_visitor');
//time fix, deleted after 2014-02-01 GW-17303
		if (gw_new_visitor.indexOf(" ") != -1) {
			var enterDay = new Date(gw_new_visitor);
		} else {
			var enterDay = new Date();
			enterDay.setTime(gw_new_visitor);
		}

		var one_day = 1000 * 60 * 60 * 24;
		var difference = today.getTime() - enterDay.getTime();
		if (gw_new_visitor != 'false' && difference > one_day) {
			var time = 30 - Math.ceil(difference) / one_day;
			CookiesManager.setCookie('gw_new_visitor', 'false', time, '/', cookieSetHost);
		}
	}
})();


(function() {
	/* start insert GW-9686 */



	var param = GetParamsManager.get2array();
	var pid = param['pid'];
	var bmid = param['bmid'];

	var refcode = CookiesManager.getCookie('gw_refcode');
	if (!!refcode) {
		bmid = refcode;
		CookiesManager.setCookie('gw_bmid', escape(bmid), '', '/', cookieSetHost);
		CookiesManager.delCookie('gw_refcode', '/', cookieSetHost);
	}

	if (pid && !(!!bmid)) {
		param['bmid'] = pid;
	}

	var flag_reload = false;
	var path = document.location.hostname + document.location.pathname;
	var converts_array = {
		'\.extremefax\.com\/msofficeapp\.asp': [{pid: 'MSOMRKTML_XF'}],
		'\.ringcentral\.com\/lp\/msmarketplace\.asp': [{pid: 'MSOMRKTML_RC'}],
		'\.ringcentral\.com\/partners\/vista\.htm': [{pid: 'VISTAOL8'}],
		'\.ringcentral\.com\/concentric\/rco': [{pid: 'CONRC08'}, {sid: '2512'}, {aid: '6505'}],
		'internetfax101\.com': [{pid: 'INTFAX101'}]
	}
	for (var pattern in converts_array) {
		var reg1 = new RegExp(pattern, "i");
		if (reg1.test(path))
			for (var i = 0; i < converts_array[pattern].length; i++) {
				var item = converts_array[pattern][i];
				for (var key in item) {
					if ((typeof param[key] != 'undefined') && (param[key] != item[key])) {
						param[key] = item[key];
						flag_reload = true;
					}
					CookiesManager.setCookie('gw_' + key, item[key], '', '/', cookieSetHost);
					if (key == 'pid')
						pid = param[key];
				}
			}
		if (flag_reload) {
			var request = [];
			for (var key in param) {
				request.push(key + '=' + param[key]);
			}
			var rr = window.document.referrer;
			if (!!rr) {
				request.push('gw_referrer=' + encodeURIComponent(rr));
			}
			window.location = 'http://' + path + "?" + request.join('&');
			break;
		}
	}
	/* end insert */
})();


})(jQuery);
!function e(t,n,a){function i(o,s){if(!n[o]){if(!t[o]){var c="function"==typeof require&&require;if(!s&&c)return c(o,!0);if(r)return r(o,!0);var l=new Error("Cannot find module '"+o+"'");throw l.code="MODULE_NOT_FOUND",l}var u=n[o]={exports:{}};t[o][0].call(u.exports,function(e){return i(t[o][1][e]||e)},u,u.exports,e,t,n,a)}return n[o].exports}for(var r="function"==typeof require&&require,o=0;o<a.length;o++)i(a[o]);return i}({1:[function(e,t){(function(n){
/** @license ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ============

Adobe Visitor API for JavaScript version: 3.0.0
Copyright 1996-2015 Adobe, Inc. All Rights Reserved
More info available at https://marketing.adobe.com/resources/help/en_US/mcvid/
*/
var a=e("./child/ChildVisitor"),i=e("./child/Message"),r=e("./child/makeChildMessageListener"),o=e("./utils/asyncParallelApply"),s=e("./utils/enums"),c=e("./utils/utils"),l=e("./utils/getDomain"),u=e("./units/version"),d=e("./units/crossDomain"),g=e("@adobe-mcid/visitor-js-shared/lib/ids/generateRandomID"),m=e("./units/makeCorsRequest"),f=e("./units/makeDestinationPublishing"),h=e("./utils/constants"),p=function(e,t,a){function p(e){var t=e;return function(e){var n=e||C.location.href;try{var a=S._extractParamFromUri(n,t);if(a)return A.parsePipeDelimetedKeyValues(a)}catch(e){}}}function v(e){function t(e,t){e&&e.match(h.VALID_VISITOR_ID_REGEX)&&t(e)}t(e[L],S.setMarketingCloudVisitorID),S._setFieldExpire(M,-1),t(e[T],S.setAnalyticsVisitorID)}function b(e){e=e||{},S._supplementalDataIDCurrent=e.supplementalDataIDCurrent||"",S._supplementalDataIDCurrentConsumed=e.supplementalDataIDCurrentConsumed||{},S._supplementalDataIDLast=e.supplementalDataIDLast||"",S._supplementalDataIDLastConsumed=e.supplementalDataIDLastConsumed||{}}function V(e){function t(e,t,n){return(n=n?n+="|":n)+(e+"=")+encodeURIComponent(t)}function n(e,n){var a=n[0],i=n[1];return null!=i&&i!==F&&(e=t(a,i,e)),e}var a,i=e.reduce(n,"");return(a=(a=i)?a+="|":a)+"TS="+A.getTimestampInSeconds()}function y(e){var t=e.minutesToLive,n="";return(S.idSyncDisableSyncs||S.disableIdSyncs)&&(n=n||"Error: id syncs have been disabled"),"string"==typeof e.dpid&&e.dpid.length||(n=n||"Error: config.dpid is empty"),"string"==typeof e.url&&e.url.length||(n=n||"Error: config.url is empty"),void 0===t?t=20160:(t=parseInt(t,10),(isNaN(t)||t<=0)&&(n=n||"Error: config.minutesToLive needs to be a positive number")),{error:n,ttl:t}}if(!a||a.split("").reverse().join("")!==e)throw new Error("Please use `Visitor.getInstance` to instantiate Visitor.");var S=this;S.version="3.0.0";var C=n,I=C.Visitor;I.version=S.version,I.AuthState=s.AUTH_STATE,I.OptOut=s.OPT_OUT,C.s_c_in||(C.s_c_il=[],C.s_c_in=0),S._c="Visitor",S._il=C.s_c_il,S._in=C.s_c_in,S._il[S._in]=S,C.s_c_in++,S._log={requests:[]},S.marketingCloudOrgID=e,S.cookieName="AMCV_"+e,S.sessionCookieName="AMCVS_"+e,S.cookieDomain=l(),S.cookieDomain===C.location.hostname&&(S.cookieDomain=""),S.loadSSL=C.location.protocol.toLowerCase().indexOf("https")>=0,S.loadTimeout=3e4,S.CORSErrors=[],S.marketingCloudServer=S.audienceManagerServer="dpm.demdex.net",S.sdidParamExpiry=30;var k=C.document,_=null,L="MCMID",D="MCIDTS",P="A",T="MCAID",E="AAM",M="MCAAMB",F="NONE",N=function(e){return!Object.prototype[e]},w=m(S,B);S.FIELDS=s.FIELDS,S.cookieRead=function(e){e=encodeURIComponent(e);var t=(";"+k.cookie).split(" ").join(";"),n=t.indexOf(";"+e+"="),a=n<0?n:t.indexOf(";",n+1);return n<0?"":decodeURIComponent(t.substring(n+2+e.length,a<0?t.length:a))},S.cookieWrite=function(e,t,n){var a,i=S.cookieLifetime;if(t=""+t,i=i?(""+i).toUpperCase():"",n&&"SESSION"!==i&&"NONE"!==i){if(a=""!==t?parseInt(i||0,10):-60)(n=new Date).setTime(n.getTime()+1e3*a);else if(1===n){var r=(n=new Date).getYear();n.setYear(r+2+(r<1900?1900:0))}}else n=0;return e&&"NONE"!==i?(k.cookie=encodeURIComponent(e)+"="+encodeURIComponent(t)+"; path=/;"+(n?" expires="+n.toGMTString()+";":"")+(S.cookieDomain?" domain="+S.cookieDomain+";":""),S.cookieRead(e)===t):0},S.resetState=function(e){e?S._mergeServerState(e):b()},S._isAllowedDone=!1,S._isAllowedFlag=!1,S.isAllowed=function(){return S._isAllowedDone||(S._isAllowedDone=!0,(S.cookieRead(S.cookieName)||S.cookieWrite(S.cookieName,"T",1))&&(S._isAllowedFlag=!0)),S._isAllowedFlag},S.setMarketingCloudVisitorID=function(e){S._setMarketingCloudFields(e)},S._use1stPartyMarketingCloudServer=!1,S.getMarketingCloudVisitorID=function(e,t){if(S.isAllowed()){S.marketingCloudServer&&S.marketingCloudServer.indexOf(".demdex.net")<0&&(S._use1stPartyMarketingCloudServer=!0);var n=S._getAudienceManagerURLData("_setMarketingCloudFields"),a=n.url;return S._getRemoteField(L,a,e,t,n)}return""},S.getVisitorValues=function(e,t){var n={MCMID:{fn:S.getMarketingCloudVisitorID,args:[!0],context:S},MCOPTOUT:{fn:S.isOptedOut,args:[void 0,!0],context:S},MCAID:{fn:S.getAnalyticsVisitorID,args:[!0],context:S},MCAAMLH:{fn:S.getAudienceManagerLocationHint,args:[!0],context:S},MCAAMB:{fn:S.getAudienceManagerBlob,args:[!0],context:S}},a=t&&t.length?A.pluck(n,t):n;o(a,e)},S._currentCustomerIDs={},S._customerIDsHashChanged=!1,S._newCustomerIDsHash="",S.setCustomerIDs=function(e){function t(){S._customerIDsHashChanged=!1}if(S.isAllowed()&&e){var n,a;for(n in S._readVisitor(),e)if(N(n)&&(a=e[n]))if("object"==typeof a){var i={};a.id&&(i.id=a.id),null!=a.authState&&(i.authState=a.authState),S._currentCustomerIDs[n]=i}else S._currentCustomerIDs[n]={id:a};var r=S.getCustomerIDs(),o=S._getField("MCCIDH"),s="";for(n in o||(o=0),r)N(n)&&(s+=(s?"|":"")+n+"|"+((a=r[n]).id?a.id:"")+(a.authState?a.authState:""));S._newCustomerIDsHash=S._hash(s),S._newCustomerIDsHash!==o&&(S._customerIDsHashChanged=!0,S._mapCustomerIDs(t))}},S.getCustomerIDs=function(){S._readVisitor();var e,t,n={};for(e in S._currentCustomerIDs)N(e)&&(t=S._currentCustomerIDs[e],n[e]||(n[e]={}),t.id&&(n[e].id=t.id),null!=t.authState?n[e].authState=t.authState:n[e].authState=I.AuthState.UNKNOWN);return n},S.setAnalyticsVisitorID=function(e){S._setAnalyticsFields(e)},S.getAnalyticsVisitorID=function(e,t,n){if(!A.isTrackingServerPopulated()&&!n)return S._callCallback(e,[""]),"";if(S.isAllowed()){var a="";if(n||(a=S.getMarketingCloudVisitorID(function(){S.getAnalyticsVisitorID(e,!0)})),a||n){var i=n?S.marketingCloudServer:S.trackingServer,r="";S.loadSSL&&(n?S.marketingCloudServerSecure&&(i=S.marketingCloudServerSecure):S.trackingServerSecure&&(i=S.trackingServerSecure));var o={};if(i){var s="http"+(S.loadSSL?"s":"")+"://"+i+"/id",c="d_visid_ver="+S.version+"&mcorgid="+encodeURIComponent(S.marketingCloudOrgID)+(a?"&mid="+encodeURIComponent(a):"")+(S.idSyncDisable3rdPartySyncing||S.disableThirdPartyCookies?"&d_coppa=true":""),l=["s_c_il",S._in,"_set"+(n?"MarketingCloud":"Analytics")+"Fields"];r=s+"?"+c+"&callback=s_c_il%5B"+S._in+"%5D._set"+(n?"MarketingCloud":"Analytics")+"Fields",o.corsUrl=s+"?"+c,o.callback=l}return o.url=r,S._getRemoteField(n?L:T,r,e,t,o)}}return""},S.getAudienceManagerLocationHint=function(e,t){if(S.isAllowed()&&S.getMarketingCloudVisitorID(function(){S.getAudienceManagerLocationHint(e,!0)})){var n=S._getField(T);if(!n&&A.isTrackingServerPopulated()&&(n=S.getAnalyticsVisitorID(function(){S.getAudienceManagerLocationHint(e,!0)})),n||!A.isTrackingServerPopulated()){var a=S._getAudienceManagerURLData(),i=a.url;return S._getRemoteField("MCAAMLH",i,e,t,a)}}return""},S.getLocationHint=S.getAudienceManagerLocationHint,S.getAudienceManagerBlob=function(e,t){if(S.isAllowed()&&S.getMarketingCloudVisitorID(function(){S.getAudienceManagerBlob(e,!0)})){var n=S._getField(T);if(!n&&A.isTrackingServerPopulated()&&(n=S.getAnalyticsVisitorID(function(){S.getAudienceManagerBlob(e,!0)})),n||!A.isTrackingServerPopulated()){var a=S._getAudienceManagerURLData(),i=a.url;return S._customerIDsHashChanged&&S._setFieldExpire(M,-1),S._getRemoteField(M,i,e,t,a)}}return""},S._supplementalDataIDCurrent="",S._supplementalDataIDCurrentConsumed={},S._supplementalDataIDLast="",S._supplementalDataIDLastConsumed={},S.getSupplementalDataID=function(e,t){S._supplementalDataIDCurrent||t||(S._supplementalDataIDCurrent=S._generateID(1));var n=S._supplementalDataIDCurrent;return S._supplementalDataIDLast&&!S._supplementalDataIDLastConsumed[e]?(n=S._supplementalDataIDLast,S._supplementalDataIDLastConsumed[e]=!0):n&&(S._supplementalDataIDCurrentConsumed[e]&&(S._supplementalDataIDLast=S._supplementalDataIDCurrent,S._supplementalDataIDLastConsumed=S._supplementalDataIDCurrentConsumed,S._supplementalDataIDCurrent=n=t?"":S._generateID(1),S._supplementalDataIDCurrentConsumed={}),n&&(S._supplementalDataIDCurrentConsumed[e]=!0)),n},S.getOptOut=function(e,t){if(S.isAllowed()){var n=S._getAudienceManagerURLData("_setMarketingCloudFields"),a=n.url;return S._getRemoteField("MCOPTOUT",a,e,t,n)}return""},S.isOptedOut=function(e,t,n){if(S.isAllowed()){t||(t=I.OptOut.GLOBAL);var a=S.getOptOut(function(n){var a=n===I.OptOut.GLOBAL||n.indexOf(t)>=0;S._callCallback(e,[a])},n);return a?a===I.OptOut.GLOBAL||a.indexOf(t)>=0:null}return!1},S._fields=null,S._fieldsExpired=null,S._hash=function(e){var t,n=0;if(e)for(t=0;t<e.length;t++)n=(n<<5)-n+e.charCodeAt(t),n&=n;return n},S._generateID=g,S._generateLocalMID=function(){var e=S._generateID(0);return O.isClientSideMarketingCloudVisitorID=!0,e},S._callbackList=null,S._callCallback=function(e,t){try{"function"==typeof e?e.apply(C,t):e[1].apply(e[0],t)}catch(e){}},S._registerCallback=function(e,t){t&&(null==S._callbackList&&(S._callbackList={}),null==S._callbackList[e]&&(S._callbackList[e]=[]),S._callbackList[e].push(t))},S._callAllCallbacks=function(e,t){if(null!=S._callbackList){var n=S._callbackList[e];if(n)for(;n.length>0;)S._callCallback(n.shift(),t)}},S._addQuerystringParam=function(e,t,n,a){var i=encodeURIComponent(t)+"="+encodeURIComponent(n),r=A.parseHash(e),o=A.hashlessUrl(e);if(-1===o.indexOf("?"))return o+"?"+i+r;var s=o.split("?"),c=s[0]+"?",l=s[1];return c+A.addQueryParamAtLocation(l,i,a)+r},S._extractParamFromUri=function(e,t){var n=new RegExp("[\\?&#]"+t+"=([^&#]*)").exec(e);if(n&&n.length)return decodeURIComponent(n[1])},S._parseAdobeMcFromUrl=p(h.ADOBE_MC),S._parseAdobeMcSdidFromUrl=p(h.ADOBE_MC_SDID),S._attemptToPopulateSdidFromUrl=function(t){var n=S._parseAdobeMcSdidFromUrl(t),a=1e9;n&&n.TS&&(a=A.getTimestampInSeconds()-n.TS),n&&n.SDID&&n.MCORGID===e&&a<S.sdidParamExpiry&&(S._supplementalDataIDCurrent=n.SDID,S._supplementalDataIDCurrentConsumed.SDID_URL_PARAM=!0)},S._attemptToPopulateIdsFromUrl=function(){var t=S._parseAdobeMcFromUrl();if(t&&t.TS){var n=A.getTimestampInSeconds()-t.TS;if(Math.floor(n/60)>h.ADOBE_MC_TTL_IN_MIN||t.MCORGID!==e)return;v(t)}},S._mergeServerState=function(e){if(e)try{if(a=e,(e=A.isObject(a)?a:JSON.parse(a))[S.marketingCloudOrgID]){var t=e[S.marketingCloudOrgID];n=t.customerIDs,A.isObject(n)&&S.setCustomerIDs(n),b(t.sdid)}}catch(e){throw new Error("`serverState` has an invalid format.")}var n,a},S._timeout=null,S._loadData=function(e,t,n,a){t=S._addQuerystringParam(t,"d_fieldgroup",e,1),a.url=S._addQuerystringParam(a.url,"d_fieldgroup",e,1),a.corsUrl=S._addQuerystringParam(a.corsUrl,"d_fieldgroup",e,1),O.fieldGroupObj[e]=!0,a===Object(a)&&a.corsUrl&&"XMLHttpRequest"===w.corsMetadata.corsType&&w.fireCORS(a,n,e)},S._clearTimeout=function(e){null!=S._timeout&&S._timeout[e]&&(clearTimeout(S._timeout[e]),S._timeout[e]=0)},S._settingsDigest=0,S._getSettingsDigest=function(){if(!S._settingsDigest){var e=S.version;S.audienceManagerServer&&(e+="|"+S.audienceManagerServer),S.audienceManagerServerSecure&&(e+="|"+S.audienceManagerServerSecure),S._settingsDigest=S._hash(e)}return S._settingsDigest},S._readVisitorDone=!1,S._readVisitor=function(){if(!S._readVisitorDone){S._readVisitorDone=!0;var e,t,n,a,i,r,o=S._getSettingsDigest(),s=!1,c=S.cookieRead(S.cookieName),l=new Date;if(null==S._fields&&(S._fields={}),c&&"T"!==c)for((c=c.split("|"))[0].match(/^[\-0-9]+$/)&&(parseInt(c[0],10)!==o&&(s=!0),c.shift()),c.length%2==1&&c.pop(),e=0;e<c.length;e+=2)n=(t=c[e].split("-"))[0],a=c[e+1],t.length>1?(i=parseInt(t[1],10),r=t[1].indexOf("s")>0):(i=0,r=!1),s&&("MCCIDH"===n&&(a=""),i>0&&(i=l.getTime()/1e3-60)),n&&a&&(S._setField(n,a,1),i>0&&(S._fields["expire"+n]=i+(r?"s":""),(l.getTime()>=1e3*i||r&&!S.cookieRead(S.sessionCookieName))&&(S._fieldsExpired||(S._fieldsExpired={}),S._fieldsExpired[n]=!0)));!S._getField(T)&&A.isTrackingServerPopulated()&&(c=S.cookieRead("s_vi"))&&((c=c.split("|")).length>1&&c[0].indexOf("v1")>=0&&((e=(a=c[1]).indexOf("["))>=0&&(a=a.substring(0,e)),a&&a.match(h.VALID_VISITOR_ID_REGEX)&&S._setField(T,a)))}},S._appendVersionTo=function(e){var t="vVersion|"+S.version,n=e?S._getCookieVersion(e):null;return n?u.areVersionsDifferent(n,S.version)&&(e=e.replace(h.VERSION_REGEX,t)):e+=(e?"|":"")+t,e},S._writeVisitor=function(){var e,t,n=S._getSettingsDigest();for(e in S._fields)N(e)&&S._fields[e]&&"expire"!==e.substring(0,6)&&(t=S._fields[e],n+=(n?"|":"")+e+(S._fields["expire"+e]?"-"+S._fields["expire"+e]:"")+"|"+t);n=S._appendVersionTo(n),S.cookieWrite(S.cookieName,n,1)},S._getField=function(e,t){return null==S._fields||!t&&S._fieldsExpired&&S._fieldsExpired[e]?null:S._fields[e]},S._setField=function(e,t,n){null==S._fields&&(S._fields={}),S._fields[e]=t,n||S._writeVisitor()},S._getFieldList=function(e,t){var n=S._getField(e,t);return n?n.split("*"):null},S._setFieldList=function(e,t,n){S._setField(e,t?t.join("*"):"",n)},S._getFieldMap=function(e,t){var n=S._getFieldList(e,t);if(n){var a,i={};for(a=0;a<n.length;a+=2)i[n[a]]=n[a+1];return i}return null},S._setFieldMap=function(e,t,n){var a,i=null;if(t)for(a in i=[],t)N(a)&&(i.push(a),i.push(t[a]));S._setFieldList(e,i,n)},S._setFieldExpire=function(e,t,n){var a=new Date;a.setTime(a.getTime()+1e3*t),null==S._fields&&(S._fields={}),S._fields["expire"+e]=Math.floor(a.getTime()/1e3)+(n?"s":""),t<0?(S._fieldsExpired||(S._fieldsExpired={}),S._fieldsExpired[e]=!0):S._fieldsExpired&&(S._fieldsExpired[e]=!1),n&&(S.cookieRead(S.sessionCookieName)||S.cookieWrite(S.sessionCookieName,"1"))},S._findVisitorID=function(e){return e&&("object"==typeof e&&(e=e.d_mid?e.d_mid:e.visitorID?e.visitorID:e.id?e.id:e.uuid?e.uuid:""+e),e&&"NOTARGET"===(e=e.toUpperCase())&&(e=F),e&&(e===F||e.match(h.VALID_VISITOR_ID_REGEX))||(e="")),e},S._setFields=function(e,t){if(S._clearTimeout(e),null!=S._loading&&(S._loading[e]=!1),O.fieldGroupObj[e]&&O.setState(e,!1),"MC"===e){!0!==O.isClientSideMarketingCloudVisitorID&&(O.isClientSideMarketingCloudVisitorID=!1);var n=S._getField(L);if(!n||S.overwriteCrossDomainMCIDAndAID){if(!(n="object"==typeof t&&t.mid?t.mid:S._findVisitorID(t))){if(S._use1stPartyMarketingCloudServer&&!S.tried1stPartyMarketingCloudServer)return S.tried1stPartyMarketingCloudServer=!0,void S.getAnalyticsVisitorID(null,!1,!0);n=S._generateLocalMID()}S._setField(L,n)}n&&n!==F||(n=""),"object"==typeof t&&((t.d_region||t.dcs_region||t.d_blob||t.blob)&&S._setFields(E,t),S._use1stPartyMarketingCloudServer&&t.mid&&S._setFields(P,{id:t.id})),S._callAllCallbacks(L,[n])}if(e===E&&"object"==typeof t){var a=604800;null!=t.id_sync_ttl&&t.id_sync_ttl&&(a=parseInt(t.id_sync_ttl,10));var i=x.getRegionAndCheckIfChanged(t,a);S._callAllCallbacks("MCAAMLH",[i]);var r=S._getField(M);(t.d_blob||t.blob)&&((r=t.d_blob)||(r=t.blob),S._setFieldExpire(M,a),S._setField(M,r)),r||(r=""),S._callAllCallbacks(M,[r]),!t.error_msg&&S._newCustomerIDsHash&&S._setField("MCCIDH",S._newCustomerIDsHash)}if(e===P){var o=S._getField(T);o&&!S.overwriteCrossDomainMCIDAndAID||((o=S._findVisitorID(t))?o!==F&&S._setFieldExpire(M,-1):o=F,S._setField(T,o)),o&&o!==F||(o=""),S._callAllCallbacks(T,[o])}if(S.idSyncDisableSyncs||S.disableIdSyncs)x.idCallNotProcesssed=!0;else{x.idCallNotProcesssed=!1;var s={};s.ibs=t.ibs,s.subdomain=t.subdomain,x.processIDCallData(s)}var c,l;t===Object(t)&&(S.isAllowed()&&(c=S._getField("MCOPTOUT")),c||(c=F,t.d_optout&&t.d_optout instanceof Array&&(c=t.d_optout.join(",")),l=parseInt(t.d_ottl,10),isNaN(l)&&(l=7200),S._setFieldExpire("MCOPTOUT",l,!0),S._setField("MCOPTOUT",c)),S._callAllCallbacks("MCOPTOUT",[c]))},S._loading=null,S._getRemoteField=function(e,t,n,a,i){var r,o="",s=A.isFirstPartyAnalyticsVisitorIDCall(e),c={MCAAMLH:!0,MCAAMB:!0};if(S.isAllowed())if(S._readVisitor(),!(!(o=S._getField(e,!0===c[e]))||S._fieldsExpired&&S._fieldsExpired[e])||S.disableThirdPartyCalls&&!s)o||(e===L?(S._registerCallback(e,n),o=S._generateLocalMID(),S.setMarketingCloudVisitorID(o)):e===T?(S._registerCallback(e,n),o="",S.setAnalyticsVisitorID(o)):(o="",a=!0));else if(e===L||"MCOPTOUT"===e?r="MC":"MCAAMLH"===e||e===M?r=E:e===T&&(r=P),r)return!t||null!=S._loading&&S._loading[r]||(null==S._loading&&(S._loading={}),S._loading[r]=!0,S._loadData(r,t,function(t){if(!S._getField(e)){t&&O.setState(r,!0);var n="";e===L?n=S._generateLocalMID():r===E&&(n={error_msg:"timeout"}),S._setFields(r,n)}},i)),S._registerCallback(e,n),o||(t||S._setFields(r,{id:F}),"");return e!==L&&e!==T||o!==F||(o="",a=!0),n&&a&&S._callCallback(n,[o]),o},S._setMarketingCloudFields=function(e){S._readVisitor(),S._setFields("MC",e)},S._mapCustomerIDs=function(e){S.getAudienceManagerBlob(e,!0)},S._setAnalyticsFields=function(e){S._readVisitor(),S._setFields(P,e)},S._setAudienceManagerFields=function(e){S._readVisitor(),S._setFields(E,e)},S._getAudienceManagerURLData=function(e){var t=S.audienceManagerServer,n="",a=S._getField(L),i=S._getField(M,!0),r=S._getField(T),o=r&&r!==F?"&d_cid_ic=AVID%01"+encodeURIComponent(r):"";if(S.loadSSL&&S.audienceManagerServerSecure&&(t=S.audienceManagerServerSecure),t){var s,c,l=S.getCustomerIDs();if(l)for(s in l)N(s)&&(c=l[s],o+="&d_cid_ic="+encodeURIComponent(s)+"%01"+encodeURIComponent(c.id?c.id:"")+(c.authState?"%01"+c.authState:""));e||(e="_setAudienceManagerFields");var u="http"+(S.loadSSL?"s":"")+"://"+t+"/id",d="d_visid_ver="+S.version+"&d_rtbd=json&d_ver=2"+(!a&&S._use1stPartyMarketingCloudServer?"&d_verify=1":"")+"&d_orgid="+encodeURIComponent(S.marketingCloudOrgID)+"&d_nsid="+(S.idSyncContainerID||0)+(a?"&d_mid="+encodeURIComponent(a):"")+(S.idSyncDisable3rdPartySyncing||S.disableThirdPartyCookies?"&d_coppa=true":"")+(!0===_?"&d_coop_safe=1":!1===_?"&d_coop_unsafe=1":"")+(i?"&d_blob="+encodeURIComponent(i):"")+o,g=["s_c_il",S._in,e];return{url:n=u+"?"+d+"&d_cb=s_c_il%5B"+S._in+"%5D."+e,corsUrl:u+"?"+d,callback:g}}return{url:n}},S.appendVisitorIDsTo=function(e){try{var t=[[L,S._getField(L)],[T,S._getField(T)],["MCORGID",S.marketingCloudOrgID]];return S._addQuerystringParam(e,h.ADOBE_MC,V(t))}catch(t){return e}},S.appendSupplementalDataIDTo=function(e,t){if(!(t=t||S.getSupplementalDataID(A.generateRandomString(),!0)))return e;try{var n=V([["SDID",t],["MCORGID",S.marketingCloudOrgID]]);return S._addQuerystringParam(e,h.ADOBE_MC_SDID,n)}catch(t){return e}};var A={parseHash:function(e){var t=e.indexOf("#");return t>0?e.substr(t):""},hashlessUrl:function(e){var t=e.indexOf("#");return t>0?e.substr(0,t):e},addQueryParamAtLocation:function(e,t,n){var a=e.split("&");return n=null!=n?n:a.length,a.splice(n,0,t),a.join("&")},isFirstPartyAnalyticsVisitorIDCall:function(e,t,n){return e===T&&(t||(t=S.trackingServer),n||(n=S.trackingServerSecure),!("string"!=typeof(a=S.loadSSL?n:t)||!a.length)&&a.indexOf("2o7.net")<0&&a.indexOf("omtrdc.net")<0);var a},isObject:function(e){return Boolean(e&&e===Object(e))},removeCookie:function(e){document.cookie=encodeURIComponent(e)+"=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;"},isTrackingServerPopulated:function(){return!!S.trackingServer||!!S.trackingServerSecure},getTimestampInSeconds:function(){return Math.round((new Date).getTime()/1e3)},parsePipeDelimetedKeyValues:function(e){return e.split("|").reduce(function(e,t){var n=t.split("=");return e[n[0]]=decodeURIComponent(n[1]),e},{})},generateRandomString:function(e){e=e||5;for(var t="",n="abcdefghijklmnopqrstuvwxyz0123456789";e--;)t+=n[Math.floor(Math.random()*n.length)];return t},parseBoolean:function(e){return"true"===e||"false"!==e&&null},replaceMethodsWithFunction:function(e,t){for(var n in e)e.hasOwnProperty(n)&&"function"==typeof e[n]&&(e[n]=t);return e},pluck:function(e,t){return t.reduce(function(t,n){return e[n]&&(t[n]=e[n]),t},Object.create(null))}};S._helpers=A;var x=f(S,I);S._destinationPublishing=x,S.timeoutMetricsLog=[];var B,O={isClientSideMarketingCloudVisitorID:null,MCIDCallTimedOut:null,AnalyticsIDCallTimedOut:null,AAMIDCallTimedOut:null,fieldGroupObj:{},setState:function(e,t){switch(e){case"MC":!1===t?!0!==this.MCIDCallTimedOut&&(this.MCIDCallTimedOut=!1):this.MCIDCallTimedOut=t;break;case P:!1===t?!0!==this.AnalyticsIDCallTimedOut&&(this.AnalyticsIDCallTimedOut=!1):this.AnalyticsIDCallTimedOut=t;break;case E:!1===t?!0!==this.AAMIDCallTimedOut&&(this.AAMIDCallTimedOut=!1):this.AAMIDCallTimedOut=t}}};S.isClientSideMarketingCloudVisitorID=function(){return O.isClientSideMarketingCloudVisitorID},S.MCIDCallTimedOut=function(){return O.MCIDCallTimedOut},S.AnalyticsIDCallTimedOut=function(){return O.AnalyticsIDCallTimedOut},S.AAMIDCallTimedOut=function(){return O.AAMIDCallTimedOut},S.idSyncGetOnPageSyncInfo=function(){return S._readVisitor(),S._getField("MCSYNCSOP")},S.idSyncByURL=function(e){var t=y(e||{});if(t.error)return t.error;var n,a,i=e.url,r=encodeURIComponent,o=x;return i=i.replace(/^https:/,"").replace(/^http:/,""),n=c.encodeAndBuildRequest(["",e.dpid,e.dpuuid||""],","),a=["ibs",r(e.dpid),"img",r(i),t.ttl,"",n],o.addMessage(a.join("|")),o.requestToProcess(),"Successfully queued"},S.idSyncByDataSource=function(e){return e===Object(e)&&"string"==typeof e.dpuuid&&e.dpuuid.length?(e.url="//dpm.demdex.net/ibs:dpid="+e.dpid+"&dpuuid="+e.dpuuid,S.idSyncByURL(e)):"Error: config or config.dpuuid is empty"},S._getCookieVersion=function(e){e=e||S.cookieRead(S.cookieName);var t=h.VERSION_REGEX.exec(e);return t&&t.length>1?t[1]:null},S._resetAmcvCookie=function(e){var t=S._getCookieVersion();t&&!u.isLessThan(t,e)||A.removeCookie(S.cookieName)},S.setAsCoopSafe=function(){_=!0},S.setAsCoopUnsafe=function(){_=!1},S.init=function(){!function(){if(t&&"object"==typeof t){for(var e in S.configs=Object.create(null),t)N(e)&&(S[e]=t[e],S.configs[e]=t[e]);S.idSyncContainerID=S.idSyncContainerID||0,_="boolean"==typeof S.isCoopSafe?S.isCoopSafe:A.parseBoolean(S.isCoopSafe),S.resetBeforeVersion&&S._resetAmcvCookie(S.resetBeforeVersion),S._attemptToPopulateIdsFromUrl(),S._attemptToPopulateSdidFromUrl(),S._readVisitor();var n=S._getField(D),a=Math.ceil((new Date).getTime()/h.MILLIS_PER_DAY);S.idSyncDisableSyncs||S.disableIdSyncs||!x.canMakeSyncIDCall(n,a)||(S._setFieldExpire(M,-1),S._setField(D,a)),S.getMarketingCloudVisitorID(),S.getAudienceManagerLocationHint(),S.getAudienceManagerBlob(),S._mergeServerState(S.serverState)}else S._attemptToPopulateIdsFromUrl(),S._attemptToPopulateSdidFromUrl()}(),function(){if(!S.idSyncDisableSyncs&&!S.disableIdSyncs){x.checkDPIframeSrc();var e=function(){var e=x;e.readyToAttachIframe()&&e.attachIframe()};C.addEventListener("load",function(){I.windowLoaded=!0,e()});try{d.receiveMessage(function(e){x.receiveMessage(e.data)},x.iframeHost)}catch(e){}}}(),S.whitelistIframeDomains&&h.POST_MESSAGE_ENABLED&&(S.whitelistIframeDomains=S.whitelistIframeDomains instanceof Array?S.whitelistIframeDomains:[S.whitelistIframeDomains],S.whitelistIframeDomains.forEach(function(t){var n=new i(e,t),a=r(S,n);d.receiveMessage(a,t)}))}};p.getInstance=function(e,t){if(!e)throw new Error("Visitor requires Adobe Marketing Cloud Org ID.");e.indexOf("@")<0&&(e+="@AdobeOrg");var i=function(){var t=n.s_c_il;if(t)for(var a=0;a<t.length;a++){var i=t[a];if(i&&"Visitor"===i._c&&i.marketingCloudOrgID===e)return i}}();if(i)return i;var r=e.split("").reverse().join(""),o=new p(e,null,r);n.s_c_il.splice(--n.s_c_in,1);var s=c.getIeVersion();if("number"==typeof s&&s<10)return o._helpers.replaceMethodsWithFunction(o,function(){});var l=o.isAllowed(),u=function(){try{return n.self!==n.parent}catch(e){return!0}}()&&!l&&n.parent?new a(e,t,o,n.parent):new p(e,t,r);return o=null,u.init(),u},function(){function e(){p.windowLoaded=!0}n.addEventListener?n.addEventListener("load",e):n.attachEvent&&n.attachEvent("onload",e),p.codeLoadEnd=(new Date).getTime()}(),n.Visitor=p,t.exports=p}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"./child/ChildVisitor":2,"./child/Message":3,"./child/makeChildMessageListener":4,"./units/crossDomain":8,"./units/makeCorsRequest":9,"./units/makeDestinationPublishing":10,"./units/version":11,"./utils/asyncParallelApply":12,"./utils/constants":14,"./utils/enums":15,"./utils/getDomain":16,"./utils/utils":18,"@adobe-mcid/visitor-js-shared/lib/ids/generateRandomID":19}],2:[function(e,t){(function(n){e("../utils/polyfills");var a=e("./strategies/LocalVisitor"),i=e("./strategies/ProxyVisitor"),r=e("./strategies/PlaceholderVisitor"),o=e("../utils/callbackRegistryFactory"),s=e("./Message"),c=e("../utils/enums").MESSAGES;t.exports=function(e,t,l,u){function d(e){Object.assign(y,e)}function g(e){Object.assign(y.state,e),y.callbackRegistry.executeAll(y.state)}function m(e){if(!I.isInvalid(e)){C=!1;var t=I.parse(e);y.setStateAndPublish(t.state)}}function f(e){!C&&S&&(C=!0,I.send(u,e))}function h(){d(new a(l._generateID)),y.getMarketingCloudVisitorID(),y.callbackRegistry.executeAll(y.state,!0),n.removeEventListener("message",p)}function p(e){if(!I.isInvalid(e)){var t=I.parse(e);C=!1,n.clearTimeout(this.timeout),n.removeEventListener("message",p),d(new i(y)),n.addEventListener("message",m),y.setStateAndPublish(t.state),y.callbackRegistry.hasCallbacks()&&f(c.GETSTATE)}}function v(){S&&postMessage?(n.addEventListener("message",p),f(c.HANDSHAKE),this.timeout=setTimeout(h,250)):h()}function b(){n.s_c_in||(n.s_c_il=[],n.s_c_in=0),y._c="Visitor",y._il=n.s_c_il,y._in=n.s_c_in,y._il[y._in]=y,n.s_c_in++}function V(){function e(e){0!==e.indexOf("_")&&"function"==typeof l[e]&&(y[e]=function(){})}Object.keys(l).forEach(e),y.getSupplementalDataID=l.getSupplementalDataID}var y=this,S=t.whitelistParentDomain;y.state={},y.version=l.version,y.marketingCloudOrgID=e;var C=!1,I=new s(e,S);y.callbackRegistry=o(),y.init=function(){b(),V(),d(new r(y)),v()},y.findField=function(e,t){if(y.state[e])return t(y.state[e]),y.state[e]},y.messageParent=f,y.setStateAndPublish=g}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"../utils/callbackRegistryFactory":13,"../utils/enums":15,"../utils/polyfills":17,"./Message":3,"./strategies/LocalVisitor":5,"./strategies/PlaceholderVisitor":6,"./strategies/ProxyVisitor":7}],3:[function(e,t){var n=e("../utils/enums").MESSAGES,a={0:"prefix",1:"orgID",2:"state"};t.exports=function(e,t){this.parse=function(e){try{var t={};return e.data.split("|").forEach(function(e,n){void 0!==e&&(t[a[n]]=2!==n?e:JSON.parse(e))}),t}catch(e){}},this.isInvalid=function(a){var i=this.parse(a);if(!i||Object.keys(i).length<2)return!0;var r=e!==i.orgID,o=!t||a.origin!==t,s=-1===Object.keys(n).indexOf(i.prefix);return r||o||s},this.send=function(n,a,i){var r=a+"|"+e;i&&i===Object(i)&&(r+="|"+JSON.stringify(i));try{n.postMessage(r,t)}catch(e){}}}},{"../utils/enums":15}],4:[function(e,t){var n=e("../utils/enums"),a=e("../utils/utils"),i=n.MESSAGES,r=n.ALL_APIS,o=n.ASYNC_API_MAP,s=n.FIELDGROUP_TO_FIELD;t.exports=function(e,t){function n(){var t={};return Object.keys(r).forEach(function(n){var i=r[n],o=e[i]();a.isValueEmpty(o)||(t[n]=o)}),t}function c(){var t=[];return e._loading&&Object.keys(e._loading).forEach(function(n){if(e._loading[n]){var a=s[n];t.push(a)}}),t.length?t:null}function l(t){return function n(){var a=c();if(a){var i=o[a[0]];e[i](n,!0)}else t()}}function u(e,a){var i=n();t.send(e,a,i)}function d(e){m(e),u(e,i.HANDSHAKE)}function g(e){l(function(){u(e,i.PARENTSTATE)})()}function m(n){function a(a){r.call(e,a),t.send(n,i.PARENTSTATE,{CUSTOMERIDS:e.getCustomerIDs()})}var r=e.setCustomerIDs;e.setCustomerIDs=a}return function(e){t.isInvalid(e)||(t.parse(e).prefix===i.HANDSHAKE?d:g)(e.source)}}},{"../utils/enums":15,"../utils/utils":18}],5:[function(e,t){var n=e("../../utils/enums").STATE_KEYS_MAP;t.exports=function(e){function t(){}function a(t,a){var i=this;return function(){var t=e(0,n.MCMID),r={};return r[n.MCMID]=t,i.setStateAndPublish(r),a(t),t}}this.getMarketingCloudVisitorID=function(e){e=e||t;var i=this.findField(n.MCMID,e),r=a.call(this,n.MCMID,e);return void 0!==i?i:r()}}},{"../../utils/enums":15}],6:[function(e,t){var n=e("../../utils/enums").ASYNC_API_MAP;t.exports=function(){Object.keys(n).forEach(function(e){this[n[e]]=function(t){this.callbackRegistry.add(e,t)}},this)}},{"../../utils/enums":15}],7:[function(e,t){var n=e("../../utils/enums"),a=n.MESSAGES,i=n.ASYNC_API_MAP,r=n.SYNC_API_MAP;t.exports=function(){function e(){}function t(e,t){var n=this;return function(){return n.callbackRegistry.add(e,t),n.messageParent(a.GETSTATE),""}}function n(n){this[i[n]]=function(a){a=a||e;var i=this.findField(n,a),r=t.call(this,n,a);return void 0!==i?i:r()}}function o(t){this[r[t]]=function(){return this.findField(t,e)||{}}}Object.keys(i).forEach(n,this),Object.keys(r).forEach(o,this)}},{"../../utils/enums":15}],8:[function(e,t){(function(e){var n=!!e.postMessage;t.exports={postMessage:function(e,t,a){var i=1;t&&(n?a.postMessage(e,t.replace(/([^:]+:\/\/[^\/]+).*/,"$1")):t&&(a.location=t.replace(/#.*$/,"")+"#"+ +new Date+i+++"&"+e))},receiveMessage:function(t,a){var i;try{n&&(t&&(i=function(e){if("string"==typeof a&&e.origin!==a||"[object Function]"===Object.prototype.toString.call(a)&&!1===a(e.origin))return!1;t(e)}),e.addEventListener?e[t?"addEventListener":"removeEventListener"]("message",i):e[t?"attachEvent":"detachEvent"]("onmessage",i))}catch(e){}}}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],9:[function(e,t){(function(e){t.exports=function(t){return{corsMetadata:(n="none",a=!0,"undefined"!=typeof XMLHttpRequest&&XMLHttpRequest===Object(XMLHttpRequest)&&("withCredentials"in new XMLHttpRequest?n="XMLHttpRequest":"undefined"!=typeof XDomainRequest&&XDomainRequest===Object(XDomainRequest)&&(a=!1),Object.prototype.toString.call(e.HTMLElement).indexOf("Constructor")>0&&(a=!1)),{corsType:n,corsCookiesEnabled:a}),getCORSInstance:function(){return"none"===this.corsMetadata.corsType?null:new e[this.corsMetadata.corsType]},fireCORS:function(n,a){function i(t){var a;try{if((a=JSON.parse(t))!==Object(a))return void r.handleCORSError(n,null,"Response is not JSON")}catch(e){return void r.handleCORSError(n,e,"Error parsing response as JSON")}try{for(var i=n.callback,o=e,s=0;s<i.length;s++)o=o[i[s]];o(a)}catch(e){r.handleCORSError(n,e,"Error forming callback function")}}var r=this;a&&(n.loadErrorHandler=a);try{var o=this.getCORSInstance();o.open("get",n.corsUrl+"&ts="+(new Date).getTime(),!0),"XMLHttpRequest"===this.corsMetadata.corsType&&(o.withCredentials=!0,o.timeout=t.loadTimeout,o.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),o.onreadystatechange=function(){4===this.readyState&&200===this.status&&i(this.responseText)}),o.onerror=function(e){r.handleCORSError(n,e,"onerror")},o.ontimeout=function(e){r.handleCORSError(n,e,"ontimeout")},o.send(),t._log.requests.push(n.corsUrl)}catch(e){this.handleCORSError(n,e,"try-catch")}},handleCORSError:function(e,n,a){t.CORSErrors.push({corsData:e,error:n,description:a}),e.loadErrorHandler&&("ontimeout"===a?e.loadErrorHandler(!0):e.loadErrorHandler(!1))}};var n,a}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],10:[function(e,t){(function(n){var a=e("../utils/constants"),i=e("./crossDomain"),r=e("../utils/utils");t.exports=function(e,t){var o=n.document;return{THROTTLE_START:3e4,MAX_SYNCS_LENGTH:649,throttleTimerSet:!1,id:null,onPagePixels:[],iframeHost:null,getIframeHost:function(e){if("string"==typeof e){var t=e.split("/");return t[0]+"//"+t[2]}},subdomain:null,url:null,getUrl:function(){var t,n="http://fast.",a="?d_nsid="+e.idSyncContainerID+"#"+encodeURIComponent(o.location.href);return this.subdomain||(this.subdomain="nosubdomainreturned"),e.loadSSL&&(n=e.idSyncSSLUseAkamai?"https://fast.":"https://"),t=n+this.subdomain+".demdex.net/dest5.html"+a,this.iframeHost=this.getIframeHost(t),this.id="destination_publishing_iframe_"+this.subdomain+"_"+e.idSyncContainerID,t},checkDPIframeSrc:function(){var t="?d_nsid="+e.idSyncContainerID+"#"+encodeURIComponent(o.location.href);"string"==typeof e.dpIframeSrc&&e.dpIframeSrc.length&&(this.id="destination_publishing_iframe_"+(e._subdomain||this.subdomain||(new Date).getTime())+"_"+e.idSyncContainerID,this.iframeHost=this.getIframeHost(e.dpIframeSrc),this.url=e.dpIframeSrc+t)},idCallNotProcesssed:null,doAttachIframe:!1,startedAttachingIframe:!1,iframeHasLoaded:null,iframeIdChanged:null,newIframeCreated:null,originalIframeHasLoadedAlready:null,regionChanged:!1,timesRegionChanged:0,sendingMessages:!1,messages:[],messagesPosted:[],messagesReceived:[],messageSendingInterval:a.POST_MESSAGE_ENABLED?null:100,jsonForComparison:[],jsonDuplicates:[],jsonWaiting:[],jsonProcessed:[],canSetThirdPartyCookies:!0,receivedThirdPartyCookiesNotification:!1,readyToAttachIframe:function(){
return!e.idSyncDisable3rdPartySyncing&&(this.doAttachIframe||e._doAttachIframe)&&(this.subdomain&&"nosubdomainreturned"!==this.subdomain||e._subdomain)&&this.url&&!this.startedAttachingIframe},attachIframe:function(){function e(){(a=o.createElement("iframe")).sandbox="allow-scripts allow-same-origin",a.title="Adobe ID Syncing iFrame",a.id=n.id,a.name=n.id+"_name",a.style.cssText="display: none; width: 0; height: 0;",a.src=n.url,n.newIframeCreated=!0,t(),o.body.appendChild(a)}function t(){a.addEventListener("load",function(){a.className="aamIframeLoaded",n.iframeHasLoaded=!0,n.requestToProcess()})}this.startedAttachingIframe=!0;var n=this,a=o.getElementById(this.id);a?"IFRAME"!==a.nodeName?(this.id+="_2",this.iframeIdChanged=!0,e()):(this.newIframeCreated=!1,"aamIframeLoaded"!==a.className?(this.originalIframeHasLoadedAlready=!1,t()):(this.originalIframeHasLoadedAlready=!0,this.iframeHasLoaded=!0,this.iframe=a,this.requestToProcess())):e(),this.iframe=a},requestToProcess:function(t){function n(){r.jsonForComparison.push(t),r.jsonWaiting.push(t),r.processSyncOnPage(t)}var i,r=this;if(t===Object(t)&&t.ibs)if(i=JSON.stringify(t.ibs||[]),this.jsonForComparison.length){var o,s,c,l=!1;for(o=0,s=this.jsonForComparison.length;o<s;o++)if(c=this.jsonForComparison[o],i===JSON.stringify(c.ibs||[])){l=!0;break}l?this.jsonDuplicates.push(t):n()}else n();if((this.receivedThirdPartyCookiesNotification||!a.POST_MESSAGE_ENABLED||this.iframeHasLoaded)&&this.jsonWaiting.length){var u=this.jsonWaiting.shift();this.process(u),this.requestToProcess()}!e.idSyncDisableSyncs&&this.iframeHasLoaded&&this.messages.length&&!this.sendingMessages&&(this.throttleTimerSet||(this.throttleTimerSet=!0,setTimeout(function(){r.messageSendingInterval=a.POST_MESSAGE_ENABLED?null:150},this.THROTTLE_START)),this.sendingMessages=!0,this.sendMessages())},getRegionAndCheckIfChanged:function(t,n){var a=e._getField("MCAAMLH"),i=t.d_region||t.dcs_region;return a?i&&(e._setFieldExpire("MCAAMLH",n),e._setField("MCAAMLH",i),parseInt(a,10)!==i&&(this.regionChanged=!0,this.timesRegionChanged++,e._setField("MCSYNCSOP",""),e._setField("MCSYNCS",""),a=i)):(a=i)&&(e._setFieldExpire("MCAAMLH",n),e._setField("MCAAMLH",a)),a||(a=""),a},processSyncOnPage:function(e){var t,n,a,i;if((t=e.ibs)&&t instanceof Array&&(n=t.length))for(a=0;a<n;a++)(i=t[a]).syncOnPage&&this.checkFirstPartyCookie(i,"","syncOnPage")},process:function(e){var t,n,a,i,o,s=encodeURIComponent,c=!1;if((t=e.ibs)&&t instanceof Array&&(n=t.length))for(c=!0,a=0;a<n;a++)i=t[a],o=[s("ibs"),s(i.id||""),s(i.tag||""),r.encodeAndBuildRequest(i.url||[],","),s(i.ttl||""),"","",i.fireURLSync?"true":"false"],i.syncOnPage||(this.canSetThirdPartyCookies?this.addMessage(o.join("|")):i.fireURLSync&&this.checkFirstPartyCookie(i,o.join("|")));c&&this.jsonProcessed.push(e)},checkFirstPartyCookie:function(t,n,i){var r="syncOnPage"===i,o=r?"MCSYNCSOP":"MCSYNCS";e._readVisitor();var s,c,l=e._getField(o),u=!1,d=!1,g=Math.ceil((new Date).getTime()/a.MILLIS_PER_DAY);l?(s=l.split("*"),u=(c=this.pruneSyncData(s,t.id,g)).dataPresent,d=c.dataValid,u&&d||this.fireSync(r,t,n,s,o,g)):(s=[],this.fireSync(r,t,n,s,o,g))},pruneSyncData:function(e,t,n){var a,i,r,o=!1,s=!1;for(i=0;i<e.length;i++)a=e[i],r=parseInt(a.split("-")[1],10),a.match("^"+t+"-")?(o=!0,n<r?s=!0:(e.splice(i,1),i--)):n>=r&&(e.splice(i,1),i--);return{dataPresent:o,dataValid:s}},manageSyncsSize:function(e){if(e.join("*").length>this.MAX_SYNCS_LENGTH)for(e.sort(function(e,t){return parseInt(e.split("-")[1],10)-parseInt(t.split("-")[1],10)});e.join("*").length>this.MAX_SYNCS_LENGTH;)e.shift()},fireSync:function(t,n,a,i,r,o){var s=this;if(t){if("img"===n.tag){var c,l,u,d,g=n.url,m=e.loadSSL?"https:":"http:";for(c=0,l=g.length;c<l;c++){u=g[c],d=/^\/\//.test(u);var f=new Image;f.addEventListener("load",function(t,n,a,i){return function(){s.onPagePixels[t]=null,e._readVisitor();var o,c,l,u,d=e._getField(r),g=[];if(d)for(c=0,l=(o=d.split("*")).length;c<l;c++)(u=o[c]).match("^"+n.id+"-")||g.push(u);s.setSyncTrackingData(g,n,a,i)}}(this.onPagePixels.length,n,r,o)),f.src=(d?m:"")+u,this.onPagePixels.push(f)}}}else this.addMessage(a),this.setSyncTrackingData(i,n,r,o)},addMessage:function(t){var n=encodeURIComponent(e._enableErrorReporting?"---destpub-debug---":"---destpub---");this.messages.push((a.POST_MESSAGE_ENABLED?"":n)+t)},setSyncTrackingData:function(t,n,a,i){t.push(n.id+"-"+(i+Math.ceil(n.ttl/60/24))),this.manageSyncsSize(t),e._setField(a,t.join("*"))},sendMessages:function(){var e,t=this,n="",i=encodeURIComponent;this.regionChanged&&(n=i("---destpub-clear-dextp---"),this.regionChanged=!1),this.messages.length?a.POST_MESSAGE_ENABLED?(e=n+i("---destpub-combined---")+this.messages.join("%01"),this.postMessage(e),this.messages=[],this.sendingMessages=!1):(e=this.messages.shift(),this.postMessage(n+e),setTimeout(function(){t.sendMessages()},this.messageSendingInterval)):this.sendingMessages=!1},postMessage:function(e){i.postMessage(e,this.url,this.iframe.contentWindow),this.messagesPosted.push(e)},receiveMessage:function(e){var t,n=/^---destpub-to-parent---/;"string"==typeof e&&n.test(e)&&("canSetThirdPartyCookies"===(t=e.replace(n,"").split("|"))[0]&&(this.canSetThirdPartyCookies="true"===t[1],this.receivedThirdPartyCookiesNotification=!0,this.requestToProcess()),this.messagesReceived.push(e))},processIDCallData:function(n){(null==this.url||n.subdomain&&"nosubdomainreturned"===this.subdomain)&&("string"==typeof e._subdomain&&e._subdomain.length?this.subdomain=e._subdomain:this.subdomain=n.subdomain||"",this.url=this.getUrl()),n.ibs instanceof Array&&n.ibs.length&&(this.doAttachIframe=!0),this.readyToAttachIframe()&&(e.idSyncAttachIframeOnWindowLoad?(t.windowLoaded||"complete"===o.readyState||"loaded"===o.readyState)&&this.attachIframe():this.attachIframeASAP()),"function"==typeof e.idSyncIDCallResult?e.idSyncIDCallResult(n):this.requestToProcess(n),"function"==typeof e.idSyncAfterIDCallResult&&e.idSyncAfterIDCallResult(n)},canMakeSyncIDCall:function(t,n){return e._forceSyncIDCall||!t||n-t>a.DAYS_BETWEEN_SYNC_ID_CALLS},attachIframeASAP:function(){function e(){t.startedAttachingIframe||(o.body?t.attachIframe():setTimeout(e,30))}var t=this;e()}}}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"../utils/constants":14,"../utils/utils":18,"./crossDomain":8}],11:[function(e,t){function n(e){for(var t=/^\d+$/,n=0,a=e.length;n<a;n++)if(!t.test(e[n]))return!1;return!0}function a(e,t){for(;e.length<t.length;)e.push("0");for(;t.length<e.length;)t.push("0")}function i(e,t){for(var n=0;n<e.length;n++){var a=parseInt(e[n],10),i=parseInt(t[n],10);if(a>i)return 1;if(i>a)return-1}return 0}function r(e,t){if(e===t)return 0;var r=e.toString().split("."),o=t.toString().split(".");return n(r.concat(o))?(a(r,o),i(r,o)):NaN}t.exports={compare:r,isLessThan:function(e,t){return r(e,t)<0},areVersionsDifferent:function(e,t){return 0!==r(e,t)},isGreaterThan:function(e,t){return r(e,t)>0},isEqual:function(e,t){return 0===r(e,t)}}},{}],12:[function(e,t){t.exports=function(e,t){function n(e){return function(n){a[e]=n,++i===r&&t(a)}}var a={},i=0,r=Object.keys(e).length;Object.keys(e).forEach(function(t){var a=e[t];if(a.fn){var i=a.args||[];i.unshift(n(t)),a.fn.apply(a.context||null,i)}})}},{}],13:[function(e,t){function n(){return{callbacks:{},add:function(e,t){this.callbacks[e]=this.callbacks[e]||[];var n=this.callbacks[e].push(t)-1;return function(){this.callbacks[e].splice(n,1)}},execute:function(e,t){if(this.callbacks[e]){t=(t=void 0===t?[]:t)instanceof Array?t:[t];try{for(;this.callbacks[e].length;){var n=this.callbacks[e].shift();"function"==typeof n?n.apply(null,t):n instanceof Array&&n[1].apply(n[0],t)}delete this.callbacks[e]}catch(e){}}},executeAll:function(e,t){(t||e&&!a.isObjectEmpty(e))&&Object.keys(this.callbacks).forEach(function(t){var n=void 0!==e[t]?e[t]:"";this.execute(t,n)},this)},hasCallbacks:function(){return Boolean(Object.keys(this.callbacks).length)}}}var a=e("./utils");t.exports=n},{"./utils":18}],14:[function(e,t){(function(e){t.exports={POST_MESSAGE_ENABLED:!!e.postMessage,DAYS_BETWEEN_SYNC_ID_CALLS:1,MILLIS_PER_DAY:864e5,ADOBE_MC:"adobe_mc",ADOBE_MC_SDID:"adobe_mc_sdid",VALID_VISITOR_ID_REGEX:/^[0-9a-fA-F\-]+$/,ADOBE_MC_TTL_IN_MIN:5,VERSION_REGEX:/vVersion\|((\d+\.)?(\d+\.)?(\*|\d+))(?=$|\|)/}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],15:[function(e,t,n){n.MESSAGES={HANDSHAKE:"HANDSHAKE",GETSTATE:"GETSTATE",PARENTSTATE:"PARENTSTATE"},n.STATE_KEYS_MAP={MCMID:"MCMID",MCAID:"MCAID",MCAAMB:"MCAAMB",MCAAMLH:"MCAAMLH",MCOPTOUT:"MCOPTOUT",CUSTOMERIDS:"CUSTOMERIDS"},n.ASYNC_API_MAP={MCMID:"getMarketingCloudVisitorID",MCAID:"getAnalyticsVisitorID",MCAAMB:"getAudienceManagerBlob",MCAAMLH:"getAudienceManagerLocationHint",MCOPTOUT:"getOptOut"},n.SYNC_API_MAP={CUSTOMERIDS:"getCustomerIDs"},n.ALL_APIS={MCMID:"getMarketingCloudVisitorID",MCAAMB:"getAudienceManagerBlob",MCAAMLH:"getAudienceManagerLocationHint",MCOPTOUT:"getOptOut",MCAID:"getAnalyticsVisitorID",CUSTOMERIDS:"getCustomerIDs"},n.FIELDGROUP_TO_FIELD={MC:"MCMID",A:"MCAID",AAM:"MCAAMB"},n.FIELDS={MCMID:"MCMID",MCOPTOUT:"MCOPTOUT",MCAID:"MCAID",MCAAMLH:"MCAAMLH",MCAAMB:"MCAAMB"},n.AUTH_STATE={UNKNOWN:0,AUTHENTICATED:1,LOGGED_OUT:2},n.OPT_OUT={GLOBAL:"global"}},{}],16:[function(e,t){(function(e){t.exports=function(t){var n;if(!t&&e.location&&(t=e.location.hostname),n=t)if(/^[0-9.]+$/.test(n))n="";else{var a=",ac,ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,ax,az,ba,bb,be,bf,bg,bh,bi,bj,bm,bo,br,bs,bt,bv,bw,by,bz,ca,cc,cd,cf,cg,ch,ci,cl,cm,cn,co,cr,cu,cv,cw,cx,cz,de,dj,dk,dm,do,dz,ec,ee,eg,es,et,eu,fi,fm,fo,fr,ga,gb,gd,ge,gf,gg,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gw,gy,hk,hm,hn,hr,ht,hu,id,ie,im,in,io,iq,ir,is,it,je,jo,jp,kg,ki,km,kn,kp,kr,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,me,mg,mh,mk,ml,mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,na,nc,ne,nf,ng,nl,no,nr,nu,nz,om,pa,pe,pf,ph,pk,pl,pm,pn,pr,ps,pt,pw,py,qa,re,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,st,su,sv,sx,sy,sz,tc,td,tf,tg,th,tj,tk,tl,tm,tn,to,tp,tr,tt,tv,tw,tz,ua,ug,uk,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,yt,",i=n.split("."),r=i.length-1,o=r-1;if(r>1&&i[r].length<=2&&(2===i[r-1].length||a.indexOf(","+i[r]+",")<0)&&o--,o>0)for(n="";r>=o;)n=i[r]+(n?".":"")+n,r--}return n}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],17:[function(){Object.assign=Object.assign||function(e){for(var t,n,a=1;a<arguments.length;++a)for(t in n=arguments[a])Object.prototype.hasOwnProperty.call(n,t)&&(e[t]=n[t]);return e}},{}],18:[function(e,t,n){n.isObjectEmpty=function(e){return e===Object(e)&&0===Object.keys(e).length},n.isValueEmpty=function(e){return""===e||n.isObjectEmpty(e)},n.getIeVersion=function(){if(document.documentMode)return document.documentMode;for(var e=7;e>4;e--){var t=document.createElement("div");if(t.innerHTML="<!--[if IE "+e+"]><span></span><![endif]-->",t.getElementsByTagName("span").length)return t=null,e;t=null}return null},n.encodeAndBuildRequest=function(e,t){return e.map(encodeURIComponent).join(t)}},{}],19:[function(e,t){t.exports=function(e){var t,n,a="0123456789",i="",r="",o=8,s=10,c=10;if(1==e){for(a+="ABCDEF",t=0;16>t;t++)n=Math.floor(Math.random()*o),i+=a.substring(n,n+1),n=Math.floor(Math.random()*o),r+=a.substring(n,n+1),o=16;return i+"-"+r}for(t=0;19>t;t++)n=Math.floor(Math.random()*s),i+=a.substring(n,n+1),0===t&&9==n?s=3:(1==t||2==t)&&10!=s&&2>n?s=10:2<t&&(s=10),n=Math.floor(Math.random()*c),r+=a.substring(n,n+1),0===t&&9==n?c=3:(1==t||2==t)&&10!=c&&2>n?c=10:2<t&&(c=10);return i+r}},{}]},{},[1]),
// All code and conventions are protected by copyright
function(e,t,n){function a(){D.addEventHandler(e,"orientationchange",a.orientationChange)}function i(t){t=t||D.rules,this.rules=D.filter(t,function(e){return"inview"===e.event}),this.elements=[],this.eventHandler=D.bind(this.track,this),D.addEventHandler(e,"scroll",this.eventHandler),D.addEventHandler(e,"load",this.eventHandler)}function r(){this.rules=D.filter(D.rules,function(e){return"videoplayed"===e.event.substring(0,11)}),this.eventHandler=D.bind(this.onUpdateTime,this)}function o(){D.getToolsByType("nielsen").length>0&&D.domReady(D.bind(this.initialize,this))}function s(e){this.delay=250,this.FB=e,D.domReady(D.bind(function(){D.poll(D.bind(this.initialize,this),this.delay,8)},this))}function c(){this.defineEvents(),this.visibilityApiHasPriority=!0,t.addEventListener?this.setVisibilityApiPriority(!1):this.attachDetachOlderEventListeners(!0,t,"focusout");D.bindEvent("aftertoolinit",function(){D.fireEvent(D.visibility.isHidden()?"tabblur":"tabfocus")})}function l(){this.lastURL=D.URL(),this._fireIfURIChanged=D.bind(this.fireIfURIChanged,this),this._onPopState=D.bind(this.onPopState,this),this._onHashChange=D.bind(this.onHashChange,this),this._pushState=D.bind(this.pushState,this),this._replaceState=D.bind(this.replaceState,this),this.initialize()}function u(){var e=D.filter(D.rules,function(e){return 0===e.event.indexOf("dataelementchange")});this.dataElementsNames=D.map(e,function(e){return e.event.match(/dataelementchange\((.*)\)/i)[1]},this),this.initPolling()}function d(){this.rules=D.filter(D.rules,function(e){return"elementexists"===e.event})}function g(){var e=this.eventRegex=/^hover\(([0-9]+)\)$/,t=this.rules=[];D.each(D.rules,function(n){n.event.match(e)&&t.push([Number(n.event.match(e)[1]),n.selector])})}function m(t){D.domReady(D.bind(function(){this.twttr=t||e.twttr,this.initialize()},this))}function f(e){D.BaseTool.call(this,e),this.styleElements={},this.targetPageParamsStore={}}function h(e){D.BaseTool.call(this,e),this.defineListeners(),this.beaconMethod="plainBeacon",this.adapt=new h.DataAdapters,this.dataProvider=new h.DataProvider.Aggregate}function p(e){D.BaseTool.call(this,e),this.varBindings={},this.events=[],this.products=[],this.customSetupFuns=[]}function v(){D.BaseTool.call(this),this.asyncScriptCallbackQueue=[],this.argsForBlockingScripts=[]}function b(e){D.BaseTool.call(this,e),this.name=e.name||"Basic"}function V(e){D.BaseTool.call(this,e),this.name=e.name||"VisitorID",this.initialize()}function y(e){D.BaseTool.call(this,e)}function S(e){D.BaseTool.call(this,e)}var C,I,k,_=Object.prototype.toString,L=e._satellite&&e._satellite.override,D={initialized:!1,$data:function(e,t,a){if(e){var i="__satellite__",r=D.dataCache,o=e[i];o||(o=e[i]=D.uuid++);var s=r[o];if(s||(s=r[o]={}),a===n)return s[t];s[t]=a}},uuid:1,dataCache:{},keys:function(e){var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(n);return t},values:function(e){var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(e[n]);return t},isArray:Array.isArray||function(e){return"[object Array]"===_.apply(e)},isObject:function(e){return null!=e&&!D.isArray(e)&&"object"==typeof e},isString:function(e){return"string"==typeof e},isNumber:function(e){return"[object Number]"===_.apply(e)&&!D.isNaN(e)},isNaN:function(e){return e!=e},isRegex:function(e){return e instanceof RegExp},isLinkTag:function(e){return!(!e||!e.nodeName||"a"!==e.nodeName.toLowerCase())},each:function(e,t,n){for(var a=0,i=e.length;a<i;a++)t.call(n,e[a],a,e)},map:function(e,t,n){for(var a=[],i=0,r=e.length;i<r;i++)a.push(t.call(n,e[i],i,e));return a},filter:function(e,t,n){for(var a=[],i=0,r=e.length;i<r;i++){var o=e[i];t.call(n,o,i,e)&&a.push(o)}return a},any:function(e,t,n){for(var a=0,i=e.length;a<i;a++){var r=e[a];if(t.call(n,r,a,e))return!0}return!1},every:function(e,t,n){for(var a=!0,i=0,r=e.length;i<r;i++){var o=e[i];a=a&&t.call(n,o,i,e)}return a},contains:function(e,t){return-1!==D.indexOf(e,t)},indexOf:function(e,t){if(e.indexOf)return e.indexOf(t);for(var n=e.length;n--;)if(t===e[n])return n;return-1},find:function(e,t,n){if(!e)return null;for(var a=0,i=e.length;a<i;a++){var r=e[a];if(t.call(n,r,a,e))return r}return null},textMatch:function(e,t){if(null==t)throw new Error("Illegal Argument: Pattern is not present");return null!=e&&("string"==typeof t?e===t:t instanceof RegExp&&t.test(e))},stringify:function(e,t){if(t=t||[],D.isObject(e)){if(D.contains(t,e))return"<Cycle>";t.push(e)}if(D.isArray(e))return"["+D.map(e,function(e){return D.stringify(e,t)}).join(",")+"]";if(D.isString(e))return'"'+String(e)+'"';if(D.isObject(e)){var n=[];for(var a in e)e.hasOwnProperty(a)&&n.push(a+": "+D.stringify(e[a],t));return"{"+n.join(", ")+"}"}return String(e)},trim:function(e){return null==e?null:e.trim?e.trim():e.replace(/^ */,"").replace(/ *$/,"")},bind:function(e,t){return function(){return e.apply(t,arguments)}},throttle:function(e,t){var n=null;return function(){var a=this,i=arguments;clearTimeout(n),n=setTimeout(function(){e.apply(a,i)},t)}},domReady:function(e){function n(e){for(g=1;e=i.shift();)e()}var a,i=[],r=!1,o=t,s=o.documentElement,c=s.doScroll,l="DOMContentLoaded",u="addEventListener",d="onreadystatechange",g=/^loade|^c/.test(o.readyState);return o[u]&&o[u](l,a=function(){o.removeEventListener(l,a,r),n()},r),c&&o.attachEvent(d,a=function(){/^c/.test(o.readyState)&&(o.detachEvent(d,a),n())}),e=c?function(t){self!=top?g?t():i.push(t):function(){try{s.doScroll("left")}catch(n){return setTimeout(function(){e(t)},50)}t()}()}:function(e){g?e():i.push(e)}}(),loadScript:function(e,n){var a=t.createElement("script");D.scriptOnLoad(e,a,n),a.src=e,t.getElementsByTagName("head")[0].appendChild(a)},scriptOnLoad:function(e,t,n){function a(e){e&&D.logError(e),n&&n(e)}"onload"in t?(t.onload=function(){a()},t.onerror=function(){a(new Error("Failed to load script "+e))}):"readyState"in t&&(t.onreadystatechange=function(){var e=t.readyState;"loaded"!==e&&"complete"!==e||(t.onreadystatechange=null,a())})},loadScriptOnce:function(e,t){D.loadedScriptRegistry[e]||D.loadScript(e,function(n){n||(D.loadedScriptRegistry[e]=!0),t&&t(n)})},loadedScriptRegistry:{},loadScriptSync:function(e){t.write?D.domReadyFired?D.notify('Cannot load sync the "'+e+'" script after DOM Ready.',1):(e.indexOf('"')>-1&&(e=encodeURI(e)),t.write('<script src="'+e+'"></script>')):D.notify('Cannot load sync the "'+e+'" script because "document.write" is not available',1)},pushAsyncScript:function(e){D.tools["default"].pushAsyncScript(e)},pushBlockingScript:function(e){D.tools["default"].pushBlockingScript(e)},addEventHandler:e.addEventListener?function(e,t,n){e.addEventListener(t,n,!1)}:function(e,t,n){e.attachEvent("on"+t,n)},removeEventHandler:e.removeEventListener?function(e,t,n){e.removeEventListener(t,n,!1)}:function(e,t,n){e.detachEvent("on"+t,n)},preventDefault:e.addEventListener?function(e){e.preventDefault()}:function(e){e.returnValue=!1},stopPropagation:function(e){e.cancelBubble=!0,e.stopPropagation&&e.stopPropagation()},containsElement:function(e,t){return e.contains?e.contains(t):!!(16&e.compareDocumentPosition(t))},matchesCss:function(n){function a(e,t){var n=t.tagName;return!!n&&e.toLowerCase()===n.toLowerCase()}var i=n.matchesSelector||n.mozMatchesSelector||n.webkitMatchesSelector||n.oMatchesSelector||n.msMatchesSelector;return i?function(n,a){if(a===t||a===e)return!1;try{return i.call(a,n)}catch(r){return!1}}:n.querySelectorAll?function(e,t){if(!t.parentNode)return!1;if(e.match(/^[a-z]+$/i))return a(e,t);try{for(var n=t.parentNode.querySelectorAll(e),i=n.length;i--;)if(n[i]===t)return!0}catch(r){}return!1}:function(e,t){if(e.match(/^[a-z]+$/i))return a(e,t);try{return D.Sizzle.matches(e,[t]).length>0}catch(n){return!1}}}(t.documentElement),cssQuery:(C=t,C.querySelectorAll?function(e,t){var n;try{n=C.querySelectorAll(e)}catch(a){n=[]}t(n)}:function(e,t){if(D.Sizzle){var n;try{n=D.Sizzle(e)}catch(a){n=[]}t(n)}else D.sizzleQueue.push([e,t])}),hasAttr:function(e,t){return e.hasAttribute?e.hasAttribute(t):e[t]!==n},inherit:function(e,t){var n=function(){};n.prototype=t.prototype,e.prototype=new n,e.prototype.constructor=e},extend:function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])},toArray:function(){try{var e=Array.prototype.slice;return e.call(t.documentElement.childNodes,0)[0].nodeType,function(t){return e.call(t,0)}}catch(n){return function(e){for(var t=[],n=0,a=e.length;n<a;n++)t.push(e[n]);return t}}}(),equalsIgnoreCase:function(e,t){return null==e?null==t:null!=t&&String(e).toLowerCase()===String(t).toLowerCase()},poll:function(e,t,n){function a(){D.isNumber(n)&&i++>=n||e()||setTimeout(a,t)}var i=0;t=t||1e3,a()},escapeForHtml:function(e){return e?String(e).replace(/\&/g,"&amp;").replace(/\</g,"&lt;").replace(/\>/g,"&gt;").replace(/\"/g,"&quot;").replace(/\'/g,"&#x27;").replace(/\//g,"&#x2F;"):e}};D.availableTools={},D.availableEventEmitters=[],D.fireOnceEvents=["condition","elementexists"],D.initEventEmitters=function(){D.eventEmitters=D.map(D.availableEventEmitters,function(e){return new e})},D.eventEmitterBackgroundTasks=function(){D.each(D.eventEmitters,function(e){"backgroundTasks"in e&&e.backgroundTasks()})},D.initTools=function(e){var t={"default":new v},n=D.settings.euCookieName||"sat_track";for(var a in e)if(e.hasOwnProperty(a)){var i,r,o;if((i=e[a]).euCookie)if("true"!==D.readCookie(n))continue;if(!(r=D.availableTools[i.engine])){var s=[];for(var c in D.availableTools)D.availableTools.hasOwnProperty(c)&&s.push(c);throw new Error("No tool engine named "+i.engine+", available: "+s.join(",")+".")}(o=new r(i)).id=a,t[a]=o}return t},D.preprocessArguments=function(e,t,n,a,i){function r(e){return a&&D.isString(e)?e.toLowerCase():e}function o(e){var c={};for(var l in e)if(e.hasOwnProperty(l)){var u=e[l];D.isObject(u)?c[l]=o(u):D.isArray(u)?c[l]=s(u,a):c[l]=r(D.replace(u,t,n,i))}return c}function s(e){for(var a=[],i=0,s=e.length;i<s;i++){var c=e[i];D.isString(c)?c=r(D.replace(c,t,n)):c&&c.constructor===Object&&(c=o(c)),a.push(c)}return a}return e?s(e,a):e},D.execute=function(e,t,n,a){function i(i){var r=a[i||"default"];if(r)try{r.triggerCommand(e,t,n)}catch(o){D.logError(o)}}if(!_satellite.settings.hideActivity)if(a=a||D.tools,e.engine){var r=e.engine;for(var o in a)if(a.hasOwnProperty(o)){var s=a[o];s.settings&&s.settings.engine===r&&i(o)}}else e.tool instanceof Array?D.each(e.tool,function(e){i(e)}):i(e.tool)},D.Logger={outputEnabled:!1,messages:[],keepLimit:100,flushed:!1,LEVELS:[null,null,"log","info","warn","error"],message:function(e,t){var n=this.LEVELS[t]||"log";this.messages.push([n,e]),this.messages.length>this.keepLimit&&this.messages.shift(),this.outputEnabled&&this.echo(n,e)},getHistory:function(){return this.messages},clearHistory:function(){this.messages=[]},setOutputState:function(e){this.outputEnabled!=e&&(this.outputEnabled=e,e?this.flush():this.flushed=!1)},echo:function(t,n){e.console&&e.console[t]("SATELLITE: "+n)},flush:function(){this.flushed||(D.each(this.messages,function(e){!0!==e[2]&&(this.echo(e[0],e[1]),e[2]=!0)},this),this.flushed=!0)}},D.notify=D.bind(D.Logger.message,D.Logger),D.cleanText=function(e){return null==e?null:D.trim(e).replace(/\s+/g," ")},D.cleanText.legacy=function(e){return null==e?null:D.trim(e).replace(/\s{2,}/g," ").replace(/[^\000-\177]*/g,"")},D.text=function(e){return e.textContent||e.innerText},D.specialProperties={text:D.text,cleanText:function(e){return D.cleanText(D.text(e))}},D.getObjectProperty=function(e,t,a){for(var i,r=t.split("."),o=e,s=D.specialProperties,c=0,l=r.length;c<l;c++){if(null==o)return n;var u=r[c];if(a&&"@"===u.charAt(0))o=s[u.slice(1)](o);else if(o.getAttribute&&(i=u.match(/^getAttribute\((.+)\)$/))){var d=i[1];o=o.getAttribute(d)}else o=o[u]}return o},D.getToolsByType=function(e){if(!e)throw new Error("Tool type is missing");var t=[];for(var n in D.tools)if(D.tools.hasOwnProperty(n)){var a=D.tools[n];a.settings&&a.settings.engine===e&&t.push(a)}return t},D.setVar=function(){var e=D.data.customVars;if(null==e&&(D.data.customVars={},e=D.data.customVars),"string"==typeof arguments[0])e[arguments[0]]=arguments[1];else if(arguments[0]){var t=arguments[0];for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])}},D.dataElementSafe=function(e,t){if(arguments.length>2){var n=arguments[2];"pageview"===t?D.dataElementSafe.pageviewCache[e]=n:"session"===t?D.setCookie("_sdsat_"+e,n):"visitor"===t&&D.setCookie("_sdsat_"+e,n,730)}else{if("pageview"===t)return D.dataElementSafe.pageviewCache[e];if("session"===t||"visitor"===t)return D.readCookie("_sdsat_"+e)}},D.dataElementSafe.pageviewCache={},D.realGetDataElement=function(t){var n;return t.selector?D.hasSelector&&D.cssQuery(t.selector,function(e){if(e.length>0){var a=e[0];"text"===t.property?n=a.innerText||a.textContent:t.property in a?n=a[t.property]:D.hasAttr(a,t.property)&&(n=a.getAttribute(t.property))}}):t.queryParam?n=t.ignoreCase?D.getQueryParamCaseInsensitive(t.queryParam):D.getQueryParam(t.queryParam):t.cookie?n=D.readCookie(t.cookie):t.jsVariable?n=D.getObjectProperty(e,t.jsVariable):t.customJS?n=t.customJS():t.contextHub&&(n=t.contextHub()),D.isString(n)&&t.cleanText&&(n=D.cleanText(n)),n},D.getDataElement=function(e,t,a){if(null==(a=a||D.dataElements[e]))return D.settings.undefinedVarsReturnEmpty?"":null;var i=D.realGetDataElement(a);return i===n&&a.storeLength?i=D.dataElementSafe(e,a.storeLength):i!==n&&a.storeLength&&D.dataElementSafe(e,a.storeLength,i),i||t||(i=a["default"]||""),D.isString(i)&&a.forceLowerCase&&(i=i.toLowerCase()),i},D.getVar=function(a,i,r){var o,s,c=D.data.customVars,l=r?r.target||r.srcElement:null,u={uri:D.URI(),protocol:t.location.protocol,hostname:t.location.hostname};if(D.dataElements&&a in D.dataElements)return D.getDataElement(a);if((s=u[a.toLowerCase()])===n)if("this."===a.substring(0,5))a=a.slice(5),s=D.getObjectProperty(i,a,!0);else if("event."===a.substring(0,6))a=a.slice(6),s=D.getObjectProperty(r,a);else if("target."===a.substring(0,7))a=a.slice(7),s=D.getObjectProperty(l,a);else if("window."===a.substring(0,7))a=a.slice(7),s=D.getObjectProperty(e,a);else if("param."===a.substring(0,6))a=a.slice(6),s=D.getQueryParam(a);else if(o=a.match(/^rand([0-9]+)$/)){var d=Number(o[1]),g=(Math.random()*(Math.pow(10,d)-1)).toFixed(0);s=Array(d-g.length+1).join("0")+g}else s=D.getObjectProperty(c,a);return s},D.getVars=function(e,t,n){var a={};return D.each(e,function(e){a[e]=D.getVar(e,t,n)}),a},D.replace=function(e,t,n,a){return"string"!=typeof e?e:e.replace(/%(.*?)%/g,function(e,i){var r=D.getVar(i,t,n);return null==r?D.settings.undefinedVarsReturnEmpty?"":e:a?D.escapeForHtml(r):r})},D.escapeHtmlParams=function(e){return e.escapeHtml=!0,e},D.searchVariables=function(e,t,n){if(!e||0===e.length)return"";for(var a=[],i=0,r=e.length;i<r;i++){var o=e[i],s=D.getVar(o,t,n);a.push(o+"="+escape(s))}return"?"+a.join("&")},D.fireRule=function(e,t,n){var a=e.trigger;if(a){for(var i=0,r=a.length;i<r;i++){var o=a[i];D.execute(o,t,n)}D.contains(D.fireOnceEvents,e.event)&&(e.expired=!0)}},D.isLinked=function(e){for(var t=e;t;t=t.parentNode)if(D.isLinkTag(t))return!0;return!1},D.firePageLoadEvent=function(e){for(var n=t.location,a={type:e,target:n},i=D.pageLoadRules,r=D.evtHandlers[a.type],o=i.length;o--;){var s=i[o];D.ruleMatches(s,a,n)&&(D.notify('Rule "'+s.name+'" fired.',1),D.fireRule(s,n,a))}for(var c in D.tools)if(D.tools.hasOwnProperty(c)){var l=D.tools[c];l.endPLPhase&&l.endPLPhase(e)}r&&D.each(r,function(e){e(a)})},D.track=function(e){e=e.replace(/^\s*/,"").replace(/\s*$/,"");for(var t=0;t<D.directCallRules.length;t++){var n=D.directCallRules[t];if(n.name===e)return D.notify('Direct call Rule "'+e+'" fired.',1),void D.fireRule(n,location,{type:e})}D.notify('Direct call Rule "'+e+'" not found.',1)},D.basePath=function(){return D.data.host?("https:"===t.location.protocol?"https://"+D.data.host.https:"http://"+D.data.host.http)+"/":this.settings.basePath},D.setLocation=function(t){e.location=t},D.parseQueryParams=function(e){var t=function(e){var t=e;try{t=decodeURIComponent(e)}catch(n){}return t};if(""===e||!1===D.isString(e))return{};0===e.indexOf("?")&&(e=e.substring(1));var n={},a=e.split("&");return D.each(a,function(e){(e=e.split("="))[1]&&(n[t(e[0])]=t(e[1]))}),n},D.getCaseSensitivityQueryParamsMap=function(e){var t=D.parseQueryParams(e),n={};for(var a in t)t.hasOwnProperty(a)&&(n[a.toLowerCase()]=t[a]);return{normal:t,caseInsensitive:n}},D.updateQueryParams=function(){D.QueryParams=D.getCaseSensitivityQueryParamsMap(e.location.search)},D.updateQueryParams(),D.getQueryParam=function(e){return D.QueryParams.normal[e]},D.getQueryParamCaseInsensitive=function(e){return D.QueryParams.caseInsensitive[e.toLowerCase()]},D.encodeObjectToURI=function(e){if(!1===D.isObject(e))return"";var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(encodeURIComponent(n)+"="+encodeURIComponent(e[n]));return t.join("&")},D.readCookie=function(e){for(var a=e+"=",i=t.cookie.split(";"),r=0;r<i.length;r++){for(var o=i[r];" "==o.charAt(0);)o=o.substring(1,o.length);if(0===o.indexOf(a))return o.substring(a.length,o.length)}return n},D.setCookie=function(e,n,a){var i;if(a){var r=new Date;r.setTime(r.getTime()+24*a*60*60*1e3),i="; expires="+r.toGMTString()}else i="";t.cookie=e+"="+n+i+"; path=/"},D.removeCookie=function(e){D.setCookie(e,"",-1)},D.getElementProperty=function(e,t){if("@"===t.charAt(0)){var a=D.specialProperties[t.substring(1)];if(a)return a(e)}return"innerText"===t?D.text(e):t in e?e[t]:e.getAttribute?e.getAttribute(t):n},D.propertiesMatch=function(e,t){if(e)for(var n in e)if(e.hasOwnProperty(n)){var a=e[n],i=D.getElementProperty(t,n);if("string"==typeof a&&a!==i)return!1;if(a instanceof RegExp&&!a.test(i))return!1}return!0},D.isRightClick=function(e){var t;return e.which?t=3==e.which:e.button&&(t=2==e.button),t},D.ruleMatches=function(e,t,n,a){var i=e.condition,r=e.conditions,o=e.property,s=t.type,c=e.value,l=t.target||t.srcElement,u=n===l;if(e.event!==s&&("custom"!==e.event||e.customEvent!==s))return!1;if(!D.ruleInScope(e))return!1;if("click"===e.event&&D.isRightClick(t))return!1;if(e.isDefault&&a>0)return!1;if(e.expired)return!1;if("inview"===s&&t.inviewDelay!==e.inviewDelay)return!1;if(!u&&(!1===e.bubbleFireIfParent||0!==a&&!1===e.bubbleFireIfChildFired))return!1;if(e.selector&&!D.matchesCss(e.selector,n))return!1;if(!D.propertiesMatch(o,n))return!1;if(null!=c)if("string"==typeof c){if(c!==n.value)return!1}else if(!c.test(n.value))return!1;if(i)try{if(!i.call(n,t,l))return D.notify('Condition for rule "'+e.name+'" not met.',1),!1}catch(g){return D.notify('Condition for rule "'+e.name+'" not met. Error: '+g.message,1),!1}if(r){var d=D.find(r,function(a){try{return!a.call(n,t,l)}catch(g){return D.notify('Condition for rule "'+e.name+'" not met. Error: '+g.message,1),!0}});if(d)return D.notify("Condition "+d.toString()+' for rule "'+e.name+'" not met.',1),!1}return!0},D.evtHandlers={},D.bindEvent=function(e,t){var n=D.evtHandlers;n[e]||(n[e]=[]),n[e].push(t)},D.whenEvent=D.bindEvent,D.unbindEvent=function(e,t){var n=D.evtHandlers;if(n[e]){var a=D.indexOf(n[e],t);n[e].splice(a,1)}},D.bindEventOnce=function(e,t){var n=function(){D.unbindEvent(e,n),t.apply(null,arguments)};D.bindEvent(e,n)},D.isVMLPoisoned=function(e){if(!e)return!1;try{e.nodeName}catch(t){if("Attribute only valid on v:image"===t.message)return!0}return!1},D.handleEvent=function(e){if(!D.$data(e,"eventProcessed")){var t=e.type.toLowerCase(),n=e.target||e.srcElement,a=0,i=D.rules,r=(D.tools,D.evtHandlers[e.type]);if(D.isVMLPoisoned(n))D.notify("detected "+t+" on poisoned VML element, skipping.",1);else{r&&D.each(r,function(t){t(e)}),n&&n.nodeName?D.notify("detected "+t+" on "+n.nodeName,1):D.notify("detected "+t,1);for(var o=n;o;o=o.parentNode){var s=!1;if(D.each(i,function(t){D.ruleMatches(t,e,o,a)&&(D.notify('Rule "'+t.name+'" fired.',1),D.fireRule(t,o,e),a++,t.bubbleStop&&(s=!0))}),s)break}D.$data(e,"eventProcessed",!0)}}},D.onEvent=t.querySelectorAll?function(e){D.handleEvent(e)}:(I=[],(k=function(e){e.selector?I.push(e):D.handleEvent(e)}).pendingEvents=I,k),D.fireEvent=function(e,t){D.onEvent({type:e,target:t})},D.registerEvents=function(e,t){for(var n=t.length-1;n>=0;n--){var a=t[n];D.$data(e,a+".tracked")||(D.addEventHandler(e,a,D.onEvent),D.$data(e,a+".tracked",!0))}},D.registerEventsForTags=function(e,n){for(var a=e.length-1;a>=0;a--)for(var i=e[a],r=t.getElementsByTagName(i),o=r.length-1;o>=0;o--)D.registerEvents(r[o],n)},D.setListeners=function(){var e=["click","submit"];D.each(D.rules,function(t){"custom"===t.event&&t.hasOwnProperty("customEvent")&&!D.contains(e,t.customEvent)&&e.push(t.customEvent)}),D.registerEvents(t,e)},D.getUniqueRuleEvents=function(){return D._uniqueRuleEvents||(D._uniqueRuleEvents=[],D.each(D.rules,function(e){-1===D.indexOf(D._uniqueRuleEvents,e.event)&&D._uniqueRuleEvents.push(e.event)})),D._uniqueRuleEvents},D.setFormListeners=function(){if(!D._relevantFormEvents){var e=["change","focus","blur","keypress"];D._relevantFormEvents=D.filter(D.getUniqueRuleEvents(),function(t){return-1!==D.indexOf(e,t)})}D._relevantFormEvents.length&&D.registerEventsForTags(["input","select","textarea","button"],D._relevantFormEvents)},D.setVideoListeners=function(){if(!D._relevantVideoEvents){var e=["play","pause","ended","volumechange","stalled","loadeddata"];D._relevantVideoEvents=D.filter(D.getUniqueRuleEvents(),function(t){return-1!==D.indexOf(e,t)})}D._relevantVideoEvents.length&&D.registerEventsForTags(["video"],D._relevantVideoEvents)},D.readStoredSetting=function(t){try{return t="sdsat_"+t,e.localStorage.getItem(t)}catch(n){return D.notify("Cannot read stored setting from localStorage: "+n.message,2),null}},D.loadStoredSettings=function(){var e=D.readStoredSetting("debug"),t=D.readStoredSetting("hide_activity");e&&(D.settings.notifications="true"===e),t&&(D.settings.hideActivity="true"===t)},D.isRuleActive=function(e,t){function n(e,t){return t=i(t,{hour:e[m](),minute:e[f]()}),Math.floor(Math.abs((e.getTime()-t.getTime())/864e5))}function a(e,t){function n(e){return 12*e[d]()+e[g]()}return Math.abs(n(e)-n(t))}function i(e,t){var n=new Date(e.getTime());for(var a in t)if(t.hasOwnProperty(a)){var i=t[a];switch(a){case"hour":n[h](i);break;case"minute":n[p](i);break;case"date":n[v](i)}}return n}function r(e,t){return 60*e[m]()+e[f]()>60*t[m]()+t[f]()}function o(e,t){return 60*e[m]()+e[f]()<60*t[m]()+t[f]()}var s=e.schedule;if(!s)return!0;var c=s.utc,l=c?"getUTCDate":"getDate",u=c?"getUTCDay":"getDay",d=c?"getUTCFullYear":"getFullYear",g=c?"getUTCMonth":"getMonth",m=c?"getUTCHours":"getHours",f=c?"getUTCMinutes":"getMinutes",h=c?"setUTCHours":"setHours",p=c?"setUTCMinutes":"setMinutes",v=c?"setUTCDate":"setDate";if(t=t||new Date,s.repeat){if(r(s.start,t))return!1;if(o(s.end,t))return!1;if(t<s.start)return!1;if(s.endRepeat&&t>=s.endRepeat)return!1;if("daily"===s.repeat){if(s.repeatEvery)if(n(s.start,t)%s.repeatEvery!=0)return!1}else if("weekly"===s.repeat){if(s.days){if(!D.contains(s.days,t[u]()))return!1}else if(s.start[u]()!==t[u]())return!1;if(s.repeatEvery)if(n(s.start,t)%(7*s.repeatEvery)!=0)return!1}else if("monthly"===s.repeat){if(s.repeatEvery)if(a(s.start,t)%s.repeatEvery!=0)return!1;if(s.nthWeek&&s.mthDay){if(s.mthDay!==t[u]())return!1;var b=Math.floor((t[l]()-t[u]()+1)/7);if(s.nthWeek!==b)return!1}else if(s.start[l]()!==t[l]())return!1}else if("yearly"===s.repeat){if(s.start[g]()!==t[g]())return!1;if(s.start[l]()!==t[l]())return!1;if(s.repeatEvery)if(Math.abs(s.start[d]()-t[d]())%s.repeatEvery!=0)return!1}}else{if(s.start>t)return!1;if(s.end<t)return!1}return!0},D.isOutboundLink=function(e){if(!e.getAttribute("href"))return!1;var t=e.hostname,n=(e.href,e.protocol);return("http:"===n||"https:"===n)&&(!D.any(D.settings.domainList,function(e){return D.isSubdomainOf(t,e)})&&t!==location.hostname)},D.isLinkerLink=function(e){return!(!e.getAttribute||!e.getAttribute("href"))&&(D.hasMultipleDomains()&&e.hostname!=location.hostname&&!e.href.match(/^javascript/i)&&!D.isOutboundLink(e))},D.isSubdomainOf=function(e,t){if(e===t)return!0;var n=e.length-t.length;return n>0&&D.equalsIgnoreCase(e.substring(n),t)},D.getVisitorId=function(){var e=D.getToolsByType("visitor_id");return 0===e.length?null:e[0].getInstance()},D.URI=function(){var e=t.location.pathname+t.location.search;return D.settings.forceLowerCase&&(e=e.toLowerCase()),e},D.URL=function(){var e=t.location.href;return D.settings.forceLowerCase&&(e=e.toLowerCase()),e},D.filterRules=function(){function e(e){return!!D.isRuleActive(e)}D.rules=D.filter(D.rules,e),D.pageLoadRules=D.filter(D.pageLoadRules,e)},D.ruleInScope=function(e,n){function a(e,t){function n(e){return t.match(e)}var a=e.include,r=e.exclude;if(a&&i(a,t))return!0;if(r){if(D.isString(r)&&r===t)return!0;if(D.isArray(r)&&D.any(r,n))return!0;if(D.isRegex(r)&&n(r))return!0}return!1}function i(e,t){function n(e){return t.match(e)}return!(!D.isString(e)||e===t)||(!(!D.isArray(e)||D.any(e,n))||!(!D.isRegex(e)||n(e)))}n=n||t.location;var r=e.scope;if(!r)return!0;var o=r.URI,s=r.subdomains,c=r.domains,l=r.protocols,u=r.hashes;return(!o||!a(o,n.pathname+n.search))&&((!s||!a(s,n.hostname))&&((!c||!i(c,n.hostname))&&((!l||!i(l,n.protocol))&&(!u||!a(u,n.hash)))))},D.backgroundTasks=function(){new Date;D.setFormListeners(),D.setVideoListeners(),D.loadStoredSettings(),D.registerNewElementsForDynamicRules(),D.eventEmitterBackgroundTasks();new Date},D.registerNewElementsForDynamicRules=function(){function e(t,n){var a=e.cache[t];if(a)return n(a);D.cssQuery(t,function(a){e.cache[t]=a,n(a)})}e.cache={},D.each(D.dynamicRules,function(t){e(t.selector,function(e){D.each(e,function(e){var n="custom"===t.event?t.customEvent:t.event;D.$data(e,"dynamicRules.seen."+n)||(D.$data(e,"dynamicRules.seen."+n,!0),D.propertiesMatch(t.property,e)&&D.registerEvents(e,[n]))})})})},D.ensureCSSSelector=function(){t.querySelectorAll?D.hasSelector=!0:(D.loadingSizzle=!0,D.sizzleQueue=[],D.loadScript(D.basePath()+"selector.js",function(){if(D.Sizzle){var e=D.onEvent.pendingEvents;D.each(e,function(e){D.handleEvent(e)},this),D.onEvent=D.handleEvent,D.hasSelector=!0,delete D.loadingSizzle,D.each(D.sizzleQueue,function(e){D.cssQuery(e[0],e[1])}),delete D.sizzleQueue}else D.logError(new Error("Failed to load selector.js"))}))},D.errors=[],D.logError=function(e){D.errors.push(e),D.notify(e.name+" - "+e.message,5)},D.pageBottom=function(){D.initialized&&(D.pageBottomFired=!0,D.firePageLoadEvent("pagebottom"))},D.stagingLibraryOverride=function(){if("true"===D.readStoredSetting("stagingLibrary")){for(var e,n,a,i=t.getElementsByTagName("script"),r=/^(.*)satelliteLib-([a-f0-9]{40})\.js$/,o=/^(.*)satelliteLib-([a-f0-9]{40})-staging\.js$/,s=0,c=i.length;s<c&&(!(a=i[s].getAttribute("src"))||(e||(e=a.match(r)),n||(n=a.match(o)),!n));s++);if(e&&!n){var l=e[1]+"satelliteLib-"+e[2]+"-staging.js";if(t.write)t.write('<script src="'+l+'"></script>');else{var u=t.createElement("script");u.src=l,t.head.appendChild(u)}return!0}}return!1},D.checkAsyncInclude=function(){e.satellite_asyncLoad&&D.notify('You may be using the async installation of Satellite. In-page HTML and the "pagebottom" event will not work. Please update your Satellite installation for these features.',5)},D.hasMultipleDomains=function(){return!!D.settings.domainList&&D.settings.domainList.length>1},D.handleOverrides=function(){if(L)for(var e in L)L.hasOwnProperty(e)&&(D.data[e]=L[e])},D.privacyManagerParams=function(){var e={};D.extend(e,D.settings.privacyManagement);var t=[];for(var n in D.tools)if(D.tools.hasOwnProperty(n)){var a=D.tools[n],i=a.settings;if(!i)continue;"sc"===i.engine&&t.push(a)}var r=D.filter(D.map(t,function(e){return e.getTrackingServer()}),function(e){return null!=e});e.adobeAnalyticsTrackingServers=r;for(var o=["bannerText","headline","introductoryText","customCSS"],s=0;s<o.length;s++){var c=o[s],l=e[c];if(l)if("text"===l.type)e[c]=l.value;else{if("data"!==l.type)throw new Error("Invalid type: "+l.type);e[c]=D.getVar(l.value)}}return e},D.prepareLoadPrivacyManager=function(){function t(e){function t(){++r===i.length&&(n(),clearTimeout(o),e())}function n(){D.each(i,function(e){D.unbindEvent(e.id+".load",t)})}function a(){n(),e()}var i=D.filter(D.values(D.tools),function(e){return e.settings&&"sc"===e.settings.engine});if(0===i.length)return e();var r=0;D.each(i,function(e){D.bindEvent(e.id+".load",t)});var o=setTimeout(a,5e3)}D.addEventHandler(e,"load",function(){t(D.loadPrivacyManager)})},D.loadPrivacyManager=function(){var e=D.basePath()+"privacy_manager.js";D.loadScript(e,function(){var e=D.privacyManager;e.configure(D.privacyManagerParams()),e.openIfRequired()})},D.init=function(t){if(!D.stagingLibraryOverride()){D.configurationSettings=t;var a=t.tools;for(var i in delete t.tools,t)t.hasOwnProperty(i)&&(D[i]=t[i]);D.data.customVars===n&&(D.data.customVars={}),D.data.queryParams=D.QueryParams.normal,D.handleOverrides(),D.detectBrowserInfo(),D.trackVisitorInfo&&D.trackVisitorInfo(),D.loadStoredSettings(),D.Logger.setOutputState(D.settings.notifications),D.checkAsyncInclude(),D.ensureCSSSelector(),D.filterRules(),D.dynamicRules=D.filter(D.rules,function(e){return e.eventHandlerOnElement}),D.tools=D.initTools(a),D.initEventEmitters(),D.firePageLoadEvent("aftertoolinit"),D.settings.privacyManagement&&D.prepareLoadPrivacyManager(),D.hasSelector&&D.domReady(D.eventEmitterBackgroundTasks),D.setListeners(),D.domReady(function(){D.poll(function(){D.backgroundTasks()},D.settings.recheckEvery||3e3)}),D.domReady(function(){D.domReadyFired=!0,D.pageBottomFired||D.pageBottom(),D.firePageLoadEvent("domready")}),D.addEventHandler(e,"load",function(){D.firePageLoadEvent("windowload")}),D.firePageLoadEvent("pagetop"),D.initialized=!0}},D.pageLoadPhases=["aftertoolinit","pagetop","pagebottom","domready","windowload"],D.loadEventBefore=function(e,t){return D.indexOf(D.pageLoadPhases,e)<=D.indexOf(D.pageLoadPhases,t)},D.flushPendingCalls=function(e){e.pending&&(D.each(e.pending,function(t){var n=t[0],a=t[1],i=t[2],r=t[3];n in e?e[n].apply(e,[a,i].concat(r)):e.emit?e.emit(n,a,i,r):D.notify("Failed to trigger "+n+" for tool "+e.id,1)}),delete e.pending)},D.setDebug=function(t){try{e.localStorage.setItem("sdsat_debug",t)}catch(n){D.notify("Cannot set debug mode: "+n.message,2)}},D.getUserAgent=function(){return navigator.userAgent},D.detectBrowserInfo=function(){function e(e){return function(t){for(var n in e){if(e.hasOwnProperty(n))if(e[n].test(t))return n}return"Unknown"}}var t=e({"IE Edge Mobile":/Windows Phone.*Edge/,"IE Edge":/Edge/,OmniWeb:/OmniWeb/,"Opera Mini":/Opera Mini/,"Opera Mobile":/Opera Mobi/,Opera:/Opera/,Chrome:/Chrome|CriOS|CrMo/,Firefox:/Firefox|FxiOS/,"IE Mobile":/IEMobile/,IE:/MSIE|Trident/,"Mobile Safari":/Mobile(\/[0-9A-z]+)? Safari/,Safari:/Safari/}),n=e({Blackberry:/BlackBerry|BB10/,"Symbian OS":/Symbian|SymbOS/,Maemo:/Maemo/,Android:/Android/,Linux:/ Linux /,Unix:/FreeBSD|OpenBSD|CrOS/,Windows:/[\( ]Windows /,iOS:/iPhone|iPad|iPod/,MacOS:/Macintosh;/}),a=e({Nokia:/Symbian|SymbOS|Maemo/,"Windows Phone":/Windows Phone/,Blackberry:/BlackBerry|BB10/,Android:/Android/,iPad:/iPad/,iPod:/iPod/,iPhone:/iPhone/,Desktop:/.*/}),i=D.getUserAgent();D.browserInfo={browser:t(i),os:n(i),deviceType:a(i)}},D.isHttps=function(){return"https:"==t.location.protocol},D.BaseTool=function(e){this.settings=e||{},this.forceLowerCase=D.settings.forceLowerCase,"forceLowerCase"in this.settings&&(this.forceLowerCase=this.settings.forceLowerCase)},D.BaseTool.prototype={triggerCommand:function(e,t,n){var a=this.settings||{};if(this.initialize&&this.isQueueAvailable()&&this.isQueueable(e)&&n&&D.loadEventBefore(n.type,a.loadOn))this.queueCommand(e,t,n);else{var i=e.command,r=this["$"+i],o=!!r&&r.escapeHtml,s=D.preprocessArguments(e.arguments,t,n,this.forceLowerCase,o);r?r.apply(this,[t,n].concat(s)):this.$missing$?this.$missing$(i,t,n,s):D.notify("Failed to trigger "+i+" for tool "+this.id,1)}},endPLPhase:function(){},isQueueable:function(e){return"cancelToolInit"!==e.command},isQueueAvailable:function(){return!this.initialized&&!this.initializing},flushQueue:function(){this.pending&&(D.each(this.pending,function(e){this.triggerCommand.apply(this,e)},this),this.pending=[])},queueCommand:function(e,t,n){this.pending||(this.pending=[]),this.pending.push([e,t,n])},$cancelToolInit:function(){this._cancelToolInit=!0}},e._satellite=D,D.visibility={isHidden:function(){var e=this.getHiddenProperty();return!!e&&t[e]},isVisible:function(){return!this.isHidden()},getHiddenProperty:function(){var e=["webkit","moz","ms","o"];if("hidden"in t)return"hidden";for(var n=0;n<e.length;n++)if(e[n]+"Hidden"in t)return e[n]+"Hidden";return null},getVisibilityEvent:function(){var e=this.getHiddenProperty();return e?e.replace(/[H|h]idden/,"")+"visibilitychange":null}},D.ecommerce={addItem:function(){var e=[].slice.call(arguments);D.onEvent({type:"ecommerce.additem",target:e})},addTrans:function(){
var e=[].slice.call(arguments);D.data.saleData.sale={orderId:e[0],revenue:e[2]},D.onEvent({type:"ecommerce.addtrans",target:e})},trackTrans:function(){D.onEvent({type:"ecommerce.tracktrans",target:[]})}},a.orientationChange=function(t){var n=0===e.orientation?"portrait":"landscape";t.orientation=n,D.onEvent(t)},D.availableEventEmitters.push(a),i.offset=function(n){var a=null,i=null;try{var r=n.getBoundingClientRect(),o=t,s=o.documentElement,c=o.body,l=e,u=s.clientTop||c.clientTop||0,d=s.clientLeft||c.clientLeft||0,g=l.pageYOffset||s.scrollTop||c.scrollTop,m=l.pageXOffset||s.scrollLeft||c.scrollLeft;a=r.top+g-u,i=r.left+m-d}catch(f){}return{top:a,left:i}},i.getViewportHeight=function(){var n=e.innerHeight,a=t.compatMode;return a&&(n="CSS1Compat"==a?t.documentElement.clientHeight:t.body.clientHeight),n},i.getScrollTop=function(){return t.documentElement.scrollTop?t.documentElement.scrollTop:t.body.scrollTop},i.isElementInDocument=function(e){return t.body.contains(e)},i.isElementInView=function(e){if(!i.isElementInDocument(e))return!1;var t=i.getViewportHeight(),n=i.getScrollTop(),a=i.offset(e).top,r=e.offsetHeight;return null!==a&&!(n>a+r||n+t<a)},i.prototype={backgroundTasks:function(){var e=this.elements;D.each(this.rules,function(t){D.cssQuery(t.selector,function(n){var a=0;D.each(n,function(t){D.contains(e,t)||(e.push(t),a++)}),a&&D.notify(t.selector+" added "+a+" elements.",1)})}),this.track()},checkInView:function(e,t,n){var a=D.$data(e,"inview");if(i.isElementInView(e)){a||D.$data(e,"inview",!0);var r=this;this.processRules(e,function(n,a,i){if(t||!n.inviewDelay)D.$data(e,a,!0),D.onEvent({type:"inview",target:e,inviewDelay:n.inviewDelay});else if(n.inviewDelay){var o=D.$data(e,i);o||(o=setTimeout(function(){r.checkInView(e,!0,n.inviewDelay)},n.inviewDelay),D.$data(e,i,o))}},n)}else{if(!i.isElementInDocument(e)){var o=D.indexOf(this.elements,e);this.elements.splice(o,1)}a&&D.$data(e,"inview",!1),this.processRules(e,function(t,n,a){var i=D.$data(e,a);i&&clearTimeout(i)},n)}},track:function(){for(var e=this.elements.length-1;e>=0;e--)this.checkInView(this.elements[e])},processRules:function(e,t,n){var a=this.rules;n&&(a=D.filter(this.rules,function(e){return e.inviewDelay==n})),D.each(a,function(n,a){var i=n.inviewDelay?"viewed_"+n.inviewDelay:"viewed",r="inview_timeout_id_"+a;D.$data(e,i)||D.matchesCss(n.selector,e)&&t(n,i,r)})}},D.availableEventEmitters.push(i),r.prototype={backgroundTasks:function(){var e=this.eventHandler;D.each(this.rules,function(t){D.cssQuery(t.selector||"video",function(t){D.each(t,function(t){D.$data(t,"videoplayed.tracked")||(D.addEventHandler(t,"timeupdate",D.throttle(e,100)),D.$data(t,"videoplayed.tracked",!0))})})})},evalRule:function(e,t){var n=t.event,a=e.seekable,i=a.start(0),r=a.end(0),o=e.currentTime,s=t.event.match(/^videoplayed\(([0-9]+)([s%])\)$/);if(s){var c=s[2],l=Number(s[1]),u="%"===c?function(){return l<=100*(o-i)/(r-i)}:function(){return l<=o-i};!D.$data(e,n)&&u()&&(D.$data(e,n,!0),D.onEvent({type:n,target:e}))}},onUpdateTime:function(e){var t=this.rules,n=e.target;if(n.seekable&&0!==n.seekable.length)for(var a=0,i=t.length;a<i;a++)this.evalRule(n,t[a])}},D.availableEventEmitters.push(r),o.prototype={obue:!1,initialize:function(){this.attachCloseListeners()},obuePrevUnload:function(){},obuePrevBeforeUnload:function(){},newObueListener:function(){this.obue||(this.obue=!0,this.triggerBeacons())},attachCloseListeners:function(){this.prevUnload=e.onunload,this.prevBeforeUnload=e.onbeforeunload,e.onunload=D.bind(function(t){this.prevUnload&&setTimeout(D.bind(function(){this.prevUnload.call(e,t)},this),1),this.newObueListener()},this),e.onbeforeunload=D.bind(function(t){this.prevBeforeUnload&&setTimeout(D.bind(function(){this.prevBeforeUnload.call(e,t)},this),1),this.newObueListener()},this)},triggerBeacons:function(){D.fireEvent("leave",t)}},D.availableEventEmitters.push(o),s.prototype={initialize:function(){if(this.FB=this.FB||e.FB,this.FB&&this.FB.Event&&this.FB.Event.subscribe)return this.bind(),!0},bind:function(){this.FB.Event.subscribe("edge.create",function(){D.notify("tracking a facebook like",1),D.onEvent({type:"facebook.like",target:t})}),this.FB.Event.subscribe("edge.remove",function(){D.notify("tracking a facebook unlike",1),D.onEvent({type:"facebook.unlike",target:t})}),this.FB.Event.subscribe("message.send",function(){D.notify("tracking a facebook share",1),D.onEvent({type:"facebook.send",target:t})})}},D.availableEventEmitters.push(s),c.prototype={defineEvents:function(){this.oldBlurClosure=function(){D.fireEvent("tabblur",t)},this.oldFocusClosure=D.bind(function(){this.visibilityApiHasPriority?D.fireEvent("tabfocus",t):null!=D.visibility.getHiddenProperty()&&D.visibility.isHidden()||D.fireEvent("tabfocus",t)},this)},attachDetachModernEventListeners:function(e){D[0==e?"removeEventHandler":"addEventHandler"](t,D.visibility.getVisibilityEvent(),this.handleVisibilityChange)},attachDetachOlderEventListeners:function(t,n,a){var i=0==t?"removeEventHandler":"addEventHandler";D[i](n,a,this.oldBlurClosure),D[i](e,"focus",this.oldFocusClosure)},handleVisibilityChange:function(){D.visibility.isHidden()?D.fireEvent("tabblur",t):D.fireEvent("tabfocus",t)},setVisibilityApiPriority:function(t){this.visibilityApiHasPriority=t,this.attachDetachOlderEventListeners(!1,e,"blur"),this.attachDetachModernEventListeners(!1),t?null!=D.visibility.getHiddenProperty()?this.attachDetachModernEventListeners(!0):this.attachDetachOlderEventListeners(!0,e,"blur"):(this.attachDetachOlderEventListeners(!0,e,"blur"),null!=D.visibility.getHiddenProperty()&&this.attachDetachModernEventListeners(!0))},oldBlurClosure:null,oldFocusClosure:null,visibilityApiHasPriority:!0},D.availableEventEmitters.push(c),l.prototype={initialize:function(){this.setupHistoryAPI(),this.setupHashChange()},fireIfURIChanged:function(){var e=D.URL();this.lastURL!==e&&(this.fireEvent(),this.lastURL=e)},fireEvent:function(){D.updateQueryParams(),D.onEvent({type:"locationchange",target:t})},setupSPASupport:function(){this.setupHistoryAPI(),this.setupHashChange()},setupHistoryAPI:function(){var t=e.history;t&&(t.pushState&&(this.originalPushState=t.pushState,t.pushState=this._pushState),t.replaceState&&(this.originalReplaceState=t.replaceState,t.replaceState=this._replaceState)),D.addEventHandler(e,"popstate",this._onPopState)},pushState:function(){var e=this.originalPushState.apply(history,arguments);return this.onPushState(),e},replaceState:function(){var e=this.originalReplaceState.apply(history,arguments);return this.onReplaceState(),e},setupHashChange:function(){D.addEventHandler(e,"hashchange",this._onHashChange)},onReplaceState:function(){setTimeout(this._fireIfURIChanged,0)},onPushState:function(){setTimeout(this._fireIfURIChanged,0)},onPopState:function(){setTimeout(this._fireIfURIChanged,0)},onHashChange:function(){setTimeout(this._fireIfURIChanged,0)},uninitialize:function(){this.cleanUpHistoryAPI(),this.cleanUpHashChange()},cleanUpHistoryAPI:function(){history.pushState===this._pushState&&(history.pushState=this.originalPushState),history.replaceState===this._replaceState&&(history.replaceState=this.originalReplaceState),D.removeEventHandler(e,"popstate",this._onPopState)},cleanUpHashChange:function(){D.removeEventHandler(e,"hashchange",this._onHashChange)}},D.availableEventEmitters.push(l),u.prototype.getStringifiedValue=e.JSON&&e.JSON.stringify||D.stringify,u.prototype.initPolling=function(){0!==this.dataElementsNames.length&&(this.dataElementsStore=this.getDataElementsValues(),D.poll(D.bind(this.checkDataElementValues,this),1e3))},u.prototype.getDataElementsValues=function(){var e={};return D.each(this.dataElementsNames,function(t){var n=D.getVar(t);e[t]=this.getStringifiedValue(n)},this),e},u.prototype.checkDataElementValues=function(){D.each(this.dataElementsNames,D.bind(function(e){var n=this.getStringifiedValue(D.getVar(e));n!==this.dataElementsStore[e]&&(this.dataElementsStore[e]=n,D.onEvent({type:"dataelementchange("+e+")",target:t}))},this))},D.availableEventEmitters.push(u),d.prototype.backgroundTasks=function(){D.each(this.rules,function(e){D.cssQuery(e.selector,function(e){if(e.length>0){var t=e[0];if(D.$data(t,"elementexists.seen"))return;D.$data(t,"elementexists.seen",!0),D.onEvent({type:"elementexists",target:t})}})})},D.availableEventEmitters.push(d),g.prototype={backgroundTasks:function(){var e=this;D.each(this.rules,function(t){var n=t[1],a=t[0];D.cssQuery(n,function(t){D.each(t,function(t){e.trackElement(t,a)})})},this)},trackElement:function(e,t){var n=this,a=D.$data(e,"hover.delays");a?D.contains(a,t)||a.push(t):(D.addEventHandler(e,"mouseover",function(t){n.onMouseOver(t,e)}),D.addEventHandler(e,"mouseout",function(t){n.onMouseOut(t,e)}),D.$data(e,"hover.delays",[t]))},onMouseOver:function(e,t){var n=e.target||e.srcElement,a=e.relatedTarget||e.fromElement;(t===n||D.containsElement(t,n))&&!D.containsElement(t,a)&&this.onMouseEnter(t)},onMouseEnter:function(e){var t=D.$data(e,"hover.delays"),n=D.map(t,function(t){return setTimeout(function(){D.onEvent({type:"hover("+t+")",target:e})},t)});D.$data(e,"hover.delayTimers",n)},onMouseOut:function(e,t){var n=e.target||e.srcElement,a=e.relatedTarget||e.toElement;(t===n||D.containsElement(t,n))&&!D.containsElement(t,a)&&this.onMouseLeave(t)},onMouseLeave:function(e){var t=D.$data(e,"hover.delayTimers");t&&D.each(t,function(e){clearTimeout(e)})}},D.availableEventEmitters.push(g),m.prototype={initialize:function(){var e=this.twttr;e&&"function"==typeof e.ready&&e.ready(D.bind(this.bind,this))},bind:function(){this.twttr.events.bind("tweet",function(e){e&&(D.notify("tracking a tweet button",1),D.onEvent({type:"twitter.tweet",target:t}))})}},D.availableEventEmitters.push(m),D.inherit(f,D.BaseTool),D.extend(f.prototype,{name:"tnt",endPLPhase:function(e){"aftertoolinit"===e&&this.initialize()},initialize:function(){D.notify("Test & Target: Initializing",1),this.initializeTargetPageParams(),this.load()},initializeTargetPageParams:function(){e.targetPageParams&&this.updateTargetPageParams(this.parseTargetPageParamsResult(e.targetPageParams())),this.updateTargetPageParams(this.settings.pageParams),this.setTargetPageParamsFunction()},load:function(){var e=this.getMboxURL(this.settings.mboxURL);!1!==this.settings.initTool?this.settings.loadSync?(D.loadScriptSync(e),this.onScriptLoaded()):(D.loadScript(e,D.bind(this.onScriptLoaded,this)),this.initializing=!0):this.initialized=!0},getMboxURL:function(t){var n=t;return D.isObject(t)&&(n="https:"===e.location.protocol?t.https:t.http),n.match(/^https?:/)?n:D.basePath()+n},onScriptLoaded:function(){D.notify("Test & Target: loaded.",1),this.flushQueue(),this.initialized=!0,this.initializing=!1},$addMbox:function(e,t,n){var a=n.mboxGoesAround,i=a+"{visibility: hidden;}",r=this.appendStyle(i);a in this.styleElements||(this.styleElements[a]=r),this.initialized?this.$addMBoxStep2(null,null,n):this.initializing&&this.queueCommand({command:"addMBoxStep2",arguments:[n]},e,t)},$addMBoxStep2:function(n,a,i){var r=this.generateID(),o=this;D.addEventHandler(e,"load",D.bind(function(){D.cssQuery(i.mboxGoesAround,function(n){var a=n[0];if(a){var s=t.createElement("div");s.id=r,a.parentNode.replaceChild(s,a),s.appendChild(a),e.mboxDefine(r,i.mboxName);var c=[i.mboxName];i.arguments&&(c=c.concat(i.arguments)),e.mboxUpdate.apply(null,c),o.reappearWhenCallComesBack(a,r,i.timeout,i)}})},this)),this.lastMboxID=r},$addTargetPageParams:function(e,t,n){this.updateTargetPageParams(n)},generateID:function(){return"_sdsat_mbox_"+String(Math.random()).substring(2)+"_"},appendStyle:function(e){var n=t.getElementsByTagName("head")[0],a=t.createElement("style");return a.type="text/css",a.styleSheet?a.styleSheet.cssText=e:a.appendChild(t.createTextNode(e)),n.appendChild(a),a},reappearWhenCallComesBack:function(e,t,n,a){function i(){var e=r.styleElements[a.mboxGoesAround];e&&(e.parentNode.removeChild(e),delete r.styleElements[a.mboxGoesAround])}var r=this;D.cssQuery('script[src*="omtrdc.net"]',function(e){var t=e[0];if(t){D.scriptOnLoad(t.src,t,function(){D.notify("Test & Target: request complete",1),i(),clearTimeout(a)});var a=setTimeout(function(){D.notify("Test & Target: bailing after "+n+"ms",1),i()},n)}else D.notify("Test & Target: failed to find T&T ajax call, bailing",1),i()})},updateTargetPageParams:function(e){var t={};for(var n in e)e.hasOwnProperty(n)&&(t[D.replace(n)]=D.replace(e[n]));D.extend(this.targetPageParamsStore,t)},getTargetPageParams:function(){return this.targetPageParamsStore},setTargetPageParamsFunction:function(){e.targetPageParams=D.bind(this.getTargetPageParams,this)},parseTargetPageParamsResult:function(e){var t=e;return D.isArray(e)&&(e=e.join("&")),D.isString(e)&&(t=D.parseQueryParams(e)),t}}),D.availableTools.tnt=f,D.inherit(h,D.BaseTool),D.extend(h.prototype,{name:"Nielsen",endPLPhase:function(e){switch(e){case"pagetop":this.initialize();break;case"pagebottom":this.enableTracking&&(this.queueCommand({command:"sendFirstBeacon",arguments:[]}),this.flushQueueWhenReady())}},defineListeners:function(){this.onTabFocus=D.bind(function(){this.notify("Tab visible, sending view beacon when ready",1),this.tabEverVisible=!0,this.flushQueueWhenReady()},this),this.onPageLeave=D.bind(function(){this.notify("isHuman? : "+this.isHuman(),1),this.isHuman()&&this.sendDurationBeacon()},this),this.onHumanDetectionChange=D.bind(function(e){this==e.target.target&&(this.human=e.target.isHuman)},this)},initialize:function(){this.initializeTracking(),this.initializeDataProviders(),this.initializeNonHumanDetection(),this.tabEverVisible=D.visibility.isVisible(),this.tabEverVisible?this.notify("Tab visible, sending view beacon when ready",1):D.bindEventOnce("tabfocus",this.onTabFocus),this.initialized=!0},initializeTracking:function(){this.initialized||(this.notify("Initializing tracking",1),this.addRemovePageLeaveEvent(this.enableTracking),this.addRemoveHumanDetectionChangeEvent(this.enableTracking),this.initialized=!0)},initializeDataProviders:function(){var e,t=this.getAnalyticsTool();this.dataProvider.register(new h.DataProvider.VisitorID(D.getVisitorId())),t?(e=new h.DataProvider.Generic("rsid",function(){return t.settings.account}),this.dataProvider.register(e)):this.notify("Missing integration with Analytics: rsid will not be sent.")},initializeNonHumanDetection:function(){D.nonhumandetection?(D.nonhumandetection.init(),this.setEnableNonHumanDetection(0!=this.settings.enableNonHumanDetection),this.settings.nonHumanDetectionDelay>0&&this.setNonHumanDetectionDelay(1e3*parseInt(this.settings.nonHumanDetectionDelay))):this.notify("NHDM is not available.")},getAnalyticsTool:function(){if(this.settings.integratesWith)return D.tools[this.settings.integratesWith]},flushQueueWhenReady:function(){this.enableTracking&&this.tabEverVisible&&D.poll(D.bind(function(){if(this.isReadyToTrack())return this.flushQueue(),!0},this),100,20)},isReadyToTrack:function(){return this.tabEverVisible&&this.dataProvider.isReady()},$setVars:function(e,t,n){for(var a in n){var i=n[a];"function"==typeof i&&(i=i()),this.settings[a]=i}this.notify("Set variables done",2),this.prepareContextData()},$setEnableTracking:function(e,t,n){this.notify("Will"+(n?"":" not")+" track time on page",1),this.enableTracking!=n&&(this.addRemovePageLeaveEvent(n),this.addRemoveHumanDetectionChangeEvent(n),this.enableTracking=n)},$sendFirstBeacon:function(){this.sendViewBeacon()},setEnableNonHumanDetection:function(e){e?D.nonhumandetection.register(this):D.nonhumandetection.unregister(this)},setNonHumanDetectionDelay:function(e){D.nonhumandetection.register(this,e)},addRemovePageLeaveEvent:function(e){this.notify((e?"Attach onto":"Detach from")+" page leave event",1),D[0==e?"unbindEvent":"bindEvent"]("leave",this.onPageLeave)},addRemoveHumanDetectionChangeEvent:function(e){this.notify((e?"Attach onto":"Detach from")+" human detection change event",1),D[0==e?"unbindEvent":"bindEvent"]("humandetection.change",this.onHumanDetectionChange)},sendViewBeacon:function(){this.notify("Tracked page view.",1),this.sendBeaconWith()},sendDurationBeacon:function(){if(D.timetracking&&"function"==typeof D.timetracking.timeOnPage&&null!=D.timetracking.timeOnPage()){this.notify("Tracked close",1),this.sendBeaconWith({timeOnPage:Math.round(D.timetracking.timeOnPage()/1e3),duration:"D",timer:"timer"});var e;for(e=0;e<this.magicConst;e++)"0"}else this.notify("Could not track close due missing time on page",5)},sendBeaconWith:function(e){this.enableTracking&&this[this.beaconMethod].call(this,this.prepareUrl(e))},plainBeacon:function(e){var t=new Image;t.src=e,t.width=1,t.height=1,t.alt=""},navigatorSendBeacon:function(e){navigator.sendBeacon(e)},prepareUrl:function(e){var t=this.settings;return D.extend(t,this.dataProvider.provide()),D.extend(t,e),this.preparePrefix(this.settings.collectionServer)+this.adapt.convertToURI(this.adapt.toNielsen(this.substituteVariables(t)))},preparePrefix:function(e){return"//"+encodeURIComponent(e)+".imrworldwide.com/cgi-bin/gn?"},substituteVariables:function(e){var t={};for(var n in e)e.hasOwnProperty(n)&&(t[n]=D.replace(e[n]));return t},prepareContextData:function(){if(this.getAnalyticsTool()){var e=this.settings;e.sdkVersion=_satellite.publishDate,this.getAnalyticsTool().$setVars(null,null,{contextData:this.adapt.toAnalytics(this.substituteVariables(e))})}else this.notify("Adobe Analytics missing.")},isHuman:function(){return this.human},onTabFocus:function(){},onPageLeave:function(){},onHumanDetectionChange:function(){},notify:function(e,t){D.notify(this.logPrefix+e,t)},beaconMethod:"plainBeacon",adapt:null,enableTracking:!1,logPrefix:"Nielsen: ",tabEverVisible:!1,human:!0,magicConst:2e6}),h.DataProvider={},h.DataProvider.Generic=function(e,t){this.key=e,this.valueFn=t},D.extend(h.DataProvider.Generic.prototype,{isReady:function(){return!0},getValue:function(){return this.valueFn()},provide:function(){this.isReady()||h.prototype.notify("Not yet ready to provide value for: "+this.key,5);var e={};return e[this.key]=this.getValue(),e}}),h.DataProvider.VisitorID=function(e,t,n){this.key=t||"uuid",this.visitorInstance=e,this.visitorInstance&&(this.visitorId=e.getMarketingCloudVisitorID([this,this._visitorIdCallback])),this.fallbackProvider=n||new h.UUID},D.inherit(h.DataProvider.VisitorID,h.DataProvider.Generic),D.extend(h.DataProvider.VisitorID.prototype,{isReady:function(){return null===this.visitorInstance||!!this.visitorId},getValue:function(){return this.visitorId||this.fallbackProvider.get()},_visitorIdCallback:function(e){this.visitorId=e}}),h.DataProvider.Aggregate=function(){this.providers=[];for(var e=0;e<arguments.length;e++)this.register(arguments[e])},D.extend(h.DataProvider.Aggregate.prototype,{register:function(e){this.providers.push(e)},isReady:function(){return D.every(this.providers,function(e){return e.isReady()})},provide:function(){var e={};return D.each(this.providers,function(t){D.extend(e,t.provide())}),e}}),h.UUID=function(){},D.extend(h.UUID.prototype,{generate:function(){return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(e){var t=16*Math.random()|0;return("x"==e?t:3&t|8).toString(16)})},get:function(){var e=D.readCookie(this.key("uuid"));return e||(e=this.generate(),D.setCookie(this.key("uuid"),e),e)},key:function(e){return"_dtm_nielsen_"+e}}),h.DataAdapters=function(){},D.extend(h.DataAdapters.prototype,{toNielsen:function(e){var t=(new Date).getTime(),a={c6:"vc,",c13:"asid,",c15:"apn,",c27:"cln,",c32:"segA,",c33:"segB,",c34:"segC,",c35:"adrsid,",c29:"plid,",c30:"bldv,",c40:"adbid,"},i={ci:e.clientId,c6:e.vcid,c13:e.appId,c15:e.appName,prv:1,forward:0,ad:0,cr:e.duration||"V",rt:"text",st:"dcr",prd:"dcr",r:t,at:e.timer||"view",c16:e.sdkVersion,c27:e.timeOnPage||0,c40:e.uuid,c35:e.rsid,ti:t,sup:0,c32:e.segmentA,c33:e.segmentB,c34:e.segmentC,asn:e.assetName,c29:e.playerID,c30:e.buildVersion};for(key in i)if(i[key]!==n&&null!=i[key]&&i[key]!==n&&null!=i&&""!=i){var r=encodeURIComponent(i[key]);a.hasOwnProperty(key)&&r&&(r=a[key]+r),i[key]=r}return this.filterObject(i)},toAnalytics:function(e){return this.filterObject({"a.nielsen.clientid":e.clientId,"a.nielsen.vcid":e.vcid,"a.nielsen.appid":e.appId,"a.nielsen.appname":e.appName,"a.nielsen.accmethod":"0","a.nielsen.ctype":"text","a.nielsen.sega":e.segmentA,"a.nielsen.segb":e.segmentB,"a.nielsen.segc":e.segmentC,"a.nielsen.asset":e.assetName})},convertToURI:function(e){if(!1===D.isObject(e))return"";var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(n+"="+e[n]);return t.join("&")},filterObject:function(e){for(var t in e)!e.hasOwnProperty(t)||null!=e[t]&&e[t]!==n||delete e[t];return e}}),D.availableTools.nielsen=h,D.inherit(p,D.BaseTool),D.extend(p.prototype,{name:"SC",endPLPhase:function(e){e===this.settings.loadOn&&this.initialize(e)},initialize:function(t){if(!this._cancelToolInit)if(this.settings.initVars=this.substituteVariables(this.settings.initVars,{type:t}),!1!==this.settings.initTool){var n=this.settings.sCodeURL||D.basePath()+"s_code.js";"object"==typeof n&&(n="https:"===e.location.protocol?n.https:n.http),n.match(/^https?:/)||(n=D.basePath()+n),this.settings.initVars&&this.$setVars(null,null,this.settings.initVars),D.loadScript(n,D.bind(this.onSCodeLoaded,this)),this.initializing=!0}else this.initializing=!0,this.pollForSC()},getS:function(t,n){var a=n&&n.hostname||e.location.hostname,i=this.concatWithToolVarBindings(n&&n.setVars||this.varBindings),r=n&&n.addEvent||this.events,o=this.getAccount(a),s=e.s_gi;if(!s)return null;if(this.isValidSCInstance(t)||(t=null),!o&&!t)return D.notify("Adobe Analytics: tracker not initialized because account was not found",1),null;t=t||s(o);var c="D"+D.appVersion;return"undefined"!=typeof t.tagContainerMarker?t.tagContainerMarker=c:"string"==typeof t.version&&t.version.substring(t.version.length-5)!=="-"+c&&(t.version+="-"+c),t.sa&&!0!==this.settings.skipSetAccount&&!1!==this.settings.initTool&&t.sa(this.settings.account),this.applyVarBindingsOnTracker(t,i),r.length>0&&(t.events=r.join(",")),D.getVisitorId()&&(t.visitor=D.getVisitorId()),t},onSCodeLoaded:function(e){this.initialized=!0,this.initializing=!1;var t=["Adobe Analytics: loaded",e?" (manual)":"","."];D.notify(t.join(""),1),D.fireEvent(this.id+".load",this.getS()),e||(this.flushQueueExceptTrackLink(),this.sendBeacon()),this.flushQueue()},getAccount:function(t){return e.s_account?e.s_account:t&&this.settings.accountByHost&&this.settings.accountByHost[t]||this.settings.account},getTrackingServer:function(){var t=this,n=t.getS();if(n){if(n.ssl&&n.trackingServerSecure)return n.trackingServerSecure;if(n.trackingServer)return n.trackingServer}var a,i=t.getAccount(e.location.hostname);if(!i)return null;var r,o,s="",c=n&&n.dc;return(r=(a=i).indexOf(","))>=0&&(a=a.gb(0,r)),a=a.replace(/[^A-Za-z0-9]/g,""),s||(s="2o7.net"),c=c?(""+c).toLowerCase():"d1","2o7.net"==s&&("d1"==c?c="112":"d2"==c&&(c="122"),o=""),r=a+"."+c+"."+o+s},sendBeacon:function(){var t=this.getS(e[this.settings.renameS||"s"]);t?this.settings.customInit&&!1===this.settings.customInit(t)?D.notify("Adobe Analytics: custom init suppressed beacon",1):(this.settings.executeCustomPageCodeFirst&&this.applyVarBindingsOnTracker(t,this.varBindings),this.executeCustomSetupFuns(t),t.t(),this.clearVarBindings(),this.clearCustomSetup(),D.notify("Adobe Analytics: tracked page view",1)):D.notify("Adobe Analytics: page code not loaded",1)},pollForSC:function(){D.poll(D.bind(function(){if("function"==typeof e.s_gi)return this.onSCodeLoaded(!0),!0},this))},flushQueueExceptTrackLink:function(){if(this.pending){for(var e=[],t=0;t<this.pending.length;t++){var n=this.pending[t];"trackLink"===n[0].command?e.push(n):this.triggerCommand.apply(this,n)}this.pending=e}},isQueueAvailable:function(){return!this.initialized},substituteVariables:function(e,t){var n={};for(var a in e)if(e.hasOwnProperty(a)){var i=e[a];n[a]=D.replace(i,location,t)}return n},$setVars:function(e,t,n){for(var a in n)if(n.hasOwnProperty(a)){var i=n[a];"function"==typeof i&&(i=i()),this.varBindings[a]=i}D.notify("Adobe Analytics: set variables.",2)},$customSetup:function(e,t,n){this.customSetupFuns.push(function(a){n.call(e,t,a)})},isValidSCInstance:function(e){return!!e&&"function"==typeof e.t&&"function"==typeof e.tl},concatWithToolVarBindings:function(e){var t=this.settings.initVars||{};return D.map(["trackingServer","trackingServerSecure"],function(n){t[n]&&!e[n]&&(e[n]=t[n])}),e},applyVarBindingsOnTracker:function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])},clearVarBindings:function(){this.varBindings={}},clearCustomSetup:function(){this.customSetupFuns=[]},executeCustomSetupFuns:function(t){D.each(this.customSetupFuns,function(n){n.call(e,t)})},$trackLink:function(e,t,n){var a=(n=n||{}).type,i=n.linkName;!i&&e&&e.nodeName&&"a"===e.nodeName.toLowerCase()&&(i=e.innerHTML),i||(i="link clicked");var r=n&&n.setVars,o=n&&n.addEvent||[],s=this.getS(null,{setVars:r,addEvent:o});if(s){var c=s.linkTrackVars,l=s.linkTrackEvents,u=this.definedVarNames(r);n&&n.customSetup&&n.customSetup.call(e,t,s),o.length>0&&u.push("events"),s.products&&u.push("products"),u=this.mergeTrackLinkVars(s.linkTrackVars,u),o=this.mergeTrackLinkVars(s.linkTrackEvents,o),s.linkTrackVars=this.getCustomLinkVarsList(u);var d=D.map(o,function(e){return e.split(":")[0]});s.linkTrackEvents=this.getCustomLinkVarsList(d),s.tl(!0,a||"o",i),D.notify(["Adobe Analytics: tracked link ","using: linkTrackVars=",D.stringify(s.linkTrackVars),"; linkTrackEvents=",D.stringify(s.linkTrackEvents)].join(""),1),s.linkTrackVars=c,s.linkTrackEvents=l}else D.notify("Adobe Analytics: page code not loaded",1)},mergeTrackLinkVars:function(e,t){return e&&(t=e.split(",").concat(t)),t},getCustomLinkVarsList:function(e){var t=D.indexOf(e,"None");return t>-1&&e.length>1&&e.splice(t,1),e.join(",")},definedVarNames:function(e){e=e||this.varBindings;var t=[];for(var n in e)e.hasOwnProperty(n)&&/^(eVar[0-9]+)|(prop[0-9]+)|(hier[0-9]+)|campaign|purchaseID|channel|server|state|zip|pageType$/.test(n)&&t.push(n);return t},$trackPageView:function(e,t,n){var a=n&&n.setVars,i=n&&n.addEvent||[],r=this.getS(null,{setVars:a,addEvent:i});r?(r.linkTrackVars="",r.linkTrackEvents="",this.executeCustomSetupFuns(r),n&&n.customSetup&&n.customSetup.call(e,t,r),r.t(),this.clearVarBindings(),this.clearCustomSetup(),D.notify("Adobe Analytics: tracked page view",1)):D.notify("Adobe Analytics: page code not loaded",1)},$postTransaction:function(t,n,a){var i=D.data.transaction=e[a],r=this.varBindings,o=this.settings.fieldVarMapping;if(D.each(i.items,function(e){this.products.push(e)},this),r.products=D.map(this.products,function(e){var t=[];if(o&&o.item)for(var n in o.item)if(o.item.hasOwnProperty(n)){var a=o.item[n];t.push(a+"="+e[n]),"event"===a.substring(0,5)&&this.events.push(a)}var i=["",e.product,e.quantity,e.unitPrice*e.quantity];return t.length>0&&i.push(t.join("|")),i.join(";")},this).join(","),o&&o.transaction){var s=[];for(var c in o.transaction)if(o.transaction.hasOwnProperty(c)){a=o.transaction[c];s.push(a+"="+i[c]),"event"===a.substring(0,5)&&this.events.push(a)}r.products.length>0&&(r.products+=","),r.products+=";;;;"+s.join("|")}},$addEvent:function(){for(var e=2,t=arguments.length;e<t;e++)this.events.push(arguments[e])},$addProduct:function(){for(var e=2,t=arguments.length;e<t;e++)this.products.push(arguments[e])}}),D.availableTools.sc=p,D.inherit(v,D.BaseTool),D.extend(v.prototype,{name:"Default",$loadIframe:function(t,n,a){var i=a.pages,r=a.loadOn,o=D.bind(function(){D.each(i,function(e){this.loadIframe(t,n,e)},this)},this);r||o(),"domready"===r&&D.domReady(o),"load"===r&&D.addEventHandler(e,"load",o)},loadIframe:function(e,n,a){var i=t.createElement("iframe");i.style.display="none";var r=D.data.host,o=a.data,s=this.scriptURL(a.src),c=D.searchVariables(o,e,n);r&&(s=D.basePath()+s),s+=c,i.src=s;var l=t.getElementsByTagName("body")[0];l?l.appendChild(i):D.domReady(function(){t.getElementsByTagName("body")[0].appendChild(i)})},scriptURL:function(e){return(D.settings.scriptDir||"")+e},$loadScript:function(t,n,a){var i=a.scripts,r=a.sequential,o=a.loadOn,s=D.bind(function(){r?this.loadScripts(t,n,i):D.each(i,function(e){this.loadScripts(t,n,[e])},this)},this);o?"domready"===o?D.domReady(s):"load"===o&&D.addEventHandler(e,"load",s):s()},loadScripts:function(e,t,n){function a(){r.length>0&&i&&r.shift().call(e,t,o);var c=n.shift();if(c){var l=D.data.host,u=s.scriptURL(c.src);l&&(u=D.basePath()+u),i=c,D.loadScript(u,a)}}try{n=n.slice(0);var i,r=this.asyncScriptCallbackQueue,o=t.target||t.srcElement,s=this}catch(c){console.error("scripts is",D.stringify(n))}a()},$loadBlockingScript:function(e,t,n){var a=n.scripts;n.loadOn;D.bind(function(){D.each(a,function(n){this.loadBlockingScript(e,t,n)},this)},this)()},loadBlockingScript:function(e,t,n){var a=this.scriptURL(n.src),i=D.data.host,r=t.target||t.srcElement;i&&(a=D.basePath()+a),this.argsForBlockingScripts.push([e,t,r]),D.loadScriptSync(a)},pushAsyncScript:function(e){this.asyncScriptCallbackQueue.push(e)},pushBlockingScript:function(e){var t=this.argsForBlockingScripts.shift(),n=t[0];e.apply(n,t.slice(1))},$writeHTML:D.escapeHtmlParams(function(e,n){if(!D.domReadyFired&&t.write)if("pagebottom"===n.type||"pagetop"===n.type)for(var a=2,i=arguments.length;a<i;a++){var r=arguments[a].html;r=D.replace(r,e,n),t.write(r)}else D.notify("You can only use writeHTML on the `pagetop` and `pagebottom` events.",1);else D.notify("Command writeHTML failed. You should try appending HTML using the async option.",1)}),linkNeedsDelayActivate:function(t,n){n=n||e;var a=t.tagName,i=t.getAttribute("target"),r=t.getAttribute("href");return(!a||"a"===a.toLowerCase())&&(!!r&&(!i||"_blank"!==i&&("_top"===i?n.top===n:"_parent"!==i&&("_self"===i||(!n.name||i===n.name)))))},$delayActivateLink:function(e,t){if(this.linkNeedsDelayActivate(e)){D.preventDefault(t);var n=D.settings.linkDelay||100;setTimeout(function(){D.setLocation(e.href)},n)}},isQueueable:function(e){return"writeHTML"!==e.command}}),D.availableTools["default"]=v,D.inherit(b,D.BaseTool),D.extend(b.prototype,{initialize:function(){var e=this.settings;if(!1!==this.settings.initTool){var t=e.url;t="string"==typeof t?D.basePath()+t:D.isHttps()?t.https:t.http,D.loadScript(t,D.bind(this.onLoad,this)),this.initializing=!0}else this.initialized=!0},isQueueAvailable:function(){return!this.initialized},onLoad:function(){this.initialized=!0,this.initializing=!1,this.settings.initialBeacon&&this.settings.initialBeacon(),this.flushQueue()},endPLPhase:function(e){e===this.settings.loadOn&&(D.notify(this.name+": Initializing at "+e,1),this.initialize())},$fire:function(e,t,n){this.initializing?this.queueCommand({command:"fire",arguments:[n]},e,t):n.call(this.settings,e,t)}}),D.availableTools.am=b,D.availableTools.adlens=b,D.availableTools.aem=b,D.availableTools.__basic=b,D.extend(V.prototype,{getInstance:function(){return this.instance},initialize:function(){var e,t=this.settings;D.notify("Visitor ID: Initializing tool",1),null!==(e=this.createInstance(t.mcOrgId,t.initVars))&&(t.customerIDs&&this.applyCustomerIDs(e,t.customerIDs),t.autoRequest&&e.getMarketingCloudVisitorID(),this.instance=e)},createInstance:function(e,t){if(!D.isString(e))return D.notify('Visitor ID: Cannot create instance using mcOrgId: "'+e+'"',4),null;e=D.replace(e),D.notify('Visitor ID: Create instance using mcOrgId: "'+e+'"',1),t=this.parseValues(t);var n=Visitor.getInstance(e,t);return D.notify("Visitor ID: Set variables: "+D.stringify(t),1),n},applyCustomerIDs:function(e,t){var n=this.parseIds(t);e.setCustomerIDs(n),D.notify("Visitor ID: Set Customer IDs: "+D.stringify(n),1)},parseValues:function(e){if(!1===D.isObject(e))return{};var t={};for(var n in e)e.hasOwnProperty(n)&&(t[n]=D.replace(e[n]));return t},parseIds:function(e){var t={};if(!1===D.isObject(e))return{};for(var n in e)if(e.hasOwnProperty(n)){var a=D.replace(e[n].id);a!==e[n].id&&a&&(t[n]={},t[n].id=a,t[n].authState=Visitor.AuthState[e[n].authState])}return t}}),D.availableTools.visitor_id=V;var P={allowLinker:function(){return D.hasMultipleDomains()},cookieDomain:function(){var t=D.settings.domainList,n=D.find(t,function(t){var n=e.location.hostname;return D.equalsIgnoreCase(n.slice(n.length-t.length),t)});return n?"."+n:"auto"}};D.inherit(y,D.BaseTool),D.extend(y.prototype,{name:"GAUniversal",endPLPhase:function(e){e===this.settings.loadOn&&(D.notify("GAU: Initializing at "+e,1),this.initialize(),this.flushQueue(),this.trackInitialPageView())},getTrackerName:function(){return this.settings.trackerSettings.name||""},isPageCodeLoadSuppressed:function(){return!1===this.settings.initTool||!0===this._cancelToolInit},initialize:function(){if(this.isPageCodeLoadSuppressed())return this.initialized=!0,void D.notify("GAU: Page code not loaded (suppressed).",1);var t="ga"
;e[t]=e[t]||this.createGAObject(),e.GoogleAnalyticsObject=t,D.notify("GAU: Page code loaded.",1),D.loadScriptOnce(this.getToolUrl());var n=this.settings;(P.allowLinker()&&!1!==n.allowLinker?this.createAccountForLinker():this.createAccount(),this.executeInitCommands(),n.customInit)&&(!1===(0,n.customInit)(e[t],this.getTrackerName())&&(this.suppressInitialPageView=!0));this.initialized=!0},createGAObject:function(){var e=function(){e.q.push(arguments)};return e.q=[],e.l=1*new Date,e},createAccount:function(){this.create()},createAccountForLinker:function(){var e={};P.allowLinker()&&(e.allowLinker=!0),this.create(e),this.call("require","linker"),this.call("linker:autoLink",this.autoLinkDomains(),!1,!0)},create:function(e){var t=this.settings.trackerSettings;(t=D.preprocessArguments([t],location,null,this.forceLowerCase)[0]).trackingId=D.replace(this.settings.trackerSettings.trackingId,location),t.cookieDomain||(t.cookieDomain=P.cookieDomain()),D.extend(t,e||{}),this.call("create",t)},autoLinkDomains:function(){var e=location.hostname;return D.filter(D.settings.domainList,function(t){return t!==e})},executeInitCommands:function(){var e=this.settings;e.initCommands&&D.each(e.initCommands,function(e){var t=e.splice(2,e.length-2);e=e.concat(D.preprocessArguments(t,location,null,this.forceLowerCase)),this.call.apply(this,e)},this)},trackInitialPageView:function(){this.suppressInitialPageView||this.isPageCodeLoadSuppressed()||this.call("send","pageview")},call:function(){"function"==typeof ga?this.isCallSuppressed()||(arguments[0]=this.cmd(arguments[0]),this.log(D.toArray(arguments)),ga.apply(e,arguments)):D.notify("GA Universal function not found!",4)},isCallSuppressed:function(){return!0===this._cancelToolInit},$missing$:function(e,t,n,a){a=a||[],a=[e].concat(a),this.call.apply(this,a)},getToolUrl:function(){var e=this.settings,t=D.isHttps();return e.url?t?e.url.https:e.url.http:(t?"https://ssl":"http://www")+".google-analytics.com/analytics.js"},cmd:function(e){var t=["send","set","get"],n=this.getTrackerName();return n&&-1!==D.indexOf(t,e)?n+"."+e:e},log:function(e){var t="GA Universal: sent command "+e[0]+" to tracker "+(this.getTrackerName()||"default");if(e.length>1){D.stringify(e.slice(1));t+=" with parameters "+D.stringify(e.slice(1))}t+=".",D.notify(t,1)}}),D.availableTools.ga_universal=y,D.inherit(S,D.BaseTool),D.extend(S.prototype,{name:"GA",initialize:function(){var t=this.settings,n=e._gaq,a=t.initCommands||[],i=t.customInit;if(n||(_gaq=[]),this.isSuppressed())D.notify("GA: page code not loaded(suppressed).",1);else{if(!n&&!S.scriptLoaded){var r=D.isHttps(),o=(r?"https://ssl":"http://www")+".google-analytics.com/ga.js";t.url&&(o=r?t.url.https:t.url.http),D.loadScript(o),S.scriptLoaded=!0,D.notify("GA: page code loaded.",1)}t.domain;var s=t.trackerName,c=P.allowLinker(),l=D.replace(t.account,location);D.settings.domainList;_gaq.push([this.cmd("setAccount"),l]),c&&_gaq.push([this.cmd("setAllowLinker"),c]),_gaq.push([this.cmd("setDomainName"),P.cookieDomain()]),D.each(a,function(e){var t=[this.cmd(e[0])].concat(D.preprocessArguments(e.slice(1),location,null,this.forceLowerCase));_gaq.push(t)},this),i&&(this.suppressInitialPageView=!1===i(_gaq,s)),t.pageName&&this.$overrideInitialPageView(null,null,t.pageName)}this.initialized=!0,D.fireEvent(this.id+".configure",_gaq,s)},isSuppressed:function(){return this._cancelToolInit||!1===this.settings.initTool},tracker:function(){return this.settings.trackerName},cmd:function(e){var t=this.tracker();return t?t+"._"+e:"_"+e},$overrideInitialPageView:function(e,t,n){this.urlOverride=n},trackInitialPageView:function(){if(!this.isSuppressed()&&!this.suppressInitialPageView)if(this.urlOverride){var e=D.preprocessArguments([this.urlOverride],location,null,this.forceLowerCase);this.$missing$("trackPageview",null,null,e)}else this.$missing$("trackPageview")},endPLPhase:function(e){e===this.settings.loadOn&&(D.notify("GA: Initializing at "+e,1),this.initialize(),this.flushQueue(),this.trackInitialPageView())},call:function(e,t,n,a){if(!this._cancelToolInit){this.settings;var i=this.tracker(),r=this.cmd(e);a=a?[r].concat(a):[r];_gaq.push(a),i?D.notify("GA: sent command "+e+" to tracker "+i+(a.length>1?" with parameters ["+a.slice(1).join(", ")+"]":"")+".",1):D.notify("GA: sent command "+e+(a.length>1?" with parameters ["+a.slice(1).join(", ")+"]":"")+".",1)}},$missing$:function(e,t,n,a){this.call(e,t,n,a)},$postTransaction:function(t,n,a){var i=D.data.customVars.transaction=e[a];this.call("addTrans",t,n,[i.orderID,i.affiliation,i.total,i.tax,i.shipping,i.city,i.state,i.country]),D.each(i.items,function(e){this.call("addItem",t,n,[e.orderID,e.sku,e.product,e.category,e.unitPrice,e.quantity])},this),this.call("trackTrans",t,n)},delayLink:function(e,t){var n=this;if(P.allowLinker()&&e.hostname.match(this.settings.linkerDomains)&&!D.isSubdomainOf(e.hostname,location.hostname)){D.preventDefault(t);var a=D.settings.linkDelay||100;setTimeout(function(){n.call("link",e,t,[e.href])},a)}},popupLink:function(t,n){if(e._gat){D.preventDefault(n);var a=this.settings.account,i=e._gat._createTracker(a)._getLinkerUrl(t.href);e.open(i)}},$link:function(e,t){"_blank"===e.getAttribute("target")?this.popupLink(e,t):this.delayLink(e,t)},$trackEvent:function(e,t){var n=Array.prototype.slice.call(arguments,2);if(n.length>=4&&null!=n[3]){var a=parseInt(n[3],10);D.isNaN(a)&&(a=1),n[3]=a}this.call("trackEvent",e,t,n)}}),D.availableTools.ga=S,_satellite.init({tools:{"3a10ae6484ea1d49ac8fb2870195ac92358316a9":{engine:"aem",name:"AEM ContextHub",data_layer_root:"window.dataLayer"},"09c200b968fcb63620a679097df78bc3":{engine:"sc",loadOn:"pagebottom",account:"rc-uk,rcentral-dev",euCookie:!1,sCodeURL:"https://blog.corp.ringcentral.co.uk/blog/wp-content/themes/rcconnect_UK_2017/js/app-measurement.js",renameS:"s",initVars:{currencyCode:"USD",trackingServer:"som.ringcentral.com",trackingServerSecure:"som.ringcentral.com",pageName:"%PageName%",channel:"%siteSection%",trackInlineStats:!0,trackDownloadLinks:!1,trackExternalLinks:!1,linkLeaveQueryString:!1,dynamicVariablePrefix:"D=",eVar5:"%bmid (evar5)%",eVar6:"%pid (evar6)%",eVar7:"%aid (evar7)%",eVar8:"%oid (evar8)%",eVar12:"%cid (evar12)%",eVar61:"%Full URL (eVar61)%",eVar55:"%PageName%",eVar11:"%NotActiveUser (eVar11)%",eVar1:"PLR Default on-load pixel",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",eVar41:"%mcvid%",prop41:"D=v41"},executeCustomPageCodeFirst:!0,customInit:function(n){function a(a,i,r){if("undefined"!=typeof FireThirdPartyTags&&FireThirdPartyTags){void 0!==i&&null!=i||(i={});var o=t.cookie.match(new RegExp("(?:^|; )vid=([^;]*)"));n.prop41=n.eVar41=o?o[1]:"";var s=function(){var r;for(r in n.events=a,n.eVar55=e.location.protocol+"//"+e.location.host+e.location.pathname,i)i.hasOwnProperty(r)&&(n[r]=i[r]);for(r in"undefined"==typeof omnitureFire.count&&(omnitureFire.count=0),omnitureFire.count++,n.eVar54=e.location.toString()+":"+omniture_channel+":"+t.referrer+":ST:"+omnitureFire.count,n.t(),i)i.hasOwnProperty(r)&&(n[r]="");n.eVar54="",n.events=""};$.isNumeric(r)?setTimeout(s,r):s()}}function i(a,i,r,o){if("undefined"!=typeof FireThirdPartyTags&&FireThirdPartyTags){void 0!==i&&null!=i||(i={}),void 0!==r&&null!=r||(r={});var s=null;"undefined"!=typeof r.target&&!0!==r.target&&(s=r.target),void 0!==o&&void 0!==$(o).attr("href")&&(s=o);var c="o";"undefined"!=typeof r.linkType&&(c=r.linkType);var l="";"undefined"!=typeof r.linkName&&(l=r.linkName);var u=null;"undefined"!=typeof r.variableOverrides&&(u=r.variableOverrides);var d=null;"undefined"!=typeof r.doneAction&&(d=r.doneAction);var g=t.cookie.match(new RegExp("(?:^|; )vid=([^;]*)"));n.prop41=n.eVar41=g?g[1]:"",n.eVar55=e.location.protocol+"//"+e.location.host+e.location.pathname,n.events=a;var m="";for(var f in"undefined"!=typeof i.pageName&&(m=n.pageName),i)i.hasOwnProperty(f)&&(n[f]=i[f]);for(var h in"undefined"==typeof omnitureFireLink.count&&(omnitureFireLink.count=0),omnitureFireLink.count++,n.eVar54=e.location.toString()+":"+omniture_channel+":"+t.referrer+":STL:"+omnitureFireLink.count,n.linkTrackVars="channel,events",n)n.hasOwnProperty(h)&&(/^eVar/.test(h)&&(n.linkTrackVars+=","+h),/^prop/.test(h)&&(n.linkTrackVars+=","+h));for(f in n.linkTrackEvents=a,n.tl(s,c,l,u,d),i)i.hasOwnProperty(f)&&(n[f]="");""!=m&&(n.pageName=m),n.eVar54="",n.events=""}}function r(e){return this.locationObject=null,this.omniture_channel=null,this.tier=[],this.generalExceptions=[],this.concreteExceptions=[],this.hostnameExceptions=[],this.__construct(e||{})}var o,s;o=t.location.hostname,s=t.location.pathname,("www.ringcentral.com"!=o&&!o.match(/^(iad|sjc)[^\.]*\.ringcentral.com$/)||s.match(/\/sg(\/|$)/))&&(omnitureFire=a,omnitureFireLink=i),r.prototype={__construct:function(e){"undefined"!=typeof e.tier&&(this.tier=e.tier),"undefined"!=typeof e.generalExceptions&&(this.generalExceptions=e.generalExceptions),"undefined"!=typeof e.concreteExceptions&&(this.concreteExceptions=e.concreteExceptions),"undefined"!=typeof e.hostnameExceptions&&(this.hostnameExceptions=e.hostnameExceptions)},get2array:function(){var e={};if(this.locationObject.search)for(var t=this.locationObject.search.substr(1).split("&"),n=t.length;n--;)if(t[n])try{var a=t[n].replace(/\+/g,"%20").split("="),i=decodeURIComponent(a[0]).toLowerCase();"undefined"==typeof e[i]&&"undefined"!=typeof a[1]&&(e[i]=decodeURIComponent(a[1]))}catch(r){}return e},checkExceptions:function(e){var t=this.getPathname().toLowerCase();for(var n in e)for(var a=0;a<e[n].length;a++){var i=e[n][a];if("string"==typeof i){if(0===t.indexOf(i))return n}else if(i.test(t))return n}return!1},checkHostnameException:function(){var e=!1,t=this.locationObject.hostname.split(".").reverse();this.getPathname().toLowerCase();e:for(var n in this.hostnameExceptions){for(var a=n.split(".").reverse(),i=0;i<a.length;i++)if(a[i]!=t[i])continue e;if(!1!==(e=this.checkExceptions(this.hostnameExceptions[n])))return e}return e},get:function(n){this.omniture_channel="Mobile",void 0!==n&&n||(n=e.location);var a=t.createElement("a");a.href=n,this.locationObject=a,/\.ringcentral\.co\.uk$/i.test(this.locationObject.hostname)&&(this.omniture_channel="Other");var i=new String(this.getPathname()).substring(this.getPathname().lastIndexOf("/")+1);/.+_[a-z]\.html$/i.test(i)&&(this.locationObject.pathname=this.locationObject.pathname.replace(/_[a-z](\.html)$/i,"$1"));var r=this.get2array(),o=r.tier;o||(o=r.upgradetier),this.flag_fax=this.getPathname().toLowerCase().indexOf("fax"),this.flag_office=this.getPathname().toLowerCase().indexOf("office"),o&&(this.flag_tier_fax=this.tier.fax.join(".").indexOf(o),this.flag_tier_office=this.tier.office.join(".").indexOf(o),this.flag_tier_mobile=this.tier.mobile.join(".").indexOf(o)),this.flag_fax>=0&&(this.omniture_channel="Fax"),this.flag_office>=0&&(this.omniture_channel="Office"),o&&(this.flag_tier_fax>=0&&(this.omniture_channel="Fax"),this.flag_tier_office>=0&&(this.omniture_channel="Office"),this.flag_tier_mobile>=0&&(this.omniture_channel="Mobile"));var s=this.checkExceptions(this.generalExceptions);return!1!==s&&(this.omniture_channel=s),!1!==(s=this.checkExceptions(this.concreteExceptions))&&(this.omniture_channel=s),"/"==this.getPathname()&&(this.omniture_channel="Other"),!1!==(s=this.checkHostnameException())&&(this.omniture_channel=s),this.omniture_channel},getPathname:function(){var e=this.locationObject.pathname;return"/"!=e.substr(0,1)&&(e="/"+e),e}},omniture_obj=new r({generalExceptions:{Fax:["/fax-"],Office:["/cities/features/local-numbers/","/local-numbers/","/lp/","/features/local-numbers/"],Mobile:["/pro-"],Other:["/company/","/contact-center/","/contact-centre/","/events/","/legal","/partner/","/sg","/solutions/","/support","/webex/","/whyringcentral","/technology"]},concreteExceptions:{Fax:["/lp/msmarketplace.html"],Office:["/business-voip.html","/cloud-pbx.html","/compare/vonage.html","/internet-phone-service.html","/franchise_signup.html","/multi-line-phone-system.html","/resources/learning-center.html","/upgrade-to-office.html","/virtual-phone-service.html","/virtual-phone-system.html","/voip.html","/voip-phone.html","/voip-providers.html","/voip-services.html"],Mobile:["/prooffice.html","/pro/features/internet-fax-service/overview.html","/lp/800-business-number.html","/lp/800-number.html","/lp/800numbers.html","/lp/800service.html","/lp/866-business-number.html","/lp/877-business-number.html","/lp/auto-attendant.html","/lp/call-forwarding.html","/lp/call-screening.html","/lp/digitalline-voip-home.html","/lp/local-numbers.html","/lp/local-numbers/index.html","/lp/toll-free-numbers.html","/lp/unified-communications.html","/lp/vanitynumber.html","/lp/virtualphonenumber.html","/lp/voicemail.html"],Other:["/aff/contactcenter.html","/browsers.html","/collaborative-communications.html","/community/index.html","/compare/vonage.html","/datagovernance.html","/demo.html","/feedback/sales-contact.html","/fr/teams/overview.html","/googlepartner","/lp/barracuda-to-ringcentral.html","/lp/messaging-app-for-teams.html","/lp/google.html","/lp/pagoo.html","/lp/ringcentralreviews.html","/office/developer-platform.html","/platform/developer.html","/ringcentralglip.html","/search.html","/security-statement.html","/sitemap.html","/synergy-research.html","/teams/overview.html","/valueassessment.html","/voip-services.html","/vonagebusinesssolutions/index.html","/cookies.html","/hp.html",/^\/bps2rc$/,/gartner/i]},hostnameExceptions:{"affiliates.ringcentral.com":{Affiliates:["/"]},"affiliates.ringcentral.co.uk":{Affiliates:["/"]},"compare.ringcentral.com":{Other:["/8x8/index.html"]},"extremefax.com":{Fax:["/"]},"ringcentral.ca":{Mobile:["/lp/virtual-pbx.html"]},"attstage101.ringcentral.com":{Office:["/"]},"officeathand.att.com":{Office:["/"]},"meetings.btcloudphone.bt.com":{Meetings:["/"]},"meetings.businessconnect.telus.com":{Meetings:["/"]},"meetings.ringcentral.com":{Meetings:["/"]},"meetings-officeathand.att.com":{Meetings:["/"]},"webinar.ringcentral.com":{Meetings:["/"]},"glipdemo.com":{Glip:["/"]},"glip.com":{Glip:["/"]}},tier:{fax:[3281,3305,3306],office:[],mobile:[4441,4442,4443]}}),"undefined"==typeof omniture_channel&&(omniture_channel=omniture_obj.get()),t.cookie="omniture_channel="+omniture_channel+" ;path= /",t.cookie.indexOf("WRUID")>-1&&t.cookie.indexOf("_CT_RS_")>-1&&e.localStorage&&localStorage.getItem("ClicktalePID")&&localStorage.getItem("ClicktaleUID")&&(n.eVar71=localStorage.getItem("ClicktaleUID")+"."+localStorage.getItem("ClicktalePID"))}},d91fc6b25f2927643ba25707f10a3878cd608d40:{engine:"tnt",mboxURL:"7618e9029c8436bd1b1d926078af6c171547716a/mbox-contents-d91fc6b25f2927643ba25707f10a3878cd608d40.js",loadSync:!0,pageParams:{}},d01116032fdfdd6e342b0178a7170616d60018d5:{engine:"visitor_id",loadOn:"pagetop",name:"VisitorID",mcOrgId:"101A678254E6D3620A4C98A5@AdobeOrg",autoRequest:!0,initVars:{trackingServer:"som.ringcentral.com",trackingServerSecure:"som.ringcentral.com",marketingCloudServer:"som.ringcentral.com",marketingCloudServerSecure:"som.ringcentral.com"}}},pageLoadRules:[{name:"Blog Default pixel",trigger:[{engine:"sc",command:"setVars",arguments:[{eVar1:"PLR Blog Default pixel",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%pageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%pageName%",channel:"Blog",pageURL:"%pageName%"}]}],scope:{subdomains:{include:[/\/blog\//i]}},event:"pagetop"},{name:"DemandBaseAnalytics",trigger:[{command:"writeHTML",arguments:[{html:'<script src="//scripts.demandbase.com/adobeanalytics/fbfcc741.min.js"></script>\n<script src="//api.demandbase.com/api/v2/ip.json?key=0cf3ed9dbedf305226c060ed76481192&callback=Dmdbase_CDC.callback"></script>'}]}],event:"pagetop"},{name:"Demandbase Script Include",trigger:[{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5ad4825e64746d6a730051d4.js"}]}]}],scope:{URI:{include:[/.*/i]}},event:"pagebottom"},{name:"Google LP Redirect",trigger:[{engine:"sc",command:"setVars",arguments:[{eVar1:"PLR Google LP Redirect",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar30:"%ROI (eVar30)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar6:"%pid (evar6)%",eVar61:"%Full URL (eVar61)%",eVar7:"%aid (evar7)%",eVar72:"%Full URL (eVar61)%",eVar8:"%oid (evar8)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",eVar9:"%eVar9%",pageName:"%PageName%",channel:"%siteSection%",pageURL:"%PageName%"}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("PageName"),/\/lp\/google-app-installed/i)}],event:"pagetop"},{name:"Google LP Redirect (via BMID)",trigger:[{engine:"sc",command:"setVars",arguments:[{eVar1:"PLR Google LP Redirect (via BMID)",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar30:"%ROI (eVar30)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar6:"%pid (evar6)%",eVar61:"%Full URL (eVar61)%",eVar7:"%aid (evar7)%",eVar72:"%Full URL (eVar61)%",eVar8:"%oid (evar8)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",eVar9:"%eVar9%",pageName:"%PageName%",channel:"%siteSection%",pageURL:"%PageName%"}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Full URL (eVar61)"),/BMID=GOOGLEMARKETPLACE/i)}],event:"pagetop"}],rules:[{name:"/agent.html Partner With US #1",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/agent.html Partner With US #1",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1  - CTA buttons",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01964746d7553005ef0.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),"cta-buttons--1184929173")},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/agent.html Partner With US #2",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/agent.html Partner With US #2",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2  - CTA buttons",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553005f0a.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),"cta-buttons-1351435307")},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/how-it-works.html Contact Sales#1 CTA button",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/how-it-works.html Contact Sales#1 CTA button",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1 - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"Ireland",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/cta-buttons--1389536187/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/how-it-works.html Contact Sales#2 CTA button",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/how-it-works.html Contact Sales#2 CTA button",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2 - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5a09940e64746d703f005ef6.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/cta-buttons--481039694/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/platform/developer.html Three Block Buttons #1",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/platform/developer.html Three Block Buttons #1",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1 - blocks buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553005fad.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/three-blocks-1814859085/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/platform/developer.html Three Block Buttons #2",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/platform/developer.html Three Block Buttons #2",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2 - blocks buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553005fc9.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/three-blocks--1467907946/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Block Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Block Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - blocks buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01964746d7553005ed5.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/three-blocks/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/^(?!three-blocks-1814859085$).*$/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/^(?!three-blocks--1467907946$).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"CTA buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"CTA buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/cta-buttons/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/^(?!cta-buttons--1184929173|cta-buttons-1351435307|cta-buttons--1389536187|cta-buttons--481039694|cta-buttons--14234230|sticky-cta-buttons--292962470|cta-buttons--0000000001$).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"CTA buttons Exclusions",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"CTA buttons Exclusions",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/cta-buttons--000003|cta-buttons-1708456905|cta-buttons--1831768591|cta-buttons--1117216167|cta-buttons-1277178604|cta-buttons--1379456220|cta-buttons-817865150|cta-buttons-2124561285|cta-buttons--598593658|cta-buttons-0000000001|cta-buttons--26841941/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Country Selector",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Country Selector",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Country Selector",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/countrySelect/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"DEMO Pop-Up Video Tab Custom Event",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"DEMO Pop-Up Video Tab Custom Event",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar14:"Video Form",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event6"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5abdf89764746d5f29002690.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Name Custom Event"),"Video Form")},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Custom Event)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"DEMO/Basic form Success",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Demo/Basic Form Success",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar26:"%Form LS Success%",eVar27:"%Form LES Success%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Success% Form > Submitted",eVar56:"%Form Name Success%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Success%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",prop56:"D=v56",pageName:"%PageName% > %Form Name Success% Form > Submitted",channel:"%siteSection%",pageURL:"%PageName% > %Form Name Success% Form > Submitted"},addEvent:["event7"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Form Name Success"),/Basic|Demo/i)}],event:"dataelementchange(GUID Form Success)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"DataSheet Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"DataSheet Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - additional resources",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553005f25.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/^(datasheet-block|additional-resources)/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Default buttons (no postfixes)",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Default buttons (no postfixes)",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/accordion-component|carousel-minimized-card|carousel-card|plans-table-row--1410832164/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/^(?!navigation-float--731257532$|navigation-float-1941329674).*$/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Download buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Download buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Download Buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/download-buttons--1892731338/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Flex Menu buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Flex Menu buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - flex buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553006055.js"}]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/flex-item-card-513830604/i)}],
event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Float Navigation Menu",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Float Navigation Menu",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - mid. nav. menu",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01964746d7553005eb5.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/navigation-float/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Footer buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Footer buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - footer",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/footer/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Form Error",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Form Error",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Error% Form > Error",eVar56:"%Form Name Error%",eVar57:"%Form Error Fields%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Error%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",prop56:"D=v56",prop57:"D=v57",prop75:"D=v79",pageName:"%PageName% > %Form Name Error% Form > Error",channel:"%siteSection%",pageURL:"%PageName% > %Form Name Error% Form > Error"}}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Form Error 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Form Start",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Form Start",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Start% Form > Start",eVar56:"%Form Name Start%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Start%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",prop56:"D=v56",prop75:"D=v79",pageName:"%PageName% > %Form Name Start% Form > Start",channel:"%siteSection%",pageURL:"%PageName% > %Form Name Start% Form > Start"},addEvent:["event37"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Form Start 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Form Success",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Form Success",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar26:"%Form LS Success%",eVar27:"%Form LES Success%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Success% Form > Submitted",eVar56:"%Form Name Success%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Success%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",prop56:"D=v56",prop75:"D=v79",pageName:"%PageName% > %Form Name Success% Form > Submitted",channel:"%siteSection%",pageURL:"%PageName% > %Form Name Success% Form > Submitted"},addEvent:["event43","event50"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Form Success 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Get Glip now button #1",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Get Glip now button #1",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/glip-form--1987754167/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/glip-form-730532715/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Get Glip now button #2",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Get Glip now button #2",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/glip-form--1840285186/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/glip-form-730532715/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP Additional Resources buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51) 1% - additional resources",setVars:{eVar1:"HP Additional Resources buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1% - additional resources",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/flex-wrapper--000001|FlatLinkIcon--664035898|FlatLinkIcon-1811632119/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP Customer Experience Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"floating buttons  Contact Us",setVars:{eVar1:"HP Customer Experience Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1% - Customer Experience",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/flex-wrapper--000005/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/^(?!Watch Video: NakedWines.com Success Story$).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP Flex Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"flexWrapper",setVars:{eVar1:"HP Flex Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1% - flex buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/flex-wrapper--000000/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP Floating Buttons buttons TEMP",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"floating buttons  %Button Name (eVar51) 1%",setVars:{eVar1:"HP Floating Buttons buttons TEMP",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"floating buttons %Button Name (eVar51) 1%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/sticky-cta-buttons/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP Hero Carousel Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"floating buttons  Contact Us",setVars:{eVar1:"HP Hero Carousel Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/cta-buttons--000000|cta-buttons--000001|cta-buttons--000002/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP News Ticker ",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"WN: %Button Name (eVar51) 1%",setVars:{eVar1:"HP News Ticker",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"WN: %Button Name (eVar51) 1%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/newsTicker--730443519/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP Review Block Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"flexWrapper",setVars:{eVar1:"HP Review Block Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1% - Review Buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/reviewsBlock/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"HP and PnP buttons id-rc-rawhtml TEMP",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"HP and PnP buttons id-rc-rawhtml TEMP",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/id-rc-rawhtml/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Hero Carousel Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"floating buttons  Contact Us",setVars:{eVar1:"Hero Carousel Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/^(?!cta-buttons--598593658|cta-buttons-2124561285|cta-buttons--000003|cta-buttons-1708456905|cta-buttons--1117216167|cta-buttons--1831768591|cta-buttons--000000|cta-buttons--000001|cta-buttons--000002|cta-buttons--000010|cta-buttons--000011|cta-buttons--000012|cta-buttons--000013|cta-buttons--000014|cta-buttons--000015|cta-buttons--000016|cta-buttons--000017$|cta-buttons-1277178604|cta-buttons--1379456220|cta-buttons-817865150|cta-buttons-928679338|cta-buttons--26841941).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/cta-buttons--14234230|hero-image--1636941758/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Info buttons on hover",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Info buttons on hover",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:'info - "%Name Hover%"',eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Component Hover"),/plansWrap-2056109753|plans-table-row--1410832164/i)},function(){return _satellite.textMatch(_satellite.getVar("Name Hover"),/^(?!Entry|Standard|Premium$).*$/i)}],event:"dataelementchange(GUID Hover)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Info buttons on hover Plans",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Info buttons on hover Plans",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"info %Name Hover%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Component Hover"),"plansWrap-2056109753")},function(){return _satellite.textMatch(_satellite.getVar("Name Hover"),/Entry|Standard|Premium/i)}],event:"dataelementchange(GUID Hover)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Logo (temporary)",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Logo (temporary)",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"Logo - header",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553006039.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/RingCentral Virtual PBX, Phone and Internet Fax Service and Software/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Middle Navigation Menu",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Middle Navigation Menu",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - mid. nav. menu",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553005f43.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/flex-item-card/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/^(?!flex-item-card-1128674491|flex-item-card-1128478484|flex-item-card-1129882933|flex-item-card-1130017522|flex-item-card-513830604$).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"PnP buttons TEMP",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51) 1% - additional resources",setVars:{eVar1:"PnP buttons TEMP",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51) 1%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/cta-buttons--14234230|plans-and-pricing-575692595/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/^(?!cta-buttons--000000|cta-buttons--000001|cta-buttons--000002$|cta-buttons--26841941).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("pageName"),/office\/plansandpricing.html|\/office\/plansandpricing_b.html|\/office\/plansandpricing_c.html/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Proactive PopUp "No" (temporary)',trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:'Proactive PopUp "No" (temporary)',eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar60:"%Button Name (eVar51) 1%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event49"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/No, don't start/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Proactive PopUp "Yes" (temporary)',trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:'Proactive PopUp "Yes" (temporary)',eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar60:"%Button Name (eVar51) 1%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event39"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/Yes, start chat/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Sales Chat",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Sales Chat",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar35:"%Status PopUp (eVar35)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event24"]}]},{command:"loadScript",arguments:[{sequential:!0,scripts:[{src:"satellite-5b69b70c64746d2bdc00340d.js"}]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59fb503e64746d2eb5000b0e.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Name PopUp"),/Live Chat/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID PopUp)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Sitemap Links",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Sitemap Links",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - sitemap links",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/sitemap/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Snippet Browser buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Snippet Browser buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Snippet Browser buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/snippets-browser/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Technology Block Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Technology Block Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Technology Block",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/technology-block/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Text Links",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Text Links",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - text links",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/^(leadform-regular|id-rc-textimage-334577456|text-block|legal-text|text-image|responsive-swiper|leadform-general)/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Top Navigation Menu "header"',trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:'Top Navigation Menu "header"',eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - header",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01964746d7553005e99.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/header/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/^(?!RingCentral Virtual PBX, Phone and Internet Fax Service and Software$).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Top Navigation Menu "navigation-primary"',trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:'Top Navigation Menu "navigation-primary"',eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - top nav. menu",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01964746d7553005e25.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/navigation-primary/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Top Navigation Menu "navigation-primary" Hover',trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:'Top Navigation Menu "navigation-primary" Hover',eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Name Hover% - top nav. menu - Hover",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5b16945a64746d41c5004689.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Hover"),/navigation-primary-grid/i)}],event:"dataelementchange(GUID Hover)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video 25%",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video 25%",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32) 1%",eVar41:"%mcvid%",eVar48:"%Video Stop Section%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName% > %Video Finished Name (eVar32) 1% > 25% Finished",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event27"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone 1"),/25/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video 50%",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video 50%",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32) 1%",eVar41:"%mcvid%",eVar48:"%Video Stop Section%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%pageName% > %Video Finished Name (eVar32) 1% > 50% Finished",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event28"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone 1"),/50/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video 75%",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video 75%",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32) 1%",eVar41:"%mcvid%",eVar48:"%Video Stop Section%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName% > %Video Finished Name (eVar32) 1% > 75% Finished",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event29"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone 1"),/75/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Video Buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - video button",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59f0b01a64746d7553006001.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/(youtube-video-card|flex-wrapper-2056109753|FlatYoutubeThumbnail-158967350|FullPageVideoCarousel-112534729|Audio Conferencing|Watch how to use RingCentral Rooms|Watch how to configure RingCentral Rooms)/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click 1"),/^(?!flex-wrapper--000000|flex-wrapper--000001|$).*$/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/^(?!NakedWines.com moves to the cloud.$).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video Finish",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video Finish",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32) 1%",eVar41:"%mcvid%",eVar48:"%Video Stop Section%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName% > %Video Finished Name (eVar32) 1% > 100% Finished",channel:"%siteSection%",pageURL:"%PageName%"
},addEvent:["event14"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone 1"),/100/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video Start",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video Start",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar32:"%Video Start Name (eVar32) 1%",eVar41:"%mcvid%",eVar48:"%Video Play Section%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName% > %Video Start Name (eVar32) 1% > Start",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event13"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Start 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"View Demo button (event4)",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"View Demo button (event4)",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar14:"%Button Name (eVar51) 1%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event4"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5a01bbcc64746d0bbd002654.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/view-demo--623061733/i)},function(){return _satellite.textMatch(_satellite.getVar("Element Click 1"),/button/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/Mobile Demo/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"View Demo button (event6)",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"View Demo button (event6)",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar14:"%Button Name (eVar51) 1%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event6"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5a049eff64746d703f0047ca.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/view-demo--623061733/i)},function(){return _satellite.textMatch(_satellite.getVar("Element Click 1"),/button/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/Video Demo/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"View Demo buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"View Demo buttons",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5a16dc2164746d2da9001851.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/view-demo--623061733/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51) 1"),/Start your free trial/i)},function(){return _satellite.textMatch(_satellite.getVar("Element Click 1"),/button/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"View Demo text links",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"View Demo text links",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - text links",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5a045b9864746d7039004c66.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/view-demo--623061733|live-chat-offline-form--1257714414|page--249736276/i)},function(){return _satellite.textMatch(_satellite.getVar("Element Click 1"),/link/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Wrapper plans'n'pricing tool",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Wrapper plans'n'pricing tool",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"Wrapper - %Button Name (eVar51) 1%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"%siteSection%",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/plansWrap/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("Component Click 1"),/^(?!plans-table-row--1410832164 ).*$/i)}],event:"dataelementchange(GUID Click 1)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1}],directCallRules:[{name:"GW_ChatFormOpen",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"DCR: Sales Chat Open",eVar11:"%NotActiveUser (eVar11)%",eVar12:"%cid (evar12)%",eVar30:"%ROI (eVar30)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > Offline Chat Form > Open",eVar56:"Offline Chat",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName% > Offline Chat Form > Open",channel:"%siteSection%",pageURL:"%PageName% > Offline Chat Form > Open"},addEvent:["event42"]}]}]}],settings:{trackInternalLinks:!0,libraryName:"satelliteLib-aed35a9669ef96a6ae2c7fe7c9d0155d4c489109",isStaging:!1,allowGATTcalls:!1,downloadExtensions:/\.(?:doc|docx|eps|jpg|png|svg|xls|ppt|pptx|pdf|xlsx|tab|csv|zip|txt|vsd|vxd|xml|js|css|rar|exe|wma|mov|avi|wmv|mp3|wav|m4v)($|\&|\?)/i,notifications:!1,utilVisible:!1,domainList:["ringcentral.co.uk"],scriptDir:"7618e9029c8436bd1b1d926078af6c171547716a/scripts/",tagTimeout:3e3},data:{URI:t.location.pathname+t.location.search,browser:{},cartItems:[],revenue:"",host:{http:"assets.adobedtm.com",https:"assets.adobedtm.com"}},dataElements:{"Additional Info Hover":{contextHub:function(){try{return e.dataLayer.events.hover.additional.info}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"aid (evar7)":{contextHub:function(){try{return e.dataLayer.marketingIDs.aid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"bmid (evar5)":{contextHub:function(){try{return e.dataLayer.marketingIDs.bmid_lead}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"session"},"BMID Lead First Touch (eVar85)":{contextHub:function(){try{return e.dataLayer.marketingIDs.bmid_lead}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Button Name (eVar51)":{contextHub:function(){try{return e.dataLayer.events.click.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Button Name (eVar51) 1":{contextHub:function(){try{return e.dataLayer.events.click.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"cid (evar12)":{contextHub:function(){try{return e.dataLayer.marketingIDs.cid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"session"},"Component Click":{contextHub:function(){try{return e.dataLayer.events.click.component}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Click 1":{contextHub:function(){try{return e.dataLayer.events.click.component}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Hover":{contextHub:function(){try{return e.dataLayer.events.hover.component}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Search":{contextHub:function(){try{return e.dataLayer.events.hover.component}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Element Click":{contextHub:function(){try{return e.dataLayer.events.click.element}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Element Click 1":{contextHub:function(){try{return e.dataLayer.events.click.element}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Element Hover":{contextHub:function(){try{return e.dataLayer.events.hover.element}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Error Fields":{contextHub:function(){try{return e.dataLayer.events.form.error.fields}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Error Fields 1":{contextHub:function(){try{return e.dataLayer.events.form.error.fields}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form LES Success":{contextHub:function(){try{return e.dataLayer.events.form.success.les}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form LES Success 1":{contextHub:function(){try{return e.dataLayer.events.form.success.les}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form LS Success":{contextHub:function(){try{return e.dataLayer.events.form.success.ls}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form LS Success 1":{contextHub:function(){try{return e.dataLayer.events.form.success.ls}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Error":{contextHub:function(){try{return e.dataLayer.events.form.error.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Error 1":{contextHub:function(){try{return e.dataLayer.events.form.error.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Start":{contextHub:function(){try{return e.dataLayer.events.form.start.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Start 1":{contextHub:function(){try{return e.dataLayer.events.form.start.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Success":{contextHub:function(){try{return e.dataLayer.events.form.success.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Success 1":{contextHub:function(){try{return e.dataLayer.events.form.success.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Error":{contextHub:function(){try{return e.dataLayer.events.form.error.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Error 1":{contextHub:function(){try{return e.dataLayer.events.form.error.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Start":{contextHub:function(){try{return e.dataLayer.events.form.start.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Start 1":{contextHub:function(){try{return e.dataLayer.events.form.start.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Success":{contextHub:function(){try{return e.dataLayer.events.form.success.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Success 1":{contextHub:function(){try{return e.dataLayer.events.form.success.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Full URL (eVar61)":{customJS:function(){return location.href},storeLength:"pageview"},"Full URL (eVar61) - July 09, 2018 06:51:14 AM":{customJS:function(){return location.href},storeLength:"pageview"},"GUID Click":{contextHub:function(){try{return e.dataLayer.events.click.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Click 1":{contextHub:function(){try{return e.dataLayer.events.click.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Custom Event":{contextHub:function(){try{return e.dataLayer.events.custom.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Error":{contextHub:function(){try{return e.dataLayer.events.form.error.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Error 1":{contextHub:function(){try{return e.dataLayer.events.form.error.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Start":{contextHub:function(){try{return e.dataLayer.events.form.start.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Start 1":{contextHub:function(){try{return e.dataLayer.events.form.start.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Success":{contextHub:function(){try{return e.dataLayer.events.form.success.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Success 1":{contextHub:function(){try{return e.dataLayer.events.form.success.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Hover":{contextHub:function(){try{return e.dataLayer.events.hover.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID PopUp":{contextHub:function(){try{return e.dataLayer.events.popup.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Search":{contextHub:function(){try{return e.dataLayer.events.search.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Video Start 1":{contextHub:function(){try{return e.dataLayer.events.video.play.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Video Stop 1":{contextHub:function(){try{return e.dataLayer.events.video.stop.guid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Hero Carousel Slide Name Click (eVar49)":{contextHub:function(){try{return e.dataLayer.events.click.additional.info}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Click":{contextHub:function(){try{return e.dataLayer.events.click.instance}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Click 1":{contextHub:function(){try{return e.dataLayer.events.click.instance}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Hover":{contextHub:function(){try{return e.dataLayer.events.hover.instance}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Search":{contextHub:function(){try{return e.dataLayer.events.search.instance}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},mcvid:{cookie:"vid",storeLength:"pageview"},"Name Custom Event":{contextHub:function(){try{return e.dataLayer.events.custom.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Name Hover":{contextHub:function(){try{return e.dataLayer.events.hover.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Name PopUp":{contextHub:function(){try{return e.dataLayer.events.popup.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Name Search":{contextHub:function(){try{return e.dataLayer.events.search.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"NotActiveUser (eVar11)":{contextHub:function(){try{return e.dataLayer.user.NotActiveUser}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"oid (evar8)":{contextHub:function(){try{return e.dataLayer.marketingIDs.oid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Old BMID (eVar86)":{contextHub:function(){try{return e.dataLayer.marketingIDs.bmid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},pageName:{customJS:function(){return location.origin+location.pathname},storeLength:"pageview"},PageName:{customJS:function(){return location.origin+location.pathname},storeLength:"pageview"},"PageName - July 09, 2018 06:51:14 AM":{customJS:function(){return location.origin+location.pathname},storeLength:"pageview"},"pid (evar6)":{contextHub:function(){try{return e.dataLayer.marketingIDs.pid}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Quantity Search (eVar31)":{contextHub:function(){try{return e.dataLayer.events.search.additional.quantity}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Query Search (eVar29)":{contextHub:function(){try{return e.dataLayer.events.search.additional.query}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"ROI (eVar30)":{contextHub:function(){try{return e.dataLayer.user.role}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},siteSection:{cookie:"omniture_channel",storeLength:"pageview"},"Status PopUp (eVar35)":{contextHub:function(){try{return e.dataLayer.events.popup.status}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Tag Click":{contextHub:function(){try{return e.dataLayer.events.click.tag}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Tag Hover":{contextHub:function(){try{return e.dataLayer.events.hover.tag}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Tag Search":{contextHub:function(){try{return e.dataLayer.events.hover.tag}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"URL Click":{contextHub:function(){try{return e.dataLayer.events.click.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"URL Hover":{contextHub:function(){try{return e.dataLayer.events.hover.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},urlPath:{customJS:function(){return location.pathname},storeLength:"pageview"},"URL Search":{contextHub:function(){try{return e.dataLayer.events.search.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Finished (Event14)":{contextHub:function(){try{return e.dataLayer.events.video.stop}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Finished (Event14) 1":{contextHub:function(){try{return e.dataLayer.events.video.stop}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Finished Name (eVar32)":{contextHub:function(){try{return e.dataLayer.events.video.stop.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Finished Name (eVar32) 1":{contextHub:function(){try{return e.dataLayer.events.video.stop.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Milestone":{contextHub:function(){try{return e.dataLayer.events.video.stop.milestone}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Milestone 1":{contextHub:function(){try{return e.dataLayer.events.video.stop.progress}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Milestone - October 26, 2017 02:27:54 AM":{contextHub:function(){try{return e.dataLayer.events.video.stop.milestone}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Milestone - October 26, 2017 02:28:08 AM":{contextHub:function(){try{return e.dataLayer.events.video.stop.milestone}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Play Section":{contextHub:function(){try{return e.dataLayer.events.video.play.section}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start (Event13)":{contextHub:function(){try{return e.dataLayer.events.video.play}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start (Event13) 1":{contextHub:function(){try{return e.dataLayer.events.video.play}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start Name (eVar32)":{contextHub:function(){try{return e.dataLayer.events.video.play.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start Name (eVar32) 1":{contextHub:function(){try{return e.dataLayer.events.video.play.name}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start URL":{contextHub:function(){try{return e.dataLayer.events.video.play.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start URL 1":{contextHub:function(){try{return e.dataLayer.events.video.play.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Stop Section":{contextHub:function(){try{return e.dataLayer.events.video.stop.section}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Stop URL":{contextHub:function(){try{return e.dataLayer.events.video.stop.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Stop URL 1":{contextHub:function(){try{return e.dataLayer.events.video.stop.url}catch(t){D.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"}},appVersion:"7QN",buildDate:"2018-08-08 14:14:12 UTC",publishDate:"2018-08-08 14:14:11 UTC"})}(window,document);
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setCookie(name, value, expires, path, domain, secure) {
		name = name.toLowerCase();
		if (!path) {
			path = '/';
		}
		if (typeof expires != 'number') {
			expires = '';
		} else {
			var expires_date = new Date((new Date()).getTime() + (expires * 24 * 60 * 60 * 1000));
			expires = ' expires=' + expires_date.toGMTString() + ';';
		}
		var d = (domain) ? ';domain=' + domain : '';
		var s = (secure) ? ";secure" : "";
		document.cookie = name + "=" + value + ";" + expires + " Path=" + path + d + s;
}

function get_domain_cookie() {
	var host = window.location.hostname;
	if(host == 'localhost') return host;
	var _domain = host.split('.');
	_domain.shift();
	if (_domain[0] == 'secure') {
		_domain.shift();
	}
	domain = '.' + _domain.join('.');
	return domain;
}