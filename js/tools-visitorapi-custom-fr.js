(function($){
	
// Cookies Manager
var CookiesManager = {
	setCookie: function(name, value, expires, path, domain, secure) {
		name = name.toLowerCase();
		if (!path) {
			path = '/';
		}
		if (typeof expires != 'number') {
			expires = '';
		} else {
			var expires_date = new Date((new Date()).getTime() + (expires * 24 * 60 * 60 * 1000));
			expires = ' expires=' + expires_date.toGMTString() + ';';
		}
		var d = (domain) ? ';domain=' + domain : '';
		var s = (secure) ? ";secure" : "";
		document.cookie = name + "=" + value + ";" + expires + " Path=" + path + d + s;
	},
	getCookie: function(name) {
		var c = document.cookie;
		var matches = c.match(new RegExp("(?:^|; )" + name + "=([^;]*)", "i"));
		return matches ? decodeURIComponent(matches[1]) : null;
	},
	attachCookie: function(cookieName, str) {
		var value = CookiesManager.getCookie(cookieName);
		var re = new RegExp("(&|\\\?)" + cookieName + "=");
		if (value && !str.match(re)) {
			str += '&' + cookieName + '=' + value;
		}
		return str;
	},
	delCookie: function(name, path, domain) {
		document.cookie = name + "=" +
			((path) ? ";path=" + path : "") +
			((domain) ? ";domain=" + domain : "") +
			";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	},
}

var GetParamsManager = {
	is_set: function(varName) {
		var regexS = "[?&]" + varName + "=([^&#]*)";
		var regex = new RegExp(regexS, 'i');
		var tmpURL = window.location.href;
		var results = regex.exec(tmpURL);
		if (results == null)
			return null;
		else
			return decodeURIComponent(results[1]);
	},
	get2array: function() {
		var ret = {};
		if (window.location.search) {
			var temp = window.location.search.substr(1).split('&');
			var i = temp.length;
			while (i--)
				if (temp[i]) {
					try {
						var tmp = temp[i].replace(/\+/g, "%20").split('=');
						var key = decodeURIComponent(tmp[0]).toLowerCase();
						if (typeof ret[key] == 'undefined' && typeof tmp[1] != 'undefined')
							ret[key] = decodeURIComponent(tmp[1]);
					} catch (e) {
						// incorrect parameter is not saved in cookie
					}
				}
		}
		return ret;
	}
}

//GWTS-623
;
(function() {

	// php functions

	function serialize(mixed_value) {
		//		note: We feel the main purpose of this function should be to ease the transport of data between php & js
		//		note: Aiming for PHP-compatibility, we have to translate objects to arrays
		//	example 1: serialize(['Kevin', 'van', 'Zonneveld']);
		//	returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
		//	example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});
		//	returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'

		var val, key, okey,
			ktype = '',
			vals = '',
			count = 0,
			_utf8Size = function(str) {
				var size = 0,
					i = 0,
					l = str.length,
					code = '';
				for (i = 0; i < l; i++) {
					code = str.charCodeAt(i);
					if (code < 0x0080) {
						size += 1;
					} else if (code < 0x0800) {
						size += 2;
					} else {
						size += 3;
					}
				}
				return size;
			};
		_getType = function(inp) {
			var match, key, cons, types, type = typeof inp;

			if (type === 'object' && !inp) {
				return 'null';
			}
			if (type === 'object') {
				if (!inp.constructor) {
					return 'object';
				}
				cons = inp.constructor.toString();
				match = cons.match(/(\w+)\(/);
				if (match) {
					cons = match[1].toLowerCase();
				}
				types = ['boolean', 'number', 'string', 'array'];
				for (key in types) {
					if (cons == types[key]) {
						type = types[key];
						break;
					}
				}
			}
			return type;
		};
		type = _getType(mixed_value);

		switch (type) {
			case 'function':
				val = '';
				break;
			case 'boolean':
				val = 'b:' + (mixed_value ? '1' : '0');
				break;
			case 'number':
				val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
				break;
			case 'string':
				val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
				break;
			case 'array':
			case 'object':
				val = 'a';
				/*
				 if (type === 'object') {
				 var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
				 if (objname == undefined) {
				 return;
				 }
				 objname[1] = this.serialize(objname[1]);
				 val = 'O' + objname[1].substring(1, objname[1].length - 1);
				 }
				 */

				for (key in mixed_value) {
					if (mixed_value.hasOwnProperty(key)) {
						ktype = _getType(mixed_value[key]);
						if (ktype === 'function') {
							continue;
						}

						okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
						vals += serialize(okey) + serialize(mixed_value[key]);
						count++;
					}
				}
				val += ':' + count + ':{' + vals + '}';
				break;
			case 'undefined':
				// Fall-through
			default:
				// if the JS object has a property which contains a null value, the string cannot be unserialized by PHP
				val = 'N';
				break;
		}
		if (type !== 'object' && type !== 'array') {
			val += ';';
		}
		return val;
	}

	// Parses the string into variables
	function parse_str(str) {
		if (str.indexOf('?') != -1)
			str = str.replace('?', '');
		var separator = '=',
			glue = '&',
			query_array = str.split(glue),
			result = [];

		for (var x = 0; x < query_array.length; x++) {
			var tmp = query_array[x].split(separator);
			result[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
		}

		return result;
	}


	function parse_url(str, component) {
		//	note: Does not replace invalid characters with '_' as in PHP, nor does it return false with  
		//	note: Besides function name, is essentially the same as parseUri as well as our allowing
		//	note: an extra slash after the scheme/protocol (to allow file:/// as in PHP)
		//	example 1: parse_url('http://username:password@hostname/path?arg=value#anchor');
		//	returns 1: {scheme: 'http', host: 'hostname', user: 'username', pass: 'password', path: '/path', query: 'arg=value', fragment: 'anchor'}

		var query, key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port',
			'relative', 'path', 'directory', 'file', 'query', 'fragment'
		],
			ini = (this.php_js && this.php_js.ini) || {},
			mode = (ini['phpjs.parse_url.mode'] && ini['phpjs.parse_url.mode'].local_value) || 'php',
			parser = {
				php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-scheme to catch file:/// (should restrict this)
			};

		var m = parser[mode].exec(str),
			uri = {},
			i = 14;
		while (i--) {
			if (m[i]) {
				uri[key[i]] = m[i];
			}
		}

		if (component) {
			return uri[component.replace('PHP_URL_', '').toLowerCase()];
		}
		if (mode !== 'php') {
			var name = (ini['phpjs.parse_url.queryKey'] && ini['phpjs.parse_url.queryKey'].local_value) || 'queryKey';
			parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
			uri[name] = {};
			query = uri[key[12]] || '';
			query.replace(parser, function($0, $1, $2) {
				if ($1) {
					uri[name][$1] = $2;
				}
			});
		}
		delete uri.source;

		return uri;
	}

	function getQueryParams(qs) {
		qs = qs.split("+").join(" ");
		var params = {}, tokens,
			re = /[?&]?([^=]+)=([^&]*)/g;

		while (tokens = re.exec(qs)) {
			params[decodeURIComponent(tokens[1]).toLowerCase()] = decodeURIComponent(tokens[2]);
		}

		return params;
	}

	function checkOtm() {
		var partnerId = null;
		if (document.referrer)
			partnerId = document.referrer.match(/partnerid=([^\&]+)(&|$)/i);

		if (partnerId)
			return partnerId[1];

		partnerId = getQueryParams(window.location.search).partnerid;
		if (!partnerId) {
			if (CookiesManager.getCookie('partnerid'))
				partnerId = CookiesManager.getCookie('partnerid');
			else if (CookiesManager.getCookie('gw_partnerid'))
				partnerId = CookiesManager.getCookie('gw_partnerid');
		}

		return partnerId;
	}

	function keysToLowerCase(arr) {
		if (!Object.keys(arr).length)
			return;

		var result = [];

		for (var key in arr) {
			result[key.toLowerCase()] = arr[key];
		}

		return result;
	}

	function setOtm() {
		var partnerId = checkOtm(),
			referer = document.referrer,
			pid = '',
			bmid = '',
			sid = '',
			$_ref = '',
			resultCookie = {},
			get = keysToLowerCase(parse_str(window.location.search));

		if (!partnerId)
			return false;

		referer = parse_url(referer);

		if (!referer['query'])
			referer['query'] = '';
		$_ref = keysToLowerCase(parse_str(referer['query']));

		if ($_ref['pid'])
			pid = $_ref['pid'];
		if ($_ref['bmid'])
			bmid = $_ref['bmid'];
		//GW-16685
		if ($_ref['tduid'])
			sid = $_ref['tduid'];
		else if (CookiesManager.getCookie('gw_tduid'))
			sid = CookiesManager.getCookie('gw_tduid');
		else if ($_ref['sid'])
			sid = $_ref['sid'];

		if (!pid) {
			if (get['pid'])
				pid = get['pid'];
			else if (CookiesManager.getCookie('gw_pid'))
				pid = CookiesManager.getCookie('gw_pid');
		}

		if (!bmid) {
			if (get['bmid'])
				bmid = get['bmid'];
			else if (CookiesManager.getCookie('gw_bmid'))
				bmid = CookiesManager.getCookie('gw_bmid');
		}

		if (get['sid'])
			sid = get['sid'];

		resultCookie = {
			'partnerid': partnerId,
			'bmid': bmid,
			'pid': pid,
			'sid': sid
		};

		var domain = get_domain_cookie();

		CookiesManager.setCookie('gw_otm', encodeURIComponent(serialize(resultCookie)), '', "/", domain);
	}

	setOtm();

})();

function $_(id) {
	return document.getElementById(id);
}

function hash_to_string(hash)
{
	arr = [];
	for (key in hash) {
		try {
			arr.push(key + ':' + hash[key]);
		} catch (e) {
		}
	}
	return arr.join(',');
}



Function.prototype.rcBind = function(obj)
{
	var m = this;
	var a = arguments.length > 1 ? arguments[1] : {};
	return function(e)
	{
		if (arguments.length)
		{
			if (arguments[0].srcElement || arguments[0].target)
			{
				a['oEvent'] = arguments[0];
			}
			else
			{
				for (var key in arguments[0])
				{
					a[key] = arguments[0][key];
				}
			}

		}
		return m.call(obj, a);
	}
}

Function.prototype.rcBindAsIs = function(obj)
{
	var method = this;
	var arg = [];
	for (var i = 1; i < arguments.length; i++)
	{
		arg.push(arguments[i]);
	}
	return function()
	{
		if (arguments.length)
		{
			for (var i = 0; i < arguments.length; i++)
			{
				arg.push(arguments[i]);
			}
		}
		var res = null;
		//try
		//{
		res = method.apply(obj, arg);
		//}
		//catch(e){}
		if (arguments.length)
		{
			for (var i = 0; i < arguments.length; i++)
			{
				arg.pop();
			}
		}
		return res;
	}
}


function addEvent(obj, ev, func, useCapture)
{
	useCapture = !useCapture ? false : true;

	//alert(useCapture);

	if (ev == 'ondomready')
	{
		onDomReady(func);
		return;
	}
	if (ev != 'onunload') {
		window.addEvent(window, 'onunload', function() {
			window.removeEvent(obj, ev, func);
			func = null;
		})
	}

	if (window.addEventListener) {
		obj.addEventListener(ev.replace(/^on/i, ''), func, useCapture);
	} else if (window.attachEvent) {
		obj.attachEvent(ev, func);
	}
}

function onDomReady(callback)
{
	var _timer;


	var onDomReadyCallBack = function()
	{
		if (arguments.callee.done)
			return;

		arguments.callee.done = true;
		if (_timer)
		{
			clearInterval(_timer);
			_timer = null;
		}


		callback();

	};



	if (document.addEventListener)
	{
		document.addEventListener("DOMContentLoaded", onDomReadyCallBack, false);
	}


	if (document.attachEvent && document.documentElement.doScroll && window == window.top)
	{
		(
			function()
			{
				try
				{
					document.documentElement.doScroll("left");
				}
				catch (error)
				{
					setTimeout(arguments.callee, 0);
					return;
				}
				onDomReadyCallBack();
			}
		)();
	}




	if (/WebKit/i.test(navigator.userAgent))
	{
		_timer = setInterval(function()
		{
			if (/loaded|complete/.test(document.readyState))
			{
				onDomReadyCallBack();
			}
		}, 10);
	}

	addEvent(window, 'onload', onDomReadyCallBack);

}



function removeEvent(obj, ev, func) {
	if (window.addEventListener) {
		obj.removeEventListener(ev.replace(/^on/i, ''), func, false);
	} else if (window.detachEvent) {
		obj.detachEvent(ev, func);
	}
}
function in_array(needle, haystack, strict) {
	var found = false, key, strict = !!strict;

	for (key in haystack) {
		if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
			found = true;
			break;
		}
	}

	return found;
}
function count(mixed_var, mode) {
	var key, cnt = 0;

	if (mode == 'COUNT_RECURSIVE')
		mode = 1;
	if (mode != 1)
		mode = 0;

	for (key in mixed_var) {
		cnt++;
		if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
			cnt += count(mixed_var[key], 1);
		}
	}

	return cnt;
}
function is_string(mixed_var) {
	return (typeof (mixed_var) == 'string');
}
function is_numeric(mixed_var) {
	return !isNaN(mixed_var);
}

function is_dom(obj) {
	return obj && obj.style;
}

function is_oObj(obj) {
	return obj && obj.getView && obj.getView();
}
is_vObj = is_oObj;

function is_array(mixed_var) {
	return (mixed_var instanceof Array);
}


function is_object(mixed_var) {
	if (mixed_var instanceof Array) {
		return false;
	} else {
		return (mixed_var !== null) && (typeof (mixed_var) == 'object');
	}
}

function empty(mixed_var) {
	var is_object_empty = false;
	if (is_object(mixed_var)) {
		for (key in mixed_var) {
			return is_object_empty;
		}
		is_object_empty = true;
	}
	return (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || (is_array(
		mixed_var) && mixed_var.length === 0) || is_object_empty);
}

function is_obj(obj) {
	return obj && typeof (obj);
}

function strToJson(str)
{
	if (typeof str != 'string')
	{
		str = '';
	}

	var res = {};

	try
	{
		res = (new Function('return ' + str.replace(/^[^{]*|[^}]$/, '')))();
	}
	catch (e) {
	}
	;

	return res;
}


function getChilds(obj, nodeName, attr, style, arr) {
	if (!arr) {
		arr = Array();
	}
	if (obj.hasChildNodes()) {
		for (var i = 0; i < obj.childNodes.length; i++) {
			if (obj.childNodes[i].nodeName == '#text') {
				continue;
			}
			if (!nodeName || obj.childNodes[i].nodeName == nodeName) {
				if (!attr) {
					if (!style) {
						arr.push(obj.childNodes[i]);
					} else {
						if (obj.childNodes[i].style) {
							var flg = false;
							for (key in style) {
								var re = new RegExp(style[key])
								if (obj.childNodes[i].style[key].match(re)) {
									flg = true;
								} else {
									flg = false;
									break;
								}
							}
							if (flg) {
								arr.push(obj.childNodes[i]);
							}
						}
					}
				} else {
					var flg = false;
					for (key in attr) {
						if (obj.childNodes[i].getAttribute(key) == attr[key]) {
							flg = true;
						} else {
							flg = false;
							break;
						}
					}
					if (flg) {
						if (!style) {
							arr.push(obj.childNodes[i]);
						} else {
							if (obj.childNodes[i].style) {
								var flg = false;
								for (key in style) {
									if (obj.childNodes[i].style[key] == style[key]) {
										flg = true;
									} else {
										flg = false;
										break;
									}
								}
								if (flg) {
									arr.push(obj.childNodes[i]);
								}
							}
						}
					}
				}
			}
			getChilds(obj.childNodes[i], nodeName, attr, style, arr);
		}
	}
	return arr;
}
function errorMsg(method, msg) {
	alert("Error:\r\r\n\n    " + method + "                        \r\n    " + msg);
}
function noticeMsg(method, msg) {
	alert('Notice!!!:\r\r    ' + method + '                        \r    ' + msg + '!');
}

function returnFalse(event) {
	event.cancelBubble = true;
	return false;
}

function getNextBrother(obj) {
	if (!is_dom(obj)) {
		errorMsg('tools.js -> Window :: getNextBrother', 'Object in not DOM element');
		return;
	}
}




function parseXML(xml) {
	var arr = Array();
	if (!xml || !xml.firstChild) {
		return '';
	}

	var root = xml.nodeName == '#document' ? xml.firstChild : xml;
	if (root.nodeName == '#text') {
		root = root.nextSibling;
	}
	if (root.nodeName == 'xml') {
		root = root.nextSibling;
	}
	if (root) {
		for (var i = 0; i < root.childNodes.length; i++) {
			var item = root.childNodes[i];
			var nodeName = item.nodeName;
			var attributes = getAttributes(item);
			if (nodeName != '#text') {
				if (nodeName != "#comment") {
					switch (nodeName) {
						case 'length':
						case 'push':
							nodeName = '_' + nodeName;
							break;
					}
					var val = parseXML(item);
					if (!arr[nodeName]) {
						if (attributes) {
							var temp = Array();
							temp['attrs'] = attributes;
							temp['textContent'] = val;
							arr[nodeName] = temp;
						} else {
							arr[nodeName] = val;
						}
					} else {
						if (!is_array(arr[nodeName])) {
							var temp = arr[nodeName];
							arr[nodeName] = Array(temp);
						}
						if (arr[nodeName].length == 0) {
							var temp = arr[nodeName];
							arr[nodeName] = Array(temp);
						}
						if (attributes) {
							var temp = Array();
							temp['attrs'] = attributes;
							temp['textContent'] = val;
							arr[nodeName].push(temp);
						} else {
							arr[nodeName].push(val);
						}
					}
				}
			}
		}
		var flg = false;
		for (key in arr) {
			flg = true;
			break;
		}
		return flg ? arr : (root.text || root.textContent);
	}
	return null;
}

function getAttributes(node) {
	var arr = Array();
	if (node && node.attributes && node.attributes.length > 0) {
		for (var i = 0; i < node.attributes.length; i++) {
			var attr = node.attributes[i];
			arr[attr.name] = attr.value;
		}
		return arr;
	}
	return null;
}

function formatPhoneNumber(number) {
	return number.substring(0, 1) + ' (' + number.substring(1, 4) + ') ' + number.substring(4,
		7) + '-' + number.substring(7, 11);
}

function templater(str, hash)
{

	var leftQ = '<%';
	var rightQ = '%>';
	var re1 = new RegExp("((^|" + rightQ + ")[^\t]*)'", 'g');
	var re2 = new RegExp("\t=(.*?)\\" + rightQ, 'g');
	var my_str = "var a=[];with(hash){a.push('" + str
		.replace(/[\t\r\n]/g, " ")
		.split(leftQ)
		.join("\t")
		.replace(re1, "$1\r")
		.replace(re2, "',$1,'")
		.split("\t")
		.join("');")
		.split(rightQ)
		.join("a.push('")
		.split("\r")
		.join("\\'") + "');} return a.join('');";

	my_str = 'try{' + my_str + '}catch(e){throw e;}';

	return new Function("hash", my_str)(hash);
}

function getCssClassNamevalue(o, className)
{
	var sClasses = o.className.replace(/\n|\r|\t/g, ' ');
	var aClasses = sClasses.split(' ');


	var re = new RegExp('^' + className + '(\-|)([^\-]+|)', '');
	for (var i = 0; i < aClasses.length; i++)
	{
		var class_ = aClasses[i];
		if (res = class_.match(re))
		{
			return res[2];
		}
	}
	return null;
}
getCssClassNameValue = getCssClassNamevalue

function setCssClassName(o, className, value)
{

	if (getCssClassNameValue(o, className) == value)
	{
		return;
	}

	if (typeof value == 'undefined')
	{
		o.className = className;
	}
	else
	{
		var sClasses = o.className.replace(/\n|\r|\t/g, ' ');
		var aClasses = sClasses.split(' ');
		//var re       = new RegExp('^' + className + '\-[^\-]+','') ;
		var re = new RegExp('^' + className + '\-.+', '');
		for (var i = 0; i < aClasses.length; i++)
		{
			var class_ = aClasses[i];
			if (class_.match(re))
			{
				aClasses.splice(i, 1);
			}
		}

		if (value == '' && value != 0)
		{
			aClasses.push(className);
		}
		else
		{


			aClasses.push(className + '-' + value);
		}
		o.className = aClasses.join(' ');
		if (o.firstChild && o.firstChild.tagName == 'B')
		{
			o.firstChild.innerHTML = o.className;
		}
	}
	//alert(o.className)
}

function addClassName(o, className)
{
	var aClasses = o.className.replace(/\n|\r|\t/g, ' ').split(' ');
	for (var i = 0; i < aClasses.length; i++)
	{
		if (aClasses[i] == className)
		{
			return;
		}
	}
	o.className += ' ' + className;
}



function getElementsByClassName(classNameVal, parent, tag)
{
	var res = [];
	parent = parent || document;
	var nodes = parent.getElementsByTagName(tag || '*');
	var l = nodes.length;
	if (l)
	{
		var re = new RegExp("(^|\\s)" + classNameVal + "(\\s|$)");

		for (var i = 0; i < l; i++)
			if (re.test(nodes[i].className))
				res.push(nodes[i]);
	}
	return res;
}

function each(obj, callBack, space)
{
	if (!obj)
	{
		throw new Error('Each\nObject is undefined');
	}
	if (typeof callBack != 'function')
	{
		throw new Error('Each\nCallBack undefined or not a function');
	}

	space = space || obj;

	if (obj.length)
	{
		for (var i = 0, l = obj.length; i < l; i++) {
			if (callBack.call(space, obj[i], parseInt(i), obj) === false)
				return false;
		}
	}
	else
	{
		for (var key in obj) {
			if (obj.hasOwnProperty && obj.hasOwnProperty(key)) {
				if (callBack.call(space, obj[key], key, obj) === false)
					return false;
			}
		}
	}
	return true;
}

function getCurrentStyle(o, name)
{
	if (window.getComputedStyle)
	{
		return window.getComputedStyle(o, null)[name];
	} else if (o.currentStyle)
	{
		return o.currentStyle[name];
	}
	return  null;
}

function each_r(obj, callBack, space)
{
	if (!obj)
	{
		throw new Error('Each\nObject is undefined');
	}
	if (typeof callBack != 'function')
	{
		throw new Error('Each\nCallBack undefined or not a function');
	}

	space = space || obj;

	var res = [];

	if (obj.length)
	{
		for (var i = 0, l = obj.length; i < l; i++) {
			var ret_val = callBack.call(space, obj[i], i, obj);
			if (ret_val)
			{
				res.push(ret_val);
			}
		}
	}
	else
	{
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				var ret_val = callBack.call(space, obj[key], key, obj);
				if (ret_val)
				{
					res.push(ret_val);
				}
			}
		}
	}
	return res.length ? res : null;
}

function getCssPropValueBySelectorAndProp(selector, prop)
{
	var css = window.document.styleSheets;
	if (css.length)
	{
		var res =
			each_r(css,
				(
					function(selector, prop, cssGroup)
					{
						try
						{
							var fullRules = cssGroup.cssRules || cssGroup.rules
							if (fullRules)
							{
								var res =
									each_r(fullRules,
										(
											function(selector, prop, rule)
											{
												if (rule.selectorText == selector)
												{
													return rule.style[prop];
												}
												return null;
											}
										).rcBindAsIs(this, selector, prop)
										);
								return res ? res : null;

							}
						} catch (e) {
						}
					}
				).rcBindAsIs(this, selector, prop)
				);
		return res ? res[0] : null;
	}
}


function opacity(id, opacStart, opacEnd, millisec) {

	var speed = Math.round(millisec / 100);
	var timer = 0;


	if (opacStart > opacEnd) {
		for (i = opacStart; i >= opacEnd; i--) {
			setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
			timer++;
		}
	} else if (opacStart < opacEnd) {
		for (i = opacStart; i <= opacEnd; i++)
		{
			setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
			timer++;
		}
	}
}


function changeOpac(opacity, id) {
	var object = document.getElementById(id).style;
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
	object.filter = "alpha(opacity=" + opacity + ")";
}

function getParentByTagsName(arr, node)
{
	while (inArray(arr, node.nodeName) == -1)
	{
		if (node.nodeName == 'BODY')
		{
			return null;
		}
		node = node.parentNode;
	}
	return node;

}



function fireEvent(el, ev)
{
	if (document.createEventObject)
	{
		var E = document.createEventObject();
		el.fireEvent(ev, E);
		return true;
	}
	else if (document.createEvent)
	{
		var E = document.createEvent("MouseEvents");
		E.initEvent(ev.replace(/^on/, ''), true, false);
		el.dispatchEvent(E);

		var E = document.createEvent("HTMLEvents");
		E.initEvent(ev.replace(/^on/, ''), true, false);
		el.dispatchEvent(E);

		return true;
	}
	else
	{
		return false;
	}
}

function rand(min, max) {
	var argc = arguments.length;
	if (argc == 0) {
		min = 0;
		max = 2147483647;
	} else if (argc == 1) {
		throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
	}
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


function concatHash()
{
	var r = {};
	for (var i = 0; i < arguments.length; i++)
	{
		var parent = arguments[i];
		for (var key in parent)
		{
			r[key] = parent[key];
		}
	}
	return r;
}


function inArray(arr, val) {
	if (arr) {
		for (var i = 0, l = arr.length; i < l; i++) {
			if (arr[i] == val) {
				return i;
			}
		}
	}
	return -1;
}

/* 
 *	Check whether page has been opened from some search engine and
 *	in case of true result set according cookies of gw_pid and gw_engine_referrer
 */
if (typeof brand_prefix == "undefined") {
	var brand_prefix = "RC";
	var tmp_results1 = /m\.ringcentral\.com/.exec(location.host);
	var tmp_results2 = /\.m\.ringcentral\.com/.exec(location.host);
	var tmp_results3 = /\/gwmobile\//.exec(location.href);
	if (tmp_results1 != null || tmp_results2 != null || tmp_results3 != null) { /* GW-6841 */
		brand_prefix = "MOB";
	} else {
		var domain = get_domain_cookie();
		switch (domain) {
			case '.ringcentral.com':
				brand_prefix = 'RC';
				break;
			case '.ringcentral.ca':
				brand_prefix = 'CA';
				break;
			case '.ringcentral.co.uk':
				brand_prefix = 'UK';
				break;
			case '.extremefax.com':
				brand_prefix = 'XF';
				break;
		}
	}
}


function get_domain_cookie() {
	var host = window.location.hostname;
	if(host == 'localhost') return host;
	var _domain = host.split('.');
	_domain.shift();
	if (_domain[0] == 'secure') {
		_domain.shift();
	}
	domain = '.' + _domain.join('.');
	return domain;
}
var cookieSetHost = get_domain_cookie();

/**********start insert*/
/*block for set 'pid', 'bmid', 'rckw', 'rcmt', 'ad',  'kid', 'aid' 'kcid' vars*/
function ParamToCookieSet() {
	var domain = get_domain_cookie();//'.' + window.location.hostname.match(/[^.]+\.(co\.uk|[^.]+)$/)[0];
//    var arr_param = ['pid', 'bmid', 'rckw', 'rcmt', 'ad',  'kid', 'aid'];
	var arr_param_del = ['pid', 'bmid', 'rckw', 'rcmt', 'ad', 'kid', 'aid', 'tduid'];
	var arr_param = ['rckw', 'rcmt', 'ad', 'kid', 'kcid'];
	var arr_param_session = ['pid', 'bmid', 'aid', 'spid', 'tduid'];

	var pid = GetParamsManager.is_set('pid');
	var bmid = GetParamsManager.is_set('bmid');
	if ( !(bmid || /(^|; )gw_bmid=(.*?)(;|$)/i.exec(document.cookie)) ) {	
		['.ringcentral.com/aff/office/small-business-phone-system_c.html',
		'.ringcentral.ca/aff/office/small-business-phone-system.html',
		'.ringcentral.com/aff/fax25p_6.html',
		'.ringcentral.com/aff/contactcenter'].forEach( function( page ){
			if ( document.location.href.indexOf(page) !== -1 ) {
				bmid = 'AFF_GEN';
				CookiesManager.setCookie('gw_bmid', bmid, '', '/', domain);
			}
		});

	}

	var afn = GetParamsManager.is_set('afn');
//    var lcid = GetParamsManager.is_set('lcid');

	var tduid = GetParamsManager.is_set('tduid');
	var cjevent = GetParamsManager.is_set('cjevent');

	if (cjevent) {
		CookiesManager.setCookie('gw_cjevent', cjevent, 120, '/', domain);
	}

	if (tduid) {
		CookiesManager.setCookie('gw_tduid', tduid, '', '/', domain);
	}

	var partnerid = GetParamsManager.is_set('partnerid');
	if (partnerid) {
		CookiesManager.setCookie('gw_partnerid', partnerid, '', '/', domain);
	}
	var safid = GetParamsManager.is_set('safid');
	if (safid) {
		CookiesManager.setCookie('gw_safid', safid, '', '/', domain);
	}
	var kcid = GetParamsManager.is_set('kcid');
	if (kcid) {
		CookiesManager.setCookie('gw_kcid', kcid, '', '/', domain);
	}

	var rckw = GetParamsManager.is_set('rckw');
	if (rckw) {
		CookiesManager.setCookie('gw_rckw', rckw, '', '/', domain);
	}


	function getBmidFromReferrer(hostname) {
		var patterns = {
			GGLPLUS: ['(^|\\.)plus\\.google\\.'],
			FSRC_GOOGLE: ['(^|\\.)google\\.'],
			FSRC_BING: ['(^|\\.)bing\\.'],
			FSRC_YAHOO: ['(^|\\.)yahoo\\.'],
			FSRC_OTHERS: ['(^|\\.)aol\\.',
				'(^|\\.)yandex\\.',
				'(^|\\.)baidu\\.',
				'(^|\\.)duckduckgo\\.',
				'(^|\\.)dogpile\\.',
				'(^|\\.)ask\\.',
				'(^|\\.)myway\\.',
				'(^|\\.)msn\\.',
				'(^|\\.)naver\\.',
				'(^|\\.)sogou\\.'
			],
			FB_L: ['(^|\\.)facebook\\.'],
			LI_L: ['(^|\\.)linkedin\\.',
				'^lnkd\\.in'
			],
			T_L: ['(^|\\.)twitter\\.',
				'^t\\.co'
			],
			YT_L: ['(^|\\.)youtube\\.'],
			RCUS_OTHERREF: ['(^|\\.)pinteres\\.',
				'(^|\\.)reddit\\.',
				'(^|\\.)instagramm\\.',
				'(^|\\.)slideshare\\.',
				'(^|\\.)yelp\\.',
				'(^|\\.)on24\\.',
			],
			GGUSCONCOMPET: ['(^|\\.)doubleclick.net$',
				'(^|\\.)googlesyndication.com$'
			],
			RCUS_INTDOMAINS: [
				'(^|\\.)glip.com$',
				'(^|\\.)ringcentral\\.',
				'(^|\\.)extremefax.com$',
				'(^|\\.)salesforce.com$',
				'(^|\\.)ringdemo\\.',
				'(^|\\.)liveperson.net$',
				'(^|\\.)serverdata.net$',
			],
			RCUS_INPRODUCT: ['(^|\\.)mindbodyonline.com$',
				'(^|\\.)e-access.att.com$',
				'(^|\\.)mybodhi.com$',
				'(^|\\.)showroomtransport.com$',
				'(^|\\.)amridgeuniversity.blackboard.com$',
				'(^|\\.)holidiumlabs.com$',
				'(^|\\.)yammer.com$',
			],
			AFF_GEN: ['(^|\\.)shawngraham.me$',
				'(^|\\.)fitsmallbusiness.com$',
				'(^|\\.)fabsuite.com$',
				'(^|\\.)komando.com$',
				'(^|\\.)saas-reviews.com$',
				'(^|\\.)smallbiztrends.com$',
				'(^|\\.)appstechnews.com$',
				'(^|\\.)gmailfax.com$',
				'(^|\\.)howtogeek.com$',
				'(^|\\.)businessnewsdaily.com$',
				'(^|\\.)getapp.com$',
			],
		};

		var ret = '';
		if (typeof hostname === 'undefined') {
			if (!window.document.referrer)
				return ret;
			var parser = document.createElement('a');
			parser.href = window.document.referrer.toLowerCase();
			hostname = parser.hostname != window.location.hostname ? parser.hostname : '';
		}
		if (!hostname)
			return ret;

		Object.keys(patterns).forEach(function(key) {
			if (!ret) {
				patterns[key].forEach(function(reg) {
					if (!ret && hostname.match(reg)) {
						ret = key;
					}
				});
			}
			;
		});

		if (!ret)
			ret = 'RCUS_OTHERREF';
		return ret;
	}



	function DelPreviousParam() {
		for (var i = 0; i < arr_param_del.length; i++) {
			CookiesManager.delCookie('gw_' + arr_param_del[i], '/', domain);
		}
	}

	function SetParam() {/* set param 'rckw', 'rcmt', 'ad',  'kid', 'aid', 'kcid' */
		var param = GetParamsManager.get2array();
		for (var i = 1; i < arr_param.length; i++) {
			if (typeof param[arr_param[i]] != 'undefined' && param[arr_param[i]].toString().length > 0) {
				var name = 'gw_' + arr_param[i];
				CookiesManager.setCookie(name, param[arr_param[i]], 30, '/', domain);
			}
		}
		for (var i = 1; i < arr_param_session.length; i++) {
			if (typeof param[arr_param_session[i]] != 'undefined' && param[arr_param_session[i]].toString().length > 0) {
				var name = 'gw_' + arr_param_session[i];
				CookiesManager.setCookie(name, param[arr_param_session[i]], '', '/', domain);
			}
		}
	}

	function BeginSetParam() {
		DelPreviousParam();
		SetParam();
		if (afn && !(!!bmid)) {
			CookiesManager.setCookie('gw_bmid', escape(afn), '', '/', domain);
			if (!!pid) {
				CookiesManager.setCookie('gw_pid', escape(pid), '', '/', domain);
			}
		} else if (pid && !(!!bmid)) {
			CookiesManager.setCookie('gw_bmid', escape(pid), '', '/', domain);
		} else {
			CookiesManager.setCookie('gw_bmid', escape(bmid), '', '/', domain);
			if (!!pid) {
				CookiesManager.setCookie('gw_pid', escape(pid), '', '/', domain);
			}
		}
	}

	var rr_host, rr_query;
	if (!(!!pid || !!bmid)) { /*set pid from referrer*/
		var rr = GetParamsManager.is_set('gw_referrer');
		if (rr == 'null')
			rr = '';
		if (!rr)
			rr = window.document.referrer;
		if (!!rr) {
			var rr_host = '';
			try {
				rr_host = rr.match(/(http|https):\/\/(?:www\.)?([^\/]+)/)[2];
			} catch (e) {
				rr_host = decodeURIComponent(rr).match(/(http|https):\/\/(?:www\.)?([^\/]+)/)[2];
			}
			var rr_query = rr.split("?")[1] || "";
			/*  hook search */
			var re = /results\/Web\/([^\/]+)\//i;
			var found = rr.match(re);
			if (found != null) {
				rr_query += '&hook_query=' + found[1];
			}
//            pid = get_GW_PID(rr_host, rr_query);
			var referer = GetParamsManager.is_set('gw_referrer');
			if (
				null == referer
				|| 'null' == referer
			) {
				referer = undefined;
			}
			pid = getBmidFromReferrer(referer);
			pid = pid == "" ? null : pid;
		}
	}

	if (!!pid || !!bmid || !!afn) {
		BeginSetParam();/*chahge Param*/
//        var gw_rckw = get_GW_RCKW(rr_host, rr_query);
//		var gw_rckw = '';
//		if (gw_rckw != "") {
//			CookiesManager.setCookie("gw_rckw", escape(gw_rckw), '', '/', domain);
//		}
	}

	if (bmid == 'CJZLKR0808_01') {
		(function() {
			var matches = [];
			if (matches = /(\.[^\.]+\.(com|ca|co\.uk|ru|net))$/.exec(window.location.hostname)) {
				//e.g. .ringcentral.com, .ringcentral.co.uk
				CookiesManager.setCookie('trial30', 'true', '', '/', matches[0]);
			} else {
				CookiesManager.setCookie('trial30', 'true');
			}
		})();
	}
	//Set cookie CID
	bmid = CookiesManager.getCookie('gw_bmid');
	// GW-7906 GW-8690

	var setcid = false;

	if (GetParamsManager.is_set('cid')) {
		var tmp_array_key = ['PARTNER', 'RETARGET', 'REFERRAL', 'MEDIA', 'RESELLER', 'OTHER', 'SERVPROV', 'SOCIAL', 'LEADGEN', 'AFF', 'SEM'];
		var tmp_value_key = GetParamsManager.is_set('cid').toUpperCase();
		for (var p in tmp_array_key) {
			if (tmp_value_key == tmp_array_key[p]) {
				CookiesManager.setCookie('gw_cid', tmp_array_key[p], 30, '/', domain);
				setcid = true;
			}
		}
	}

	if (!setcid && !CookiesManager.getCookie('gw_cid')) {
		if (!CookiesManager.getCookie('gw_bmid') && !CookiesManager.getCookie('gw_partnerid')) {
			CookiesManager.setCookie('gw_cid', 'DIRECT', 30, '/', domain);
		} else if (GetParamsManager.is_set('rckw') && GetParamsManager.is_set('rcmt')) {
			CookiesManager.setCookie('gw_cid', 'SEM', 30, '/', domain);
		} else if (bmid && (bmid.substring(0, 2)).toUpperCase() == 'FS') {
//GW-17470
			CookiesManager.setCookie('gw_cid', 'SEO', 30, '/', domain);
		} else if (typeof (GetParamsManager.is_set('afn')) == 'string') {
			CookiesManager.setCookie('gw_cid', 'AFF', 30, '/', domain);
		}
	}


	/* block for set 'adgrpid', 'oid', 'product', 'afn', 'sid', 'office_phone', 'oppid' 'kcid' 'rckw' vars*/

	var domain = get_domain_cookie();//'.' + window.location.hostname.match(/[^.]+\.(co\.uk|[^.]+)$/)[0];
	var param = GetParamsManager.get2array();
	var get_param = ['adgrpid', 'oid', 'product', 'afn', 'sid', 'office_phone', 'oppid', 'kcid', 'rckw'];

	for (var p in get_param) {
		if (typeof param[get_param[p]] != 'undefined') {
			var name = get_param[p];
			if (name == 'adgrpid'
				|| name == 'afn'
				|| name == 'sid'
				|| name == 'kcid'
				|| name == 'rckw'
				|| name == 'oppid')
			{
				name = 'gw_' + name;
			}

			if (name == 'gw_oppid' && /https/i.test(window.location.protocol)) {
				CookiesManager.setCookie(name, param[get_param[p]], '', '/', domain, true);
			} else {
				CookiesManager.setCookie(name, param[get_param[p]], '', '/', domain);
			}

			if (name == 'product') {
				name = name + '_tags';
				var cValue = (param[get_param[p]].toLowerCase() == 'mobile') ? 'online' : param[get_param[p]];
				switch (cValue) {
					case 'office':
					case 'online':
					case 'fax':
						CookiesManager.setCookie(name, cValue, '', '/', domain);
						break;
				}
			}
		}
	}
	//GW-6315
	if (typeof param['siteid'] != 'undefined') {
		CookiesManager.setCookie('gw_sid', escape(param['siteid']), '', '/', domain);
	}
	//GW-5054
	if (typeof param['spid'] != 'undefined') {
		CookiesManager.setCookie('gw_spid', escape(param['spid']), '', '/', domain);
	}


}
;

ParamToCookieSet();

!function() {
	var params = [];

	$.each({
		'gw_bmid': 'BMID',
		'gw_kcid': 'kcid',
		'gw_pid': 'PID',
		'gw_aid': 'AID',
		'gw_rckw': 'RCKW'
	}, function(key, val) {
		var v = CookiesManager.getCookie(key);
		if (v)
			params.push(val + '=' + v);
	});

	if (params.length > 0) {
		var reg = /^(http:\/\/|https:\/\/|\/\/|)(marketo|go).ringcentral.com([^\?]+)(\??.*)$/;
		$(function() {
			$("a").attr('href', function(i, h) {
				if (h) {
					var t = h.match(reg);
					if (t) {
						if (t[4])
							if (t[4].length == 1)
								h += params.join('&');
							else
								h += '&' + params.join('&');
						else
							h += '?' + params.join('&');
						return h;
					}
				}
			})
		});
	}
}();

/**********end insert*/
var nead_cookie_afn = ['/cj/toll-free-numbers.asp', '/cj/toll-free-numbersb.asp', '/cj/toll-free-numbersb_sp.asp',
	'/cj/toll-free-numbers10p.html', '/cj/toll-free-numbers30b.asp', '/cj/toll-free-numbersZLKR30.asp',
	'/cj/virtual-phone-system.html', '/cj/virtual-phone-systemsf30.html',
	'/cj/virtual-pbx-new.asp', '/cj/faxandphone.html', '/cj/voicemail.asp', '/cj/vanitynumber.asp',
	'/cj/call-forwarding.asp', '/cj/digitalline-voip.html', '/cj/local-numbers/area-codes.html',
	'/cj/fax.asp', '/cj/fax30.asp', '/cj/fax.asp', '/cj/office-everywhere-phone-fax.asp'];



var ref_full = window.location.pathname;// + window.location.search;
if (inArray(nead_cookie_afn, ref_full) != -1) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'cj', '', '/', cookieSetHost);
}

if (ref_full.match(/^\/aff\//)) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'aff', '', '/', cookieSetHost);
}

if (ref_full.match(/^\/az\//)) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'az', '', '/', cookieSetHost);
}

if (CookiesManager.getCookie('gw_afn')) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', CookiesManager.getCookie('gw_afn'), '', '/', cookieSetHost);
	var hasAfnCookie = true;
}

var p = GetParamsManager.is_set('p');
if (p) {
	CookiesManager.setCookie('p', p);
}

var o = GetParamsManager.is_set('o');
if (o) {
	CookiesManager.setCookie('o', o, 1 / 64);
}

if (CookiesManager.getCookie('afn')) {
	var hasAfnCookie = true;
}


(function() {
	/* begin GW-536 */
	function getProductTag(url) {
		var _productTags = {
			'/fax/default.html': 'fax',
			'/fax/emailfax.html': 'fax',
			'/fax/default.asp': 'fax',
			'/fax/emailfax.asp': 'fax',
			'/office/business-phone-service.html': 'office',
			'/office/business-voip-phone.html': 'office',
			'/office/index.html': 'office',
			'/office/internet-business-phone-system.html': 'office',
			'/office/online-business-phone-system.html': 'office',
			'/office/phone-system.html': 'office',
			'/office/small-business-phone-system.html': 'office',
			'/office/virtual-business-phone-service.html': 'office',
			'/office/virtual-business-phone-system.html': 'office',
			'/office/virtual-pbx-phone-system.html': 'office',
			'/business-toll-free.dhtml': 'online',
			'/features/real-time-control/overview.html': 'online',
			'/lp/800-business-number.html': 'online',
			'/lp/800-business-number.asp': 'online',
			'/lp/800numbers.html': 'online',
			'/lp/800numbers.asp': 'online',
			'/lp/800numbers-yh-a.asp': 'online',
			'/lp/800service.html': 'online',
			'/lp/800service.asp': 'online',
			'/lp/866-business-number.html': 'online',
			'/lp/866-business-number.asp': 'online',
			'/lp/877-business-number.html': 'online',
			'/lp/877-business-number.asp': 'online',
			'/lp/auto-attendant.asp': 'online',
			'/lp/call-forwarding.html': 'online',
			'/lp/call-forwarding.asp': 'online',
			'/lp/callscreening.asp': 'online',
			'/lp/call-screening.html': 'online',
			'/lp/call-screening.asp': 'online',
			'/lp/digitalline-voip.html': 'online',
			'/lp/digitalline-voip-home.html': 'online',
			'/lp/local-numbers.html': 'online',
			'/lp/local-numbers.asp': 'online',
			'/lp/small-business-phone.html': 'online',
			'/lp/toll-free-numbers.html': 'online',
			'/lp/toll-free-numbers.asp': 'online',
			'/lp/toll-free-numbers.asp'                  : 'online',
				'/lp/toll-free-numbers-yh-a.asp': 'online',
			'/lp/unified-communications.asp': 'online',
			'/lp/vanitynumber.html': 'online',
			'/lp/vanitynumber.asp': 'online',
			'/lp/virtual-pbx.asp': 'online',
			'/lp/virtualphonenumber.asp': 'online',
			'/lp/virtual-phone-service.html': 'online',
			'/lp/virtual-phone-system.html': 'online',
			'/lp/voicemail.asp': 'online',
			'/lp/auto-attendant.html': 'online',
			'/lp/unified-communications.html': 'online',
			'/lp/virtualphonenumber.html': 'online',
			'/lp/virtual-pbx.html': 'online',
			'/lp/voicemail.html': 'online',
			'/lp/toll-free-numbers-yh-a.asp'  :  'online'


		};

		var _regex = new RegExp(
			'^\/features\/local-numbers\/area-code-(201|202|203|205|206|208|209|210|212|213|214|216|217|218|219|224|225|228|229|231|234|239|240|248|251|252|253|254|256|260|262|267|269|270|276|281|301|302|303|304|305|307|309|310|312|313|314|315|316|317|318|321|323|325|330|334|336|337|339|347|352|360|361|386|401|402|404|405|406|407|408|409|410|412|413|414|415|417|419|423|424|425|432|434|435|440|443|469|478|479|480|484|501|502|503|504|505|507|508|509|510|512|513|516|517|518|520|530|540|541|559|561|562|567|570|571|573|575|580|585|586|601|603|605|606|607|608|609|610|612|614|615|616|617|618|619|620|623|626|630|631|636|646|650|651|661|662|678|682|701|702|703|704|706|707|708|712|713|714|715|716|717|718|719|720|724|727|731|732|734|740|754|757|760|763|765|769|770|772|773|775|785|786|801|802|803|804|805|806|810|812|813|814|815|816|817|818|828|830|831|832|843|845|847|850|856|858|859|860|862|863|864|865|870|901|903|904|906|908|909|913|914|915|916|918|919|920|925|928|936|937|940|949|951|952|954|956|970|971|973|979|989)\.html',
			'i');

		if (typeof _productTags[url] == 'string') {
			return _productTags[url];
		} else if (_regex.test(url)) {
			return 'online';
		} else {
			return false;
		}
	}

	var _productTag = getProductTag(window.location.pathname);
	if (_productTag && !CookiesManager.getCookie('product_tags')) {
		CookiesManager.setCookie('product_tags', _productTag, 30, '/', cookieSetHost);
	}
})();

(function() {
	// gw-3616
	var tellapalId = GetParamsManager.is_set('tellapal.id');
	if (!!tellapalId) {
		CookiesManager.setCookie('tellapal_id', tellapalId, '', '/', cookieSetHost);
	}

// gw-4291
	var today = new Date();
	if (CookiesManager.getCookie('gw_new_visitor') == null) {
		CookiesManager.setCookie('gw_new_visitor', today.getTime(), 30, '/', cookieSetHost);
	} else {
		var gw_new_visitor = CookiesManager.getCookie('gw_new_visitor');
//time fix, deleted after 2014-02-01 GW-17303
		if (gw_new_visitor.indexOf(" ") != -1) {
			var enterDay = new Date(gw_new_visitor);
		} else {
			var enterDay = new Date();
			enterDay.setTime(gw_new_visitor);
		}

		var one_day = 1000 * 60 * 60 * 24;
		var difference = today.getTime() - enterDay.getTime();
		if (gw_new_visitor != 'false' && difference > one_day) {
			var time = 30 - Math.ceil(difference) / one_day;
			CookiesManager.setCookie('gw_new_visitor', 'false', time, '/', cookieSetHost);
		}
	}
})();


(function() {
	/* start insert GW-9686 */



	var param = GetParamsManager.get2array();
	var pid = param['pid'];
	var bmid = param['bmid'];

	var refcode = CookiesManager.getCookie('gw_refcode');
	if (!!refcode) {
		bmid = refcode;
		CookiesManager.setCookie('gw_bmid', escape(bmid), '', '/', cookieSetHost);
		CookiesManager.delCookie('gw_refcode', '/', cookieSetHost);
	}

	if (pid && !(!!bmid)) {
		param['bmid'] = pid;
	}

	var flag_reload = false;
	var path = document.location.hostname + document.location.pathname;
	var converts_array = {
		'\.extremefax\.com\/msofficeapp\.asp': [{pid: 'MSOMRKTML_XF'}],
		'\.ringcentral\.com\/lp\/msmarketplace\.asp': [{pid: 'MSOMRKTML_RC'}],
		'\.ringcentral\.com\/partners\/vista\.htm': [{pid: 'VISTAOL8'}],
		'\.ringcentral\.com\/concentric\/rco': [{pid: 'CONRC08'}, {sid: '2512'}, {aid: '6505'}],
		'internetfax101\.com': [{pid: 'INTFAX101'}]
	}
	for (var pattern in converts_array) {
		var reg1 = new RegExp(pattern, "i");
		if (reg1.test(path))
			for (var i = 0; i < converts_array[pattern].length; i++) {
				var item = converts_array[pattern][i];
				for (var key in item) {
					if ((typeof param[key] != 'undefined') && (param[key] != item[key])) {
						param[key] = item[key];
						flag_reload = true;
					}
					CookiesManager.setCookie('gw_' + key, item[key], '', '/', cookieSetHost);
					if (key == 'pid')
						pid = param[key];
				}
			}
		if (flag_reload) {
			var request = [];
			for (var key in param) {
				request.push(key + '=' + param[key]);
			}
			var rr = window.document.referrer;
			if (!!rr) {
				request.push('gw_referrer=' + encodeURIComponent(rr));
			}
			window.location = 'http://' + path + "?" + request.join('&');
			break;
		}
	}
	/* end insert */
})();


})(jQuery);
!function e(t,n,a){function i(o,s){if(!n[o]){if(!t[o]){var c="function"==typeof require&&require;if(!s&&c)return c(o,!0);if(r)return r(o,!0);var l=new Error("Cannot find module '"+o+"'");throw l.code="MODULE_NOT_FOUND",l}var u=n[o]={exports:{}};t[o][0].call(u.exports,function(e){return i(t[o][1][e]||e)},u,u.exports,e,t,n,a)}return n[o].exports}for(var r="function"==typeof require&&require,o=0;o<a.length;o++)i(a[o]);return i}({1:[function(e,t){(function(n){
	/** @license ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ============
	
	Adobe Visitor API for JavaScript version: 3.0.0
	Copyright 1996-2015 Adobe, Inc. All Rights Reserved
	More info available at https://marketing.adobe.com/resources/help/en_US/mcvid/
	*/
	var a=e("./child/ChildVisitor"),i=e("./child/Message"),r=e("./child/makeChildMessageListener"),o=e("./utils/asyncParallelApply"),s=e("./utils/enums"),c=e("./utils/utils"),l=e("./utils/getDomain"),u=e("./units/version"),d=e("./units/crossDomain"),g=e("@adobe-mcid/visitor-js-shared/lib/ids/generateRandomID"),f=e("./units/makeCorsRequest"),m=e("./units/makeDestinationPublishing"),h=e("./utils/constants"),p=function(e,t,a){function p(e){var t=e;return function(e){var n=e||C.location.href;try{var a=S._extractParamFromUri(n,t);if(a)return N.parsePipeDelimetedKeyValues(a)}catch(e){}}}function v(e){function t(e,t){e&&e.match(h.VALID_VISITOR_ID_REGEX)&&t(e)}t(e[D],S.setMarketingCloudVisitorID),S._setFieldExpire(M,-1),t(e[P],S.setAnalyticsVisitorID)}function b(e){e=e||{},S._supplementalDataIDCurrent=e.supplementalDataIDCurrent||"",S._supplementalDataIDCurrentConsumed=e.supplementalDataIDCurrentConsumed||{},S._supplementalDataIDLast=e.supplementalDataIDLast||"",S._supplementalDataIDLastConsumed=e.supplementalDataIDLastConsumed||{}}function y(e){function t(e,t,n){return(n=n?n+="|":n)+(e+"=")+encodeURIComponent(t)}function n(e,n){var a=n[0],i=n[1];return null!=i&&i!==F&&(e=t(a,i,e)),e}var a,i=e.reduce(n,"");return(a=(a=i)?a+="|":a)+"TS="+N.getTimestampInSeconds()}function V(e){var t=e.minutesToLive,n="";return(S.idSyncDisableSyncs||S.disableIdSyncs)&&(n=n||"Error: id syncs have been disabled"),"string"==typeof e.dpid&&e.dpid.length||(n=n||"Error: config.dpid is empty"),"string"==typeof e.url&&e.url.length||(n=n||"Error: config.url is empty"),void 0===t?t=20160:(t=parseInt(t,10),(isNaN(t)||t<=0)&&(n=n||"Error: config.minutesToLive needs to be a positive number")),{error:n,ttl:t}}if(!a||a.split("").reverse().join("")!==e)throw new Error("Please use `Visitor.getInstance` to instantiate Visitor.");var S=this;S.version="3.0.0";var C=n,I=C.Visitor;I.version=S.version,I.AuthState=s.AUTH_STATE,I.OptOut=s.OPT_OUT,C.s_c_in||(C.s_c_il=[],C.s_c_in=0),S._c="Visitor",S._il=C.s_c_il,S._in=C.s_c_in,S._il[S._in]=S,C.s_c_in++,S._log={requests:[]},S.marketingCloudOrgID=e,S.cookieName="AMCV_"+e,S.sessionCookieName="AMCVS_"+e,S.cookieDomain=l(),S.cookieDomain===C.location.hostname&&(S.cookieDomain=""),S.loadSSL=C.location.protocol.toLowerCase().indexOf("https")>=0,S.loadTimeout=3e4,S.CORSErrors=[],S.marketingCloudServer=S.audienceManagerServer="dpm.demdex.net",S.sdidParamExpiry=30;var k=C.document,_=null,D="MCMID",L="MCIDTS",E="A",P="MCAID",T="AAM",M="MCAAMB",F="NONE",w=function(e){return!Object.prototype[e]},A=f(S,O);S.FIELDS=s.FIELDS,S.cookieRead=function(e){e=encodeURIComponent(e);var t=(";"+k.cookie).split(" ").join(";"),n=t.indexOf(";"+e+"="),a=n<0?n:t.indexOf(";",n+1);return n<0?"":decodeURIComponent(t.substring(n+2+e.length,a<0?t.length:a))},S.cookieWrite=function(e,t,n){var a,i=S.cookieLifetime;if(t=""+t,i=i?(""+i).toUpperCase():"",n&&"SESSION"!==i&&"NONE"!==i){if(a=""!==t?parseInt(i||0,10):-60)(n=new Date).setTime(n.getTime()+1e3*a);else if(1===n){var r=(n=new Date).getYear();n.setYear(r+2+(r<1900?1900:0))}}else n=0;return e&&"NONE"!==i?(k.cookie=encodeURIComponent(e)+"="+encodeURIComponent(t)+"; path=/;"+(n?" expires="+n.toGMTString()+";":"")+(S.cookieDomain?" domain="+S.cookieDomain+";":""),S.cookieRead(e)===t):0},S.resetState=function(e){e?S._mergeServerState(e):b()},S._isAllowedDone=!1,S._isAllowedFlag=!1,S.isAllowed=function(){return S._isAllowedDone||(S._isAllowedDone=!0,(S.cookieRead(S.cookieName)||S.cookieWrite(S.cookieName,"T",1))&&(S._isAllowedFlag=!0)),S._isAllowedFlag},S.setMarketingCloudVisitorID=function(e){S._setMarketingCloudFields(e)},S._use1stPartyMarketingCloudServer=!1,S.getMarketingCloudVisitorID=function(e,t){if(S.isAllowed()){S.marketingCloudServer&&S.marketingCloudServer.indexOf(".demdex.net")<0&&(S._use1stPartyMarketingCloudServer=!0);var n=S._getAudienceManagerURLData("_setMarketingCloudFields"),a=n.url;return S._getRemoteField(D,a,e,t,n)}return""},S.getVisitorValues=function(e,t){var n={MCMID:{fn:S.getMarketingCloudVisitorID,args:[!0],context:S},MCOPTOUT:{fn:S.isOptedOut,args:[void 0,!0],context:S},MCAID:{fn:S.getAnalyticsVisitorID,args:[!0],context:S},MCAAMLH:{fn:S.getAudienceManagerLocationHint,args:[!0],context:S},MCAAMB:{fn:S.getAudienceManagerBlob,args:[!0],context:S}},a=t&&t.length?N.pluck(n,t):n;o(a,e)},S._currentCustomerIDs={},S._customerIDsHashChanged=!1,S._newCustomerIDsHash="",S.setCustomerIDs=function(e){function t(){S._customerIDsHashChanged=!1}if(S.isAllowed()&&e){var n,a;for(n in S._readVisitor(),e)if(w(n)&&(a=e[n]))if("object"==typeof a){var i={};a.id&&(i.id=a.id),null!=a.authState&&(i.authState=a.authState),S._currentCustomerIDs[n]=i}else S._currentCustomerIDs[n]={id:a};var r=S.getCustomerIDs(),o=S._getField("MCCIDH"),s="";for(n in o||(o=0),r)w(n)&&(s+=(s?"|":"")+n+"|"+((a=r[n]).id?a.id:"")+(a.authState?a.authState:""));S._newCustomerIDsHash=S._hash(s),S._newCustomerIDsHash!==o&&(S._customerIDsHashChanged=!0,S._mapCustomerIDs(t))}},S.getCustomerIDs=function(){S._readVisitor();var e,t,n={};for(e in S._currentCustomerIDs)w(e)&&(t=S._currentCustomerIDs[e],n[e]||(n[e]={}),t.id&&(n[e].id=t.id),null!=t.authState?n[e].authState=t.authState:n[e].authState=I.AuthState.UNKNOWN);return n},S.setAnalyticsVisitorID=function(e){S._setAnalyticsFields(e)},S.getAnalyticsVisitorID=function(e,t,n){if(!N.isTrackingServerPopulated()&&!n)return S._callCallback(e,[""]),"";if(S.isAllowed()){var a="";if(n||(a=S.getMarketingCloudVisitorID(function(){S.getAnalyticsVisitorID(e,!0)})),a||n){var i=n?S.marketingCloudServer:S.trackingServer,r="";S.loadSSL&&(n?S.marketingCloudServerSecure&&(i=S.marketingCloudServerSecure):S.trackingServerSecure&&(i=S.trackingServerSecure));var o={};if(i){var s="http"+(S.loadSSL?"s":"")+"://"+i+"/id",c="d_visid_ver="+S.version+"&mcorgid="+encodeURIComponent(S.marketingCloudOrgID)+(a?"&mid="+encodeURIComponent(a):"")+(S.idSyncDisable3rdPartySyncing||S.disableThirdPartyCookies?"&d_coppa=true":""),l=["s_c_il",S._in,"_set"+(n?"MarketingCloud":"Analytics")+"Fields"];r=s+"?"+c+"&callback=s_c_il%5B"+S._in+"%5D._set"+(n?"MarketingCloud":"Analytics")+"Fields",o.corsUrl=s+"?"+c,o.callback=l}return o.url=r,S._getRemoteField(n?D:P,r,e,t,o)}}return""},S.getAudienceManagerLocationHint=function(e,t){if(S.isAllowed()&&S.getMarketingCloudVisitorID(function(){S.getAudienceManagerLocationHint(e,!0)})){var n=S._getField(P);if(!n&&N.isTrackingServerPopulated()&&(n=S.getAnalyticsVisitorID(function(){S.getAudienceManagerLocationHint(e,!0)})),n||!N.isTrackingServerPopulated()){var a=S._getAudienceManagerURLData(),i=a.url;return S._getRemoteField("MCAAMLH",i,e,t,a)}}return""},S.getLocationHint=S.getAudienceManagerLocationHint,S.getAudienceManagerBlob=function(e,t){if(S.isAllowed()&&S.getMarketingCloudVisitorID(function(){S.getAudienceManagerBlob(e,!0)})){var n=S._getField(P);if(!n&&N.isTrackingServerPopulated()&&(n=S.getAnalyticsVisitorID(function(){S.getAudienceManagerBlob(e,!0)})),n||!N.isTrackingServerPopulated()){var a=S._getAudienceManagerURLData(),i=a.url;return S._customerIDsHashChanged&&S._setFieldExpire(M,-1),S._getRemoteField(M,i,e,t,a)}}return""},S._supplementalDataIDCurrent="",S._supplementalDataIDCurrentConsumed={},S._supplementalDataIDLast="",S._supplementalDataIDLastConsumed={},S.getSupplementalDataID=function(e,t){S._supplementalDataIDCurrent||t||(S._supplementalDataIDCurrent=S._generateID(1));var n=S._supplementalDataIDCurrent;return S._supplementalDataIDLast&&!S._supplementalDataIDLastConsumed[e]?(n=S._supplementalDataIDLast,S._supplementalDataIDLastConsumed[e]=!0):n&&(S._supplementalDataIDCurrentConsumed[e]&&(S._supplementalDataIDLast=S._supplementalDataIDCurrent,S._supplementalDataIDLastConsumed=S._supplementalDataIDCurrentConsumed,S._supplementalDataIDCurrent=n=t?"":S._generateID(1),S._supplementalDataIDCurrentConsumed={}),n&&(S._supplementalDataIDCurrentConsumed[e]=!0)),n},S.getOptOut=function(e,t){if(S.isAllowed()){var n=S._getAudienceManagerURLData("_setMarketingCloudFields"),a=n.url;return S._getRemoteField("MCOPTOUT",a,e,t,n)}return""},S.isOptedOut=function(e,t,n){if(S.isAllowed()){t||(t=I.OptOut.GLOBAL);var a=S.getOptOut(function(n){var a=n===I.OptOut.GLOBAL||n.indexOf(t)>=0;S._callCallback(e,[a])},n);return a?a===I.OptOut.GLOBAL||a.indexOf(t)>=0:null}return!1},S._fields=null,S._fieldsExpired=null,S._hash=function(e){var t,n=0;if(e)for(t=0;t<e.length;t++)n=(n<<5)-n+e.charCodeAt(t),n&=n;return n},S._generateID=g,S._generateLocalMID=function(){var e=S._generateID(0);return B.isClientSideMarketingCloudVisitorID=!0,e},S._callbackList=null,S._callCallback=function(e,t){try{"function"==typeof e?e.apply(C,t):e[1].apply(e[0],t)}catch(e){}},S._registerCallback=function(e,t){t&&(null==S._callbackList&&(S._callbackList={}),null==S._callbackList[e]&&(S._callbackList[e]=[]),S._callbackList[e].push(t))},S._callAllCallbacks=function(e,t){if(null!=S._callbackList){var n=S._callbackList[e];if(n)for(;n.length>0;)S._callCallback(n.shift(),t)}},S._addQuerystringParam=function(e,t,n,a){var i=encodeURIComponent(t)+"="+encodeURIComponent(n),r=N.parseHash(e),o=N.hashlessUrl(e);if(-1===o.indexOf("?"))return o+"?"+i+r;var s=o.split("?"),c=s[0]+"?",l=s[1];return c+N.addQueryParamAtLocation(l,i,a)+r},S._extractParamFromUri=function(e,t){var n=new RegExp("[\\?&#]"+t+"=([^&#]*)").exec(e);if(n&&n.length)return decodeURIComponent(n[1])},S._parseAdobeMcFromUrl=p(h.ADOBE_MC),S._parseAdobeMcSdidFromUrl=p(h.ADOBE_MC_SDID),S._attemptToPopulateSdidFromUrl=function(t){var n=S._parseAdobeMcSdidFromUrl(t),a=1e9;n&&n.TS&&(a=N.getTimestampInSeconds()-n.TS),n&&n.SDID&&n.MCORGID===e&&a<S.sdidParamExpiry&&(S._supplementalDataIDCurrent=n.SDID,S._supplementalDataIDCurrentConsumed.SDID_URL_PARAM=!0)},S._attemptToPopulateIdsFromUrl=function(){var t=S._parseAdobeMcFromUrl();if(t&&t.TS){var n=N.getTimestampInSeconds()-t.TS;if(Math.floor(n/60)>h.ADOBE_MC_TTL_IN_MIN||t.MCORGID!==e)return;v(t)}},S._mergeServerState=function(e){if(e)try{if(a=e,(e=N.isObject(a)?a:JSON.parse(a))[S.marketingCloudOrgID]){var t=e[S.marketingCloudOrgID];n=t.customerIDs,N.isObject(n)&&S.setCustomerIDs(n),b(t.sdid)}}catch(e){throw new Error("`serverState` has an invalid format.")}var n,a},S._timeout=null,S._loadData=function(e,t,n,a){t=S._addQuerystringParam(t,"d_fieldgroup",e,1),a.url=S._addQuerystringParam(a.url,"d_fieldgroup",e,1),a.corsUrl=S._addQuerystringParam(a.corsUrl,"d_fieldgroup",e,1),B.fieldGroupObj[e]=!0,a===Object(a)&&a.corsUrl&&"XMLHttpRequest"===A.corsMetadata.corsType&&A.fireCORS(a,n,e)},S._clearTimeout=function(e){null!=S._timeout&&S._timeout[e]&&(clearTimeout(S._timeout[e]),S._timeout[e]=0)},S._settingsDigest=0,S._getSettingsDigest=function(){if(!S._settingsDigest){var e=S.version;S.audienceManagerServer&&(e+="|"+S.audienceManagerServer),S.audienceManagerServerSecure&&(e+="|"+S.audienceManagerServerSecure),S._settingsDigest=S._hash(e)}return S._settingsDigest},S._readVisitorDone=!1,S._readVisitor=function(){if(!S._readVisitorDone){S._readVisitorDone=!0;var e,t,n,a,i,r,o=S._getSettingsDigest(),s=!1,c=S.cookieRead(S.cookieName),l=new Date;if(null==S._fields&&(S._fields={}),c&&"T"!==c)for((c=c.split("|"))[0].match(/^[\-0-9]+$/)&&(parseInt(c[0],10)!==o&&(s=!0),c.shift()),c.length%2==1&&c.pop(),e=0;e<c.length;e+=2)n=(t=c[e].split("-"))[0],a=c[e+1],t.length>1?(i=parseInt(t[1],10),r=t[1].indexOf("s")>0):(i=0,r=!1),s&&("MCCIDH"===n&&(a=""),i>0&&(i=l.getTime()/1e3-60)),n&&a&&(S._setField(n,a,1),i>0&&(S._fields["expire"+n]=i+(r?"s":""),(l.getTime()>=1e3*i||r&&!S.cookieRead(S.sessionCookieName))&&(S._fieldsExpired||(S._fieldsExpired={}),S._fieldsExpired[n]=!0)));!S._getField(P)&&N.isTrackingServerPopulated()&&(c=S.cookieRead("s_vi"))&&((c=c.split("|")).length>1&&c[0].indexOf("v1")>=0&&((e=(a=c[1]).indexOf("["))>=0&&(a=a.substring(0,e)),a&&a.match(h.VALID_VISITOR_ID_REGEX)&&S._setField(P,a)))}},S._appendVersionTo=function(e){var t="vVersion|"+S.version,n=e?S._getCookieVersion(e):null;return n?u.areVersionsDifferent(n,S.version)&&(e=e.replace(h.VERSION_REGEX,t)):e+=(e?"|":"")+t,e},S._writeVisitor=function(){var e,t,n=S._getSettingsDigest();for(e in S._fields)w(e)&&S._fields[e]&&"expire"!==e.substring(0,6)&&(t=S._fields[e],n+=(n?"|":"")+e+(S._fields["expire"+e]?"-"+S._fields["expire"+e]:"")+"|"+t);n=S._appendVersionTo(n),S.cookieWrite(S.cookieName,n,1)},S._getField=function(e,t){return null==S._fields||!t&&S._fieldsExpired&&S._fieldsExpired[e]?null:S._fields[e]},S._setField=function(e,t,n){null==S._fields&&(S._fields={}),S._fields[e]=t,n||S._writeVisitor()},S._getFieldList=function(e,t){var n=S._getField(e,t);return n?n.split("*"):null},S._setFieldList=function(e,t,n){S._setField(e,t?t.join("*"):"",n)},S._getFieldMap=function(e,t){var n=S._getFieldList(e,t);if(n){var a,i={};for(a=0;a<n.length;a+=2)i[n[a]]=n[a+1];return i}return null},S._setFieldMap=function(e,t,n){var a,i=null;if(t)for(a in i=[],t)w(a)&&(i.push(a),i.push(t[a]));S._setFieldList(e,i,n)},S._setFieldExpire=function(e,t,n){var a=new Date;a.setTime(a.getTime()+1e3*t),null==S._fields&&(S._fields={}),S._fields["expire"+e]=Math.floor(a.getTime()/1e3)+(n?"s":""),t<0?(S._fieldsExpired||(S._fieldsExpired={}),S._fieldsExpired[e]=!0):S._fieldsExpired&&(S._fieldsExpired[e]=!1),n&&(S.cookieRead(S.sessionCookieName)||S.cookieWrite(S.sessionCookieName,"1"))},S._findVisitorID=function(e){return e&&("object"==typeof e&&(e=e.d_mid?e.d_mid:e.visitorID?e.visitorID:e.id?e.id:e.uuid?e.uuid:""+e),e&&"NOTARGET"===(e=e.toUpperCase())&&(e=F),e&&(e===F||e.match(h.VALID_VISITOR_ID_REGEX))||(e="")),e},S._setFields=function(e,t){if(S._clearTimeout(e),null!=S._loading&&(S._loading[e]=!1),B.fieldGroupObj[e]&&B.setState(e,!1),"MC"===e){!0!==B.isClientSideMarketingCloudVisitorID&&(B.isClientSideMarketingCloudVisitorID=!1);var n=S._getField(D);if(!n||S.overwriteCrossDomainMCIDAndAID){if(!(n="object"==typeof t&&t.mid?t.mid:S._findVisitorID(t))){if(S._use1stPartyMarketingCloudServer&&!S.tried1stPartyMarketingCloudServer)return S.tried1stPartyMarketingCloudServer=!0,void S.getAnalyticsVisitorID(null,!1,!0);n=S._generateLocalMID()}S._setField(D,n)}n&&n!==F||(n=""),"object"==typeof t&&((t.d_region||t.dcs_region||t.d_blob||t.blob)&&S._setFields(T,t),S._use1stPartyMarketingCloudServer&&t.mid&&S._setFields(E,{id:t.id})),S._callAllCallbacks(D,[n])}if(e===T&&"object"==typeof t){var a=604800;null!=t.id_sync_ttl&&t.id_sync_ttl&&(a=parseInt(t.id_sync_ttl,10));var i=x.getRegionAndCheckIfChanged(t,a);S._callAllCallbacks("MCAAMLH",[i]);var r=S._getField(M);(t.d_blob||t.blob)&&((r=t.d_blob)||(r=t.blob),S._setFieldExpire(M,a),S._setField(M,r)),r||(r=""),S._callAllCallbacks(M,[r]),!t.error_msg&&S._newCustomerIDsHash&&S._setField("MCCIDH",S._newCustomerIDsHash)}if(e===E){var o=S._getField(P);o&&!S.overwriteCrossDomainMCIDAndAID||((o=S._findVisitorID(t))?o!==F&&S._setFieldExpire(M,-1):o=F,S._setField(P,o)),o&&o!==F||(o=""),S._callAllCallbacks(P,[o])}if(S.idSyncDisableSyncs||S.disableIdSyncs)x.idCallNotProcesssed=!0;else{x.idCallNotProcesssed=!1;var s={};s.ibs=t.ibs,s.subdomain=t.subdomain,x.processIDCallData(s)}var c,l;t===Object(t)&&(S.isAllowed()&&(c=S._getField("MCOPTOUT")),c||(c=F,t.d_optout&&t.d_optout instanceof Array&&(c=t.d_optout.join(",")),l=parseInt(t.d_ottl,10),isNaN(l)&&(l=7200),S._setFieldExpire("MCOPTOUT",l,!0),S._setField("MCOPTOUT",c)),S._callAllCallbacks("MCOPTOUT",[c]))},S._loading=null,S._getRemoteField=function(e,t,n,a,i){var r,o="",s=N.isFirstPartyAnalyticsVisitorIDCall(e),c={MCAAMLH:!0,MCAAMB:!0};if(S.isAllowed())if(S._readVisitor(),!(!(o=S._getField(e,!0===c[e]))||S._fieldsExpired&&S._fieldsExpired[e])||S.disableThirdPartyCalls&&!s)o||(e===D?(S._registerCallback(e,n),o=S._generateLocalMID(),S.setMarketingCloudVisitorID(o)):e===P?(S._registerCallback(e,n),o="",S.setAnalyticsVisitorID(o)):(o="",a=!0));else if(e===D||"MCOPTOUT"===e?r="MC":"MCAAMLH"===e||e===M?r=T:e===P&&(r=E),r)return!t||null!=S._loading&&S._loading[r]||(null==S._loading&&(S._loading={}),S._loading[r]=!0,S._loadData(r,t,function(t){if(!S._getField(e)){t&&B.setState(r,!0);var n="";e===D?n=S._generateLocalMID():r===T&&(n={error_msg:"timeout"}),S._setFields(r,n)}},i)),S._registerCallback(e,n),o||(t||S._setFields(r,{id:F}),"");return e!==D&&e!==P||o!==F||(o="",a=!0),n&&a&&S._callCallback(n,[o]),o},S._setMarketingCloudFields=function(e){S._readVisitor(),S._setFields("MC",e)},S._mapCustomerIDs=function(e){S.getAudienceManagerBlob(e,!0)},S._setAnalyticsFields=function(e){S._readVisitor(),S._setFields(E,e)},S._setAudienceManagerFields=function(e){S._readVisitor(),S._setFields(T,e)},S._getAudienceManagerURLData=function(e){var t=S.audienceManagerServer,n="",a=S._getField(D),i=S._getField(M,!0),r=S._getField(P),o=r&&r!==F?"&d_cid_ic=AVID%01"+encodeURIComponent(r):"";if(S.loadSSL&&S.audienceManagerServerSecure&&(t=S.audienceManagerServerSecure),t){var s,c,l=S.getCustomerIDs();if(l)for(s in l)w(s)&&(c=l[s],o+="&d_cid_ic="+encodeURIComponent(s)+"%01"+encodeURIComponent(c.id?c.id:"")+(c.authState?"%01"+c.authState:""));e||(e="_setAudienceManagerFields");var u="http"+(S.loadSSL?"s":"")+"://"+t+"/id",d="d_visid_ver="+S.version+"&d_rtbd=json&d_ver=2"+(!a&&S._use1stPartyMarketingCloudServer?"&d_verify=1":"")+"&d_orgid="+encodeURIComponent(S.marketingCloudOrgID)+"&d_nsid="+(S.idSyncContainerID||0)+(a?"&d_mid="+encodeURIComponent(a):"")+(S.idSyncDisable3rdPartySyncing||S.disableThirdPartyCookies?"&d_coppa=true":"")+(!0===_?"&d_coop_safe=1":!1===_?"&d_coop_unsafe=1":"")+(i?"&d_blob="+encodeURIComponent(i):"")+o,g=["s_c_il",S._in,e];return{url:n=u+"?"+d+"&d_cb=s_c_il%5B"+S._in+"%5D."+e,corsUrl:u+"?"+d,callback:g}}return{url:n}},S.appendVisitorIDsTo=function(e){try{var t=[[D,S._getField(D)],[P,S._getField(P)],["MCORGID",S.marketingCloudOrgID]];return S._addQuerystringParam(e,h.ADOBE_MC,y(t))}catch(t){return e}},S.appendSupplementalDataIDTo=function(e,t){if(!(t=t||S.getSupplementalDataID(N.generateRandomString(),!0)))return e;try{var n=y([["SDID",t],["MCORGID",S.marketingCloudOrgID]]);return S._addQuerystringParam(e,h.ADOBE_MC_SDID,n)}catch(t){return e}};var N={parseHash:function(e){var t=e.indexOf("#");return t>0?e.substr(t):""},hashlessUrl:function(e){var t=e.indexOf("#");return t>0?e.substr(0,t):e},addQueryParamAtLocation:function(e,t,n){var a=e.split("&");return n=null!=n?n:a.length,a.splice(n,0,t),a.join("&")},isFirstPartyAnalyticsVisitorIDCall:function(e,t,n){return e===P&&(t||(t=S.trackingServer),n||(n=S.trackingServerSecure),!("string"!=typeof(a=S.loadSSL?n:t)||!a.length)&&a.indexOf("2o7.net")<0&&a.indexOf("omtrdc.net")<0);var a},isObject:function(e){return Boolean(e&&e===Object(e))},removeCookie:function(e){document.cookie=encodeURIComponent(e)+"=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;"},isTrackingServerPopulated:function(){return!!S.trackingServer||!!S.trackingServerSecure},getTimestampInSeconds:function(){return Math.round((new Date).getTime()/1e3)},parsePipeDelimetedKeyValues:function(e){return e.split("|").reduce(function(e,t){var n=t.split("=");return e[n[0]]=decodeURIComponent(n[1]),e},{})},generateRandomString:function(e){e=e||5;for(var t="",n="abcdefghijklmnopqrstuvwxyz0123456789";e--;)t+=n[Math.floor(Math.random()*n.length)];return t},parseBoolean:function(e){return"true"===e||"false"!==e&&null},replaceMethodsWithFunction:function(e,t){for(var n in e)e.hasOwnProperty(n)&&"function"==typeof e[n]&&(e[n]=t);return e},pluck:function(e,t){return t.reduce(function(t,n){return e[n]&&(t[n]=e[n]),t},Object.create(null))}};S._helpers=N;var x=m(S,I);S._destinationPublishing=x,S.timeoutMetricsLog=[];var O,B={isClientSideMarketingCloudVisitorID:null,MCIDCallTimedOut:null,AnalyticsIDCallTimedOut:null,AAMIDCallTimedOut:null,fieldGroupObj:{},setState:function(e,t){switch(e){case"MC":!1===t?!0!==this.MCIDCallTimedOut&&(this.MCIDCallTimedOut=!1):this.MCIDCallTimedOut=t;break;case E:!1===t?!0!==this.AnalyticsIDCallTimedOut&&(this.AnalyticsIDCallTimedOut=!1):this.AnalyticsIDCallTimedOut=t;break;case T:!1===t?!0!==this.AAMIDCallTimedOut&&(this.AAMIDCallTimedOut=!1):this.AAMIDCallTimedOut=t}}};S.isClientSideMarketingCloudVisitorID=function(){return B.isClientSideMarketingCloudVisitorID},S.MCIDCallTimedOut=function(){return B.MCIDCallTimedOut},S.AnalyticsIDCallTimedOut=function(){return B.AnalyticsIDCallTimedOut},S.AAMIDCallTimedOut=function(){return B.AAMIDCallTimedOut},S.idSyncGetOnPageSyncInfo=function(){return S._readVisitor(),S._getField("MCSYNCSOP")},S.idSyncByURL=function(e){var t=V(e||{});if(t.error)return t.error;var n,a,i=e.url,r=encodeURIComponent,o=x;return i=i.replace(/^https:/,"").replace(/^http:/,""),n=c.encodeAndBuildRequest(["",e.dpid,e.dpuuid||""],","),a=["ibs",r(e.dpid),"img",r(i),t.ttl,"",n],o.addMessage(a.join("|")),o.requestToProcess(),"Successfully queued"},S.idSyncByDataSource=function(e){return e===Object(e)&&"string"==typeof e.dpuuid&&e.dpuuid.length?(e.url="//dpm.demdex.net/ibs:dpid="+e.dpid+"&dpuuid="+e.dpuuid,S.idSyncByURL(e)):"Error: config or config.dpuuid is empty"},S._getCookieVersion=function(e){e=e||S.cookieRead(S.cookieName);var t=h.VERSION_REGEX.exec(e);return t&&t.length>1?t[1]:null},S._resetAmcvCookie=function(e){var t=S._getCookieVersion();t&&!u.isLessThan(t,e)||N.removeCookie(S.cookieName)},S.setAsCoopSafe=function(){_=!0},S.setAsCoopUnsafe=function(){_=!1},S.init=function(){!function(){if(t&&"object"==typeof t){for(var e in S.configs=Object.create(null),t)w(e)&&(S[e]=t[e],S.configs[e]=t[e]);S.idSyncContainerID=S.idSyncContainerID||0,_="boolean"==typeof S.isCoopSafe?S.isCoopSafe:N.parseBoolean(S.isCoopSafe),S.resetBeforeVersion&&S._resetAmcvCookie(S.resetBeforeVersion),S._attemptToPopulateIdsFromUrl(),S._attemptToPopulateSdidFromUrl(),S._readVisitor();var n=S._getField(L),a=Math.ceil((new Date).getTime()/h.MILLIS_PER_DAY);S.idSyncDisableSyncs||S.disableIdSyncs||!x.canMakeSyncIDCall(n,a)||(S._setFieldExpire(M,-1),S._setField(L,a)),S.getMarketingCloudVisitorID(),S.getAudienceManagerLocationHint(),S.getAudienceManagerBlob(),S._mergeServerState(S.serverState)}else S._attemptToPopulateIdsFromUrl(),S._attemptToPopulateSdidFromUrl()}(),function(){if(!S.idSyncDisableSyncs&&!S.disableIdSyncs){x.checkDPIframeSrc();var e=function(){var e=x;e.readyToAttachIframe()&&e.attachIframe()};C.addEventListener("load",function(){I.windowLoaded=!0,e()});try{d.receiveMessage(function(e){x.receiveMessage(e.data)},x.iframeHost)}catch(e){}}}(),S.whitelistIframeDomains&&h.POST_MESSAGE_ENABLED&&(S.whitelistIframeDomains=S.whitelistIframeDomains instanceof Array?S.whitelistIframeDomains:[S.whitelistIframeDomains],S.whitelistIframeDomains.forEach(function(t){var n=new i(e,t),a=r(S,n);d.receiveMessage(a,t)}))}};p.getInstance=function(e,t){if(!e)throw new Error("Visitor requires Adobe Marketing Cloud Org ID.");e.indexOf("@")<0&&(e+="@AdobeOrg");var i=function(){var t=n.s_c_il;if(t)for(var a=0;a<t.length;a++){var i=t[a];if(i&&"Visitor"===i._c&&i.marketingCloudOrgID===e)return i}}();if(i)return i;var r=e.split("").reverse().join(""),o=new p(e,null,r);n.s_c_il.splice(--n.s_c_in,1);var s=c.getIeVersion();if("number"==typeof s&&s<10)return o._helpers.replaceMethodsWithFunction(o,function(){});var l=o.isAllowed(),u=function(){try{return n.self!==n.parent}catch(e){return!0}}()&&!l&&n.parent?new a(e,t,o,n.parent):new p(e,t,r);return o=null,u.init(),u},function(){function e(){p.windowLoaded=!0}n.addEventListener?n.addEventListener("load",e):n.attachEvent&&n.attachEvent("onload",e),p.codeLoadEnd=(new Date).getTime()}(),n.Visitor=p,t.exports=p}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"./child/ChildVisitor":2,"./child/Message":3,"./child/makeChildMessageListener":4,"./units/crossDomain":8,"./units/makeCorsRequest":9,"./units/makeDestinationPublishing":10,"./units/version":11,"./utils/asyncParallelApply":12,"./utils/constants":14,"./utils/enums":15,"./utils/getDomain":16,"./utils/utils":18,"@adobe-mcid/visitor-js-shared/lib/ids/generateRandomID":19}],2:[function(e,t){(function(n){e("../utils/polyfills");var a=e("./strategies/LocalVisitor"),i=e("./strategies/ProxyVisitor"),r=e("./strategies/PlaceholderVisitor"),o=e("../utils/callbackRegistryFactory"),s=e("./Message"),c=e("../utils/enums").MESSAGES;t.exports=function(e,t,l,u){function d(e){Object.assign(V,e)}function g(e){Object.assign(V.state,e),V.callbackRegistry.executeAll(V.state)}function f(e){if(!I.isInvalid(e)){C=!1;var t=I.parse(e);V.setStateAndPublish(t.state)}}function m(e){!C&&S&&(C=!0,I.send(u,e))}function h(){d(new a(l._generateID)),V.getMarketingCloudVisitorID(),V.callbackRegistry.executeAll(V.state,!0),n.removeEventListener("message",p)}function p(e){if(!I.isInvalid(e)){var t=I.parse(e);C=!1,n.clearTimeout(this.timeout),n.removeEventListener("message",p),d(new i(V)),n.addEventListener("message",f),V.setStateAndPublish(t.state),V.callbackRegistry.hasCallbacks()&&m(c.GETSTATE)}}function v(){S&&postMessage?(n.addEventListener("message",p),m(c.HANDSHAKE),this.timeout=setTimeout(h,250)):h()}function b(){n.s_c_in||(n.s_c_il=[],n.s_c_in=0),V._c="Visitor",V._il=n.s_c_il,V._in=n.s_c_in,V._il[V._in]=V,n.s_c_in++}function y(){function e(e){0!==e.indexOf("_")&&"function"==typeof l[e]&&(V[e]=function(){})}Object.keys(l).forEach(e),V.getSupplementalDataID=l.getSupplementalDataID}var V=this,S=t.whitelistParentDomain;V.state={},V.version=l.version,V.marketingCloudOrgID=e;var C=!1,I=new s(e,S);V.callbackRegistry=o(),V.init=function(){b(),y(),d(new r(V)),v()},V.findField=function(e,t){if(V.state[e])return t(V.state[e]),V.state[e]},V.messageParent=m,V.setStateAndPublish=g}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"../utils/callbackRegistryFactory":13,"../utils/enums":15,"../utils/polyfills":17,"./Message":3,"./strategies/LocalVisitor":5,"./strategies/PlaceholderVisitor":6,"./strategies/ProxyVisitor":7}],3:[function(e,t){var n=e("../utils/enums").MESSAGES,a={0:"prefix",1:"orgID",2:"state"};t.exports=function(e,t){this.parse=function(e){try{var t={};return e.data.split("|").forEach(function(e,n){void 0!==e&&(t[a[n]]=2!==n?e:JSON.parse(e))}),t}catch(e){}},this.isInvalid=function(a){var i=this.parse(a);if(!i||Object.keys(i).length<2)return!0;var r=e!==i.orgID,o=!t||a.origin!==t,s=-1===Object.keys(n).indexOf(i.prefix);return r||o||s},this.send=function(n,a,i){var r=a+"|"+e;i&&i===Object(i)&&(r+="|"+JSON.stringify(i));try{n.postMessage(r,t)}catch(e){}}}},{"../utils/enums":15}],4:[function(e,t){var n=e("../utils/enums"),a=e("../utils/utils"),i=n.MESSAGES,r=n.ALL_APIS,o=n.ASYNC_API_MAP,s=n.FIELDGROUP_TO_FIELD;t.exports=function(e,t){function n(){var t={};return Object.keys(r).forEach(function(n){var i=r[n],o=e[i]();a.isValueEmpty(o)||(t[n]=o)}),t}function c(){var t=[];return e._loading&&Object.keys(e._loading).forEach(function(n){if(e._loading[n]){var a=s[n];t.push(a)}}),t.length?t:null}function l(t){return function n(){var a=c();if(a){var i=o[a[0]];e[i](n,!0)}else t()}}function u(e,a){var i=n();t.send(e,a,i)}function d(e){f(e),u(e,i.HANDSHAKE)}function g(e){l(function(){u(e,i.PARENTSTATE)})()}function f(n){function a(a){r.call(e,a),t.send(n,i.PARENTSTATE,{CUSTOMERIDS:e.getCustomerIDs()})}var r=e.setCustomerIDs;e.setCustomerIDs=a}return function(e){t.isInvalid(e)||(t.parse(e).prefix===i.HANDSHAKE?d:g)(e.source)}}},{"../utils/enums":15,"../utils/utils":18}],5:[function(e,t){var n=e("../../utils/enums").STATE_KEYS_MAP;t.exports=function(e){function t(){}function a(t,a){var i=this;return function(){var t=e(0,n.MCMID),r={};return r[n.MCMID]=t,i.setStateAndPublish(r),a(t),t}}this.getMarketingCloudVisitorID=function(e){e=e||t;var i=this.findField(n.MCMID,e),r=a.call(this,n.MCMID,e);return void 0!==i?i:r()}}},{"../../utils/enums":15}],6:[function(e,t){var n=e("../../utils/enums").ASYNC_API_MAP;t.exports=function(){Object.keys(n).forEach(function(e){this[n[e]]=function(t){this.callbackRegistry.add(e,t)}},this)}},{"../../utils/enums":15}],7:[function(e,t){var n=e("../../utils/enums"),a=n.MESSAGES,i=n.ASYNC_API_MAP,r=n.SYNC_API_MAP;t.exports=function(){function e(){}function t(e,t){var n=this;return function(){return n.callbackRegistry.add(e,t),n.messageParent(a.GETSTATE),""}}function n(n){this[i[n]]=function(a){a=a||e;var i=this.findField(n,a),r=t.call(this,n,a);return void 0!==i?i:r()}}function o(t){this[r[t]]=function(){return this.findField(t,e)||{}}}Object.keys(i).forEach(n,this),Object.keys(r).forEach(o,this)}},{"../../utils/enums":15}],8:[function(e,t){(function(e){var n=!!e.postMessage;t.exports={postMessage:function(e,t,a){var i=1;t&&(n?a.postMessage(e,t.replace(/([^:]+:\/\/[^\/]+).*/,"$1")):t&&(a.location=t.replace(/#.*$/,"")+"#"+ +new Date+i+++"&"+e))},receiveMessage:function(t,a){var i;try{n&&(t&&(i=function(e){if("string"==typeof a&&e.origin!==a||"[object Function]"===Object.prototype.toString.call(a)&&!1===a(e.origin))return!1;t(e)}),e.addEventListener?e[t?"addEventListener":"removeEventListener"]("message",i):e[t?"attachEvent":"detachEvent"]("onmessage",i))}catch(e){}}}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],9:[function(e,t){(function(e){t.exports=function(t){return{corsMetadata:(n="none",a=!0,"undefined"!=typeof XMLHttpRequest&&XMLHttpRequest===Object(XMLHttpRequest)&&("withCredentials"in new XMLHttpRequest?n="XMLHttpRequest":"undefined"!=typeof XDomainRequest&&XDomainRequest===Object(XDomainRequest)&&(a=!1),Object.prototype.toString.call(e.HTMLElement).indexOf("Constructor")>0&&(a=!1)),{corsType:n,corsCookiesEnabled:a}),getCORSInstance:function(){return"none"===this.corsMetadata.corsType?null:new e[this.corsMetadata.corsType]},fireCORS:function(n,a){function i(t){var a;try{if((a=JSON.parse(t))!==Object(a))return void r.handleCORSError(n,null,"Response is not JSON")}catch(e){return void r.handleCORSError(n,e,"Error parsing response as JSON")}try{for(var i=n.callback,o=e,s=0;s<i.length;s++)o=o[i[s]];o(a)}catch(e){r.handleCORSError(n,e,"Error forming callback function")}}var r=this;a&&(n.loadErrorHandler=a);try{var o=this.getCORSInstance();o.open("get",n.corsUrl+"&ts="+(new Date).getTime(),!0),"XMLHttpRequest"===this.corsMetadata.corsType&&(o.withCredentials=!0,o.timeout=t.loadTimeout,o.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),o.onreadystatechange=function(){4===this.readyState&&200===this.status&&i(this.responseText)}),o.onerror=function(e){r.handleCORSError(n,e,"onerror")},o.ontimeout=function(e){r.handleCORSError(n,e,"ontimeout")},o.send(),t._log.requests.push(n.corsUrl)}catch(e){this.handleCORSError(n,e,"try-catch")}},handleCORSError:function(e,n,a){t.CORSErrors.push({corsData:e,error:n,description:a}),e.loadErrorHandler&&("ontimeout"===a?e.loadErrorHandler(!0):e.loadErrorHandler(!1))}};var n,a}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],10:[function(e,t){(function(n){var a=e("../utils/constants"),i=e("./crossDomain"),r=e("../utils/utils");t.exports=function(e,t){var o=n.document;return{THROTTLE_START:3e4,MAX_SYNCS_LENGTH:649,throttleTimerSet:!1,id:null,onPagePixels:[],iframeHost:null,getIframeHost:function(e){if("string"==typeof e){var t=e.split("/");return t[0]+"//"+t[2]}},subdomain:null,url:null,getUrl:function(){var t,n="http://fast.",a="?d_nsid="+e.idSyncContainerID+"#"+encodeURIComponent(o.location.href);return this.subdomain||(this.subdomain="nosubdomainreturned"),e.loadSSL&&(n=e.idSyncSSLUseAkamai?"https://fast.":"https://"),t=n+this.subdomain+".demdex.net/dest5.html"+a,this.iframeHost=this.getIframeHost(t),this.id="destination_publishing_iframe_"+this.subdomain+"_"+e.idSyncContainerID,t},checkDPIframeSrc:function(){var t="?d_nsid="+e.idSyncContainerID+"#"+encodeURIComponent(o.location.href);"string"==typeof e.dpIframeSrc&&e.dpIframeSrc.length&&(this.id="destination_publishing_iframe_"+(e._subdomain||this.subdomain||(new Date).getTime())+"_"+e.idSyncContainerID,this.iframeHost=this.getIframeHost(e.dpIframeSrc),this.url=e.dpIframeSrc+t)},idCallNotProcesssed:null,doAttachIframe:!1,startedAttachingIframe:!1,iframeHasLoaded:null,iframeIdChanged:null,newIframeCreated:null,originalIframeHasLoadedAlready:null,regionChanged:!1,timesRegionChanged:0,sendingMessages:!1,messages:[],messagesPosted:[],messagesReceived:[],messageSendingInterval:a.POST_MESSAGE_ENABLED?null:100,jsonForComparison:[],jsonDuplicates:[],jsonWaiting:[],jsonProcessed:[],canSetThirdPartyCookies:!0,receivedThirdPartyCookiesNotification:!1,readyToAttachIframe:function(){
	return!e.idSyncDisable3rdPartySyncing&&(this.doAttachIframe||e._doAttachIframe)&&(this.subdomain&&"nosubdomainreturned"!==this.subdomain||e._subdomain)&&this.url&&!this.startedAttachingIframe},attachIframe:function(){function e(){(a=o.createElement("iframe")).sandbox="allow-scripts allow-same-origin",a.title="Adobe ID Syncing iFrame",a.id=n.id,a.name=n.id+"_name",a.style.cssText="display: none; width: 0; height: 0;",a.src=n.url,n.newIframeCreated=!0,t(),o.body.appendChild(a)}function t(){a.addEventListener("load",function(){a.className="aamIframeLoaded",n.iframeHasLoaded=!0,n.requestToProcess()})}this.startedAttachingIframe=!0;var n=this,a=o.getElementById(this.id);a?"IFRAME"!==a.nodeName?(this.id+="_2",this.iframeIdChanged=!0,e()):(this.newIframeCreated=!1,"aamIframeLoaded"!==a.className?(this.originalIframeHasLoadedAlready=!1,t()):(this.originalIframeHasLoadedAlready=!0,this.iframeHasLoaded=!0,this.iframe=a,this.requestToProcess())):e(),this.iframe=a},requestToProcess:function(t){function n(){r.jsonForComparison.push(t),r.jsonWaiting.push(t),r.processSyncOnPage(t)}var i,r=this;if(t===Object(t)&&t.ibs)if(i=JSON.stringify(t.ibs||[]),this.jsonForComparison.length){var o,s,c,l=!1;for(o=0,s=this.jsonForComparison.length;o<s;o++)if(c=this.jsonForComparison[o],i===JSON.stringify(c.ibs||[])){l=!0;break}l?this.jsonDuplicates.push(t):n()}else n();if((this.receivedThirdPartyCookiesNotification||!a.POST_MESSAGE_ENABLED||this.iframeHasLoaded)&&this.jsonWaiting.length){var u=this.jsonWaiting.shift();this.process(u),this.requestToProcess()}!e.idSyncDisableSyncs&&this.iframeHasLoaded&&this.messages.length&&!this.sendingMessages&&(this.throttleTimerSet||(this.throttleTimerSet=!0,setTimeout(function(){r.messageSendingInterval=a.POST_MESSAGE_ENABLED?null:150},this.THROTTLE_START)),this.sendingMessages=!0,this.sendMessages())},getRegionAndCheckIfChanged:function(t,n){var a=e._getField("MCAAMLH"),i=t.d_region||t.dcs_region;return a?i&&(e._setFieldExpire("MCAAMLH",n),e._setField("MCAAMLH",i),parseInt(a,10)!==i&&(this.regionChanged=!0,this.timesRegionChanged++,e._setField("MCSYNCSOP",""),e._setField("MCSYNCS",""),a=i)):(a=i)&&(e._setFieldExpire("MCAAMLH",n),e._setField("MCAAMLH",a)),a||(a=""),a},processSyncOnPage:function(e){var t,n,a,i;if((t=e.ibs)&&t instanceof Array&&(n=t.length))for(a=0;a<n;a++)(i=t[a]).syncOnPage&&this.checkFirstPartyCookie(i,"","syncOnPage")},process:function(e){var t,n,a,i,o,s=encodeURIComponent,c=!1;if((t=e.ibs)&&t instanceof Array&&(n=t.length))for(c=!0,a=0;a<n;a++)i=t[a],o=[s("ibs"),s(i.id||""),s(i.tag||""),r.encodeAndBuildRequest(i.url||[],","),s(i.ttl||""),"","",i.fireURLSync?"true":"false"],i.syncOnPage||(this.canSetThirdPartyCookies?this.addMessage(o.join("|")):i.fireURLSync&&this.checkFirstPartyCookie(i,o.join("|")));c&&this.jsonProcessed.push(e)},checkFirstPartyCookie:function(t,n,i){var r="syncOnPage"===i,o=r?"MCSYNCSOP":"MCSYNCS";e._readVisitor();var s,c,l=e._getField(o),u=!1,d=!1,g=Math.ceil((new Date).getTime()/a.MILLIS_PER_DAY);l?(s=l.split("*"),u=(c=this.pruneSyncData(s,t.id,g)).dataPresent,d=c.dataValid,u&&d||this.fireSync(r,t,n,s,o,g)):(s=[],this.fireSync(r,t,n,s,o,g))},pruneSyncData:function(e,t,n){var a,i,r,o=!1,s=!1;for(i=0;i<e.length;i++)a=e[i],r=parseInt(a.split("-")[1],10),a.match("^"+t+"-")?(o=!0,n<r?s=!0:(e.splice(i,1),i--)):n>=r&&(e.splice(i,1),i--);return{dataPresent:o,dataValid:s}},manageSyncsSize:function(e){if(e.join("*").length>this.MAX_SYNCS_LENGTH)for(e.sort(function(e,t){return parseInt(e.split("-")[1],10)-parseInt(t.split("-")[1],10)});e.join("*").length>this.MAX_SYNCS_LENGTH;)e.shift()},fireSync:function(t,n,a,i,r,o){var s=this;if(t){if("img"===n.tag){var c,l,u,d,g=n.url,f=e.loadSSL?"https:":"http:";for(c=0,l=g.length;c<l;c++){u=g[c],d=/^\/\//.test(u);var m=new Image;m.addEventListener("load",function(t,n,a,i){return function(){s.onPagePixels[t]=null,e._readVisitor();var o,c,l,u,d=e._getField(r),g=[];if(d)for(c=0,l=(o=d.split("*")).length;c<l;c++)(u=o[c]).match("^"+n.id+"-")||g.push(u);s.setSyncTrackingData(g,n,a,i)}}(this.onPagePixels.length,n,r,o)),m.src=(d?f:"")+u,this.onPagePixels.push(m)}}}else this.addMessage(a),this.setSyncTrackingData(i,n,r,o)},addMessage:function(t){var n=encodeURIComponent(e._enableErrorReporting?"---destpub-debug---":"---destpub---");this.messages.push((a.POST_MESSAGE_ENABLED?"":n)+t)},setSyncTrackingData:function(t,n,a,i){t.push(n.id+"-"+(i+Math.ceil(n.ttl/60/24))),this.manageSyncsSize(t),e._setField(a,t.join("*"))},sendMessages:function(){var e,t=this,n="",i=encodeURIComponent;this.regionChanged&&(n=i("---destpub-clear-dextp---"),this.regionChanged=!1),this.messages.length?a.POST_MESSAGE_ENABLED?(e=n+i("---destpub-combined---")+this.messages.join("%01"),this.postMessage(e),this.messages=[],this.sendingMessages=!1):(e=this.messages.shift(),this.postMessage(n+e),setTimeout(function(){t.sendMessages()},this.messageSendingInterval)):this.sendingMessages=!1},postMessage:function(e){i.postMessage(e,this.url,this.iframe.contentWindow),this.messagesPosted.push(e)},receiveMessage:function(e){var t,n=/^---destpub-to-parent---/;"string"==typeof e&&n.test(e)&&("canSetThirdPartyCookies"===(t=e.replace(n,"").split("|"))[0]&&(this.canSetThirdPartyCookies="true"===t[1],this.receivedThirdPartyCookiesNotification=!0,this.requestToProcess()),this.messagesReceived.push(e))},processIDCallData:function(n){(null==this.url||n.subdomain&&"nosubdomainreturned"===this.subdomain)&&("string"==typeof e._subdomain&&e._subdomain.length?this.subdomain=e._subdomain:this.subdomain=n.subdomain||"",this.url=this.getUrl()),n.ibs instanceof Array&&n.ibs.length&&(this.doAttachIframe=!0),this.readyToAttachIframe()&&(e.idSyncAttachIframeOnWindowLoad?(t.windowLoaded||"complete"===o.readyState||"loaded"===o.readyState)&&this.attachIframe():this.attachIframeASAP()),"function"==typeof e.idSyncIDCallResult?e.idSyncIDCallResult(n):this.requestToProcess(n),"function"==typeof e.idSyncAfterIDCallResult&&e.idSyncAfterIDCallResult(n)},canMakeSyncIDCall:function(t,n){return e._forceSyncIDCall||!t||n-t>a.DAYS_BETWEEN_SYNC_ID_CALLS},attachIframeASAP:function(){function e(){t.startedAttachingIframe||(o.body?t.attachIframe():setTimeout(e,30))}var t=this;e()}}}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"../utils/constants":14,"../utils/utils":18,"./crossDomain":8}],11:[function(e,t){function n(e){for(var t=/^\d+$/,n=0,a=e.length;n<a;n++)if(!t.test(e[n]))return!1;return!0}function a(e,t){for(;e.length<t.length;)e.push("0");for(;t.length<e.length;)t.push("0")}function i(e,t){for(var n=0;n<e.length;n++){var a=parseInt(e[n],10),i=parseInt(t[n],10);if(a>i)return 1;if(i>a)return-1}return 0}function r(e,t){if(e===t)return 0;var r=e.toString().split("."),o=t.toString().split(".");return n(r.concat(o))?(a(r,o),i(r,o)):NaN}t.exports={compare:r,isLessThan:function(e,t){return r(e,t)<0},areVersionsDifferent:function(e,t){return 0!==r(e,t)},isGreaterThan:function(e,t){return r(e,t)>0},isEqual:function(e,t){return 0===r(e,t)}}},{}],12:[function(e,t){t.exports=function(e,t){function n(e){return function(n){a[e]=n,++i===r&&t(a)}}var a={},i=0,r=Object.keys(e).length;Object.keys(e).forEach(function(t){var a=e[t];if(a.fn){var i=a.args||[];i.unshift(n(t)),a.fn.apply(a.context||null,i)}})}},{}],13:[function(e,t){function n(){return{callbacks:{},add:function(e,t){this.callbacks[e]=this.callbacks[e]||[];var n=this.callbacks[e].push(t)-1;return function(){this.callbacks[e].splice(n,1)}},execute:function(e,t){if(this.callbacks[e]){t=(t=void 0===t?[]:t)instanceof Array?t:[t];try{for(;this.callbacks[e].length;){var n=this.callbacks[e].shift();"function"==typeof n?n.apply(null,t):n instanceof Array&&n[1].apply(n[0],t)}delete this.callbacks[e]}catch(e){}}},executeAll:function(e,t){(t||e&&!a.isObjectEmpty(e))&&Object.keys(this.callbacks).forEach(function(t){var n=void 0!==e[t]?e[t]:"";this.execute(t,n)},this)},hasCallbacks:function(){return Boolean(Object.keys(this.callbacks).length)}}}var a=e("./utils");t.exports=n},{"./utils":18}],14:[function(e,t){(function(e){t.exports={POST_MESSAGE_ENABLED:!!e.postMessage,DAYS_BETWEEN_SYNC_ID_CALLS:1,MILLIS_PER_DAY:864e5,ADOBE_MC:"adobe_mc",ADOBE_MC_SDID:"adobe_mc_sdid",VALID_VISITOR_ID_REGEX:/^[0-9a-fA-F\-]+$/,ADOBE_MC_TTL_IN_MIN:5,VERSION_REGEX:/vVersion\|((\d+\.)?(\d+\.)?(\*|\d+))(?=$|\|)/}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],15:[function(e,t,n){n.MESSAGES={HANDSHAKE:"HANDSHAKE",GETSTATE:"GETSTATE",PARENTSTATE:"PARENTSTATE"},n.STATE_KEYS_MAP={MCMID:"MCMID",MCAID:"MCAID",MCAAMB:"MCAAMB",MCAAMLH:"MCAAMLH",MCOPTOUT:"MCOPTOUT",CUSTOMERIDS:"CUSTOMERIDS"},n.ASYNC_API_MAP={MCMID:"getMarketingCloudVisitorID",MCAID:"getAnalyticsVisitorID",MCAAMB:"getAudienceManagerBlob",MCAAMLH:"getAudienceManagerLocationHint",MCOPTOUT:"getOptOut"},n.SYNC_API_MAP={CUSTOMERIDS:"getCustomerIDs"},n.ALL_APIS={MCMID:"getMarketingCloudVisitorID",MCAAMB:"getAudienceManagerBlob",MCAAMLH:"getAudienceManagerLocationHint",MCOPTOUT:"getOptOut",MCAID:"getAnalyticsVisitorID",CUSTOMERIDS:"getCustomerIDs"},n.FIELDGROUP_TO_FIELD={MC:"MCMID",A:"MCAID",AAM:"MCAAMB"},n.FIELDS={MCMID:"MCMID",MCOPTOUT:"MCOPTOUT",MCAID:"MCAID",MCAAMLH:"MCAAMLH",MCAAMB:"MCAAMB"},n.AUTH_STATE={UNKNOWN:0,AUTHENTICATED:1,LOGGED_OUT:2},n.OPT_OUT={GLOBAL:"global"}},{}],16:[function(e,t){(function(e){t.exports=function(t){var n;if(!t&&e.location&&(t=e.location.hostname),n=t)if(/^[0-9.]+$/.test(n))n="";else{var a=",ac,ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,ax,az,ba,bb,be,bf,bg,bh,bi,bj,bm,bo,br,bs,bt,bv,bw,by,bz,ca,cc,cd,cf,cg,ch,ci,cl,cm,cn,co,cr,cu,cv,cw,cx,cz,de,dj,dk,dm,do,dz,ec,ee,eg,es,et,eu,fi,fm,fo,fr,ga,gb,gd,ge,gf,gg,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gw,gy,hk,hm,hn,hr,ht,hu,id,ie,im,in,io,iq,ir,is,it,je,jo,jp,kg,ki,km,kn,kp,kr,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,me,mg,mh,mk,ml,mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,na,nc,ne,nf,ng,nl,no,nr,nu,nz,om,pa,pe,pf,ph,pk,pl,pm,pn,pr,ps,pt,pw,py,qa,re,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,st,su,sv,sx,sy,sz,tc,td,tf,tg,th,tj,tk,tl,tm,tn,to,tp,tr,tt,tv,tw,tz,ua,ug,uk,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,yt,",i=n.split("."),r=i.length-1,o=r-1;if(r>1&&i[r].length<=2&&(2===i[r-1].length||a.indexOf(","+i[r]+",")<0)&&o--,o>0)for(n="";r>=o;)n=i[r]+(n?".":"")+n,r--}return n}}).call(this,"undefined"!=typeof window&&"undefined"!=typeof global&&window.global===global?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],17:[function(){Object.assign=Object.assign||function(e){for(var t,n,a=1;a<arguments.length;++a)for(t in n=arguments[a])Object.prototype.hasOwnProperty.call(n,t)&&(e[t]=n[t]);return e}},{}],18:[function(e,t,n){n.isObjectEmpty=function(e){return e===Object(e)&&0===Object.keys(e).length},n.isValueEmpty=function(e){return""===e||n.isObjectEmpty(e)},n.getIeVersion=function(){if(document.documentMode)return document.documentMode;for(var e=7;e>4;e--){var t=document.createElement("div");if(t.innerHTML="<!--[if IE "+e+"]><span></span><![endif]-->",t.getElementsByTagName("span").length)return t=null,e;t=null}return null},n.encodeAndBuildRequest=function(e,t){return e.map(encodeURIComponent).join(t)}},{}],19:[function(e,t){t.exports=function(e){var t,n,a="0123456789",i="",r="",o=8,s=10,c=10;if(1==e){for(a+="ABCDEF",t=0;16>t;t++)n=Math.floor(Math.random()*o),i+=a.substring(n,n+1),n=Math.floor(Math.random()*o),r+=a.substring(n,n+1),o=16;return i+"-"+r}for(t=0;19>t;t++)n=Math.floor(Math.random()*s),i+=a.substring(n,n+1),0===t&&9==n?s=3:(1==t||2==t)&&10!=s&&2>n?s=10:2<t&&(s=10),n=Math.floor(Math.random()*c),r+=a.substring(n,n+1),0===t&&9==n?c=3:(1==t||2==t)&&10!=c&&2>n?c=10:2<t&&(c=10);return i+r}},{}]},{},[1]),
	// All code and conventions are protected by copyright
	function(e,t,n){function a(){L.getToolsByType("nielsen").length>0&&L.domReady(L.bind(this.initialize,this))}function i(t){L.domReady(L.bind(function(){this.twttr=t||e.twttr,this.initialize()},this))}function r(){this.lastURL=L.URL(),this._fireIfURIChanged=L.bind(this.fireIfURIChanged,this),this._onPopState=L.bind(this.onPopState,this),this._onHashChange=L.bind(this.onHashChange,this),this._pushState=L.bind(this.pushState,this),this._replaceState=L.bind(this.replaceState,this),this.initialize()}function o(){var e=L.filter(L.rules,function(e){return 0===e.event.indexOf("dataelementchange")});this.dataElementsNames=L.map(e,function(e){return e.event.match(/dataelementchange\((.*)\)/i)[1]},this),this.initPolling()}function s(){L.addEventHandler(e,"orientationchange",s.orientationChange)}function c(){this.rules=L.filter(L.rules,function(e){return"videoplayed"===e.event.substring(0,11)}),this.eventHandler=L.bind(this.onUpdateTime,this)}function l(){this.defineEvents(),this.visibilityApiHasPriority=!0,t.addEventListener?this.setVisibilityApiPriority(!1):this.attachDetachOlderEventListeners(!0,t,"focusout");L.bindEvent("aftertoolinit",function(){L.fireEvent(L.visibility.isHidden()?"tabblur":"tabfocus")})}function u(t){t=t||L.rules,this.rules=L.filter(t,function(e){return"inview"===e.event}),this.elements=[],this.eventHandler=L.bind(this.track,this),L.addEventHandler(e,"scroll",this.eventHandler),L.addEventHandler(e,"load",this.eventHandler)}function d(){this.rules=L.filter(L.rules,function(e){return"elementexists"===e.event})}function g(e){this.delay=250,this.FB=e,L.domReady(L.bind(function(){L.poll(L.bind(this.initialize,this),this.delay,8)},this))}function f(){var e=this.eventRegex=/^hover\(([0-9]+)\)$/,t=this.rules=[];L.each(L.rules,function(n){n.event.match(e)&&t.push([Number(n.event.match(e)[1]),n.selector])})}function m(e){L.BaseTool.call(this,e),this.defineListeners(),this.beaconMethod="plainBeacon",this.adapt=new m.DataAdapters,this.dataProvider=new m.DataProvider.Aggregate}function h(e){L.BaseTool.call(this,e),this.styleElements={},this.targetPageParamsStore={}}function p(){L.BaseTool.call(this),this.asyncScriptCallbackQueue=[],this.argsForBlockingScripts=[]}function v(e){L.BaseTool.call(this,e),this.varBindings={},this.events=[],this.products=[],this.customSetupFuns=[]}function b(e){L.BaseTool.call(this,e),this.name=e.name||"Basic"}function y(e){L.BaseTool.call(this,e)}function V(e){L.BaseTool.call(this,e)}function S(e){L.BaseTool.call(this,e),this.name=e.name||"VisitorID",this.initialize()}var C,I,k,_=Object.prototype.toString,D=e._satellite&&e._satellite.override,L={initialized:!1,$data:function(e,t,a){if(e){var i="__satellite__",r=L.dataCache,o=e[i];o||(o=e[i]=L.uuid++);var s=r[o];if(s||(s=r[o]={}),a===n)return s[t];s[t]=a}},uuid:1,dataCache:{},keys:function(e){var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(n);return t},values:function(e){var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(e[n]);return t},isArray:Array.isArray||function(e){return"[object Array]"===_.apply(e)},isObject:function(e){return null!=e&&!L.isArray(e)&&"object"==typeof e},isString:function(e){return"string"==typeof e},isNumber:function(e){return"[object Number]"===_.apply(e)&&!L.isNaN(e)},isNaN:function(e){return e!=e},isRegex:function(e){return e instanceof RegExp},isLinkTag:function(e){return!(!e||!e.nodeName||"a"!==e.nodeName.toLowerCase())},each:function(e,t,n){for(var a=0,i=e.length;a<i;a++)t.call(n,e[a],a,e)},map:function(e,t,n){for(var a=[],i=0,r=e.length;i<r;i++)a.push(t.call(n,e[i],i,e));return a},filter:function(e,t,n){for(var a=[],i=0,r=e.length;i<r;i++){var o=e[i];t.call(n,o,i,e)&&a.push(o)}return a},any:function(e,t,n){for(var a=0,i=e.length;a<i;a++){var r=e[a];if(t.call(n,r,a,e))return!0}return!1},every:function(e,t,n){for(var a=!0,i=0,r=e.length;i<r;i++){var o=e[i];a=a&&t.call(n,o,i,e)}return a},contains:function(e,t){return-1!==L.indexOf(e,t)},indexOf:function(e,t){if(e.indexOf)return e.indexOf(t);for(var n=e.length;n--;)if(t===e[n])return n;return-1},find:function(e,t,n){if(!e)return null;for(var a=0,i=e.length;a<i;a++){var r=e[a];if(t.call(n,r,a,e))return r}return null},textMatch:function(e,t){if(null==t)throw new Error("Illegal Argument: Pattern is not present");return null!=e&&("string"==typeof t?e===t:t instanceof RegExp&&t.test(e))},stringify:function(e,t){if(t=t||[],L.isObject(e)){if(L.contains(t,e))return"<Cycle>";t.push(e)}if(L.isArray(e))return"["+L.map(e,function(e){return L.stringify(e,t)}).join(",")+"]";if(L.isString(e))return'"'+String(e)+'"';if(L.isObject(e)){var n=[];for(var a in e)e.hasOwnProperty(a)&&n.push(a+": "+L.stringify(e[a],t));return"{"+n.join(", ")+"}"}return String(e)},trim:function(e){return null==e?null:e.trim?e.trim():e.replace(/^ */,"").replace(/ *$/,"")},bind:function(e,t){return function(){return e.apply(t,arguments)}},throttle:function(e,t){var n=null;return function(){var a=this,i=arguments;clearTimeout(n),n=setTimeout(function(){e.apply(a,i)},t)}},domReady:function(e){function n(e){for(g=1;e=i.shift();)e()}var a,i=[],r=!1,o=t,s=o.documentElement,c=s.doScroll,l="DOMContentLoaded",u="addEventListener",d="onreadystatechange",g=/^loade|^c/.test(o.readyState);return o[u]&&o[u](l,a=function(){o.removeEventListener(l,a,r),n()},r),c&&o.attachEvent(d,a=function(){/^c/.test(o.readyState)&&(o.detachEvent(d,a),n())}),e=c?function(t){self!=top?g?t():i.push(t):function(){try{s.doScroll("left")}catch(n){return setTimeout(function(){e(t)},50)}t()}()}:function(e){g?e():i.push(e)}}(),loadScript:function(e,n){var a=t.createElement("script");L.scriptOnLoad(e,a,n),a.src=e,t.getElementsByTagName("head")[0].appendChild(a)},scriptOnLoad:function(e,t,n){function a(e){e&&L.logError(e),n&&n(e)}"onload"in t?(t.onload=function(){a()},t.onerror=function(){a(new Error("Failed to load script "+e))}):"readyState"in t&&(t.onreadystatechange=function(){var e=t.readyState;"loaded"!==e&&"complete"!==e||(t.onreadystatechange=null,a())})},loadScriptOnce:function(e,t){L.loadedScriptRegistry[e]||L.loadScript(e,function(n){n||(L.loadedScriptRegistry[e]=!0),t&&t(n)})},loadedScriptRegistry:{},loadScriptSync:function(e){t.write?L.domReadyFired?L.notify('Cannot load sync the "'+e+'" script after DOM Ready.',1):(e.indexOf('"')>-1&&(e=encodeURI(e)),t.write('<script src="'+e+'"></script>')):L.notify('Cannot load sync the "'+e+'" script because "document.write" is not available',1)},pushAsyncScript:function(e){L.tools["default"].pushAsyncScript(e)},pushBlockingScript:function(e){L.tools["default"].pushBlockingScript(e)},addEventHandler:e.addEventListener?function(e,t,n){e.addEventListener(t,n,!1)}:function(e,t,n){e.attachEvent("on"+t,n)},removeEventHandler:e.removeEventListener?function(e,t,n){e.removeEventListener(t,n,!1)}:function(e,t,n){e.detachEvent("on"+t,n)},preventDefault:e.addEventListener?function(e){e.preventDefault()}:function(e){e.returnValue=!1},stopPropagation:function(e){e.cancelBubble=!0,e.stopPropagation&&e.stopPropagation()},containsElement:function(e,t){return e.contains?e.contains(t):!!(16&e.compareDocumentPosition(t))},matchesCss:function(n){function a(e,t){var n=t.tagName;return!!n&&e.toLowerCase()===n.toLowerCase()}var i=n.matchesSelector||n.mozMatchesSelector||n.webkitMatchesSelector||n.oMatchesSelector||n.msMatchesSelector;return i?function(n,a){if(a===t||a===e)return!1;try{return i.call(a,n)}catch(r){return!1}}:n.querySelectorAll?function(e,t){if(!t.parentNode)return!1;if(e.match(/^[a-z]+$/i))return a(e,t);try{for(var n=t.parentNode.querySelectorAll(e),i=n.length;i--;)if(n[i]===t)return!0}catch(r){}return!1}:function(e,t){if(e.match(/^[a-z]+$/i))return a(e,t);try{return L.Sizzle.matches(e,[t]).length>0}catch(n){return!1}}}(t.documentElement),cssQuery:(C=t,C.querySelectorAll?function(e,t){var n;try{n=C.querySelectorAll(e)}catch(a){n=[]}t(n)}:function(e,t){if(L.Sizzle){var n;try{n=L.Sizzle(e)}catch(a){n=[]}t(n)}else L.sizzleQueue.push([e,t])}),hasAttr:function(e,t){return e.hasAttribute?e.hasAttribute(t):e[t]!==n},inherit:function(e,t){var n=function(){};n.prototype=t.prototype,e.prototype=new n,e.prototype.constructor=e},extend:function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])},toArray:function(){try{var e=Array.prototype.slice;return e.call(t.documentElement.childNodes,0)[0].nodeType,function(t){return e.call(t,0)}}catch(n){return function(e){for(var t=[],n=0,a=e.length;n<a;n++)t.push(e[n]);return t}}}(),equalsIgnoreCase:function(e,t){return null==e?null==t:null!=t&&String(e).toLowerCase()===String(t).toLowerCase()},poll:function(e,t,n){function a(){L.isNumber(n)&&i++>=n||e()||setTimeout(a,t)}var i=0;t=t||1e3,a()},escapeForHtml:function(e){return e?String(e).replace(/\&/g,"&amp;").replace(/\</g,"&lt;").replace(/\>/g,"&gt;").replace(/\"/g,"&quot;").replace(/\'/g,"&#x27;").replace(/\//g,"&#x2F;"):e}};L.availableTools={},L.availableEventEmitters=[],L.fireOnceEvents=["condition","elementexists"],L.initEventEmitters=function(){L.eventEmitters=L.map(L.availableEventEmitters,function(e){return new e})},L.eventEmitterBackgroundTasks=function(){L.each(L.eventEmitters,function(e){"backgroundTasks"in e&&e.backgroundTasks()})},L.initTools=function(e){var t={"default":new p},n=L.settings.euCookieName||"sat_track";for(var a in e)if(e.hasOwnProperty(a)){var i,r,o;if((i=e[a]).euCookie)if("true"!==L.readCookie(n))continue;if(!(r=L.availableTools[i.engine])){var s=[];for(var c in L.availableTools)L.availableTools.hasOwnProperty(c)&&s.push(c);throw new Error("No tool engine named "+i.engine+", available: "+s.join(",")+".")}(o=new r(i)).id=a,t[a]=o}return t},L.preprocessArguments=function(e,t,n,a,i){function r(e){return a&&L.isString(e)?e.toLowerCase():e}function o(e){var c={};for(var l in e)if(e.hasOwnProperty(l)){var u=e[l];L.isObject(u)?c[l]=o(u):L.isArray(u)?c[l]=s(u,a):c[l]=r(L.replace(u,t,n,i))}return c}function s(e){for(var a=[],i=0,s=e.length;i<s;i++){var c=e[i];L.isString(c)?c=r(L.replace(c,t,n)):c&&c.constructor===Object&&(c=o(c)),a.push(c)}return a}return e?s(e,a):e},L.execute=function(e,t,n,a){function i(i){var r=a[i||"default"];if(r)try{r.triggerCommand(e,t,n)}catch(o){L.logError(o)}}if(!_satellite.settings.hideActivity)if(a=a||L.tools,e.engine){var r=e.engine;for(var o in a)if(a.hasOwnProperty(o)){var s=a[o];s.settings&&s.settings.engine===r&&i(o)}}else e.tool instanceof Array?L.each(e.tool,function(e){i(e)}):i(e.tool)},L.Logger={outputEnabled:!1,messages:[],keepLimit:100,flushed:!1,LEVELS:[null,null,"log","info","warn","error"],message:function(e,t){var n=this.LEVELS[t]||"log";this.messages.push([n,e]),this.messages.length>this.keepLimit&&this.messages.shift(),this.outputEnabled&&this.echo(n,e)},getHistory:function(){return this.messages},clearHistory:function(){this.messages=[]},setOutputState:function(e){this.outputEnabled!=e&&(this.outputEnabled=e,e?this.flush():this.flushed=!1)},echo:function(t,n){e.console&&e.console[t]("SATELLITE: "+n)},flush:function(){this.flushed||(L.each(this.messages,function(e){!0!==e[2]&&(this.echo(e[0],e[1]),e[2]=!0)},this),this.flushed=!0)}},L.notify=L.bind(L.Logger.message,L.Logger),L.cleanText=function(e){return null==e?null:L.trim(e).replace(/\s+/g," ")},L.cleanText.legacy=function(e){return null==e?null:L.trim(e).replace(/\s{2,}/g," ").replace(/[^\000-\177]*/g,"")},L.text=function(e){return e.textContent||e.innerText},L.specialProperties={text:L.text,cleanText:function(e){return L.cleanText(L.text(e))}},L.getObjectProperty=function(e,t,a){for(var i,r=t.split("."),o=e,s=L.specialProperties,c=0,l=r.length;c<l;c++){if(null==o)return n;var u=r[c];if(a&&"@"===u.charAt(0))o=s[u.slice(1)](o);else if(o.getAttribute&&(i=u.match(/^getAttribute\((.+)\)$/))){var d=i[1];o=o.getAttribute(d)}else o=o[u]}return o},L.getToolsByType=function(e){if(!e)throw new Error("Tool type is missing");var t=[];for(var n in L.tools)if(L.tools.hasOwnProperty(n)){var a=L.tools[n];a.settings&&a.settings.engine===e&&t.push(a)}return t},L.setVar=function(){var e=L.data.customVars;if(null==e&&(L.data.customVars={},e=L.data.customVars),"string"==typeof arguments[0])e[arguments[0]]=arguments[1];else if(arguments[0]){var t=arguments[0];for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])}},L.dataElementSafe=function(e,t){if(arguments.length>2){var n=arguments[2];"pageview"===t?L.dataElementSafe.pageviewCache[e]=n:"session"===t?L.setCookie("_sdsat_"+e,n):"visitor"===t&&L.setCookie("_sdsat_"+e,n,730)}else{if("pageview"===t)return L.dataElementSafe.pageviewCache[e];if("session"===t||"visitor"===t)return L.readCookie("_sdsat_"+e)}},L.dataElementSafe.pageviewCache={},L.realGetDataElement=function(t){var n;return t.selector?L.hasSelector&&L.cssQuery(t.selector,function(e){if(e.length>0){var a=e[0];"text"===t.property?n=a.innerText||a.textContent:t.property in a?n=a[t.property]:L.hasAttr(a,t.property)&&(n=a.getAttribute(t.property))}}):t.queryParam?n=t.ignoreCase?L.getQueryParamCaseInsensitive(t.queryParam):L.getQueryParam(t.queryParam):t.cookie?n=L.readCookie(t.cookie):t.jsVariable?n=L.getObjectProperty(e,t.jsVariable):t.customJS?n=t.customJS():t.contextHub&&(n=t.contextHub()),L.isString(n)&&t.cleanText&&(n=L.cleanText(n)),n},L.getDataElement=function(e,t,a){if(null==(a=a||L.dataElements[e]))return L.settings.undefinedVarsReturnEmpty?"":null;var i=L.realGetDataElement(a);return i===n&&a.storeLength?i=L.dataElementSafe(e,a.storeLength):i!==n&&a.storeLength&&L.dataElementSafe(e,a.storeLength,i),i||t||(i=a["default"]||""),L.isString(i)&&a.forceLowerCase&&(i=i.toLowerCase()),i},L.getVar=function(a,i,r){var o,s,c=L.data.customVars,l=r?r.target||r.srcElement:null,u={uri:L.URI(),protocol:t.location.protocol,hostname:t.location.hostname};if(L.dataElements&&a in L.dataElements)return L.getDataElement(a);if((s=u[a.toLowerCase()])===n)if("this."===a.substring(0,5))a=a.slice(5),s=L.getObjectProperty(i,a,!0);else if("event."===a.substring(0,6))a=a.slice(6),s=L.getObjectProperty(r,a);else if("target."===a.substring(0,7))a=a.slice(7),s=L.getObjectProperty(l,a);else if("window."===a.substring(0,7))a=a.slice(7),s=L.getObjectProperty(e,a);else if("param."===a.substring(0,6))a=a.slice(6),s=L.getQueryParam(a);else if(o=a.match(/^rand([0-9]+)$/)){var d=Number(o[1]),g=(Math.random()*(Math.pow(10,d)-1)).toFixed(0);s=Array(d-g.length+1).join("0")+g}else s=L.getObjectProperty(c,a);return s},L.getVars=function(e,t,n){var a={};return L.each(e,function(e){a[e]=L.getVar(e,t,n)}),a},L.replace=function(e,t,n,a){return"string"!=typeof e?e:e.replace(/%(.*?)%/g,function(e,i){var r=L.getVar(i,t,n);return null==r?L.settings.undefinedVarsReturnEmpty?"":e:a?L.escapeForHtml(r):r})},L.escapeHtmlParams=function(e){return e.escapeHtml=!0,e},L.searchVariables=function(e,t,n){if(!e||0===e.length)return"";for(var a=[],i=0,r=e.length;i<r;i++){var o=e[i],s=L.getVar(o,t,n);a.push(o+"="+escape(s))}return"?"+a.join("&")},L.fireRule=function(e,t,n){var a=e.trigger;if(a){for(var i=0,r=a.length;i<r;i++){var o=a[i];L.execute(o,t,n)}L.contains(L.fireOnceEvents,e.event)&&(e.expired=!0)}},L.isLinked=function(e){for(var t=e;t;t=t.parentNode)if(L.isLinkTag(t))return!0;return!1},L.firePageLoadEvent=function(e){for(var n=t.location,a={type:e,target:n},i=L.pageLoadRules,r=L.evtHandlers[a.type],o=i.length;o--;){var s=i[o];L.ruleMatches(s,a,n)&&(L.notify('Rule "'+s.name+'" fired.',1),L.fireRule(s,n,a))}for(var c in L.tools)if(L.tools.hasOwnProperty(c)){var l=L.tools[c];l.endPLPhase&&l.endPLPhase(e)}r&&L.each(r,function(e){e(a)})},L.track=function(e){e=e.replace(/^\s*/,"").replace(/\s*$/,"");for(var t=0;t<L.directCallRules.length;t++){var n=L.directCallRules[t];if(n.name===e)return L.notify('Direct call Rule "'+e+'" fired.',1),void L.fireRule(n,location,{type:e})}L.notify('Direct call Rule "'+e+'" not found.',1)},L.basePath=function(){return L.data.host?("https:"===t.location.protocol?"https://"+L.data.host.https:"http://"+L.data.host.http)+"/":this.settings.basePath},L.setLocation=function(t){e.location=t},L.parseQueryParams=function(e){var t=function(e){var t=e;try{t=decodeURIComponent(e)}catch(n){}return t};if(""===e||!1===L.isString(e))return{};0===e.indexOf("?")&&(e=e.substring(1));var n={},a=e.split("&");return L.each(a,function(e){(e=e.split("="))[1]&&(n[t(e[0])]=t(e[1]))}),n},L.getCaseSensitivityQueryParamsMap=function(e){var t=L.parseQueryParams(e),n={};for(var a in t)t.hasOwnProperty(a)&&(n[a.toLowerCase()]=t[a]);return{normal:t,caseInsensitive:n}},L.updateQueryParams=function(){L.QueryParams=L.getCaseSensitivityQueryParamsMap(e.location.search)},L.updateQueryParams(),L.getQueryParam=function(e){return L.QueryParams.normal[e]},L.getQueryParamCaseInsensitive=function(e){return L.QueryParams.caseInsensitive[e.toLowerCase()]},L.encodeObjectToURI=function(e){if(!1===L.isObject(e))return"";var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(encodeURIComponent(n)+"="+encodeURIComponent(e[n]));return t.join("&")},L.readCookie=function(e){for(var a=e+"=",i=t.cookie.split(";"),r=0;r<i.length;r++){for(var o=i[r];" "==o.charAt(0);)o=o.substring(1,o.length);if(0===o.indexOf(a))return o.substring(a.length,o.length)}return n},L.setCookie=function(e,n,a){var i;if(a){var r=new Date;r.setTime(r.getTime()+24*a*60*60*1e3),i="; expires="+r.toGMTString()}else i="";t.cookie=e+"="+n+i+"; path=/"},L.removeCookie=function(e){L.setCookie(e,"",-1)},L.getElementProperty=function(e,t){if("@"===t.charAt(0)){var a=L.specialProperties[t.substring(1)];if(a)return a(e)}return"innerText"===t?L.text(e):t in e?e[t]:e.getAttribute?e.getAttribute(t):n},L.propertiesMatch=function(e,t){if(e)for(var n in e)if(e.hasOwnProperty(n)){var a=e[n],i=L.getElementProperty(t,n);if("string"==typeof a&&a!==i)return!1;if(a instanceof RegExp&&!a.test(i))return!1}return!0},L.isRightClick=function(e){var t;return e.which?t=3==e.which:e.button&&(t=2==e.button),t},L.ruleMatches=function(e,t,n,a){var i=e.condition,r=e.conditions,o=e.property,s=t.type,c=e.value,l=t.target||t.srcElement,u=n===l;if(e.event!==s&&("custom"!==e.event||e.customEvent!==s))return!1;if(!L.ruleInScope(e))return!1;if("click"===e.event&&L.isRightClick(t))return!1;if(e.isDefault&&a>0)return!1;if(e.expired)return!1;if("inview"===s&&t.inviewDelay!==e.inviewDelay)return!1;if(!u&&(!1===e.bubbleFireIfParent||0!==a&&!1===e.bubbleFireIfChildFired))return!1;if(e.selector&&!L.matchesCss(e.selector,n))return!1;if(!L.propertiesMatch(o,n))return!1;if(null!=c)if("string"==typeof c){if(c!==n.value)return!1}else if(!c.test(n.value))return!1;if(i)try{if(!i.call(n,t,l))return L.notify('Condition for rule "'+e.name+'" not met.',1),!1}catch(g){return L.notify('Condition for rule "'+e.name+'" not met. Error: '+g.message,1),!1}if(r){var d=L.find(r,function(a){try{return!a.call(n,t,l)}catch(g){return L.notify('Condition for rule "'+e.name+'" not met. Error: '+g.message,1),!0}});if(d)return L.notify("Condition "+d.toString()+' for rule "'+e.name+'" not met.',1),!1}return!0},L.evtHandlers={},L.bindEvent=function(e,t){var n=L.evtHandlers;n[e]||(n[e]=[]),n[e].push(t)},L.whenEvent=L.bindEvent,L.unbindEvent=function(e,t){var n=L.evtHandlers;if(n[e]){var a=L.indexOf(n[e],t);n[e].splice(a,1)}},L.bindEventOnce=function(e,t){var n=function(){L.unbindEvent(e,n),t.apply(null,arguments)};L.bindEvent(e,n)},L.isVMLPoisoned=function(e){if(!e)return!1;try{e.nodeName}catch(t){if("Attribute only valid on v:image"===t.message)return!0}return!1},L.handleEvent=function(e){if(!L.$data(e,"eventProcessed")){var t=e.type.toLowerCase(),n=e.target||e.srcElement,a=0,i=L.rules,r=(L.tools,L.evtHandlers[e.type]);if(L.isVMLPoisoned(n))L.notify("detected "+t+" on poisoned VML element, skipping.",1);else{r&&L.each(r,function(t){t(e)}),n&&n.nodeName?L.notify("detected "+t+" on "+n.nodeName,1):L.notify("detected "+t,1);for(var o=n;o;o=o.parentNode){var s=!1;if(L.each(i,function(t){L.ruleMatches(t,e,o,a)&&(L.notify('Rule "'+t.name+'" fired.',1),L.fireRule(t,o,e),a++,t.bubbleStop&&(s=!0))}),s)break}L.$data(e,"eventProcessed",!0)}}},L.onEvent=t.querySelectorAll?function(e){L.handleEvent(e)}:(I=[],(k=function(e){e.selector?I.push(e):L.handleEvent(e)}).pendingEvents=I,k),L.fireEvent=function(e,t){L.onEvent({type:e,target:t})},L.registerEvents=function(e,t){for(var n=t.length-1;n>=0;n--){var a=t[n];L.$data(e,a+".tracked")||(L.addEventHandler(e,a,L.onEvent),L.$data(e,a+".tracked",!0))}},L.registerEventsForTags=function(e,n){for(var a=e.length-1;a>=0;a--)for(var i=e[a],r=t.getElementsByTagName(i),o=r.length-1;o>=0;o--)L.registerEvents(r[o],n)},L.setListeners=function(){var e=["click","submit"];L.each(L.rules,function(t){"custom"===t.event&&t.hasOwnProperty("customEvent")&&!L.contains(e,t.customEvent)&&e.push(t.customEvent)}),L.registerEvents(t,e)},L.getUniqueRuleEvents=function(){return L._uniqueRuleEvents||(L._uniqueRuleEvents=[],L.each(L.rules,function(e){-1===L.indexOf(L._uniqueRuleEvents,e.event)&&L._uniqueRuleEvents.push(e.event)})),L._uniqueRuleEvents},L.setFormListeners=function(){if(!L._relevantFormEvents){var e=["change","focus","blur","keypress"];L._relevantFormEvents=L.filter(L.getUniqueRuleEvents(),function(t){return-1!==L.indexOf(e,t)})}L._relevantFormEvents.length&&L.registerEventsForTags(["input","select","textarea","button"],L._relevantFormEvents)},L.setVideoListeners=function(){if(!L._relevantVideoEvents){var e=["play","pause","ended","volumechange","stalled","loadeddata"];L._relevantVideoEvents=L.filter(L.getUniqueRuleEvents(),function(t){return-1!==L.indexOf(e,t)})}L._relevantVideoEvents.length&&L.registerEventsForTags(["video"],L._relevantVideoEvents)},L.readStoredSetting=function(t){try{return t="sdsat_"+t,e.localStorage.getItem(t)}catch(n){return L.notify("Cannot read stored setting from localStorage: "+n.message,2),null}},L.loadStoredSettings=function(){var e=L.readStoredSetting("debug"),t=L.readStoredSetting("hide_activity");e&&(L.settings.notifications="true"===e),t&&(L.settings.hideActivity="true"===t)},L.isRuleActive=function(e,t){function n(e,t){return t=i(t,{hour:e[f](),minute:e[m]()}),Math.floor(Math.abs((e.getTime()-t.getTime())/864e5))}function a(e,t){function n(e){return 12*e[d]()+e[g]()}return Math.abs(n(e)-n(t))}function i(e,t){var n=new Date(e.getTime());for(var a in t)if(t.hasOwnProperty(a)){var i=t[a];switch(a){case"hour":n[h](i);break;case"minute":n[p](i);break;case"date":n[v](i)}}return n}function r(e,t){return 60*e[f]()+e[m]()>60*t[f]()+t[m]()}function o(e,t){return 60*e[f]()+e[m]()<60*t[f]()+t[m]()}var s=e.schedule;if(!s)return!0;var c=s.utc,l=c?"getUTCDate":"getDate",u=c?"getUTCDay":"getDay",d=c?"getUTCFullYear":"getFullYear",g=c?"getUTCMonth":"getMonth",f=c?"getUTCHours":"getHours",m=c?"getUTCMinutes":"getMinutes",h=c?"setUTCHours":"setHours",p=c?"setUTCMinutes":"setMinutes",v=c?"setUTCDate":"setDate";if(t=t||new Date,s.repeat){if(r(s.start,t))return!1;if(o(s.end,t))return!1;if(t<s.start)return!1;if(s.endRepeat&&t>=s.endRepeat)return!1;if("daily"===s.repeat){if(s.repeatEvery)if(n(s.start,t)%s.repeatEvery!=0)return!1}else if("weekly"===s.repeat){if(s.days){if(!L.contains(s.days,t[u]()))return!1}else if(s.start[u]()!==t[u]())return!1;if(s.repeatEvery)if(n(s.start,t)%(7*s.repeatEvery)!=0)return!1}else if("monthly"===s.repeat){if(s.repeatEvery)if(a(s.start,t)%s.repeatEvery!=0)return!1;if(s.nthWeek&&s.mthDay){if(s.mthDay!==t[u]())return!1;var b=Math.floor((t[l]()-t[u]()+1)/7);if(s.nthWeek!==b)return!1}else if(s.start[l]()!==t[l]())return!1}else if("yearly"===s.repeat){if(s.start[g]()!==t[g]())return!1;if(s.start[l]()!==t[l]())return!1;if(s.repeatEvery)if(Math.abs(s.start[d]()-t[d]())%s.repeatEvery!=0)return!1}}else{if(s.start>t)return!1;if(s.end<t)return!1}return!0},L.isOutboundLink=function(e){if(!e.getAttribute("href"))return!1;var t=e.hostname,n=(e.href,e.protocol);return("http:"===n||"https:"===n)&&(!L.any(L.settings.domainList,function(e){return L.isSubdomainOf(t,e)})&&t!==location.hostname)},L.isLinkerLink=function(e){return!(!e.getAttribute||!e.getAttribute("href"))&&(L.hasMultipleDomains()&&e.hostname!=location.hostname&&!e.href.match(/^javascript/i)&&!L.isOutboundLink(e))},L.isSubdomainOf=function(e,t){if(e===t)return!0;var n=e.length-t.length;return n>0&&L.equalsIgnoreCase(e.substring(n),t)},L.getVisitorId=function(){var e=L.getToolsByType("visitor_id");return 0===e.length?null:e[0].getInstance()},L.URI=function(){var e=t.location.pathname+t.location.search;return L.settings.forceLowerCase&&(e=e.toLowerCase()),e},L.URL=function(){var e=t.location.href;return L.settings.forceLowerCase&&(e=e.toLowerCase()),e},L.filterRules=function(){function e(e){return!!L.isRuleActive(e)}L.rules=L.filter(L.rules,e),L.pageLoadRules=L.filter(L.pageLoadRules,e)},L.ruleInScope=function(e,n){function a(e,t){function n(e){return t.match(e)}var a=e.include,r=e.exclude;if(a&&i(a,t))return!0;if(r){if(L.isString(r)&&r===t)return!0;if(L.isArray(r)&&L.any(r,n))return!0;if(L.isRegex(r)&&n(r))return!0}return!1}function i(e,t){function n(e){return t.match(e)}return!(!L.isString(e)||e===t)||(!(!L.isArray(e)||L.any(e,n))||!(!L.isRegex(e)||n(e)))}n=n||t.location;var r=e.scope;if(!r)return!0;var o=r.URI,s=r.subdomains,c=r.domains,l=r.protocols,u=r.hashes;return(!o||!a(o,n.pathname+n.search))&&((!s||!a(s,n.hostname))&&((!c||!i(c,n.hostname))&&((!l||!i(l,n.protocol))&&(!u||!a(u,n.hash)))))},L.backgroundTasks=function(){new Date;L.setFormListeners(),L.setVideoListeners(),L.loadStoredSettings(),L.registerNewElementsForDynamicRules(),L.eventEmitterBackgroundTasks();new Date},L.registerNewElementsForDynamicRules=function(){function e(t,n){var a=e.cache[t];if(a)return n(a);L.cssQuery(t,function(a){e.cache[t]=a,n(a)})}e.cache={},L.each(L.dynamicRules,function(t){e(t.selector,function(e){L.each(e,function(e){var n="custom"===t.event?t.customEvent:t.event;L.$data(e,"dynamicRules.seen."+n)||(L.$data(e,"dynamicRules.seen."+n,!0),L.propertiesMatch(t.property,e)&&L.registerEvents(e,[n]))})})})},L.ensureCSSSelector=function(){t.querySelectorAll?L.hasSelector=!0:(L.loadingSizzle=!0,L.sizzleQueue=[],L.loadScript(L.basePath()+"selector.js",function(){if(L.Sizzle){var e=L.onEvent.pendingEvents;L.each(e,function(e){L.handleEvent(e)},this),L.onEvent=L.handleEvent,L.hasSelector=!0,delete L.loadingSizzle,L.each(L.sizzleQueue,function(e){L.cssQuery(e[0],e[1])}),delete L.sizzleQueue}else L.logError(new Error("Failed to load selector.js"))}))},L.errors=[],L.logError=function(e){L.errors.push(e),L.notify(e.name+" - "+e.message,5)},L.pageBottom=function(){L.initialized&&(L.pageBottomFired=!0,L.firePageLoadEvent("pagebottom"))},L.stagingLibraryOverride=function(){if("true"===L.readStoredSetting("stagingLibrary")){for(var e,n,a,i=t.getElementsByTagName("script"),r=/^(.*)satelliteLib-([a-f0-9]{40})\.js$/,o=/^(.*)satelliteLib-([a-f0-9]{40})-staging\.js$/,s=0,c=i.length;s<c&&(!(a=i[s].getAttribute("src"))||(e||(e=a.match(r)),n||(n=a.match(o)),!n));s++);if(e&&!n){var l=e[1]+"satelliteLib-"+e[2]+"-staging.js";if(t.write)t.write('<script src="'+l+'"></script>');else{var u=t.createElement("script");u.src=l,t.head.appendChild(u)}return!0}}return!1},L.checkAsyncInclude=function(){e.satellite_asyncLoad&&L.notify('You may be using the async installation of Satellite. In-page HTML and the "pagebottom" event will not work. Please update your Satellite installation for these features.',5)},L.hasMultipleDomains=function(){return!!L.settings.domainList&&L.settings.domainList.length>1},L.handleOverrides=function(){if(D)for(var e in D)D.hasOwnProperty(e)&&(L.data[e]=D[e])},L.privacyManagerParams=function(){var e={};L.extend(e,L.settings.privacyManagement);var t=[];for(var n in L.tools)if(L.tools.hasOwnProperty(n)){var a=L.tools[n],i=a.settings;if(!i)continue;"sc"===i.engine&&t.push(a)}var r=L.filter(L.map(t,function(e){return e.getTrackingServer()}),function(e){return null!=e});e.adobeAnalyticsTrackingServers=r;for(var o=["bannerText","headline","introductoryText","customCSS"],s=0;s<o.length;s++){var c=o[s],l=e[c];if(l)if("text"===l.type)e[c]=l.value;else{if("data"!==l.type)throw new Error("Invalid type: "+l.type);e[c]=L.getVar(l.value)}}return e},L.prepareLoadPrivacyManager=function(){function t(e){function t(){++r===i.length&&(n(),clearTimeout(o),e())}function n(){L.each(i,function(e){L.unbindEvent(e.id+".load",t)})}function a(){n(),e()}var i=L.filter(L.values(L.tools),function(e){return e.settings&&"sc"===e.settings.engine});if(0===i.length)return e();var r=0;L.each(i,function(e){L.bindEvent(e.id+".load",t)});var o=setTimeout(a,5e3)}L.addEventHandler(e,"load",function(){t(L.loadPrivacyManager)})},L.loadPrivacyManager=function(){var e=L.basePath()+"privacy_manager.js";L.loadScript(e,function(){var e=L.privacyManager;e.configure(L.privacyManagerParams()),e.openIfRequired()})},L.init=function(t){if(!L.stagingLibraryOverride()){L.configurationSettings=t;var a=t.tools;for(var i in delete t.tools,t)t.hasOwnProperty(i)&&(L[i]=t[i]);L.data.customVars===n&&(L.data.customVars={}),L.data.queryParams=L.QueryParams.normal,L.handleOverrides(),L.detectBrowserInfo(),L.trackVisitorInfo&&L.trackVisitorInfo(),L.loadStoredSettings(),L.Logger.setOutputState(L.settings.notifications),L.checkAsyncInclude(),L.ensureCSSSelector(),L.filterRules(),L.dynamicRules=L.filter(L.rules,function(e){return e.eventHandlerOnElement}),L.tools=L.initTools(a),L.initEventEmitters(),L.firePageLoadEvent("aftertoolinit"),L.settings.privacyManagement&&L.prepareLoadPrivacyManager(),L.hasSelector&&L.domReady(L.eventEmitterBackgroundTasks),L.setListeners(),L.domReady(function(){L.poll(function(){L.backgroundTasks()},L.settings.recheckEvery||3e3)}),L.domReady(function(){L.domReadyFired=!0,L.pageBottomFired||L.pageBottom(),L.firePageLoadEvent("domready")}),L.addEventHandler(e,"load",function(){L.firePageLoadEvent("windowload")}),L.firePageLoadEvent("pagetop"),L.initialized=!0}},L.pageLoadPhases=["aftertoolinit","pagetop","pagebottom","domready","windowload"],L.loadEventBefore=function(e,t){return L.indexOf(L.pageLoadPhases,e)<=L.indexOf(L.pageLoadPhases,t)},L.flushPendingCalls=function(e){e.pending&&(L.each(e.pending,function(t){var n=t[0],a=t[1],i=t[2],r=t[3];n in e?e[n].apply(e,[a,i].concat(r)):e.emit?e.emit(n,a,i,r):L.notify("Failed to trigger "+n+" for tool "+e.id,1)}),delete e.pending)},L.setDebug=function(t){try{e.localStorage.setItem("sdsat_debug",t)}catch(n){L.notify("Cannot set debug mode: "+n.message,2)}},L.getUserAgent=function(){return navigator.userAgent},L.detectBrowserInfo=function(){function e(e){return function(t){for(var n in e){if(e.hasOwnProperty(n))if(e[n].test(t))return n}return"Unknown"}}var t=e({"IE Edge Mobile":/Windows Phone.*Edge/,"IE Edge":/Edge/,OmniWeb:/OmniWeb/,"Opera Mini":/Opera Mini/,"Opera Mobile":/Opera Mobi/,Opera:/Opera/,Chrome:/Chrome|CriOS|CrMo/,Firefox:/Firefox|FxiOS/,"IE Mobile":/IEMobile/,IE:/MSIE|Trident/,"Mobile Safari":/Mobile(\/[0-9A-z]+)? Safari/,Safari:/Safari/}),n=e({Blackberry:/BlackBerry|BB10/,"Symbian OS":/Symbian|SymbOS/,Maemo:/Maemo/,Android:/Android/,Linux:/ Linux /,Unix:/FreeBSD|OpenBSD|CrOS/,Windows:/[\( ]Windows /,iOS:/iPhone|iPad|iPod/,MacOS:/Macintosh;/}),a=e({Nokia:/Symbian|SymbOS|Maemo/,"Windows Phone":/Windows Phone/,Blackberry:/BlackBerry|BB10/,Android:/Android/,iPad:/iPad/,iPod:/iPod/,iPhone:/iPhone/,Desktop:/.*/}),i=L.getUserAgent();L.browserInfo={browser:t(i),os:n(i),deviceType:a(i)}},L.isHttps=function(){return"https:"==t.location.protocol},L.BaseTool=function(e){this.settings=e||{},this.forceLowerCase=L.settings.forceLowerCase,"forceLowerCase"in this.settings&&(this.forceLowerCase=this.settings.forceLowerCase)},L.BaseTool.prototype={triggerCommand:function(e,t,n){var a=this.settings||{};if(this.initialize&&this.isQueueAvailable()&&this.isQueueable(e)&&n&&L.loadEventBefore(n.type,a.loadOn))this.queueCommand(e,t,n);else{var i=e.command,r=this["$"+i],o=!!r&&r.escapeHtml,s=L.preprocessArguments(e.arguments,t,n,this.forceLowerCase,o);r?r.apply(this,[t,n].concat(s)):this.$missing$?this.$missing$(i,t,n,s):L.notify("Failed to trigger "+i+" for tool "+this.id,1)}},endPLPhase:function(){},isQueueable:function(e){return"cancelToolInit"!==e.command},isQueueAvailable:function(){return!this.initialized&&!this.initializing},flushQueue:function(){this.pending&&(L.each(this.pending,function(e){this.triggerCommand.apply(this,e)},this),this.pending=[])},queueCommand:function(e,t,n){this.pending||(this.pending=[]),this.pending.push([e,t,n])},$cancelToolInit:function(){this._cancelToolInit=!0}},e._satellite=L,L.ecommerce={addItem:function(){var e=[].slice.call(arguments);L.onEvent({type:"ecommerce.additem",target:e})},addTrans:function(){var e=[].slice.call(arguments);L.data.saleData.sale={orderId:e[0],revenue:e[2]},L.onEvent({type:"ecommerce.addtrans",target:e})},trackTrans:function(){L.onEvent({type:"ecommerce.tracktrans",target:[]})}},L.visibility={isHidden:function(){var e=this.getHiddenProperty();return!!e&&t[e]},isVisible:function(){return!this.isHidden()},getHiddenProperty:function(){var e=["webkit","moz","ms","o"];if("hidden"in t)return"hidden"
	;for(var n=0;n<e.length;n++)if(e[n]+"Hidden"in t)return e[n]+"Hidden";return null},getVisibilityEvent:function(){var e=this.getHiddenProperty();return e?e.replace(/[H|h]idden/,"")+"visibilitychange":null}},a.prototype={obue:!1,initialize:function(){this.attachCloseListeners()},obuePrevUnload:function(){},obuePrevBeforeUnload:function(){},newObueListener:function(){this.obue||(this.obue=!0,this.triggerBeacons())},attachCloseListeners:function(){this.prevUnload=e.onunload,this.prevBeforeUnload=e.onbeforeunload,e.onunload=L.bind(function(t){this.prevUnload&&setTimeout(L.bind(function(){this.prevUnload.call(e,t)},this),1),this.newObueListener()},this),e.onbeforeunload=L.bind(function(t){this.prevBeforeUnload&&setTimeout(L.bind(function(){this.prevBeforeUnload.call(e,t)},this),1),this.newObueListener()},this)},triggerBeacons:function(){L.fireEvent("leave",t)}},L.availableEventEmitters.push(a),i.prototype={initialize:function(){var e=this.twttr;e&&"function"==typeof e.ready&&e.ready(L.bind(this.bind,this))},bind:function(){this.twttr.events.bind("tweet",function(e){e&&(L.notify("tracking a tweet button",1),L.onEvent({type:"twitter.tweet",target:t}))})}},L.availableEventEmitters.push(i),r.prototype={initialize:function(){this.setupHistoryAPI(),this.setupHashChange()},fireIfURIChanged:function(){var e=L.URL();this.lastURL!==e&&(this.fireEvent(),this.lastURL=e)},fireEvent:function(){L.updateQueryParams(),L.onEvent({type:"locationchange",target:t})},setupSPASupport:function(){this.setupHistoryAPI(),this.setupHashChange()},setupHistoryAPI:function(){var t=e.history;t&&(t.pushState&&(this.originalPushState=t.pushState,t.pushState=this._pushState),t.replaceState&&(this.originalReplaceState=t.replaceState,t.replaceState=this._replaceState)),L.addEventHandler(e,"popstate",this._onPopState)},pushState:function(){var e=this.originalPushState.apply(history,arguments);return this.onPushState(),e},replaceState:function(){var e=this.originalReplaceState.apply(history,arguments);return this.onReplaceState(),e},setupHashChange:function(){L.addEventHandler(e,"hashchange",this._onHashChange)},onReplaceState:function(){setTimeout(this._fireIfURIChanged,0)},onPushState:function(){setTimeout(this._fireIfURIChanged,0)},onPopState:function(){setTimeout(this._fireIfURIChanged,0)},onHashChange:function(){setTimeout(this._fireIfURIChanged,0)},uninitialize:function(){this.cleanUpHistoryAPI(),this.cleanUpHashChange()},cleanUpHistoryAPI:function(){history.pushState===this._pushState&&(history.pushState=this.originalPushState),history.replaceState===this._replaceState&&(history.replaceState=this.originalReplaceState),L.removeEventHandler(e,"popstate",this._onPopState)},cleanUpHashChange:function(){L.removeEventHandler(e,"hashchange",this._onHashChange)}},L.availableEventEmitters.push(r),o.prototype.getStringifiedValue=e.JSON&&e.JSON.stringify||L.stringify,o.prototype.initPolling=function(){0!==this.dataElementsNames.length&&(this.dataElementsStore=this.getDataElementsValues(),L.poll(L.bind(this.checkDataElementValues,this),1e3))},o.prototype.getDataElementsValues=function(){var e={};return L.each(this.dataElementsNames,function(t){var n=L.getVar(t);e[t]=this.getStringifiedValue(n)},this),e},o.prototype.checkDataElementValues=function(){L.each(this.dataElementsNames,L.bind(function(e){var n=this.getStringifiedValue(L.getVar(e));n!==this.dataElementsStore[e]&&(this.dataElementsStore[e]=n,L.onEvent({type:"dataelementchange("+e+")",target:t}))},this))},L.availableEventEmitters.push(o),s.orientationChange=function(t){var n=0===e.orientation?"portrait":"landscape";t.orientation=n,L.onEvent(t)},L.availableEventEmitters.push(s),c.prototype={backgroundTasks:function(){var e=this.eventHandler;L.each(this.rules,function(t){L.cssQuery(t.selector||"video",function(t){L.each(t,function(t){L.$data(t,"videoplayed.tracked")||(L.addEventHandler(t,"timeupdate",L.throttle(e,100)),L.$data(t,"videoplayed.tracked",!0))})})})},evalRule:function(e,t){var n=t.event,a=e.seekable,i=a.start(0),r=a.end(0),o=e.currentTime,s=t.event.match(/^videoplayed\(([0-9]+)([s%])\)$/);if(s){var c=s[2],l=Number(s[1]),u="%"===c?function(){return l<=100*(o-i)/(r-i)}:function(){return l<=o-i};!L.$data(e,n)&&u()&&(L.$data(e,n,!0),L.onEvent({type:n,target:e}))}},onUpdateTime:function(e){var t=this.rules,n=e.target;if(n.seekable&&0!==n.seekable.length)for(var a=0,i=t.length;a<i;a++)this.evalRule(n,t[a])}},L.availableEventEmitters.push(c),l.prototype={defineEvents:function(){this.oldBlurClosure=function(){L.fireEvent("tabblur",t)},this.oldFocusClosure=L.bind(function(){this.visibilityApiHasPriority?L.fireEvent("tabfocus",t):null!=L.visibility.getHiddenProperty()&&L.visibility.isHidden()||L.fireEvent("tabfocus",t)},this)},attachDetachModernEventListeners:function(e){L[0==e?"removeEventHandler":"addEventHandler"](t,L.visibility.getVisibilityEvent(),this.handleVisibilityChange)},attachDetachOlderEventListeners:function(t,n,a){var i=0==t?"removeEventHandler":"addEventHandler";L[i](n,a,this.oldBlurClosure),L[i](e,"focus",this.oldFocusClosure)},handleVisibilityChange:function(){L.visibility.isHidden()?L.fireEvent("tabblur",t):L.fireEvent("tabfocus",t)},setVisibilityApiPriority:function(t){this.visibilityApiHasPriority=t,this.attachDetachOlderEventListeners(!1,e,"blur"),this.attachDetachModernEventListeners(!1),t?null!=L.visibility.getHiddenProperty()?this.attachDetachModernEventListeners(!0):this.attachDetachOlderEventListeners(!0,e,"blur"):(this.attachDetachOlderEventListeners(!0,e,"blur"),null!=L.visibility.getHiddenProperty()&&this.attachDetachModernEventListeners(!0))},oldBlurClosure:null,oldFocusClosure:null,visibilityApiHasPriority:!0},L.availableEventEmitters.push(l),u.offset=function(n){var a=null,i=null;try{var r=n.getBoundingClientRect(),o=t,s=o.documentElement,c=o.body,l=e,u=s.clientTop||c.clientTop||0,d=s.clientLeft||c.clientLeft||0,g=l.pageYOffset||s.scrollTop||c.scrollTop,f=l.pageXOffset||s.scrollLeft||c.scrollLeft;a=r.top+g-u,i=r.left+f-d}catch(m){}return{top:a,left:i}},u.getViewportHeight=function(){var n=e.innerHeight,a=t.compatMode;return a&&(n="CSS1Compat"==a?t.documentElement.clientHeight:t.body.clientHeight),n},u.getScrollTop=function(){return t.documentElement.scrollTop?t.documentElement.scrollTop:t.body.scrollTop},u.isElementInDocument=function(e){return t.body.contains(e)},u.isElementInView=function(e){if(!u.isElementInDocument(e))return!1;var t=u.getViewportHeight(),n=u.getScrollTop(),a=u.offset(e).top,i=e.offsetHeight;return null!==a&&!(n>a+i||n+t<a)},u.prototype={backgroundTasks:function(){var e=this.elements;L.each(this.rules,function(t){L.cssQuery(t.selector,function(n){var a=0;L.each(n,function(t){L.contains(e,t)||(e.push(t),a++)}),a&&L.notify(t.selector+" added "+a+" elements.",1)})}),this.track()},checkInView:function(e,t,n){var a=L.$data(e,"inview");if(u.isElementInView(e)){a||L.$data(e,"inview",!0);var i=this;this.processRules(e,function(n,a,r){if(t||!n.inviewDelay)L.$data(e,a,!0),L.onEvent({type:"inview",target:e,inviewDelay:n.inviewDelay});else if(n.inviewDelay){var o=L.$data(e,r);o||(o=setTimeout(function(){i.checkInView(e,!0,n.inviewDelay)},n.inviewDelay),L.$data(e,r,o))}},n)}else{if(!u.isElementInDocument(e)){var r=L.indexOf(this.elements,e);this.elements.splice(r,1)}a&&L.$data(e,"inview",!1),this.processRules(e,function(t,n,a){var i=L.$data(e,a);i&&clearTimeout(i)},n)}},track:function(){for(var e=this.elements.length-1;e>=0;e--)this.checkInView(this.elements[e])},processRules:function(e,t,n){var a=this.rules;n&&(a=L.filter(this.rules,function(e){return e.inviewDelay==n})),L.each(a,function(n,a){var i=n.inviewDelay?"viewed_"+n.inviewDelay:"viewed",r="inview_timeout_id_"+a;L.$data(e,i)||L.matchesCss(n.selector,e)&&t(n,i,r)})}},L.availableEventEmitters.push(u),d.prototype.backgroundTasks=function(){L.each(this.rules,function(e){L.cssQuery(e.selector,function(e){if(e.length>0){var t=e[0];if(L.$data(t,"elementexists.seen"))return;L.$data(t,"elementexists.seen",!0),L.onEvent({type:"elementexists",target:t})}})})},L.availableEventEmitters.push(d),g.prototype={initialize:function(){if(this.FB=this.FB||e.FB,this.FB&&this.FB.Event&&this.FB.Event.subscribe)return this.bind(),!0},bind:function(){this.FB.Event.subscribe("edge.create",function(){L.notify("tracking a facebook like",1),L.onEvent({type:"facebook.like",target:t})}),this.FB.Event.subscribe("edge.remove",function(){L.notify("tracking a facebook unlike",1),L.onEvent({type:"facebook.unlike",target:t})}),this.FB.Event.subscribe("message.send",function(){L.notify("tracking a facebook share",1),L.onEvent({type:"facebook.send",target:t})})}},L.availableEventEmitters.push(g),f.prototype={backgroundTasks:function(){var e=this;L.each(this.rules,function(t){var n=t[1],a=t[0];L.cssQuery(n,function(t){L.each(t,function(t){e.trackElement(t,a)})})},this)},trackElement:function(e,t){var n=this,a=L.$data(e,"hover.delays");a?L.contains(a,t)||a.push(t):(L.addEventHandler(e,"mouseover",function(t){n.onMouseOver(t,e)}),L.addEventHandler(e,"mouseout",function(t){n.onMouseOut(t,e)}),L.$data(e,"hover.delays",[t]))},onMouseOver:function(e,t){var n=e.target||e.srcElement,a=e.relatedTarget||e.fromElement;(t===n||L.containsElement(t,n))&&!L.containsElement(t,a)&&this.onMouseEnter(t)},onMouseEnter:function(e){var t=L.$data(e,"hover.delays"),n=L.map(t,function(t){return setTimeout(function(){L.onEvent({type:"hover("+t+")",target:e})},t)});L.$data(e,"hover.delayTimers",n)},onMouseOut:function(e,t){var n=e.target||e.srcElement,a=e.relatedTarget||e.toElement;(t===n||L.containsElement(t,n))&&!L.containsElement(t,a)&&this.onMouseLeave(t)},onMouseLeave:function(e){var t=L.$data(e,"hover.delayTimers");t&&L.each(t,function(e){clearTimeout(e)})}},L.availableEventEmitters.push(f),L.inherit(m,L.BaseTool),L.extend(m.prototype,{name:"Nielsen",endPLPhase:function(e){switch(e){case"pagetop":this.initialize();break;case"pagebottom":this.enableTracking&&(this.queueCommand({command:"sendFirstBeacon",arguments:[]}),this.flushQueueWhenReady())}},defineListeners:function(){this.onTabFocus=L.bind(function(){this.notify("Tab visible, sending view beacon when ready",1),this.tabEverVisible=!0,this.flushQueueWhenReady()},this),this.onPageLeave=L.bind(function(){this.notify("isHuman? : "+this.isHuman(),1),this.isHuman()&&this.sendDurationBeacon()},this),this.onHumanDetectionChange=L.bind(function(e){this==e.target.target&&(this.human=e.target.isHuman)},this)},initialize:function(){this.initializeTracking(),this.initializeDataProviders(),this.initializeNonHumanDetection(),this.tabEverVisible=L.visibility.isVisible(),this.tabEverVisible?this.notify("Tab visible, sending view beacon when ready",1):L.bindEventOnce("tabfocus",this.onTabFocus),this.initialized=!0},initializeTracking:function(){this.initialized||(this.notify("Initializing tracking",1),this.addRemovePageLeaveEvent(this.enableTracking),this.addRemoveHumanDetectionChangeEvent(this.enableTracking),this.initialized=!0)},initializeDataProviders:function(){var e,t=this.getAnalyticsTool();this.dataProvider.register(new m.DataProvider.VisitorID(L.getVisitorId())),t?(e=new m.DataProvider.Generic("rsid",function(){return t.settings.account}),this.dataProvider.register(e)):this.notify("Missing integration with Analytics: rsid will not be sent.")},initializeNonHumanDetection:function(){L.nonhumandetection?(L.nonhumandetection.init(),this.setEnableNonHumanDetection(0!=this.settings.enableNonHumanDetection),this.settings.nonHumanDetectionDelay>0&&this.setNonHumanDetectionDelay(1e3*parseInt(this.settings.nonHumanDetectionDelay))):this.notify("NHDM is not available.")},getAnalyticsTool:function(){if(this.settings.integratesWith)return L.tools[this.settings.integratesWith]},flushQueueWhenReady:function(){this.enableTracking&&this.tabEverVisible&&L.poll(L.bind(function(){if(this.isReadyToTrack())return this.flushQueue(),!0},this),100,20)},isReadyToTrack:function(){return this.tabEverVisible&&this.dataProvider.isReady()},$setVars:function(e,t,n){for(var a in n){var i=n[a];"function"==typeof i&&(i=i()),this.settings[a]=i}this.notify("Set variables done",2),this.prepareContextData()},$setEnableTracking:function(e,t,n){this.notify("Will"+(n?"":" not")+" track time on page",1),this.enableTracking!=n&&(this.addRemovePageLeaveEvent(n),this.addRemoveHumanDetectionChangeEvent(n),this.enableTracking=n)},$sendFirstBeacon:function(){this.sendViewBeacon()},setEnableNonHumanDetection:function(e){e?L.nonhumandetection.register(this):L.nonhumandetection.unregister(this)},setNonHumanDetectionDelay:function(e){L.nonhumandetection.register(this,e)},addRemovePageLeaveEvent:function(e){this.notify((e?"Attach onto":"Detach from")+" page leave event",1),L[0==e?"unbindEvent":"bindEvent"]("leave",this.onPageLeave)},addRemoveHumanDetectionChangeEvent:function(e){this.notify((e?"Attach onto":"Detach from")+" human detection change event",1),L[0==e?"unbindEvent":"bindEvent"]("humandetection.change",this.onHumanDetectionChange)},sendViewBeacon:function(){this.notify("Tracked page view.",1),this.sendBeaconWith()},sendDurationBeacon:function(){if(L.timetracking&&"function"==typeof L.timetracking.timeOnPage&&null!=L.timetracking.timeOnPage()){this.notify("Tracked close",1),this.sendBeaconWith({timeOnPage:Math.round(L.timetracking.timeOnPage()/1e3),duration:"D",timer:"timer"});var e;for(e=0;e<this.magicConst;e++)"0"}else this.notify("Could not track close due missing time on page",5)},sendBeaconWith:function(e){this.enableTracking&&this[this.beaconMethod].call(this,this.prepareUrl(e))},plainBeacon:function(e){var t=new Image;t.src=e,t.width=1,t.height=1,t.alt=""},navigatorSendBeacon:function(e){navigator.sendBeacon(e)},prepareUrl:function(e){var t=this.settings;return L.extend(t,this.dataProvider.provide()),L.extend(t,e),this.preparePrefix(this.settings.collectionServer)+this.adapt.convertToURI(this.adapt.toNielsen(this.substituteVariables(t)))},preparePrefix:function(e){return"//"+encodeURIComponent(e)+".imrworldwide.com/cgi-bin/gn?"},substituteVariables:function(e){var t={};for(var n in e)e.hasOwnProperty(n)&&(t[n]=L.replace(e[n]));return t},prepareContextData:function(){if(this.getAnalyticsTool()){var e=this.settings;e.sdkVersion=_satellite.publishDate,this.getAnalyticsTool().$setVars(null,null,{contextData:this.adapt.toAnalytics(this.substituteVariables(e))})}else this.notify("Adobe Analytics missing.")},isHuman:function(){return this.human},onTabFocus:function(){},onPageLeave:function(){},onHumanDetectionChange:function(){},notify:function(e,t){L.notify(this.logPrefix+e,t)},beaconMethod:"plainBeacon",adapt:null,enableTracking:!1,logPrefix:"Nielsen: ",tabEverVisible:!1,human:!0,magicConst:2e6}),m.DataProvider={},m.DataProvider.Generic=function(e,t){this.key=e,this.valueFn=t},L.extend(m.DataProvider.Generic.prototype,{isReady:function(){return!0},getValue:function(){return this.valueFn()},provide:function(){this.isReady()||m.prototype.notify("Not yet ready to provide value for: "+this.key,5);var e={};return e[this.key]=this.getValue(),e}}),m.DataProvider.VisitorID=function(e,t,n){this.key=t||"uuid",this.visitorInstance=e,this.visitorInstance&&(this.visitorId=e.getMarketingCloudVisitorID([this,this._visitorIdCallback])),this.fallbackProvider=n||new m.UUID},L.inherit(m.DataProvider.VisitorID,m.DataProvider.Generic),L.extend(m.DataProvider.VisitorID.prototype,{isReady:function(){return null===this.visitorInstance||!!this.visitorId},getValue:function(){return this.visitorId||this.fallbackProvider.get()},_visitorIdCallback:function(e){this.visitorId=e}}),m.DataProvider.Aggregate=function(){this.providers=[];for(var e=0;e<arguments.length;e++)this.register(arguments[e])},L.extend(m.DataProvider.Aggregate.prototype,{register:function(e){this.providers.push(e)},isReady:function(){return L.every(this.providers,function(e){return e.isReady()})},provide:function(){var e={};return L.each(this.providers,function(t){L.extend(e,t.provide())}),e}}),m.UUID=function(){},L.extend(m.UUID.prototype,{generate:function(){return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(e){var t=16*Math.random()|0;return("x"==e?t:3&t|8).toString(16)})},get:function(){var e=L.readCookie(this.key("uuid"));return e||(e=this.generate(),L.setCookie(this.key("uuid"),e),e)},key:function(e){return"_dtm_nielsen_"+e}}),m.DataAdapters=function(){},L.extend(m.DataAdapters.prototype,{toNielsen:function(e){var t=(new Date).getTime(),a={c6:"vc,",c13:"asid,",c15:"apn,",c27:"cln,",c32:"segA,",c33:"segB,",c34:"segC,",c35:"adrsid,",c29:"plid,",c30:"bldv,",c40:"adbid,"},i={ci:e.clientId,c6:e.vcid,c13:e.appId,c15:e.appName,prv:1,forward:0,ad:0,cr:e.duration||"V",rt:"text",st:"dcr",prd:"dcr",r:t,at:e.timer||"view",c16:e.sdkVersion,c27:e.timeOnPage||0,c40:e.uuid,c35:e.rsid,ti:t,sup:0,c32:e.segmentA,c33:e.segmentB,c34:e.segmentC,asn:e.assetName,c29:e.playerID,c30:e.buildVersion};for(key in i)if(i[key]!==n&&null!=i[key]&&i[key]!==n&&null!=i&&""!=i){var r=encodeURIComponent(i[key]);a.hasOwnProperty(key)&&r&&(r=a[key]+r),i[key]=r}return this.filterObject(i)},toAnalytics:function(e){return this.filterObject({"a.nielsen.clientid":e.clientId,"a.nielsen.vcid":e.vcid,"a.nielsen.appid":e.appId,"a.nielsen.appname":e.appName,"a.nielsen.accmethod":"0","a.nielsen.ctype":"text","a.nielsen.sega":e.segmentA,"a.nielsen.segb":e.segmentB,"a.nielsen.segc":e.segmentC,"a.nielsen.asset":e.assetName})},convertToURI:function(e){if(!1===L.isObject(e))return"";var t=[];for(var n in e)e.hasOwnProperty(n)&&t.push(n+"="+e[n]);return t.join("&")},filterObject:function(e){for(var t in e)!e.hasOwnProperty(t)||null!=e[t]&&e[t]!==n||delete e[t];return e}}),L.availableTools.nielsen=m,L.inherit(h,L.BaseTool),L.extend(h.prototype,{name:"tnt",endPLPhase:function(e){"aftertoolinit"===e&&this.initialize()},initialize:function(){L.notify("Test & Target: Initializing",1),this.initializeTargetPageParams(),this.load()},initializeTargetPageParams:function(){e.targetPageParams&&this.updateTargetPageParams(this.parseTargetPageParamsResult(e.targetPageParams())),this.updateTargetPageParams(this.settings.pageParams),this.setTargetPageParamsFunction()},load:function(){var e=this.getMboxURL(this.settings.mboxURL);!1!==this.settings.initTool?this.settings.loadSync?(L.loadScriptSync(e),this.onScriptLoaded()):(L.loadScript(e,L.bind(this.onScriptLoaded,this)),this.initializing=!0):this.initialized=!0},getMboxURL:function(t){var n=t;return L.isObject(t)&&(n="https:"===e.location.protocol?t.https:t.http),n.match(/^https?:/)?n:L.basePath()+n},onScriptLoaded:function(){L.notify("Test & Target: loaded.",1),this.flushQueue(),this.initialized=!0,this.initializing=!1},$addMbox:function(e,t,n){var a=n.mboxGoesAround,i=a+"{visibility: hidden;}",r=this.appendStyle(i);a in this.styleElements||(this.styleElements[a]=r),this.initialized?this.$addMBoxStep2(null,null,n):this.initializing&&this.queueCommand({command:"addMBoxStep2",arguments:[n]},e,t)},$addMBoxStep2:function(n,a,i){var r=this.generateID(),o=this;L.addEventHandler(e,"load",L.bind(function(){L.cssQuery(i.mboxGoesAround,function(n){var a=n[0];if(a){var s=t.createElement("div");s.id=r,a.parentNode.replaceChild(s,a),s.appendChild(a),e.mboxDefine(r,i.mboxName);var c=[i.mboxName];i.arguments&&(c=c.concat(i.arguments)),e.mboxUpdate.apply(null,c),o.reappearWhenCallComesBack(a,r,i.timeout,i)}})},this)),this.lastMboxID=r},$addTargetPageParams:function(e,t,n){this.updateTargetPageParams(n)},generateID:function(){return"_sdsat_mbox_"+String(Math.random()).substring(2)+"_"},appendStyle:function(e){var n=t.getElementsByTagName("head")[0],a=t.createElement("style");return a.type="text/css",a.styleSheet?a.styleSheet.cssText=e:a.appendChild(t.createTextNode(e)),n.appendChild(a),a},reappearWhenCallComesBack:function(e,t,n,a){function i(){var e=r.styleElements[a.mboxGoesAround];e&&(e.parentNode.removeChild(e),delete r.styleElements[a.mboxGoesAround])}var r=this;L.cssQuery('script[src*="omtrdc.net"]',function(e){var t=e[0];if(t){L.scriptOnLoad(t.src,t,function(){L.notify("Test & Target: request complete",1),i(),clearTimeout(a)});var a=setTimeout(function(){L.notify("Test & Target: bailing after "+n+"ms",1),i()},n)}else L.notify("Test & Target: failed to find T&T ajax call, bailing",1),i()})},updateTargetPageParams:function(e){var t={};for(var n in e)e.hasOwnProperty(n)&&(t[L.replace(n)]=L.replace(e[n]));L.extend(this.targetPageParamsStore,t)},getTargetPageParams:function(){return this.targetPageParamsStore},setTargetPageParamsFunction:function(){e.targetPageParams=L.bind(this.getTargetPageParams,this)},parseTargetPageParamsResult:function(e){var t=e;return L.isArray(e)&&(e=e.join("&")),L.isString(e)&&(t=L.parseQueryParams(e)),t}}),L.availableTools.tnt=h,L.inherit(p,L.BaseTool),L.extend(p.prototype,{name:"Default",$loadIframe:function(t,n,a){var i=a.pages,r=a.loadOn,o=L.bind(function(){L.each(i,function(e){this.loadIframe(t,n,e)},this)},this);r||o(),"domready"===r&&L.domReady(o),"load"===r&&L.addEventHandler(e,"load",o)},loadIframe:function(e,n,a){var i=t.createElement("iframe");i.style.display="none";var r=L.data.host,o=a.data,s=this.scriptURL(a.src),c=L.searchVariables(o,e,n);r&&(s=L.basePath()+s),s+=c,i.src=s;var l=t.getElementsByTagName("body")[0];l?l.appendChild(i):L.domReady(function(){t.getElementsByTagName("body")[0].appendChild(i)})},scriptURL:function(e){return(L.settings.scriptDir||"")+e},$loadScript:function(t,n,a){var i=a.scripts,r=a.sequential,o=a.loadOn,s=L.bind(function(){r?this.loadScripts(t,n,i):L.each(i,function(e){this.loadScripts(t,n,[e])},this)},this);o?"domready"===o?L.domReady(s):"load"===o&&L.addEventHandler(e,"load",s):s()},loadScripts:function(e,t,n){function a(){r.length>0&&i&&r.shift().call(e,t,o);var c=n.shift();if(c){var l=L.data.host,u=s.scriptURL(c.src);l&&(u=L.basePath()+u),i=c,L.loadScript(u,a)}}try{n=n.slice(0);var i,r=this.asyncScriptCallbackQueue,o=t.target||t.srcElement,s=this}catch(c){console.error("scripts is",L.stringify(n))}a()},$loadBlockingScript:function(e,t,n){var a=n.scripts;n.loadOn;L.bind(function(){L.each(a,function(n){this.loadBlockingScript(e,t,n)},this)},this)()},loadBlockingScript:function(e,t,n){var a=this.scriptURL(n.src),i=L.data.host,r=t.target||t.srcElement;i&&(a=L.basePath()+a),this.argsForBlockingScripts.push([e,t,r]),L.loadScriptSync(a)},pushAsyncScript:function(e){this.asyncScriptCallbackQueue.push(e)},pushBlockingScript:function(e){var t=this.argsForBlockingScripts.shift(),n=t[0];e.apply(n,t.slice(1))},$writeHTML:L.escapeHtmlParams(function(e,n){if(!L.domReadyFired&&t.write)if("pagebottom"===n.type||"pagetop"===n.type)for(var a=2,i=arguments.length;a<i;a++){var r=arguments[a].html;r=L.replace(r,e,n),t.write(r)}else L.notify("You can only use writeHTML on the `pagetop` and `pagebottom` events.",1);else L.notify("Command writeHTML failed. You should try appending HTML using the async option.",1)}),linkNeedsDelayActivate:function(t,n){n=n||e;var a=t.tagName,i=t.getAttribute("target"),r=t.getAttribute("href");return(!a||"a"===a.toLowerCase())&&(!!r&&(!i||"_blank"!==i&&("_top"===i?n.top===n:"_parent"!==i&&("_self"===i||(!n.name||i===n.name)))))},$delayActivateLink:function(e,t){if(this.linkNeedsDelayActivate(e)){L.preventDefault(t);var n=L.settings.linkDelay||100;setTimeout(function(){L.setLocation(e.href)},n)}},isQueueable:function(e){return"writeHTML"!==e.command}}),L.availableTools["default"]=p,L.inherit(v,L.BaseTool),L.extend(v.prototype,{name:"SC",endPLPhase:function(e){e===this.settings.loadOn&&this.initialize(e)},initialize:function(t){if(!this._cancelToolInit)if(this.settings.initVars=this.substituteVariables(this.settings.initVars,{type:t}),!1!==this.settings.initTool){var n=this.settings.sCodeURL||L.basePath()+"s_code.js";"object"==typeof n&&(n="https:"===e.location.protocol?n.https:n.http),n.match(/^https?:/)||(n=L.basePath()+n),this.settings.initVars&&this.$setVars(null,null,this.settings.initVars),L.loadScript(n,L.bind(this.onSCodeLoaded,this)),this.initializing=!0}else this.initializing=!0,this.pollForSC()},getS:function(t,n){var a=n&&n.hostname||e.location.hostname,i=this.concatWithToolVarBindings(n&&n.setVars||this.varBindings),r=n&&n.addEvent||this.events,o=this.getAccount(a),s=e.s_gi;if(!s)return null;if(this.isValidSCInstance(t)||(t=null),!o&&!t)return L.notify("Adobe Analytics: tracker not initialized because account was not found",1),null;t=t||s(o);var c="D"+L.appVersion;return"undefined"!=typeof t.tagContainerMarker?t.tagContainerMarker=c:"string"==typeof t.version&&t.version.substring(t.version.length-5)!=="-"+c&&(t.version+="-"+c),t.sa&&!0!==this.settings.skipSetAccount&&!1!==this.settings.initTool&&t.sa(this.settings.account),this.applyVarBindingsOnTracker(t,i),r.length>0&&(t.events=r.join(",")),L.getVisitorId()&&(t.visitor=L.getVisitorId()),t},onSCodeLoaded:function(e){this.initialized=!0,this.initializing=!1;var t=["Adobe Analytics: loaded",e?" (manual)":"","."];L.notify(t.join(""),1),L.fireEvent(this.id+".load",this.getS()),e||(this.flushQueueExceptTrackLink(),this.sendBeacon()),this.flushQueue()},getAccount:function(t){return e.s_account?e.s_account:t&&this.settings.accountByHost&&this.settings.accountByHost[t]||this.settings.account},getTrackingServer:function(){var t=this,n=t.getS();if(n){if(n.ssl&&n.trackingServerSecure)return n.trackingServerSecure;if(n.trackingServer)return n.trackingServer}var a,i=t.getAccount(e.location.hostname);if(!i)return null;var r,o,s="",c=n&&n.dc;return(r=(a=i).indexOf(","))>=0&&(a=a.gb(0,r)),a=a.replace(/[^A-Za-z0-9]/g,""),s||(s="2o7.net"),c=c?(""+c).toLowerCase():"d1","2o7.net"==s&&("d1"==c?c="112":"d2"==c&&(c="122"),o=""),r=a+"."+c+"."+o+s},sendBeacon:function(){var t=this.getS(e[this.settings.renameS||"s"]);t?this.settings.customInit&&!1===this.settings.customInit(t)?L.notify("Adobe Analytics: custom init suppressed beacon",1):(this.settings.executeCustomPageCodeFirst&&this.applyVarBindingsOnTracker(t,this.varBindings),this.executeCustomSetupFuns(t),t.t(),this.clearVarBindings(),this.clearCustomSetup(),L.notify("Adobe Analytics: tracked page view",1)):L.notify("Adobe Analytics: page code not loaded",1)},pollForSC:function(){L.poll(L.bind(function(){if("function"==typeof e.s_gi)return this.onSCodeLoaded(!0),!0},this))},flushQueueExceptTrackLink:function(){if(this.pending){for(var e=[],t=0;t<this.pending.length;t++){var n=this.pending[t];"trackLink"===n[0].command?e.push(n):this.triggerCommand.apply(this,n)}this.pending=e}},isQueueAvailable:function(){return!this.initialized},substituteVariables:function(e,t){var n={};for(var a in e)if(e.hasOwnProperty(a)){var i=e[a];n[a]=L.replace(i,location,t)}return n},$setVars:function(e,t,n){for(var a in n)if(n.hasOwnProperty(a)){var i=n[a];"function"==typeof i&&(i=i()),this.varBindings[a]=i}L.notify("Adobe Analytics: set variables.",2)},$customSetup:function(e,t,n){this.customSetupFuns.push(function(a){n.call(e,t,a)})},isValidSCInstance:function(e){return!!e&&"function"==typeof e.t&&"function"==typeof e.tl},concatWithToolVarBindings:function(e){var t=this.settings.initVars||{};return L.map(["trackingServer","trackingServerSecure"],function(n){t[n]&&!e[n]&&(e[n]=t[n])}),e},applyVarBindingsOnTracker:function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])},clearVarBindings:function(){this.varBindings={}},clearCustomSetup:function(){this.customSetupFuns=[]},executeCustomSetupFuns:function(t){L.each(this.customSetupFuns,function(n){n.call(e,t)})},$trackLink:function(e,t,n){var a=(n=n||{}).type,i=n.linkName;!i&&e&&e.nodeName&&"a"===e.nodeName.toLowerCase()&&(i=e.innerHTML),i||(i="link clicked");var r=n&&n.setVars,o=n&&n.addEvent||[],s=this.getS(null,{setVars:r,addEvent:o});if(s){var c=s.linkTrackVars,l=s.linkTrackEvents,u=this.definedVarNames(r);n&&n.customSetup&&n.customSetup.call(e,t,s),o.length>0&&u.push("events"),s.products&&u.push("products"),u=this.mergeTrackLinkVars(s.linkTrackVars,u),o=this.mergeTrackLinkVars(s.linkTrackEvents,o),s.linkTrackVars=this.getCustomLinkVarsList(u);var d=L.map(o,function(e){return e.split(":")[0]});s.linkTrackEvents=this.getCustomLinkVarsList(d),s.tl(!0,a||"o",i),L.notify(["Adobe Analytics: tracked link ","using: linkTrackVars=",L.stringify(s.linkTrackVars),"; linkTrackEvents=",L.stringify(s.linkTrackEvents)].join(""),1),s.linkTrackVars=c,s.linkTrackEvents=l}else L.notify("Adobe Analytics: page code not loaded",1)},mergeTrackLinkVars:function(e,t){return e&&(t=e.split(",").concat(t)),t},getCustomLinkVarsList:function(e){var t=L.indexOf(e,"None");return t>-1&&e.length>1&&e.splice(t,1),e.join(",")},definedVarNames:function(e){e=e||this.varBindings;var t=[];for(var n in e)e.hasOwnProperty(n)&&/^(eVar[0-9]+)|(prop[0-9]+)|(hier[0-9]+)|campaign|purchaseID|channel|server|state|zip|pageType$/.test(n)&&t.push(n);return t},$trackPageView:function(e,t,n){var a=n&&n.setVars,i=n&&n.addEvent||[],r=this.getS(null,{setVars:a,addEvent:i});r?(r.linkTrackVars="",r.linkTrackEvents="",this.executeCustomSetupFuns(r),n&&n.customSetup&&n.customSetup.call(e,t,r),r.t(),this.clearVarBindings(),this.clearCustomSetup(),L.notify("Adobe Analytics: tracked page view",1)):L.notify("Adobe Analytics: page code not loaded",1)},$postTransaction:function(t,n,a){var i=L.data.transaction=e[a],r=this.varBindings,o=this.settings.fieldVarMapping;if(L.each(i.items,function(e){this.products.push(e)},this),r.products=L.map(this.products,function(e){var t=[];if(o&&o.item)for(var n in o.item)if(o.item.hasOwnProperty(n)){var a=o.item[n];t.push(a+"="+e[n]),"event"===a.substring(0,5)&&this.events.push(a)}var i=["",e.product,e.quantity,e.unitPrice*e.quantity];return t.length>0&&i.push(t.join("|")),i.join(";")},this).join(","),o&&o.transaction){var s=[];for(var c in o.transaction)if(o.transaction.hasOwnProperty(c)){a=o.transaction[c];s.push(a+"="+i[c]),"event"===a.substring(0,5)&&this.events.push(a)}r.products.length>0&&(r.products+=","),r.products+=";;;;"+s.join("|")}},$addEvent:function(){for(var e=2,t=arguments.length;e<t;e++)this.events.push(arguments[e])},$addProduct:function(){for(var e=2,t=arguments.length;e<t;e++)this.products.push(arguments[e])}}),L.availableTools.sc=v,L.inherit(b,L.BaseTool),L.extend(b.prototype,{initialize:function(){var e=this.settings;if(!1!==this.settings.initTool){var t=e.url;t="string"==typeof t?L.basePath()+t:L.isHttps()?t.https:t.http,L.loadScript(t,L.bind(this.onLoad,this)),this.initializing=!0}else this.initialized=!0},isQueueAvailable:function(){return!this.initialized},onLoad:function(){this.initialized=!0,this.initializing=!1,this.settings.initialBeacon&&this.settings.initialBeacon(),this.flushQueue()},endPLPhase:function(e){e===this.settings.loadOn&&(L.notify(this.name+": Initializing at "+e,1),this.initialize())},$fire:function(e,t,n){this.initializing?this.queueCommand({command:"fire",arguments:[n]},e,t):n.call(this.settings,e,t)}}),L.availableTools.am=b,L.availableTools.adlens=b,L.availableTools.aem=b,L.availableTools.__basic=b,L.inherit(y,L.BaseTool),L.extend(y.prototype,{name:"GA",initialize:function(){var t=this.settings,n=e._gaq,a=t.initCommands||[],i=t.customInit;if(n||(_gaq=[]),this.isSuppressed())L.notify("GA: page code not loaded(suppressed).",1);else{if(!n&&!y.scriptLoaded){var r=L.isHttps(),o=(r?"https://ssl":"http://www")+".google-analytics.com/ga.js";t.url&&(o=r?t.url.https:t.url.http),L.loadScript(o),y.scriptLoaded=!0,L.notify("GA: page code loaded.",1)}t.domain;var s=t.trackerName,c=E.allowLinker(),l=L.replace(t.account,location);L.settings.domainList;_gaq.push([this.cmd("setAccount"),l]),c&&_gaq.push([this.cmd("setAllowLinker"),c]),_gaq.push([this.cmd("setDomainName"),E.cookieDomain()]),L.each(a,function(e){var t=[this.cmd(e[0])].concat(L.preprocessArguments(e.slice(1),location,null,this.forceLowerCase));_gaq.push(t)},this),i&&(this.suppressInitialPageView=!1===i(_gaq,s)),t.pageName&&this.$overrideInitialPageView(null,null,t.pageName)}this.initialized=!0,L.fireEvent(this.id+".configure",_gaq,s)},isSuppressed:function(){return this._cancelToolInit||!1===this.settings.initTool},tracker:function(){return this.settings.trackerName},cmd:function(e){var t=this.tracker();return t?t+"._"+e:"_"+e},$overrideInitialPageView:function(e,t,n){this.urlOverride=n},trackInitialPageView:function(){if(!this.isSuppressed()&&!this.suppressInitialPageView)if(this.urlOverride){var e=L.preprocessArguments([this.urlOverride],location,null,this.forceLowerCase);this.$missing$("trackPageview",null,null,e)}else this.$missing$("trackPageview")},endPLPhase:function(e){e===this.settings.loadOn&&(L.notify("GA: Initializing at "+e,1),this.initialize(),this.flushQueue(),this.trackInitialPageView())},call:function(e,t,n,a){if(!this._cancelToolInit){this.settings;var i=this.tracker(),r=this.cmd(e);a=a?[r].concat(a):[r];_gaq.push(a),
	i?L.notify("GA: sent command "+e+" to tracker "+i+(a.length>1?" with parameters ["+a.slice(1).join(", ")+"]":"")+".",1):L.notify("GA: sent command "+e+(a.length>1?" with parameters ["+a.slice(1).join(", ")+"]":"")+".",1)}},$missing$:function(e,t,n,a){this.call(e,t,n,a)},$postTransaction:function(t,n,a){var i=L.data.customVars.transaction=e[a];this.call("addTrans",t,n,[i.orderID,i.affiliation,i.total,i.tax,i.shipping,i.city,i.state,i.country]),L.each(i.items,function(e){this.call("addItem",t,n,[e.orderID,e.sku,e.product,e.category,e.unitPrice,e.quantity])},this),this.call("trackTrans",t,n)},delayLink:function(e,t){var n=this;if(E.allowLinker()&&e.hostname.match(this.settings.linkerDomains)&&!L.isSubdomainOf(e.hostname,location.hostname)){L.preventDefault(t);var a=L.settings.linkDelay||100;setTimeout(function(){n.call("link",e,t,[e.href])},a)}},popupLink:function(t,n){if(e._gat){L.preventDefault(n);var a=this.settings.account,i=e._gat._createTracker(a)._getLinkerUrl(t.href);e.open(i)}},$link:function(e,t){"_blank"===e.getAttribute("target")?this.popupLink(e,t):this.delayLink(e,t)},$trackEvent:function(e,t){var n=Array.prototype.slice.call(arguments,2);if(n.length>=4&&null!=n[3]){var a=parseInt(n[3],10);L.isNaN(a)&&(a=1),n[3]=a}this.call("trackEvent",e,t,n)}}),L.availableTools.ga=y;var E={allowLinker:function(){return L.hasMultipleDomains()},cookieDomain:function(){var t=L.settings.domainList,n=L.find(t,function(t){var n=e.location.hostname;return L.equalsIgnoreCase(n.slice(n.length-t.length),t)});return n?"."+n:"auto"}};L.inherit(V,L.BaseTool),L.extend(V.prototype,{name:"GAUniversal",endPLPhase:function(e){e===this.settings.loadOn&&(L.notify("GAU: Initializing at "+e,1),this.initialize(),this.flushQueue(),this.trackInitialPageView())},getTrackerName:function(){return this.settings.trackerSettings.name||""},isPageCodeLoadSuppressed:function(){return!1===this.settings.initTool||!0===this._cancelToolInit},initialize:function(){if(this.isPageCodeLoadSuppressed())return this.initialized=!0,void L.notify("GAU: Page code not loaded (suppressed).",1);var t="ga";e[t]=e[t]||this.createGAObject(),e.GoogleAnalyticsObject=t,L.notify("GAU: Page code loaded.",1),L.loadScriptOnce(this.getToolUrl());var n=this.settings;(E.allowLinker()&&!1!==n.allowLinker?this.createAccountForLinker():this.createAccount(),this.executeInitCommands(),n.customInit)&&(!1===(0,n.customInit)(e[t],this.getTrackerName())&&(this.suppressInitialPageView=!0));this.initialized=!0},createGAObject:function(){var e=function(){e.q.push(arguments)};return e.q=[],e.l=1*new Date,e},createAccount:function(){this.create()},createAccountForLinker:function(){var e={};E.allowLinker()&&(e.allowLinker=!0),this.create(e),this.call("require","linker"),this.call("linker:autoLink",this.autoLinkDomains(),!1,!0)},create:function(e){var t=this.settings.trackerSettings;(t=L.preprocessArguments([t],location,null,this.forceLowerCase)[0]).trackingId=L.replace(this.settings.trackerSettings.trackingId,location),t.cookieDomain||(t.cookieDomain=E.cookieDomain()),L.extend(t,e||{}),this.call("create",t)},autoLinkDomains:function(){var e=location.hostname;return L.filter(L.settings.domainList,function(t){return t!==e})},executeInitCommands:function(){var e=this.settings;e.initCommands&&L.each(e.initCommands,function(e){var t=e.splice(2,e.length-2);e=e.concat(L.preprocessArguments(t,location,null,this.forceLowerCase)),this.call.apply(this,e)},this)},trackInitialPageView:function(){this.suppressInitialPageView||this.isPageCodeLoadSuppressed()||this.call("send","pageview")},call:function(){"function"==typeof ga?this.isCallSuppressed()||(arguments[0]=this.cmd(arguments[0]),this.log(L.toArray(arguments)),ga.apply(e,arguments)):L.notify("GA Universal function not found!",4)},isCallSuppressed:function(){return!0===this._cancelToolInit},$missing$:function(e,t,n,a){a=a||[],a=[e].concat(a),this.call.apply(this,a)},getToolUrl:function(){var e=this.settings,t=L.isHttps();return e.url?t?e.url.https:e.url.http:(t?"https://ssl":"http://www")+".google-analytics.com/analytics.js"},cmd:function(e){var t=["send","set","get"],n=this.getTrackerName();return n&&-1!==L.indexOf(t,e)?n+"."+e:e},log:function(e){var t="GA Universal: sent command "+e[0]+" to tracker "+(this.getTrackerName()||"default");if(e.length>1){L.stringify(e.slice(1));t+=" with parameters "+L.stringify(e.slice(1))}t+=".",L.notify(t,1)}}),L.availableTools.ga_universal=V,L.extend(S.prototype,{getInstance:function(){return this.instance},initialize:function(){var e,t=this.settings;L.notify("Visitor ID: Initializing tool",1),null!==(e=this.createInstance(t.mcOrgId,t.initVars))&&(t.customerIDs&&this.applyCustomerIDs(e,t.customerIDs),t.autoRequest&&e.getMarketingCloudVisitorID(),this.instance=e)},createInstance:function(e,t){if(!L.isString(e))return L.notify('Visitor ID: Cannot create instance using mcOrgId: "'+e+'"',4),null;e=L.replace(e),L.notify('Visitor ID: Create instance using mcOrgId: "'+e+'"',1),t=this.parseValues(t);var n=Visitor.getInstance(e,t);return L.notify("Visitor ID: Set variables: "+L.stringify(t),1),n},applyCustomerIDs:function(e,t){var n=this.parseIds(t);e.setCustomerIDs(n),L.notify("Visitor ID: Set Customer IDs: "+L.stringify(n),1)},parseValues:function(e){if(!1===L.isObject(e))return{};var t={};for(var n in e)e.hasOwnProperty(n)&&(t[n]=L.replace(e[n]));return t},parseIds:function(e){var t={};if(!1===L.isObject(e))return{};for(var n in e)if(e.hasOwnProperty(n)){var a=L.replace(e[n].id);a!==e[n].id&&a&&(t[n]={},t[n].id=a,t[n].authState=Visitor.AuthState[e[n].authState])}return t}}),L.availableTools.visitor_id=S,_satellite.init({tools:{d500d335c7e5b6c8a19213537b6795556f5635a7:{engine:"aem",name:"AEM ContextHub",data_layer_root:"window.dataLayer"},a4d443bd5cebe988fc120fe4a108938b:{engine:"sc",loadOn:"pagebottom",account:"rc-france,rcentral-dev",euCookie:!1,sCodeURL:"https://blog.corp.ringcentral.fr/blog/wp-content/themes/rcconnect_UK_2017/js/app-measurement-fr.js",initVars:{currencyCode:"USD",trackingServer:"som.ringcentral.com",trackingServerSecure:"som.ringcentral.com",pageName:"%PageName%",channel:"France",trackInlineStats:!0,trackDownloadLinks:!1,trackExternalLinks:!1,linkLeaveQueryString:!1,dynamicVariablePrefix:"D=",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar6:"%pid (evar6)%",eVar7:"%aid (evar7)%",eVar8:"%oid (evar8)%",eVar1:"Default PLR rule",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%"},customInit:function(){setTimeout(function(){var e=t.createElement("script"),n=t.getElementsByTagName("script")[0];e.src=t.location.protocol+"//script.crazyegg.com/pages/scripts/0034/7442.js?"+Math.floor((new Date).getTime()/36e5),e.async=!0,e.type="text/javascript",n.parentNode.insertBefore(e,n)},1)}},"82dc061bd78612a31895153f00c079a838c2a8ac":{engine:"visitor_id",loadOn:"pagetop",name:"VisitorID",mcOrgId:"101A678254E6D3620A4C98A5@AdobeOrg",autoRequest:!0,initVars:{trackingServer:"som.ringcentral.com",trackingServerSecure:"som.ringcentral.com"}}},pageLoadRules:[{name:"Search Page Default Pixel Block",trigger:[{engine:"sc",command:"setVars",arguments:[{eVar1:"PLR Search Page Default Pixel Block"}]},{engine:"sc",command:"customSetup",arguments:[function(e,t){t.abort=!0}]}],scope:{URI:{include:[/search.html/i]}},conditions:[function(){return _satellite.textMatch(_satellite.getQueryParam("q"),/.+/i)}],event:"pagetop"}],rules:[{name:"/agent.html Partner With US #1",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/agent.html Partner With US #1",eVar12:"%cid (evar12)%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1  - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59c3a4ce64746d3524012503.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),"cta-buttons--786509928")}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/agent.html Partner With US #2",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/agent.html Partner With US #2",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2  - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59c3a4d564746d383200bf0d.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),"cta-buttons-1816515864")}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/platform/developer.html Three Block Buttons #1",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/platform/developer.html Three Block Buttons #1",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1 - blocks buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59c9161564746d517100ee30.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/three-blocks--1461684933/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/platform/developer.html Three Block Buttons #2",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/platform/developer.html Three Block Buttons #2",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2 - blocks buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59c9166764746d21fe000878.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/three-blocks--449484668/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"/support/index.html Flex Menu buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"/support/index.html Flex Menu buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - flex buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5a1d692764746d1efb0077c7.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/flex-item-card-2085983016|flex-item-card-2085787009|flex-item-card-2087191458|flex-item-card-2087326047/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click"),"flex-item-card-513830604")}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Block Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Block Buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - blocks buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59a5473064746d516e000b3b.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/three-blocks/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/^(?!three-blocks--1461684933$).*$/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/^(?!three-blocks--449484668$).*$/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Buy Now/Free Trial buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Buy Now/Free Trial buttons",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5abb8ebe64746d3291005e17.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/plans-and-pricing-575692595/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Buy Now/Free Trial buttons hover left infos",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Buy Now/Free Trial buttons hover left infos",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:'info - "%Name Hover%"',eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5abbb97264746d5370000248.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Hover"),/plans-table-row--1410832164/i)}],event:"dataelementchange(GUID Hover)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"CTA buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"CTA buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/cta-buttons/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/^(?!cta-buttons--786509928|cta-buttons--411118921$|cta-buttons-1816515864|cta-buttons--612013134$|cta-buttons--1260547495|cta-buttons-1607547187|cta-buttons--518430636|cta-buttons--612013134|cta-buttons-237057604|cta-buttons-777969018|cta-buttons-121745867|cta-buttons--1860888720|cta-buttons-432991667|cta-buttons-2051605996|cta-buttons-735150014|cta-buttons-252587428|cta-buttons-131234102|cta-buttons-281042075|cta-buttons--2003257842|cta-buttons--1089073992|cta-buttons--1855057991).*$/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"CTA buttons Exceptions",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"CTA buttons Exceptions",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/cta-buttons--411118921/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Country Selector",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Country Selector",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Country Selector",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/countrySelect/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"DEMO Pop-Up Video Tab Custom Event",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"DEMO Pop-Up Video Tab Custom Event",eVar12:"%cid (evar12)%",eVar14:"Video Form",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"France",pageURL:"%PageName%"},addEvent:["event6"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5abb820d64746d2458004c68.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Name Custom Event"),"Video Form")},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Custom Event)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"DataSheet Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"DataSheet Buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - additional resources",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59c3bdba64746d08f2010f43.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/^(datasheet-block|additional-resources)/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Default buttons (no postfix)",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Default buttons (no postfix)",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/plans-table-row--1410832164|carousel-minimized-card|carousel-card/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Download buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Download buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Download Buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/download-buttons--1892731338/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Flex Menu buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Flex Menu buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - flex buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59ccbe1764746d1dc60097d6.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/^(?!flex-item-card-2085983016|flex-item-card-2085787009|flex-item-card-2087191458|flex-item-card-2087326047).*$/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/flex-item-card-646428675|flex-item-card-1367660430|flex-item-card-1369214863|flex-item-card-1367866377|flex-item-card-647657827|flex-item-card-647760957|flex-item-card-646343610|flex-item-card-646315356|flex-item-card-1371227912|flex-item-card-1371256910/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Float Navigation Menu",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Float Navigation Menu",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - mid. nav. menu",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59a5456c64746d6e37006070.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/navigation-float/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Footer buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Footer buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - footer",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/footer/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Form Error",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Form Error",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Error% Form > Error",eVar56:"%Form Name Error%",eVar57:"%Form Error Fields%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Error%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop56:"D=v56",prop57:"D=v57",prop75:"D=v79",pageName:"%PageName% > %Form Name Error% Form > Error",channel:"France",pageURL:"%PageName% > %Form Name Error% Form > Error"}}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Form Error)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Form Start",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Form Start",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Start% Form > Start",eVar56:"%Form Name Start%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Start%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop56:"D=v56",prop75:"D=v79",pageName:"%PageName% > %Form Name Start% Form > Start",channel:"France",pageURL:"%PageName% > %Form Name Start% Form > Start"},addEvent:["event37"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Form Start)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Form Success",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Form Success",eVar12:"%cid (evar12)%",eVar26:"%Form LS Success%",eVar27:"%Form LES Success%",eVar5:"%bmid (evar5)%",eVar55:"%PageName% > %Form Name Success% Form > Submitted",eVar56:"%Form Name Success%",eVar61:"%Full URL (eVar61)%",eVar79:"%Form Parent URL Success%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop56:"D=v56",prop75:"D=v79",pageName:"%PageName% > %Form Name Success% Form > Submitted",channel:"France",pageURL:"%PageName% > %Form Name Success% Form > Submitted"},addEvent:["event7"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Form Success)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Get Glip now button #1",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Get Glip now button #1",eVar12:"%cid (evar12)%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #1",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/glip-form--1530884709/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/glip-form-730532715/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Get Glip now button #2",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Get Glip now button #2",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% #2",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/glip-form-1508671952/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/glip-form-730532715/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Hero Carousel Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Hero Carousel Buttons",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - CTA buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/cta-buttons--1260547495|cta-buttons--612013134|cta-buttons-1607547187|cta-buttons--518430636|cta-buttons--612013134|cta-buttons-237057604|cta-buttons-777969018|cta-buttons-121745867|cta-buttons--1860888720|cta-buttons-432991667|cta-buttons-2051605996|cta-buttons-735150014|cta-buttons-252587428|cta-buttons-131234102|cta-buttons-281042075|cta-buttons--2003257842|cta-buttons--1089073992|cta-buttons--1855057991|hero-image--1735068181/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("URL Click"),/^(?!.*\.pdf$).*$/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Hero Carousel Buttons For PDF",trigger:[{engine:"sc",command:"trackLink",arguments:[{setVars:{eVar1:"Hero Carousel Buttons For PDF",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar49:"%Hero Carousel Slide Name Click (eVar49)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - CTA buttons",eVar55:"%PageName%",eVar58:"%URL Click%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"Ireland",pageURL:"%PageName%"},addEvent:["event31","event38"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/cta-buttons-735150014/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("URL Click"),/.pdf/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Logo (temporary)",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Logo (temporary)",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"Logo - header",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59cba46c64746d6427002dea.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51)"),/RingCentral Virtual PBX, Phone and Internet Fax Service and Software/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Middle Navigation Menu",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Middle Navigation Menu",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - mid. nav. menu",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59c3ccb564746d520500de38.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/flex-item-card/i)},function(){return _satellite.textMatch(_satellite.getVar("Instance Click"),/^(?!flex-item-card-2085983016|flex-item-card-2085787009|flex-item-card-2087191458|flex-item-card-2087326047|flex-item-card-646428675|flex-item-card-1367660430|flex-item-card-1369214863|flex-item-card-1367866377|flex-item-card-647657827|flex-item-card-647760957|flex-item-card-646343610|flex-item-card-646315356$|flex-item-card-1371227912|flex-item-card-1371256910).*$/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Search",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Search",eVar12:"%cid (evar12)%",eVar29:"%Query Search (eVar29)%",eVar31:"%Quantity Search (eVar31)%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",pageName:"%PageName%",channel:"France",pageURL:"%PageName%"},addEvent:["event12"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Search)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Search click on the found",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Search click on the found",eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/search-v2/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0},function(){return _satellite.textMatch(_satellite.getVar("URL Click"),/^(?!.*\.pdf$).*$/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Sitemap Links",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Sitemap Links",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",
	eVar51:"%Button Name (eVar51)% - sitemap links",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/sitemap/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Technology Block Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Technology Block Buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - Technology Block",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/technology-block/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Terms of Services buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Terms of Services buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - ToS buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/legal-844535100/i)},function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/^(?!legal-text$).*$/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Text Links",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Text Links",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - text links",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/^(leadform-regular|text-block|legal-text|text-image|responsive-swiper|leadform-general|leadform-gartner)/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Top Navigation Menu "header"',trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:'Top Navigation Menu "header"',eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - header",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59a540ad64746d7ade005e30.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/header/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51)"),/^(?!RingCentral Virtual PBX, Phone and Internet Fax Service and Software$).*$/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Top Navigation Menu "navigation-primary"',trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:'Top Navigation Menu "navigation-primary"',eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - top nav. menu",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59a53b0964746d217a004380.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/navigation-primary/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:'Top Navigation Menu "navigation-primary" Hover',trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:'Top Navigation Menu "navigation-primary" Hover',eVar12:"%cid (evar12)%",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar51:"%Name Hover% - top nav. menu - Hover",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Hover"),/navigation-primary/i)}],event:"dataelementchange(GUID Hover)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video 25%",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video 25%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32)%",eVar48:"%Video Content Type (evar48) Stop%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",pageName:"%PageName% > %Video Finished Name (eVar32)% > 25% Finished",channel:"France",pageURL:"%PageName%"},addEvent:["event27"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone"),/25/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video 50%",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video 50%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32)%",eVar48:"%Video Content Type (evar48) Stop%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",pageName:"%PageName% > %Video Finished Name (eVar32)% > 50% Finished",channel:"France",pageURL:"%PageName%"},addEvent:["event28"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone"),/50/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video 75%",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video 75%",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32)%",eVar48:"%Video Content Type (evar48) Stop%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",pageName:"%PageName% > %Video Finished Name (eVar32)% > 75% Finished",channel:"France",pageURL:"%PageName%"},addEvent:["event29"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone"),/75/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video Buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"Video Buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - video button",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-59ca14dd64746d3672000e4e.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/youtube-video-card/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video Finish",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video Finish",eVar12:"%cid (evar12)%",eVar32:"%Video Finished Name (eVar32)%",eVar48:"%Video Content Type (evar48) Stop%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",pageName:"%PageName% > %Video Finished Name (eVar32)% > 100% Finished",channel:"France",pageURL:"%PageName%"},addEvent:["event14"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Video Milestone"),/100/i)},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Stop)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"Video Start",trigger:[{engine:"sc",command:"trackPageView",arguments:[{setVars:{eVar1:"Video Start",eVar12:"%cid (evar12)%",eVar32:"%Video Start Name (eVar32)%",eVar48:"%Video Content Type (evar48) Start%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",pageName:"%PageName% > %Video Start Name (eVar32)% > Start",channel:"France",pageURL:"%PageName%"},addEvent:["event13"]}]}],conditions:[function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Video Start)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"View Demo button (event4)",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"View Demo button (event4)",eVar12:"%cid (evar12)%",eVar14:"Mobile Demo",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"France",pageURL:"%PageName%"},addEvent:["event4"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5abb820d64746d2458004c40.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/view-demo--623061733/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51)"),"Mobile Demo")},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"View Demo button (event6)",trigger:[{engine:"sc",command:"trackPageView",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"View Demo button (event6)",eVar12:"%cid (evar12)%",eVar14:"Video Demo",eVar41:"%mcvid%",eVar5:"%bmid (evar5)%",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",prop41:"D=v41",pageName:"%PageName%",channel:"France",pageURL:"%PageName%"},addEvent:["event6"]}]},{command:"loadScript",arguments:[{sequential:!1,scripts:[{src:"satellite-5abb820d64746d2458004c16.js"}]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/view-demo--623061733/i)},function(){return _satellite.textMatch(_satellite.getVar("Button Name (eVar51)"),"Video Demo")},function(){return _satellite.getToolsByType("sc")[0].getS().clearVars(),!0}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1},{name:"White Paper buttons",trigger:[{engine:"sc",command:"trackLink",arguments:[{type:"o",linkName:"%Button Name (eVar51)%",setVars:{eVar1:"White Paper buttons",eVar12:"%cid (evar12)%",eVar5:"%bmid (evar5)%",eVar51:"%Button Name (eVar51)% - white paper buttons",eVar55:"%PageName%",eVar61:"%Full URL (eVar61)%",eVar85:"%BMID Lead First Touch (eVar85)%",eVar86:"%Old BMID (eVar86)%",channel:"France",pageURL:"%PageName%"},addEvent:["event31"]}]}],conditions:[function(){return _satellite.textMatch(_satellite.getVar("Component Click"),/doc-block/i)}],event:"dataelementchange(GUID Click)",bubbleFireIfParent:!0,bubbleFireIfChildFired:!0,bubbleStop:!1}],directCallRules:[],settings:{trackInternalLinks:!0,libraryName:"satelliteLib-87dce09d1dee8bace50538b1b1eacb3b14533967",isStaging:!1,allowGATTcalls:!1,downloadExtensions:/\.(?:doc|docx|eps|jpg|png|svg|xls|ppt|pptx|pdf|xlsx|tab|csv|zip|txt|vsd|vxd|xml|js|css|rar|exe|wma|mov|avi|wmv|mp3|wav|m4v)($|\&|\?)/i,notifications:!1,utilVisible:!1,domainList:["ringcentral.fr"],scriptDir:"7618e9029c8436bd1b1d926078af6c171547716a/scripts/",linkDelay:500,tagTimeout:3e3},data:{URI:t.location.pathname+t.location.search,browser:{},cartItems:[],revenue:"",host:{http:"assets.adobedtm.com",https:"assets.adobedtm.com"}},dataElements:{"Additional Info Hover":{contextHub:function(){try{return e.dataLayer.events.hover.additional.info}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"aid (evar7)":{contextHub:function(){try{return e.dataLayer.marketingIDs.aid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"bmid (evar5)":{contextHub:function(){try{return e.dataLayer.marketingIDs.bmid_lead}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"BMID Lead First Touch (eVar85)":{contextHub:function(){try{return e.dataLayer.marketingIDs.bmid_lead}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Button Name (eVar51)":{contextHub:function(){try{return e.dataLayer.events.click.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"cid (evar12)":{contextHub:function(){try{return e.dataLayer.marketingIDs.cid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Click":{contextHub:function(){try{return e.dataLayer.events.click.component}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Click 1":{contextHub:function(){try{return e.dataLayer.events.click.component}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Hover":{contextHub:function(){try{return e.dataLayer.events.hover.component}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Component Search":{contextHub:function(){try{return e.dataLayer.events.search.component}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Element Click":{contextHub:function(){try{return e.dataLayer.events.click.element}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Element Hover":{contextHub:function(){try{return e.dataLayer.events.hover.element}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Error Fields":{contextHub:function(){try{return e.dataLayer.events.form.error.fields}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form LES Success":{contextHub:function(){try{return e.dataLayer.events.form.success.les}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form LS Success":{contextHub:function(){try{return e.dataLayer.events.form.success.ls}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Error":{contextHub:function(){try{return e.dataLayer.events.form.error.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Start":{contextHub:function(){try{return e.dataLayer.events.form.start.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Name Success":{contextHub:function(){try{return e.dataLayer.events.form.success.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Error":{contextHub:function(){try{return e.dataLayer.events.form.error.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Start":{contextHub:function(){try{return e.dataLayer.events.form.start.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Form Parent URL Success":{contextHub:function(){try{return e.dataLayer.events.form.success.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Full URL (eVar61)":{customJS:function(){return location.href},storeLength:"pageview"},"GUID Click":{contextHub:function(){try{return e.dataLayer.events.click.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Custom Event":{contextHub:function(){try{return e.dataLayer.events.custom.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Error":{contextHub:function(){try{return e.dataLayer.events.form.error.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Start":{contextHub:function(){try{return e.dataLayer.events.form.start.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Form Success":{contextHub:function(){try{return e.dataLayer.events.form.success.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Hover":{contextHub:function(){try{return e.dataLayer.events.hover.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Search":{contextHub:function(){try{return e.dataLayer.events.search.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Video Start":{contextHub:function(){try{return e.dataLayer.events.video.play.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"GUID Video Stop":{contextHub:function(){try{return e.dataLayer.events.video.stop.guid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Hero Carousel Slide Name Click (eVar49)":{contextHub:function(){try{return e.dataLayer.events.click.additional.info}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Click":{contextHub:function(){try{return e.dataLayer.events.click.instance}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Hover":{contextHub:function(){try{return e.dataLayer.events.hover.instance}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Instance Search":{contextHub:function(){try{return e.dataLayer.events.search.instance}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},mcvid:{cookie:"vid",storeLength:"pageview"},"Name Custom Event":{contextHub:function(){try{return e.dataLayer.events.custom.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Name Hover":{contextHub:function(){try{return e.dataLayer.events.hover.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Name Search":{contextHub:function(){try{return e.dataLayer.events.search.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"NotActiveUser (eVar11)":{contextHub:function(){try{return e.dataLayer.user.NotActiveUser}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"oid (evar8)":{contextHub:function(){try{return e.dataLayer.marketingIDs.oid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Old BMID (eVar86)":{contextHub:function(){try{return e.dataLayer.marketingIDs.bmid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},PageName:{customJS:function(){return location.origin+location.pathname},storeLength:"pageview"},"pid (evar6)":{contextHub:function(){try{return e.dataLayer.marketingIDs.pid}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Quantity Search (eVar31)":{contextHub:function(){try{return e.dataLayer.events.search.additional.quantity}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Query Search (eVar29)":{contextHub:function(){try{return e.dataLayer.events.search.additional.query}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Query Search (from QSP)":{queryParam:"q",storeLength:"pageview",ignoreCase:1},"ROI (eVar30)":{contextHub:function(){try{return e.dataLayer.user.role}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Tag Click":{contextHub:function(){try{return e.dataLayer.events.click.tag}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Tag Hover":{contextHub:function(){try{return e.dataLayer.events.hover.tag}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Tag Search":{contextHub:function(){try{return e.dataLayer.events.search.tag}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"URL Click":{contextHub:function(){try{return e.dataLayer.events.click.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"URL Hover":{contextHub:function(){try{return e.dataLayer.events.hover.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"URL Search":{contextHub:function(){try{return e.dataLayer.events.search.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Content Type (evar48) Start":{contextHub:function(){try{return e.dataLayer.events.video.play.section}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Content Type (evar48) Stop":{contextHub:function(){try{return e.dataLayer.events.video.stop.section}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Finished (Event14)":{contextHub:function(){try{return e.dataLayer.events.video.stop}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Finished Name (eVar32)":{contextHub:function(){try{return e.dataLayer.events.video.stop.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Milestone":{contextHub:function(){try{return e.dataLayer.events.video.stop.progress}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start (Event13)":{contextHub:function(){try{return e.dataLayer.events.video.play}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start Name (eVar32)":{contextHub:function(){try{return e.dataLayer.events.video.play.name}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Start URL":{contextHub:function(){try{return e.dataLayer.events.video.play.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"},"Video Stop URL":{contextHub:function(){try{return e.dataLayer.events.video.stop.url}catch(t){L.notify(t.name+" - "+t.message,4)}},storeLength:"pageview"}},appVersion:"7QN",buildDate:"2018-07-20 10:43:07 UTC",publishDate:"2018-07-20 10:43:07 UTC"})}(window,document);
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setCookie(name, value, expires, path, domain, secure) {
		name = name.toLowerCase();
		if (!path) {
			path = '/';
		}
		if (typeof expires != 'number') {
			expires = '';
		} else {
			var expires_date = new Date((new Date()).getTime() + (expires * 24 * 60 * 60 * 1000));
			expires = ' expires=' + expires_date.toGMTString() + ';';
		}
		var d = (domain) ? ';domain=' + domain : '';
		var s = (secure) ? ";secure" : "";
		document.cookie = name + "=" + value + ";" + expires + " Path=" + path + d + s;
}

function get_domain_cookie() {
	var host = window.location.hostname;
	if(host == 'localhost') return host;
	var _domain = host.split('.');
	_domain.shift();
	if (_domain[0] == 'secure') {
		_domain.shift();
	}
	domain = '.' + _domain.join('.');
	return domain;
}