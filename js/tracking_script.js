function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function createParam(clist){
    var list = clist;
    var cookieValue, cookieName;
    var tempArray = [];
    for(var i = 0; i < list.length; i++){
        cookieValue = getCookie(list[i]);
        if(cookieValue !== ""){
            cookieName = list[i].substring(3).toUpperCase();
            tempArray.push(cookieName + "=" + encodeURIComponent(cookieValue));
        }
    }
    //return "?" + tempArray.join("&");
    return tempArray.join("&");
}

window.onload = function () {
    var cookieList = ["gw_cid", "gw_bmid", "gw_pid", "gw_sid", "gw_rckw", "gw_aid", "gw_rcmt", "gw_rckw"];
    var cidCookieValue = getCookie('gw_cid');
    var params = createParam(cookieList);
    
    if(cidCookieValue !== ""){
        var anchors = document.getElementsByTagName('a');
        for (var i = 0; i < anchors.length; i++) {
            var old = anchors[i].getAttribute('href');
			
			
            if (old !== null) {
					
				if( !old.includes( "?", 0 ) ) {
					old = old + "?" + params;
				} else {
					old = old + "&" + params;
				}
				
                var pos = old.search("www.ringcentral.co.uk");
                var res = old.charAt(0);
                if (pos !== -1) {
                    anchors[i].setAttribute('href', old);
                } else if (res.indexOf('/') === 0) {
                    var p = old.indexOf('.co.uk');
                    if (p === -1) {
                        anchors[i].setAttribute('href', old);
                    }
                }
            }
        }
    }
};