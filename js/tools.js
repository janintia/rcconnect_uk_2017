(function($){
	
// Cookies Manager
var CookiesManager = {
	setCookie: function(name, value, expires, path, domain, secure) {
		name = name.toLowerCase();
		if (!path) {
			path = '/';
		}
		if (typeof expires != 'number') {
			expires = '';
		} else {
			var expires_date = new Date((new Date()).getTime() + (expires * 24 * 60 * 60 * 1000));
			expires = ' expires=' + expires_date.toGMTString() + ';';
		}
		var d = (domain) ? ';domain=' + domain : '';
		var s = (secure) ? ";secure" : "";
		document.cookie = name + "=" + value + ";" + expires + " Path=" + path + d + s;
	},
	getCookie: function(name) {
		var c = document.cookie;
		var matches = c.match(new RegExp("(?:^|; )" + name + "=([^;]*)", "i"));
		return matches ? decodeURIComponent(matches[1]) : null;
	},
	attachCookie: function(cookieName, str) {
		var value = CookiesManager.getCookie(cookieName);
		var re = new RegExp("(&|\\\?)" + cookieName + "=");
		if (value && !str.match(re)) {
			str += '&' + cookieName + '=' + value;
		}
		return str;
	},
	delCookie: function(name, path, domain) {
		document.cookie = name + "=" +
			((path) ? ";path=" + path : "") +
			((domain) ? ";domain=" + domain : "") +
			";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	},
}

var GetParamsManager = {
	is_set: function(varName) {
		var regexS = "[?&]" + varName + "=([^&#]*)";
		var regex = new RegExp(regexS, 'i');
		var tmpURL = window.location.href;
		var results = regex.exec(tmpURL);
		if (results == null)
			return null;
		else
			return decodeURIComponent(results[1]);
	},
	get2array: function() {
		var ret = {};
		if (window.location.search) {
			var temp = window.location.search.substr(1).split('&');
			var i = temp.length;
			while (i--)
				if (temp[i]) {
					try {
						var tmp = temp[i].replace(/\+/g, "%20").split('=');
						var key = decodeURIComponent(tmp[0]).toLowerCase();
						if (typeof ret[key] == 'undefined' && typeof tmp[1] != 'undefined')
							ret[key] = decodeURIComponent(tmp[1]);
					} catch (e) {
						// incorrect parameter is not saved in cookie
					}
				}
		}
		return ret;
	}
}

//GWTS-623
;
(function() {

	// php functions

	function serialize(mixed_value) {
		//		note: We feel the main purpose of this function should be to ease the transport of data between php & js
		//		note: Aiming for PHP-compatibility, we have to translate objects to arrays
		//	example 1: serialize(['Kevin', 'van', 'Zonneveld']);
		//	returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
		//	example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});
		//	returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'

		var val, key, okey,
			ktype = '',
			vals = '',
			count = 0,
			_utf8Size = function(str) {
				var size = 0,
					i = 0,
					l = str.length,
					code = '';
				for (i = 0; i < l; i++) {
					code = str.charCodeAt(i);
					if (code < 0x0080) {
						size += 1;
					} else if (code < 0x0800) {
						size += 2;
					} else {
						size += 3;
					}
				}
				return size;
			};
		_getType = function(inp) {
			var match, key, cons, types, type = typeof inp;

			if (type === 'object' && !inp) {
				return 'null';
			}
			if (type === 'object') {
				if (!inp.constructor) {
					return 'object';
				}
				cons = inp.constructor.toString();
				match = cons.match(/(\w+)\(/);
				if (match) {
					cons = match[1].toLowerCase();
				}
				types = ['boolean', 'number', 'string', 'array'];
				for (key in types) {
					if (cons == types[key]) {
						type = types[key];
						break;
					}
				}
			}
			return type;
		};
		type = _getType(mixed_value);

		switch (type) {
			case 'function':
				val = '';
				break;
			case 'boolean':
				val = 'b:' + (mixed_value ? '1' : '0');
				break;
			case 'number':
				val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
				break;
			case 'string':
				val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
				break;
			case 'array':
			case 'object':
				val = 'a';
				/*
				 if (type === 'object') {
				 var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
				 if (objname == undefined) {
				 return;
				 }
				 objname[1] = this.serialize(objname[1]);
				 val = 'O' + objname[1].substring(1, objname[1].length - 1);
				 }
				 */

				for (key in mixed_value) {
					if (mixed_value.hasOwnProperty(key)) {
						ktype = _getType(mixed_value[key]);
						if (ktype === 'function') {
							continue;
						}

						okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
						vals += serialize(okey) + serialize(mixed_value[key]);
						count++;
					}
				}
				val += ':' + count + ':{' + vals + '}';
				break;
			case 'undefined':
				// Fall-through
			default:
				// if the JS object has a property which contains a null value, the string cannot be unserialized by PHP
				val = 'N';
				break;
		}
		if (type !== 'object' && type !== 'array') {
			val += ';';
		}
		return val;
	}

	// Parses the string into variables
	function parse_str(str) {
		if (str.indexOf('?') != -1)
			str = str.replace('?', '');
		var separator = '=',
			glue = '&',
			query_array = str.split(glue),
			result = [];

		for (var x = 0; x < query_array.length; x++) {
			var tmp = query_array[x].split(separator);
			result[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
		}

		return result;
	}


	function parse_url(str, component) {
		//	note: Does not replace invalid characters with '_' as in PHP, nor does it return false with  
		//	note: Besides function name, is essentially the same as parseUri as well as our allowing
		//	note: an extra slash after the scheme/protocol (to allow file:/// as in PHP)
		//	example 1: parse_url('http://username:password@hostname/path?arg=value#anchor');
		//	returns 1: {scheme: 'http', host: 'hostname', user: 'username', pass: 'password', path: '/path', query: 'arg=value', fragment: 'anchor'}

		var query, key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port',
			'relative', 'path', 'directory', 'file', 'query', 'fragment'
		],
			ini = (this.php_js && this.php_js.ini) || {},
			mode = (ini['phpjs.parse_url.mode'] && ini['phpjs.parse_url.mode'].local_value) || 'php',
			parser = {
				php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-scheme to catch file:/// (should restrict this)
			};

		var m = parser[mode].exec(str),
			uri = {},
			i = 14;
		while (i--) {
			if (m[i]) {
				uri[key[i]] = m[i];
			}
		}

		if (component) {
			return uri[component.replace('PHP_URL_', '').toLowerCase()];
		}
		if (mode !== 'php') {
			var name = (ini['phpjs.parse_url.queryKey'] && ini['phpjs.parse_url.queryKey'].local_value) || 'queryKey';
			parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
			uri[name] = {};
			query = uri[key[12]] || '';
			query.replace(parser, function($0, $1, $2) {
				if ($1) {
					uri[name][$1] = $2;
				}
			});
		}
		delete uri.source;

		return uri;
	}

	function getQueryParams(qs) {
		qs = qs.split("+").join(" ");
		var params = {}, tokens,
			re = /[?&]?([^=]+)=([^&]*)/g;

		while (tokens = re.exec(qs)) {
			params[decodeURIComponent(tokens[1]).toLowerCase()] = decodeURIComponent(tokens[2]);
		}

		return params;
	}

	function checkOtm() {
		var partnerId = null;
		if (document.referrer)
			partnerId = document.referrer.match(/partnerid=([^\&]+)(&|$)/i);

		if (partnerId)
			return partnerId[1];

		partnerId = getQueryParams(window.location.search).partnerid;
		if (!partnerId) {
			if (CookiesManager.getCookie('partnerid'))
				partnerId = CookiesManager.getCookie('partnerid');
			else if (CookiesManager.getCookie('gw_partnerid'))
				partnerId = CookiesManager.getCookie('gw_partnerid');
		}

		return partnerId;
	}

	function keysToLowerCase(arr) {
		if (!Object.keys(arr).length)
			return;

		var result = [];

		for (var key in arr) {
			result[key.toLowerCase()] = arr[key];
		}

		return result;
	}

	function setOtm() {
		var partnerId = checkOtm(),
			referer = document.referrer,
			pid = '',
			bmid = '',
			sid = '',
			$_ref = '',
			resultCookie = {},
			get = keysToLowerCase(parse_str(window.location.search));

		if (!partnerId)
			return false;

		referer = parse_url(referer);

		if (!referer['query'])
			referer['query'] = '';
		$_ref = keysToLowerCase(parse_str(referer['query']));

		if ($_ref['pid'])
			pid = $_ref['pid'];
		if ($_ref['bmid'])
			bmid = $_ref['bmid'];
		//GW-16685
		if ($_ref['tduid'])
			sid = $_ref['tduid'];
		else if (CookiesManager.getCookie('gw_tduid'))
			sid = CookiesManager.getCookie('gw_tduid');
		else if ($_ref['sid'])
			sid = $_ref['sid'];

		if (!pid) {
			if (get['pid'])
				pid = get['pid'];
			else if (CookiesManager.getCookie('gw_pid'))
				pid = CookiesManager.getCookie('gw_pid');
		}

		if (!bmid) {
			if (get['bmid'])
				bmid = get['bmid'];
			else if (CookiesManager.getCookie('gw_bmid'))
				bmid = CookiesManager.getCookie('gw_bmid');
		}

		if (get['sid'])
			sid = get['sid'];

		resultCookie = {
			'partnerid': partnerId,
			'bmid': bmid,
			'pid': pid,
			'sid': sid
		};

		var domain = get_domain_cookie();

		CookiesManager.setCookie('gw_otm', encodeURIComponent(serialize(resultCookie)), '', "/", domain);
	}

	setOtm();

})();

function $_(id) {
	return document.getElementById(id);
}

function hash_to_string(hash)
{
	arr = [];
	for (key in hash) {
		try {
			arr.push(key + ':' + hash[key]);
		} catch (e) {
		}
	}
	return arr.join(',');
}



Function.prototype.rcBind = function(obj)
{
	var m = this;
	var a = arguments.length > 1 ? arguments[1] : {};
	return function(e)
	{
		if (arguments.length)
		{
			if (arguments[0].srcElement || arguments[0].target)
			{
				a['oEvent'] = arguments[0];
			}
			else
			{
				for (var key in arguments[0])
				{
					a[key] = arguments[0][key];
				}
			}

		}
		return m.call(obj, a);
	}
}

Function.prototype.rcBindAsIs = function(obj)
{
	var method = this;
	var arg = [];
	for (var i = 1; i < arguments.length; i++)
	{
		arg.push(arguments[i]);
	}
	return function()
	{
		if (arguments.length)
		{
			for (var i = 0; i < arguments.length; i++)
			{
				arg.push(arguments[i]);
			}
		}
		var res = null;
		//try
		//{
		res = method.apply(obj, arg);
		//}
		//catch(e){}
		if (arguments.length)
		{
			for (var i = 0; i < arguments.length; i++)
			{
				arg.pop();
			}
		}
		return res;
	}
}


function addEvent(obj, ev, func, useCapture)
{
	useCapture = !useCapture ? false : true;

	//alert(useCapture);

	if (ev == 'ondomready')
	{
		onDomReady(func);
		return;
	}
	if (ev != 'onunload') {
		window.addEvent(window, 'onunload', function() {
			window.removeEvent(obj, ev, func);
			func = null;
		})
	}

	if (window.addEventListener) {
		obj.addEventListener(ev.replace(/^on/i, ''), func, useCapture);
	} else if (window.attachEvent) {
		obj.attachEvent(ev, func);
	}
}

function onDomReady(callback)
{
	var _timer;


	var onDomReadyCallBack = function()
	{
		if (arguments.callee.done)
			return;

		arguments.callee.done = true;
		if (_timer)
		{
			clearInterval(_timer);
			_timer = null;
		}


		callback();

	};



	if (document.addEventListener)
	{
		document.addEventListener("DOMContentLoaded", onDomReadyCallBack, false);
	}


	if (document.attachEvent && document.documentElement.doScroll && window == window.top)
	{
		(
			function()
			{
				try
				{
					document.documentElement.doScroll("left");
				}
				catch (error)
				{
					setTimeout(arguments.callee, 0);
					return;
				}
				onDomReadyCallBack();
			}
		)();
	}




	if (/WebKit/i.test(navigator.userAgent))
	{
		_timer = setInterval(function()
		{
			if (/loaded|complete/.test(document.readyState))
			{
				onDomReadyCallBack();
			}
		}, 10);
	}

	addEvent(window, 'onload', onDomReadyCallBack);

}



function removeEvent(obj, ev, func) {
	if (window.addEventListener) {
		obj.removeEventListener(ev.replace(/^on/i, ''), func, false);
	} else if (window.detachEvent) {
		obj.detachEvent(ev, func);
	}
}
function in_array(needle, haystack, strict) {
	var found = false, key, strict = !!strict;

	for (key in haystack) {
		if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
			found = true;
			break;
		}
	}

	return found;
}
function count(mixed_var, mode) {
	var key, cnt = 0;

	if (mode == 'COUNT_RECURSIVE')
		mode = 1;
	if (mode != 1)
		mode = 0;

	for (key in mixed_var) {
		cnt++;
		if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
			cnt += count(mixed_var[key], 1);
		}
	}

	return cnt;
}
function is_string(mixed_var) {
	return (typeof (mixed_var) == 'string');
}
function is_numeric(mixed_var) {
	return !isNaN(mixed_var);
}

function is_dom(obj) {
	return obj && obj.style;
}

function is_oObj(obj) {
	return obj && obj.getView && obj.getView();
}
is_vObj = is_oObj;

function is_array(mixed_var) {
	return (mixed_var instanceof Array);
}


function is_object(mixed_var) {
	if (mixed_var instanceof Array) {
		return false;
	} else {
		return (mixed_var !== null) && (typeof (mixed_var) == 'object');
	}
}

function empty(mixed_var) {
	var is_object_empty = false;
	if (is_object(mixed_var)) {
		for (key in mixed_var) {
			return is_object_empty;
		}
		is_object_empty = true;
	}
	return (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || (is_array(
		mixed_var) && mixed_var.length === 0) || is_object_empty);
}

function is_obj(obj) {
	return obj && typeof (obj);
}

function strToJson(str)
{
	if (typeof str != 'string')
	{
		str = '';
	}

	var res = {};

	try
	{
		res = (new Function('return ' + str.replace(/^[^{]*|[^}]$/, '')))();
	}
	catch (e) {
	}
	;

	return res;
}


function getChilds(obj, nodeName, attr, style, arr) {
	if (!arr) {
		arr = Array();
	}
	if (obj.hasChildNodes()) {
		for (var i = 0; i < obj.childNodes.length; i++) {
			if (obj.childNodes[i].nodeName == '#text') {
				continue;
			}
			if (!nodeName || obj.childNodes[i].nodeName == nodeName) {
				if (!attr) {
					if (!style) {
						arr.push(obj.childNodes[i]);
					} else {
						if (obj.childNodes[i].style) {
							var flg = false;
							for (key in style) {
								var re = new RegExp(style[key])
								if (obj.childNodes[i].style[key].match(re)) {
									flg = true;
								} else {
									flg = false;
									break;
								}
							}
							if (flg) {
								arr.push(obj.childNodes[i]);
							}
						}
					}
				} else {
					var flg = false;
					for (key in attr) {
						if (obj.childNodes[i].getAttribute(key) == attr[key]) {
							flg = true;
						} else {
							flg = false;
							break;
						}
					}
					if (flg) {
						if (!style) {
							arr.push(obj.childNodes[i]);
						} else {
							if (obj.childNodes[i].style) {
								var flg = false;
								for (key in style) {
									if (obj.childNodes[i].style[key] == style[key]) {
										flg = true;
									} else {
										flg = false;
										break;
									}
								}
								if (flg) {
									arr.push(obj.childNodes[i]);
								}
							}
						}
					}
				}
			}
			getChilds(obj.childNodes[i], nodeName, attr, style, arr);
		}
	}
	return arr;
}
function errorMsg(method, msg) {
	alert("Error:\r\r\n\n    " + method + "                        \r\n    " + msg);
}
function noticeMsg(method, msg) {
	alert('Notice!!!:\r\r    ' + method + '                        \r    ' + msg + '!');
}

function returnFalse(event) {
	event.cancelBubble = true;
	return false;
}

function getNextBrother(obj) {
	if (!is_dom(obj)) {
		errorMsg('tools.js -> Window :: getNextBrother', 'Object in not DOM element');
		return;
	}
}




function parseXML(xml) {
	var arr = Array();
	if (!xml || !xml.firstChild) {
		return '';
	}

	var root = xml.nodeName == '#document' ? xml.firstChild : xml;
	if (root.nodeName == '#text') {
		root = root.nextSibling;
	}
	if (root.nodeName == 'xml') {
		root = root.nextSibling;
	}
	if (root) {
		for (var i = 0; i < root.childNodes.length; i++) {
			var item = root.childNodes[i];
			var nodeName = item.nodeName;
			var attributes = getAttributes(item);
			if (nodeName != '#text') {
				if (nodeName != "#comment") {
					switch (nodeName) {
						case 'length':
						case 'push':
							nodeName = '_' + nodeName;
							break;
					}
					var val = parseXML(item);
					if (!arr[nodeName]) {
						if (attributes) {
							var temp = Array();
							temp['attrs'] = attributes;
							temp['textContent'] = val;
							arr[nodeName] = temp;
						} else {
							arr[nodeName] = val;
						}
					} else {
						if (!is_array(arr[nodeName])) {
							var temp = arr[nodeName];
							arr[nodeName] = Array(temp);
						}
						if (arr[nodeName].length == 0) {
							var temp = arr[nodeName];
							arr[nodeName] = Array(temp);
						}
						if (attributes) {
							var temp = Array();
							temp['attrs'] = attributes;
							temp['textContent'] = val;
							arr[nodeName].push(temp);
						} else {
							arr[nodeName].push(val);
						}
					}
				}
			}
		}
		var flg = false;
		for (key in arr) {
			flg = true;
			break;
		}
		return flg ? arr : (root.text || root.textContent);
	}
	return null;
}

function getAttributes(node) {
	var arr = Array();
	if (node && node.attributes && node.attributes.length > 0) {
		for (var i = 0; i < node.attributes.length; i++) {
			var attr = node.attributes[i];
			arr[attr.name] = attr.value;
		}
		return arr;
	}
	return null;
}

function formatPhoneNumber(number) {
	return number.substring(0, 1) + ' (' + number.substring(1, 4) + ') ' + number.substring(4,
		7) + '-' + number.substring(7, 11);
}

function templater(str, hash)
{

	var leftQ = '<%';
	var rightQ = '%>';
	var re1 = new RegExp("((^|" + rightQ + ")[^\t]*)'", 'g');
	var re2 = new RegExp("\t=(.*?)\\" + rightQ, 'g');
	var my_str = "var a=[];with(hash){a.push('" + str
		.replace(/[\t\r\n]/g, " ")
		.split(leftQ)
		.join("\t")
		.replace(re1, "$1\r")
		.replace(re2, "',$1,'")
		.split("\t")
		.join("');")
		.split(rightQ)
		.join("a.push('")
		.split("\r")
		.join("\\'") + "');} return a.join('');";

	my_str = 'try{' + my_str + '}catch(e){throw e;}';

	return new Function("hash", my_str)(hash);
}

function getCssClassNamevalue(o, className)
{
	var sClasses = o.className.replace(/\n|\r|\t/g, ' ');
	var aClasses = sClasses.split(' ');


	var re = new RegExp('^' + className + '(\-|)([^\-]+|)', '');
	for (var i = 0; i < aClasses.length; i++)
	{
		var class_ = aClasses[i];
		if (res = class_.match(re))
		{
			return res[2];
		}
	}
	return null;
}
getCssClassNameValue = getCssClassNamevalue

function setCssClassName(o, className, value)
{

	if (getCssClassNameValue(o, className) == value)
	{
		return;
	}

	if (typeof value == 'undefined')
	{
		o.className = className;
	}
	else
	{
		var sClasses = o.className.replace(/\n|\r|\t/g, ' ');
		var aClasses = sClasses.split(' ');
		//var re       = new RegExp('^' + className + '\-[^\-]+','') ;
		var re = new RegExp('^' + className + '\-.+', '');
		for (var i = 0; i < aClasses.length; i++)
		{
			var class_ = aClasses[i];
			if (class_.match(re))
			{
				aClasses.splice(i, 1);
			}
		}

		if (value == '' && value != 0)
		{
			aClasses.push(className);
		}
		else
		{


			aClasses.push(className + '-' + value);
		}
		o.className = aClasses.join(' ');
		if (o.firstChild && o.firstChild.tagName == 'B')
		{
			o.firstChild.innerHTML = o.className;
		}
	}
	//alert(o.className)
}

function addClassName(o, className)
{
	var aClasses = o.className.replace(/\n|\r|\t/g, ' ').split(' ');
	for (var i = 0; i < aClasses.length; i++)
	{
		if (aClasses[i] == className)
		{
			return;
		}
	}
	o.className += ' ' + className;
}



function getElementsByClassName(classNameVal, parent, tag)
{
	var res = [];
	parent = parent || document;
	var nodes = parent.getElementsByTagName(tag || '*');
	var l = nodes.length;
	if (l)
	{
		var re = new RegExp("(^|\\s)" + classNameVal + "(\\s|$)");

		for (var i = 0; i < l; i++)
			if (re.test(nodes[i].className))
				res.push(nodes[i]);
	}
	return res;
}

function each(obj, callBack, space)
{
	if (!obj)
	{
		throw new Error('Each\nObject is undefined');
	}
	if (typeof callBack != 'function')
	{
		throw new Error('Each\nCallBack undefined or not a function');
	}

	space = space || obj;

	if (obj.length)
	{
		for (var i = 0, l = obj.length; i < l; i++) {
			if (callBack.call(space, obj[i], parseInt(i), obj) === false)
				return false;
		}
	}
	else
	{
		for (var key in obj) {
			if (obj.hasOwnProperty && obj.hasOwnProperty(key)) {
				if (callBack.call(space, obj[key], key, obj) === false)
					return false;
			}
		}
	}
	return true;
}

function getCurrentStyle(o, name)
{
	if (window.getComputedStyle)
	{
		return window.getComputedStyle(o, null)[name];
	} else if (o.currentStyle)
	{
		return o.currentStyle[name];
	}
	return  null;
}

function each_r(obj, callBack, space)
{
	if (!obj)
	{
		throw new Error('Each\nObject is undefined');
	}
	if (typeof callBack != 'function')
	{
		throw new Error('Each\nCallBack undefined or not a function');
	}

	space = space || obj;

	var res = [];

	if (obj.length)
	{
		for (var i = 0, l = obj.length; i < l; i++) {
			var ret_val = callBack.call(space, obj[i], i, obj);
			if (ret_val)
			{
				res.push(ret_val);
			}
		}
	}
	else
	{
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				var ret_val = callBack.call(space, obj[key], key, obj);
				if (ret_val)
				{
					res.push(ret_val);
				}
			}
		}
	}
	return res.length ? res : null;
}

function getCssPropValueBySelectorAndProp(selector, prop)
{
	var css = window.document.styleSheets;
	if (css.length)
	{
		var res =
			each_r(css,
				(
					function(selector, prop, cssGroup)
					{
						try
						{
							var fullRules = cssGroup.cssRules || cssGroup.rules
							if (fullRules)
							{
								var res =
									each_r(fullRules,
										(
											function(selector, prop, rule)
											{
												if (rule.selectorText == selector)
												{
													return rule.style[prop];
												}
												return null;
											}
										).rcBindAsIs(this, selector, prop)
										);
								return res ? res : null;

							}
						} catch (e) {
						}
					}
				).rcBindAsIs(this, selector, prop)
				);
		return res ? res[0] : null;
	}
}


function opacity(id, opacStart, opacEnd, millisec) {

	var speed = Math.round(millisec / 100);
	var timer = 0;


	if (opacStart > opacEnd) {
		for (i = opacStart; i >= opacEnd; i--) {
			setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
			timer++;
		}
	} else if (opacStart < opacEnd) {
		for (i = opacStart; i <= opacEnd; i++)
		{
			setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
			timer++;
		}
	}
}


function changeOpac(opacity, id) {
	var object = document.getElementById(id).style;
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
	object.filter = "alpha(opacity=" + opacity + ")";
}

function getParentByTagsName(arr, node)
{
	while (inArray(arr, node.nodeName) == -1)
	{
		if (node.nodeName == 'BODY')
		{
			return null;
		}
		node = node.parentNode;
	}
	return node;

}



function fireEvent(el, ev)
{
	if (document.createEventObject)
	{
		var E = document.createEventObject();
		el.fireEvent(ev, E);
		return true;
	}
	else if (document.createEvent)
	{
		var E = document.createEvent("MouseEvents");
		E.initEvent(ev.replace(/^on/, ''), true, false);
		el.dispatchEvent(E);

		var E = document.createEvent("HTMLEvents");
		E.initEvent(ev.replace(/^on/, ''), true, false);
		el.dispatchEvent(E);

		return true;
	}
	else
	{
		return false;
	}
}

function rand(min, max) {
	var argc = arguments.length;
	if (argc == 0) {
		min = 0;
		max = 2147483647;
	} else if (argc == 1) {
		throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
	}
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


function concatHash()
{
	var r = {};
	for (var i = 0; i < arguments.length; i++)
	{
		var parent = arguments[i];
		for (var key in parent)
		{
			r[key] = parent[key];
		}
	}
	return r;
}


function inArray(arr, val) {
	if (arr) {
		for (var i = 0, l = arr.length; i < l; i++) {
			if (arr[i] == val) {
				return i;
			}
		}
	}
	return -1;
}

/* 
 *	Check whether page has been opened from some search engine and
 *	in case of true result set according cookies of gw_pid and gw_engine_referrer
 */
if (typeof brand_prefix == "undefined") {
	var brand_prefix = "RC";
	var tmp_results1 = /m\.ringcentral\.com/.exec(location.host);
	var tmp_results2 = /\.m\.ringcentral\.com/.exec(location.host);
	var tmp_results3 = /\/gwmobile\//.exec(location.href);
	if (tmp_results1 != null || tmp_results2 != null || tmp_results3 != null) { /* GW-6841 */
		brand_prefix = "MOB";
	} else {
		var domain = get_domain_cookie();
		switch (domain) {
			case '.ringcentral.com':
				brand_prefix = 'RC';
				break;
			case '.ringcentral.ca':
				brand_prefix = 'CA';
				break;
			case '.ringcentral.co.uk':
				brand_prefix = 'UK';
				break;
			case '.extremefax.com':
				brand_prefix = 'XF';
				break;
		}
	}
}


function get_domain_cookie() {
	var host = window.location.hostname;
	if(host == 'localhost') return host;
	var _domain = host.split('.');
	_domain.shift();
	if (_domain[0] == 'secure') {
		_domain.shift();
	}
	domain = '.' + _domain.join('.');
	return domain;
}
var cookieSetHost = get_domain_cookie();

/**********start insert*/
/*block for set 'pid', 'bmid', 'rckw', 'rcmt', 'ad',  'kid', 'aid' 'kcid' vars*/
function ParamToCookieSet() {
	var domain = get_domain_cookie();//'.' + window.location.hostname.match(/[^.]+\.(co\.uk|[^.]+)$/)[0];
//    var arr_param = ['pid', 'bmid', 'rckw', 'rcmt', 'ad',  'kid', 'aid'];
	var arr_param_del = ['pid', 'bmid', 'rckw', 'rcmt', 'ad', 'kid', 'aid', 'tduid'];
	var arr_param = ['rckw', 'rcmt', 'ad', 'kid', 'kcid'];
	var arr_param_session = ['pid', 'bmid', 'aid', 'spid', 'tduid'];

	var pid = GetParamsManager.is_set('pid');
	var bmid = GetParamsManager.is_set('bmid');
	if ( !(bmid || /(^|; )gw_bmid=(.*?)(;|$)/i.exec(document.cookie)) ) {	
		['.ringcentral.com/aff/office/small-business-phone-system_c.html',
		'.ringcentral.ca/aff/office/small-business-phone-system.html',
		'.ringcentral.com/aff/fax25p_6.html',
		'.ringcentral.com/aff/contactcenter'].forEach( function( page ){
			if ( document.location.href.indexOf(page) !== -1 ) {
				bmid = 'AFF_GEN';
				CookiesManager.setCookie('gw_bmid', bmid, '', '/', domain);
			}
		});

	}

	var afn = GetParamsManager.is_set('afn');
//    var lcid = GetParamsManager.is_set('lcid');

	var tduid = GetParamsManager.is_set('tduid');
	var cjevent = GetParamsManager.is_set('cjevent');

	if (cjevent) {
		CookiesManager.setCookie('gw_cjevent', cjevent, 120, '/', domain);
	}

	if (tduid) {
		CookiesManager.setCookie('gw_tduid', tduid, '', '/', domain);
	}

	var partnerid = GetParamsManager.is_set('partnerid');
	if (partnerid) {
		CookiesManager.setCookie('gw_partnerid', partnerid, '', '/', domain);
	}
	var safid = GetParamsManager.is_set('safid');
	if (safid) {
		CookiesManager.setCookie('gw_safid', safid, '', '/', domain);
	}
	var kcid = GetParamsManager.is_set('kcid');
	if (kcid) {
		CookiesManager.setCookie('gw_kcid', kcid, '', '/', domain);
	}

	var rckw = GetParamsManager.is_set('rckw');
	if (rckw) {
		CookiesManager.setCookie('gw_rckw', rckw, '', '/', domain);
	}


	function getBmidFromReferrer(hostname) {
		var patterns = {
			GGLPLUS: ['(^|\\.)plus\\.google\\.'],
			FSRC_GOOGLE: ['(^|\\.)google\\.'],
			FSRC_BING: ['(^|\\.)bing\\.'],
			FSRC_YAHOO: ['(^|\\.)yahoo\\.'],
			FSRC_OTHERS: ['(^|\\.)aol\\.',
				'(^|\\.)yandex\\.',
				'(^|\\.)baidu\\.',
				'(^|\\.)duckduckgo\\.',
				'(^|\\.)dogpile\\.',
				'(^|\\.)ask\\.',
				'(^|\\.)myway\\.',
				'(^|\\.)msn\\.',
				'(^|\\.)naver\\.',
				'(^|\\.)sogou\\.'
			],
			FB_L: ['(^|\\.)facebook\\.'],
			LI_L: ['(^|\\.)linkedin\\.',
				'^lnkd\\.in'
			],
			T_L: ['(^|\\.)twitter\\.',
				'^t\\.co'
			],
			YT_L: ['(^|\\.)youtube\\.'],
			RCUS_OTHERREF: ['(^|\\.)pinteres\\.',
				'(^|\\.)reddit\\.',
				'(^|\\.)instagramm\\.',
				'(^|\\.)slideshare\\.',
				'(^|\\.)yelp\\.',
				'(^|\\.)on24\\.',
			],
			GGUSCONCOMPET: ['(^|\\.)doubleclick.net$',
				'(^|\\.)googlesyndication.com$'
			],
			RCUS_INTDOMAINS: [
				'(^|\\.)glip.com$',
				'(^|\\.)ringcentral\\.',
				'(^|\\.)extremefax.com$',
				'(^|\\.)salesforce.com$',
				'(^|\\.)ringdemo\\.',
				'(^|\\.)liveperson.net$',
				'(^|\\.)serverdata.net$',
			],
			RCUS_INPRODUCT: ['(^|\\.)mindbodyonline.com$',
				'(^|\\.)e-access.att.com$',
				'(^|\\.)mybodhi.com$',
				'(^|\\.)showroomtransport.com$',
				'(^|\\.)amridgeuniversity.blackboard.com$',
				'(^|\\.)holidiumlabs.com$',
				'(^|\\.)yammer.com$',
			],
			AFF_GEN: ['(^|\\.)shawngraham.me$',
				'(^|\\.)fitsmallbusiness.com$',
				'(^|\\.)fabsuite.com$',
				'(^|\\.)komando.com$',
				'(^|\\.)saas-reviews.com$',
				'(^|\\.)smallbiztrends.com$',
				'(^|\\.)appstechnews.com$',
				'(^|\\.)gmailfax.com$',
				'(^|\\.)howtogeek.com$',
				'(^|\\.)businessnewsdaily.com$',
				'(^|\\.)getapp.com$',
			],
		};

		var ret = '';
		if (typeof hostname === 'undefined') {
			if (!window.document.referrer)
				return ret;
			var parser = document.createElement('a');
			parser.href = window.document.referrer.toLowerCase();
			hostname = parser.hostname != window.location.hostname ? parser.hostname : '';
		}
		if (!hostname)
			return ret;

		Object.keys(patterns).forEach(function(key) {
			if (!ret) {
				patterns[key].forEach(function(reg) {
					if (!ret && hostname.match(reg)) {
						ret = key;
					}
				});
			}
			;
		});

		if (!ret)
			ret = 'RCUS_OTHERREF';
		return ret;
	}



	function DelPreviousParam() {
		for (var i = 0; i < arr_param_del.length; i++) {
			CookiesManager.delCookie('gw_' + arr_param_del[i], '/', domain);
		}
	}

	function SetParam() {/* set param 'rckw', 'rcmt', 'ad',  'kid', 'aid', 'kcid' */
		var param = GetParamsManager.get2array();
		for (var i = 1; i < arr_param.length; i++) {
			if (typeof param[arr_param[i]] != 'undefined' && param[arr_param[i]].toString().length > 0) {
				var name = 'gw_' + arr_param[i];
				CookiesManager.setCookie(name, param[arr_param[i]], 30, '/', domain);
			}
		}
		for (var i = 1; i < arr_param_session.length; i++) {
			if (typeof param[arr_param_session[i]] != 'undefined' && param[arr_param_session[i]].toString().length > 0) {
				var name = 'gw_' + arr_param_session[i];
				CookiesManager.setCookie(name, param[arr_param_session[i]], '', '/', domain);
			}
		}
	}

	function BeginSetParam() {
		DelPreviousParam();
		SetParam();
		if (afn && !(!!bmid)) {
			CookiesManager.setCookie('gw_bmid', escape(afn), '', '/', domain);
			if (!!pid) {
				CookiesManager.setCookie('gw_pid', escape(pid), '', '/', domain);
			}
		} else if (pid && !(!!bmid)) {
			CookiesManager.setCookie('gw_bmid', escape(pid), '', '/', domain);
		} else {
			CookiesManager.setCookie('gw_bmid', escape(bmid), '', '/', domain);
			if (!!pid) {
				CookiesManager.setCookie('gw_pid', escape(pid), '', '/', domain);
			}
		}
	}

	var rr_host, rr_query;
	if (!(!!pid || !!bmid)) { /*set pid from referrer*/
		var rr = GetParamsManager.is_set('gw_referrer');
		if (rr == 'null')
			rr = '';
		if (!rr)
			rr = window.document.referrer;
		if (!!rr) {
			var rr_host = '';
			try {
				rr_host = rr.match(/(http|https):\/\/(?:www\.)?([^\/]+)/)[2];
			} catch (e) {
				rr_host = decodeURIComponent(rr).match(/(http|https):\/\/(?:www\.)?([^\/]+)/)[2];
			}
			var rr_query = rr.split("?")[1] || "";
			/*  hook search */
			var re = /results\/Web\/([^\/]+)\//i;
			var found = rr.match(re);
			if (found != null) {
				rr_query += '&hook_query=' + found[1];
			}
//            pid = get_GW_PID(rr_host, rr_query);
			var referer = GetParamsManager.is_set('gw_referrer');
			if (
				null == referer
				|| 'null' == referer
			) {
				referer = undefined;
			}
			pid = getBmidFromReferrer(referer);
			pid = pid == "" ? null : pid;
		}
	}

	if (!!pid || !!bmid || !!afn) {
		BeginSetParam();/*chahge Param*/
//        var gw_rckw = get_GW_RCKW(rr_host, rr_query);
//		var gw_rckw = '';
//		if (gw_rckw != "") {
//			CookiesManager.setCookie("gw_rckw", escape(gw_rckw), '', '/', domain);
//		}
	}

	if (bmid == 'CJZLKR0808_01') {
		(function() {
			var matches = [];
			if (matches = /(\.[^\.]+\.(com|ca|co\.uk|ru|net))$/.exec(window.location.hostname)) {
				//e.g. .ringcentral.com, .ringcentral.co.uk
				CookiesManager.setCookie('trial30', 'true', '', '/', matches[0]);
			} else {
				CookiesManager.setCookie('trial30', 'true');
			}
		})();
	}
	//Set cookie CID
	bmid = CookiesManager.getCookie('gw_bmid');
	// GW-7906 GW-8690

	var setcid = false;

	if (GetParamsManager.is_set('cid')) {
		var tmp_array_key = ['PARTNER', 'RETARGET', 'REFERRAL', 'MEDIA', 'RESELLER', 'OTHER', 'SERVPROV', 'SOCIAL', 'LEADGEN', 'AFF', 'SEM'];
		var tmp_value_key = GetParamsManager.is_set('cid').toUpperCase();
		for (var p in tmp_array_key) {
			if (tmp_value_key == tmp_array_key[p]) {
				CookiesManager.setCookie('gw_cid', tmp_array_key[p], 30, '/', domain);
				setcid = true;
			}
		}
	}

	if (!setcid && !CookiesManager.getCookie('gw_cid')) {
		if (!CookiesManager.getCookie('gw_bmid') && !CookiesManager.getCookie('gw_partnerid')) {
			CookiesManager.setCookie('gw_cid', 'DIRECT', 30, '/', domain);
		} else if (GetParamsManager.is_set('rckw') && GetParamsManager.is_set('rcmt')) {
			CookiesManager.setCookie('gw_cid', 'SEM', 30, '/', domain);
		} else if (bmid && (bmid.substring(0, 2)).toUpperCase() == 'FS') {
//GW-17470
			CookiesManager.setCookie('gw_cid', 'SEO', 30, '/', domain);
		} else if (typeof (GetParamsManager.is_set('afn')) == 'string') {
			CookiesManager.setCookie('gw_cid', 'AFF', 30, '/', domain);
		}
	}


	/* block for set 'adgrpid', 'oid', 'product', 'afn', 'sid', 'office_phone', 'oppid' 'kcid' 'rckw' vars*/

	var domain = get_domain_cookie();//'.' + window.location.hostname.match(/[^.]+\.(co\.uk|[^.]+)$/)[0];
	var param = GetParamsManager.get2array();
	var get_param = ['adgrpid', 'oid', 'product', 'afn', 'sid', 'office_phone', 'oppid', 'kcid', 'rckw'];

	for (var p in get_param) {
		if (typeof param[get_param[p]] != 'undefined') {
			var name = get_param[p];
			if (name == 'adgrpid'
				|| name == 'afn'
				|| name == 'sid'
				|| name == 'kcid'
				|| name == 'rckw'
				|| name == 'oppid')
			{
				name = 'gw_' + name;
			}

			if (name == 'gw_oppid' && /https/i.test(window.location.protocol)) {
				CookiesManager.setCookie(name, param[get_param[p]], '', '/', domain, true);
			} else {
				CookiesManager.setCookie(name, param[get_param[p]], '', '/', domain);
			}

			if (name == 'product') {
				name = name + '_tags';
				var cValue = (param[get_param[p]].toLowerCase() == 'mobile') ? 'online' : param[get_param[p]];
				switch (cValue) {
					case 'office':
					case 'online':
					case 'fax':
						CookiesManager.setCookie(name, cValue, '', '/', domain);
						break;
				}
			}
		}
	}
	//GW-6315
	if (typeof param['siteid'] != 'undefined') {
		CookiesManager.setCookie('gw_sid', escape(param['siteid']), '', '/', domain);
	}
	//GW-5054
	if (typeof param['spid'] != 'undefined') {
		CookiesManager.setCookie('gw_spid', escape(param['spid']), '', '/', domain);
	}


}
;

ParamToCookieSet();

!function() {
	var params = [];

	$.each({
		'gw_bmid': 'BMID',
		'gw_kcid': 'kcid',
		'gw_pid': 'PID',
		'gw_aid': 'AID',
		'gw_rckw': 'RCKW'
	}, function(key, val) {
		var v = CookiesManager.getCookie(key);
		if (v)
			params.push(val + '=' + v);
	});

	if (params.length > 0) {
		var reg = /^(http:\/\/|https:\/\/|\/\/|)(marketo|go).ringcentral.com([^\?]+)(\??.*)$/;
		$(function() {
			$("a").attr('href', function(i, h) {
				if (h) {
					var t = h.match(reg);
					if (t) {
						if (t[4])
							if (t[4].length == 1)
								h += params.join('&');
							else
								h += '&' + params.join('&');
						else
							h += '?' + params.join('&');
						return h;
					}
				}
			})
		});
	}
}();

/**********end insert*/
var nead_cookie_afn = ['/cj/toll-free-numbers.asp', '/cj/toll-free-numbersb.asp', '/cj/toll-free-numbersb_sp.asp',
	'/cj/toll-free-numbers10p.html', '/cj/toll-free-numbers30b.asp', '/cj/toll-free-numbersZLKR30.asp',
	'/cj/virtual-phone-system.html', '/cj/virtual-phone-systemsf30.html',
	'/cj/virtual-pbx-new.asp', '/cj/faxandphone.html', '/cj/voicemail.asp', '/cj/vanitynumber.asp',
	'/cj/call-forwarding.asp', '/cj/digitalline-voip.html', '/cj/local-numbers/area-codes.html',
	'/cj/fax.asp', '/cj/fax30.asp', '/cj/fax.asp', '/cj/office-everywhere-phone-fax.asp'];



var ref_full = window.location.pathname;// + window.location.search;
if (inArray(nead_cookie_afn, ref_full) != -1) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'cj', '', '/', cookieSetHost);
}

if (ref_full.match(/^\/aff\//)) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'aff', '', '/', cookieSetHost);
}

if (ref_full.match(/^\/az\//)) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', 'az', '', '/', cookieSetHost);
}

if (CookiesManager.getCookie('gw_afn')) {
	CookiesManager.setCookie('gw_afn_to_offer_tests', CookiesManager.getCookie('gw_afn'), '', '/', cookieSetHost);
	var hasAfnCookie = true;
}

var p = GetParamsManager.is_set('p');
if (p) {
	CookiesManager.setCookie('p', p);
}

var o = GetParamsManager.is_set('o');
if (o) {
	CookiesManager.setCookie('o', o, 1 / 64);
}

if (CookiesManager.getCookie('afn')) {
	var hasAfnCookie = true;
}


(function() {
	/* begin GW-536 */
	function getProductTag(url) {
		var _productTags = {
			'/fax/default.html': 'fax',
			'/fax/emailfax.html': 'fax',
			'/fax/default.asp': 'fax',
			'/fax/emailfax.asp': 'fax',
			'/office/business-phone-service.html': 'office',
			'/office/business-voip-phone.html': 'office',
			'/office/index.html': 'office',
			'/office/internet-business-phone-system.html': 'office',
			'/office/online-business-phone-system.html': 'office',
			'/office/phone-system.html': 'office',
			'/office/small-business-phone-system.html': 'office',
			'/office/virtual-business-phone-service.html': 'office',
			'/office/virtual-business-phone-system.html': 'office',
			'/office/virtual-pbx-phone-system.html': 'office',
			'/business-toll-free.dhtml': 'online',
			'/features/real-time-control/overview.html': 'online',
			'/lp/800-business-number.html': 'online',
			'/lp/800-business-number.asp': 'online',
			'/lp/800numbers.html': 'online',
			'/lp/800numbers.asp': 'online',
			'/lp/800numbers-yh-a.asp': 'online',
			'/lp/800service.html': 'online',
			'/lp/800service.asp': 'online',
			'/lp/866-business-number.html': 'online',
			'/lp/866-business-number.asp': 'online',
			'/lp/877-business-number.html': 'online',
			'/lp/877-business-number.asp': 'online',
			'/lp/auto-attendant.asp': 'online',
			'/lp/call-forwarding.html': 'online',
			'/lp/call-forwarding.asp': 'online',
			'/lp/callscreening.asp': 'online',
			'/lp/call-screening.html': 'online',
			'/lp/call-screening.asp': 'online',
			'/lp/digitalline-voip.html': 'online',
			'/lp/digitalline-voip-home.html': 'online',
			'/lp/local-numbers.html': 'online',
			'/lp/local-numbers.asp': 'online',
			'/lp/small-business-phone.html': 'online',
			'/lp/toll-free-numbers.html': 'online',
			'/lp/toll-free-numbers.asp': 'online',
			'/lp/toll-free-numbers.asp'                  : 'online',
				'/lp/toll-free-numbers-yh-a.asp': 'online',
			'/lp/unified-communications.asp': 'online',
			'/lp/vanitynumber.html': 'online',
			'/lp/vanitynumber.asp': 'online',
			'/lp/virtual-pbx.asp': 'online',
			'/lp/virtualphonenumber.asp': 'online',
			'/lp/virtual-phone-service.html': 'online',
			'/lp/virtual-phone-system.html': 'online',
			'/lp/voicemail.asp': 'online',
			'/lp/auto-attendant.html': 'online',
			'/lp/unified-communications.html': 'online',
			'/lp/virtualphonenumber.html': 'online',
			'/lp/virtual-pbx.html': 'online',
			'/lp/voicemail.html': 'online',
			'/lp/toll-free-numbers-yh-a.asp'  :  'online'


		};

		var _regex = new RegExp(
			'^\/features\/local-numbers\/area-code-(201|202|203|205|206|208|209|210|212|213|214|216|217|218|219|224|225|228|229|231|234|239|240|248|251|252|253|254|256|260|262|267|269|270|276|281|301|302|303|304|305|307|309|310|312|313|314|315|316|317|318|321|323|325|330|334|336|337|339|347|352|360|361|386|401|402|404|405|406|407|408|409|410|412|413|414|415|417|419|423|424|425|432|434|435|440|443|469|478|479|480|484|501|502|503|504|505|507|508|509|510|512|513|516|517|518|520|530|540|541|559|561|562|567|570|571|573|575|580|585|586|601|603|605|606|607|608|609|610|612|614|615|616|617|618|619|620|623|626|630|631|636|646|650|651|661|662|678|682|701|702|703|704|706|707|708|712|713|714|715|716|717|718|719|720|724|727|731|732|734|740|754|757|760|763|765|769|770|772|773|775|785|786|801|802|803|804|805|806|810|812|813|814|815|816|817|818|828|830|831|832|843|845|847|850|856|858|859|860|862|863|864|865|870|901|903|904|906|908|909|913|914|915|916|918|919|920|925|928|936|937|940|949|951|952|954|956|970|971|973|979|989)\.html',
			'i');

		if (typeof _productTags[url] == 'string') {
			return _productTags[url];
		} else if (_regex.test(url)) {
			return 'online';
		} else {
			return false;
		}
	}

	var _productTag = getProductTag(window.location.pathname);
	if (_productTag && !CookiesManager.getCookie('product_tags')) {
		CookiesManager.setCookie('product_tags', _productTag, 30, '/', cookieSetHost);
	}
})();

(function() {
	// gw-3616
	var tellapalId = GetParamsManager.is_set('tellapal.id');
	if (!!tellapalId) {
		CookiesManager.setCookie('tellapal_id', tellapalId, '', '/', cookieSetHost);
	}

// gw-4291
	var today = new Date();
	if (CookiesManager.getCookie('gw_new_visitor') == null) {
		CookiesManager.setCookie('gw_new_visitor', today.getTime(), 30, '/', cookieSetHost);
	} else {
		var gw_new_visitor = CookiesManager.getCookie('gw_new_visitor');
//time fix, deleted after 2014-02-01 GW-17303
		if (gw_new_visitor.indexOf(" ") != -1) {
			var enterDay = new Date(gw_new_visitor);
		} else {
			var enterDay = new Date();
			enterDay.setTime(gw_new_visitor);
		}

		var one_day = 1000 * 60 * 60 * 24;
		var difference = today.getTime() - enterDay.getTime();
		if (gw_new_visitor != 'false' && difference > one_day) {
			var time = 30 - Math.ceil(difference) / one_day;
			CookiesManager.setCookie('gw_new_visitor', 'false', time, '/', cookieSetHost);
		}
	}
})();


(function() {
	/* start insert GW-9686 */



	var param = GetParamsManager.get2array();
	var pid = param['pid'];
	var bmid = param['bmid'];

	var refcode = CookiesManager.getCookie('gw_refcode');
	if (!!refcode) {
		bmid = refcode;
		CookiesManager.setCookie('gw_bmid', escape(bmid), '', '/', cookieSetHost);
		CookiesManager.delCookie('gw_refcode', '/', cookieSetHost);
	}

	if (pid && !(!!bmid)) {
		param['bmid'] = pid;
	}

	var flag_reload = false;
	var path = document.location.hostname + document.location.pathname;
	var converts_array = {
		'\.extremefax\.com\/msofficeapp\.asp': [{pid: 'MSOMRKTML_XF'}],
		'\.ringcentral\.com\/lp\/msmarketplace\.asp': [{pid: 'MSOMRKTML_RC'}],
		'\.ringcentral\.com\/partners\/vista\.htm': [{pid: 'VISTAOL8'}],
		'\.ringcentral\.com\/concentric\/rco': [{pid: 'CONRC08'}, {sid: '2512'}, {aid: '6505'}],
		'internetfax101\.com': [{pid: 'INTFAX101'}]
	}
	for (var pattern in converts_array) {
		var reg1 = new RegExp(pattern, "i");
		if (reg1.test(path))
			for (var i = 0; i < converts_array[pattern].length; i++) {
				var item = converts_array[pattern][i];
				for (var key in item) {
					if ((typeof param[key] != 'undefined') && (param[key] != item[key])) {
						param[key] = item[key];
						flag_reload = true;
					}
					CookiesManager.setCookie('gw_' + key, item[key], '', '/', cookieSetHost);
					if (key == 'pid')
						pid = param[key];
				}
			}
		if (flag_reload) {
			var request = [];
			for (var key in param) {
				request.push(key + '=' + param[key]);
			}
			var rr = window.document.referrer;
			if (!!rr) {
				request.push('gw_referrer=' + encodeURIComponent(rr));
			}
			window.location = 'http://' + path + "?" + request.join('&');
			break;
		}
	}
	/* end insert */
})();


})(jQuery);