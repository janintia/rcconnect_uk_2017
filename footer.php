<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
				</div><!-- .row -->
			</div><!-- .container -->
			<?php // get_template_part( 'template-parts/related', 'posts' ); ?>
			<?php get_template_part( 'template-parts/footer', 'resources' ); ?>
		</div><!-- #content -->
	
		<footer id="colophon" class="site-footer pt-40 pb-30">
			<div class="wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="logo-footer-container">
								<div class="logo-wrap pull-sm-left">
									<a href="<?php echo rc_gw_url(); ?>" class="display-block">
										<?php rc_uk_logo_white(); ?>
									</a>
								</div>
								
								<!--<ul id="footer-menu" class="hidden-xs list-unstyled list-inline text-uppercase pull-left">
									<li><a href="<?php echo site_url(); ?>" class="">Home</a></li>
								</ul>-->
																
								<div class="iconBlock pull-sm-right">
									<div class="item">
										<a href="https://www.skyhighnetworks.com/cloud-trust-program/" target="_blank" title="Skyhigh Enterprise-ready">
											<img class="lazy lazy-hidden" data-lazy-type="image" data-src="https://www.ringcentral.co.uk/etc/designs/rc-common/gem/images/skyhight.png" width="50" height="50" class="lazy-ready processed" src="<?php echo site_url(). '/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif'; ?>">
										</a>
									</div>
									<div class="item">
										<a href="javascript:void('');" title="<?php esc_html_e( 'Trustwave Trusted Commerce', 'rcconnect_uk_2017' ); ?>">
											<img onclick="javascript:window.open('https://sealserver.trustkeeper.net/compliance/cert.php?code=331bb3cfa0cf06b30cd5fafd57e7188e', 'hATW', 'location=no, toolbar=no, resizable=no, scrollbars=yes, directories=no, status=no, width=520, height=335'); return false;" class="lazy lazy-hidden" data-lazy-type="image" data-src="https://www.ringcentral.co.uk/etc/designs/rc-common/gem/images/trustwave.png" width="82" height="43" class="lazy-ready processed" src="<?php echo site_url(). '/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif'; ?>" style="width:82px;height:43px;">
										</a>
									</div>
								</div>
								
								<div class="pull-sm-right mt-10">
									<ul class="social-media-icons small mb-0 list-unstyled list-inline">
										<li><a href="https://twitter.com/RingCentralUK" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li><a href="https://plus.google.com/+ringcentral" target="_blank"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="https://ph.linkedin.com/company/ringcentral" target="_blank"><i class="fa fa-linkedin"></i></a></li>
										<li><a href="https://www.youtube.com/channel/UCb-RwqXeY_wcPWpBIw6dvNQ" target="_blank"><i class="fa fa-youtube"></i></a></li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="bottomFooter">
								<div class="copyrightBlock">
									<?php esc_html_e( '© 1999-2018 RingCentral, Inc. All rights reserved.', 'rcconnect_uk_2017' ); ?>
								</div>
								<div class="bottomMenu">
									<ul>
										<li class="firstItem">
											<a href="/whyringcentral/contactus.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'Contact Us', 'rcconnect_uk_2017' ); ?>">
												<?php esc_html_e( 'Contact Us', 'rcconnect_uk_2017' ); ?></a>
										</li>
										<li class="secondItem">
												<a href="/whyringcentral/company.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'About RingCentral', 'rcconnect_uk_2017' ); ?>">
													<?php esc_html_e( 'About RingCentral', 'rcconnect_uk_2017' ); ?></a>
											</li>
										<li class="thirdItem">
												<a href="/partner/agent.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'Partners', 'rcconnect_uk_2017' ); ?>">
													<?php esc_html_e( 'Partners', 'rcconnect_uk_2017' ); ?></a>
											</li>
										<li class="fourthItem mobItem">
												<a href="/legal/tos.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'Legal', 'rcconnect_uk_2017' ); ?>">
													<?php esc_html_e( 'Legal', 'rcconnect_uk_2017' ); ?></a>
											</li>
										<li class="fifthItem">
												<a href="/cookies.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'Cookie Policy', 'rcconnect_uk_2017' ); ?>">
													<?php esc_html_e( 'Cookie Policy', 'rcconnect_uk_2017' ); ?></a>
											</li>
										<li class="sixthItem mobItem">
												<a href="/legal/privacy-notice.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'Privacy Notice', 'rcconnect_uk_2017' ); ?>">
													<?php esc_html_e( 'Privacy Notice', 'rcconnect_uk_2017' ); ?></a>
											</li>
										<li class="seventhItem mobItem">
												<a href="/sitemap.html<?php echo rc_bmid_param(); ?>" title="<?php esc_html_e( 'Sitemap', 'rcconnect_uk_2017' ); ?>">
													<?php esc_html_e( 'Sitemap', 'rcconnect_uk_2017' ); ?></a>
										</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
						</div><!-- .col-md-12 -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-body">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close X</button>
		<div>
			<iframe width="100%" height="550" src="" sandbox="allow-scripts allow-same-origin allow-popups allow-presentation"></iframe>
		</div>
		</div>
	</div>
	</div>
</div>
<?php wp_footer(); ?>

<script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>
