/**
 *
 * Gulpfile setup
 *
 * @since 1.0.0
 * @authors Jan Intia
 * @package rc_uk_blog_2017
 */

/* Project Config */
var 
	buildInclude 	= [
		// include common file types
		'**/*.php',
		'**/*.html',
		'**/*.css',
		'**/*.js',
		'**/*.svg',
		'**/*.ttf',
		'**/*.otf',
		'**/*.eot',
		'**/*.woff',
		'**/*.woff2',

		// include specific files and folders
		'screenshot.png',

		// exclude files and folders
		'!node_modules/**/*',
		'!assets/bower_components/**/*',
		'!style.css.map',
		'!assets/js/custom/*',
		'!assets/css/patrials/*'

	]
;

/* Gulp and Plugins */
var
	gulp          	= require('gulp'),
	plumber 		= require('gulp-plumber'),
	sass          	= require('gulp-sass'),
	watch 			= require('gulp-watch'),
	cssnano 		= require('gulp-cssnano'),
	rename 			= require('gulp-rename'),
	concat        	= require('gulp-concat'),
	uglify        	= require('gulp-uglify'),
	merge2 			= require('merge2'),
	imagemin 		= require('gulp-imagemin'),
	ignore 			= require('gulp-ignore'),
	rimraf 			= require('gulp-rimraf'),
	clone 			= require('gulp-clone'),
	merge 			= require('gulp-merge'),
	sourcemaps 		= require('gulp-sourcemaps'),
	browserSync 	= require('browser-sync').create(),
	del 			= require('del'),
	cleanCSS 		= require('gulp-clean-css'),
	gulpSequence 	= require('gulp-sequence'),

	gutil         	= require('gulp-util'),
	newer         	= require('gulp-newer'),
	postcss       	= require('gulp-postcss'),
	deporder      	= require('gulp-deporder'),
	stripdebug    	= require('gulp-strip-debug')
;

// Defining base paths
var basePaths = {
    node: './node_modules/',
    build_css: './css/',
    build_js: './js/',
    dev: './src/'
};

// browser-sync watched files
// automatically reloads the page when files changed
var browserSyncWatchFiles = [
    './css/*.min.css',
    './js/*.min.js',
    './**/*.html',
    './**/*.php'
];

// browser-sync options
// see: https://www.browsersync.io/docs/options/
var browserSyncOptions = {
    proxy: "localhost/ringcentral-uk/blog/",
    notify: false
};

// Run:
// gulp browser-sync
// Starts browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    browserSync.init( browserSyncWatchFiles, browserSyncOptions );
});

// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task('watch', function() {
	gulp.watch( basePaths.dev + 'scss/**/*.scss', ['styles'] );
    gulp.watch( [ basePaths.dev + 'js/**/*.js' ], ['scripts'] );
});

// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function () {
    var stream = gulp.src(basePaths.dev + 'scss/*.scss')
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(gulp.dest('./css'))
        //.pipe(rename('custom-editor-style.css'))
    return stream;
});

// Run:
// gulp cssnano
// Minifies CSS files
gulp.task('cssnano', function(){
  return gulp.src('./css/theme.css')
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano({discardComments: {removeAll: true}}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./css/'))
});

gulp.task('minify-css', function() {
  return gulp.src('./css/theme.css')
  .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(cleanCSS({compatibility: '*'}))
    .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
    .pipe(rename({suffix: '.min'}))
     .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./css/'));
});

gulp.task('cleancss', function() {
  return gulp.src('./css/*.min.css', { read: false }) // much faster
    .pipe(ignore('theme.css'))
    .pipe(rimraf());
});

gulp.task('styles', function(callback){ gulpSequence('sass', 'minify-css')(callback) });

// Run: 
// gulp scripts. 
// Uglifies and concat all JS files into one
gulp.task('scripts', function() {
    var scripts = [
		basePaths.dev + 'js/bootstrap/bootstrap.min.js',
		basePaths.dev + 'js/theia/ResizeSensor.min.js',
		basePaths.dev + 'js/theia/theia-sticky-sidebar.min.js',
		// basePaths.dev + 'js/theia/theia-sticky-sidebar.js',
		basePaths.dev + 'js/jquery.fitvids.js',
		basePaths.dev + 'js/fontfaceobserver.standalone.js',
		basePaths.dev + 'js/main.js'
	];
	
  gulp.src(scripts)
    .pipe(concat('theme.min.js'))
    .pipe(uglify())
	.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest('./js/'));

  gulp.src(scripts)
    .pipe(concat('theme.js'))
	.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest('./js/'));
});

// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch-bs', ['browser-sync', 'watch', 'scripts'], function () { });

// Run:
// gulp copy-assets.
// Copy all needed dependency assets files from bower_component assets to themes /js, /scss and /fonts folder. Run this task after bower install or bower update

////////////////// All Bootstrap SASS  Assets /////////////////////////
gulp.task('copy-assets', function() {

////////////////// All Bootstrap 3.37 Assets /////////////////////////
// Copy all Bootstrap JS files
    var stream = gulp.src( [ basePaths.node + 'bootstrap/dist/js/**/*.js', '!'+ basePaths.node + 'bootstrap/dist/js/**/npm.js' ] )
       .pipe(gulp.dest(basePaths.dev + '/js/bootstrap'));
  

// Copy all Bootstrap SCSS files
    gulp.src(basePaths.node + 'bootstrap-sass/assets/stylesheets/**/*.scss')
       .pipe(gulp.dest(basePaths.dev + '/scss/bootstrap'));

////////////////// End Bootstrap 3.37 Assets /////////////////////////

// Copy Fontfaceobserver
	gulp.src(basePaths.node + 'fontfaceobserver/fontfaceobserver.standalone.js')
        .pipe(gulp.dest(basePaths.dev + '/js'));

// Copy all theia sidebar
    gulp.src(basePaths.node + 'theia-sticky-sidebar/dist/ResizeSensor.min.js')
        .pipe(gulp.dest(basePaths.dev + '/js/theia'));
    gulp.src(basePaths.node + 'theia-sticky-sidebar/dist/theia-sticky-sidebar.min.js')
        .pipe(gulp.dest(basePaths.dev + '/js/theia'));

// // Copy all Font Awesome Fonts
    // gulp.src(basePaths.node + 'font-awesome/fonts/**/*.{ttf,woff,woff2,eof,svg}')
        // .pipe(gulp.dest('./fonts'));

// // Copy all Font Awesome SCSS files
    // gulp.src(basePaths.node + 'font-awesome/scss/*.scss')
        // .pipe(gulp.dest(basePaths.dev + '/scss/fontawesome'));

// Copy jQuery
    gulp.src(basePaths.node + 'jquery/dist/jquery.min.js')
        .pipe(gulp.dest(basePaths.dev + '/js'));

    return stream;
});
