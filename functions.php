<?php
/**
 * RC_Connect_UK_2017 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage RC_Connect_UK_2017
 * @since 1.0
 */

/**
 * Widget Areas.
 */
require get_parent_theme_file_path( '/inc/widget-areas.php' );

/**
 * Setup functions.
 */
require get_parent_theme_file_path( '/inc/setup.php' );

/**
 * Enqueue styles and scripts.
 */
require get_parent_theme_file_path( '/inc/enqueue.php' );

/**
 * Custom template functions for this theme.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Login Page
 */
require get_parent_theme_file_path( '/inc/login-page.php' );

/**
 * Theme Customizer
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

//Author box
if( function_exists( 'add_amp_theme_support' ) ) {
	add_amp_theme_support('AMP-author-box');
}
	
/**
 * Extras
 */
require get_parent_theme_file_path( '/inc/extras.php' );
	
/**
 * Shortcodes
 */
require get_parent_theme_file_path( '/inc/shortcodes.php' );
	
/**
 * Share Buttons
 */
require get_parent_theme_file_path( '/inc/share-buttons.php' );

/**
 * Author Box
 */
require get_parent_theme_file_path( '/inc/author-box.php' );

/**
 * Twitter Sidebar
 */
require get_parent_theme_file_path( '/inc/tweets.php' );

/**
 * User Profile
 */
require get_parent_theme_file_path( '/inc/user-profile.php' );

/**
 * Security functions
 */
require get_parent_theme_file_path( '/inc/security.php' );

/**
 * Proxy Specific functions
 */
require get_parent_theme_file_path( '/inc/proxy-server.php' );

/**
 * Tracking Codes
 */
require get_parent_theme_file_path( '/inc/tracking-codes.php' );

/**
 * SEO
 */
require get_parent_theme_file_path( '/inc/seo.php' );

/**
 * Social BMID
 */
require get_parent_theme_file_path( '/inc/social-bmid.php' );

/**
 * Related posts
 */
require get_parent_theme_file_path( '/inc/related-posts.php' );

/**
 * Content Banners
 */
require get_parent_theme_file_path( '/inc/content-banner.php' );

/**
 * Font Loader
 */
require get_parent_theme_file_path( '/inc/font-loader.php' );
// require get_parent_theme_file_path( '/inc/content-banner.php' );

/**
 * Metabox
 */
require get_parent_theme_file_path( '/inc/metabox.php' );

/**
 * Widgets
 */
require get_parent_theme_file_path( '/inc/widgets/rc-uk-newsletter-subscribe.php' );
require get_parent_theme_file_path( '/inc/widgets/rc-uk-social-media-profiles.php' );
require get_parent_theme_file_path( '/inc/widgets/rc-uk-video.php' );
