<?php
/**
 * The template for displaying author pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package underscores_sample
 */

get_header(); 
$current_author = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		//if ( have_posts() || !have_posts() ) : ?>

			<header class="page-header">
				<h1><?php esc_html_e( 'Author Profile', 'rcconnect_uk_2017' ); ?></h1>
			</header><!-- .page-header -->
			<div class="row mb-60">
				<div class="col-md-3">					
					<?php
					//Assuming $post is in scope
					if (function_exists ( 'mt_profile_img' ) ) {
						$author_id = $current_author->ID;
						mt_profile_img( $author_id, array(
							'size' => 'rcconnect_uk_2017-author-profile',
							'attr' => array( 'class' => 'mb-30 circle author-avatar' ),
							'echo' => true )
						);
					}
					?>
					
					<?php
					$author_social_icons = '';
					$author_social = '<div class="author-social-links mb-20"><ul class="author_box_social list-unstyled text-left d-inline-block">';	
						if( get_the_author_meta( 'twitter', get_query_var('author') ) ) {
							$author_social_icons .= '<li><a href="' . esc_url( get_the_author_meta( 'twitter' ) ) . '" target="_blank"><i class="fa fa-twitter"></i><span>Twitter profile</span></a></li>';
						}
						if( get_the_author_meta('gplus',get_query_var('author') ) ) {
							$author_social_icons .= '<li><a href="' . esc_url( get_the_author_meta( 'gplus' ) ) . '" target="_blank"><i class="fa fa-google-plus"></i><span>Google+ profile</span></a></li>';
						}		
						if( get_the_author_meta('linkedin',get_query_var('author') ) ) {
							$author_social_icons .= '<li><a href="' . esc_url( get_the_author_meta( 'linkedin' ) ) . '" target="_blank"><i class="fa fa-linkedin"></i><span>LinkedIn profile</span></a></li>';
						}
					$author_social .= $author_social_icons . '</ul></div>';
					
					if( !empty( $author_social_icons ) ) {
						echo $author_social;
					}
					?>
				</div>
				<div class="col-md-9 author-desc">
					<?php // the_archive_title( '<h2 class="page-title">', '</h2>' ); ?>
					<?php echo '<h2 class="page-title">' . get_the_author_meta( 'display_name', $author_id ) . '</h2>'; ?>
					<span class="position"><?php echo ( get_the_author_meta( 'company_position' ) ) ? get_the_author_meta( 'company_position' ) : ''; ?></span>
					
					<h3 class="about-me"><?php esc_html_e( 'About me', 'rcconnect_uk_2017' ); ?></h3>
					<?php // the_archive_description( '<div class="author-description">', '</div>' ); ?>
					<?php echo '<div class="author-description">' . wpautop( get_the_author_meta( 'description', $author_id ) ) . '</div>'; ?>
					
					<h3 class="about-me mb-20"><?php esc_html_e( 'The RingCentral Blog topics I\'m interested in', 'rcconnect_uk_2017' ); ?></h3>
					<?php rc_uk_list_author_categories(); ?>
				</div>
			</div>
			<div class="bg-light section-wide">
				<h2 class="h3 mb-30"><?php esc_html_e( 'Articles', 'rcconnect_uk_2017' ); ?></h2>
				<div class="row multi-columns-row author-posts">
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						?>
						<div class="col-md-4">
						<?php
						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/post/author-content' );
						?>
						</div>
						<?php
					endwhile;
					?>
				</div>
			</div>
			<?php
			the_posts_navigation();

		//else :

		//	get_template_part( 'template-parts/content', 'none' );

		//endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
