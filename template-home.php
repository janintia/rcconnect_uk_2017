<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage RingCentral UK Blog
 * @since RingCentral UK Blog 1.0
 */
global $wp_query; 
get_header(); ?>

<div id="primary" class="content-area">
	<div class="theiaStickySidebar">
		<main id="main" class="site-main" role="main">
			<?php // Show the selected frontpage content.
			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
			 
			$the_query = new WP_Query(
				array(
					'post_type' => 'post',
					'posts_per_page' => get_option( 'posts_per_page' ),
					'paged' => $paged
				)
			);
			
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) :  $the_query->the_post();
					get_template_part( 'template-parts/post/content-list' );
				endwhile;
				
				if (  $the_query->max_num_pages > 1 ) {
					echo '
						<div class="clearfix"></div>
						<div class="ajax-load-more mb-40">
							<a href="#" class="btn btn-default rc_loadmore_btn"><i class="fa fa-plus-circle"></i>Show More</a>
						</div>';					
				}
				
			} else { 
				get_template_part( 'template-parts/post/content', 'none' );
			} 
			
			wp_reset_postdata();
			?>
		</main><!-- #main -->
	</div>
</div><!-- #primary -->

<?php 
get_sidebar();
get_footer();
