<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage RC_Connect_UK_2017
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() && ! is_author() ) : ?>
		<div class="post-thumbnail featured-image">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'rcconnect_uk_2017-post-feature' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
	
	<div class="entry-box">	
		<header class="entry-header">
			<?php
			
			if ( is_single() ) {
				the_title( '<h1 class="entry_title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry_title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
			
			if ( 'post' === get_post_type() ) {
				echo '<div class="entry-meta">';
					if ( is_single() ) {
						rcconnect_uk_2017_posted_on();
					} else {
						echo rcconnect_uk_2017_time_link();
						rcconnect_uk_2017_edit_link();
					};
				echo '</div><!-- .entry-meta -->';
			};

			?>
			
		</header><!-- .entry-header -->


		<div class="entry_summary">
			<?php
			/* translators: %s: Name of current post */
			// the_content( sprintf(
				// __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'rcconnect_uk_2017' ),
				// get_the_title()
			// ) );
			
			the_excerpt();

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'rcconnect_uk_2017' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
			?>
		</div><!-- .entry_summary -->

		<?php
		if ( is_single() ) {
			rcconnect_uk_2017_entry_footer();
		} else {
			?>
			<div class="entry-footer">
				<div class="entry-auth pull-left">
					<div class="avatar">
						<?php
						if ( function_exists ( 'mt_profile_img' ) ) {
							$author_id = $post->post_author;
							mt_profile_img( $author_id, array(
								'size' => 'rcconnect_uk_2017-avatar',
								'echo' => true )
							);
						}
						?>						
					</div>
					<div class="auth-name">
						<span class="name">
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">
								<?php the_author(); ?>
							</a>
						</span>
						<span class="desc"><?php echo ( get_the_author_meta( 'author_role' ) ) ? get_the_author_meta( 'author_role' ) : 'Author'; ?></span>
					</div>
				</div>
				<div class="entry-categories pull-right">					
					<?php
						$cat_info = rc_uk_get_category_info( wp_get_post_categories( get_the_ID() ) );
						
						if( ! empty( $cat_info ) ) {
							?>
							
						<ul class="entry-categories list-unstyled list-inline pull-left">
							<?php
							foreach( $cat_info as $ci ) { ?>
								<li><a href="<?php echo esc_url( $ci['url'] ); ?>"><?php echo $ci['name']; ?></a></li>
							<?php }
							?>
						</ul>
						<?php
						}
					?>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php
		}
		?>
	</div>
</article><!-- #post-## -->
