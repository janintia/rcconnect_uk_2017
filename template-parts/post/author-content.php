<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage RC_Connect_UK_2017
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() && ! is_author() ) : ?>
		<div class="post-thumbnail featured-image">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'rcconnect_uk_2017-post-feature' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
	
	<div class="entry-box">	
		<header class="entry-header">
			<?php
			if ( 'post' === get_post_type() ) {
				echo '<div class="entry-meta">';
					if ( is_single() ) {
						rcconnect_uk_2017_posted_on();
					} else {
						echo rcconnect_uk_2017_time_link();
						rcconnect_uk_2017_edit_link();
					};
				echo '</div><!-- .entry-meta -->';
			};

			if ( is_single() ) {
				the_title( '<h1 class="entry_title">', '</h1>' );
			} else {
				the_title( '<h3 class="entry_title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			}
			?>
		</header><!-- .entry-header -->


		<div class="entry_summary">
			<?php
			/* translators: %s: Name of current post */
			// the_content( sprintf(
				// __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'rcconnect_uk_2017' ),
				// get_the_title()
			// ) );
			
			the_excerpt();

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'rcconnect_uk_2017' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
			?>
		</div><!-- .entry_summary -->

		<?php
		if ( is_single() ) {
			rcconnect_uk_2017_entry_footer();
		} else {
			?>
			<div class="entry-footer">
				<div class="entry-categories pull-left">					
					<?php
					$cat_info = rc_uk_get_category_info( wp_get_post_categories( get_the_ID() ) );
					
					if( ! empty( $cat_info ) ) {
						?>
						<ul class="entry-categories list-unstyled list-inline pull-left">
							<?php
							foreach( $cat_info as $ci ) { ?>
								<li><a href="<?php echo esc_url( $ci['url'] ); ?>"><?php echo $ci['name']; ?></a></li>
							<?php }
							?>
						</ul>
						<?php
					}
					?>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php
		}
		?>
	</div>
</article><!-- #post-## -->
