<?php ?>
<div class="container mb-20 mt-20">
	<div class="row">	
		<div class="col-md-12">
			<div class="curated-contents display-row">
				<?php 
					for ( $count = 1; $count <= 3; $count++ ) {
						// Setup the Post data
						$post = get_post( get_theme_mod( 'post_dropdown_setting_' . $count ) );
						setup_postdata( $post );
						$thumb_url = get_the_post_thumbnail_url( null, 'rcconnect_uk_2017-post-feature-1000x664' );
						$cat_info = rc_uk_get_category_info( wp_get_post_categories( get_the_ID() ) );
						
						if( $count == 1 ) {
							?>
							<div class="main-card"> 
								<div class="card border-card">
									<a href="<?php the_permalink(); ?>" class="link-block">
										<div class="featured-image" style="background-image: url('<?php echo $thumb_url; ?>')"></div>
									</a>
									<div class="card-content ">
										<?php if( ! empty( $cat_info ) ) { ?>
											<ul class="card-meta">
												<?php foreach( $cat_info as $ci ) { ?>
													<li class="card-subtext">
														<a href="<?php echo esc_url( $ci['url'] ); ?>"><?php echo $ci['name']; ?></a>
													</li>
												<?php } ?>
											</ul>
										<?php } ?>
										<h2 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<div class="card-author">by <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></div>
									</div>
								
								</div>
							</div><!--main-card-->
							<?php
						} else {
							if( $count == 2 ) {
								?>
								<div class="sub-cards">
								<?php
							}
							?>
							
							<div class="sub-card-wrapper <?php echo ( 3 == $count ) ? 'two' : ''; ?>">
								<div class="card border-card">
									<a href="<?php the_permalink(); ?>" class="link-block">
										<div class="featured-image" style="background-image: url('<?php echo $thumb_url; ?>')"></div>
									</a>
									<div class="card-content">
										<?php if( ! empty( $cat_info ) ) { ?>
										<ul class="card-meta">
											<?php foreach( $cat_info as $ci ) { ?>
												<li class="card-subtext">
													<a href="<?php echo esc_url( $ci['url'] ); ?>"><?php echo $ci['name']; ?></a>
												</li>
											<?php } ?>
										</ul>
										<?php } ?>
										<h2 class="card-title"><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
										<div class="card-author">by <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></div>
									</div>
								</div>
							</div><!--sub-card-wrapper-->
							
							<?php
							if( $count == 3 ) {
								?>
								</div><!--sub-cards-->
								<?php
							}
						}
					} 
					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</div>