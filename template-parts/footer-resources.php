		<div class="bg-light pt-60 pb-40">
			<div class="container">
				<div class="row mediaBlock">
					<div class="col-md-4 mb-20">
						<a class="item" href="https://netstorage.ringcentral.com/uk/documents/buyers_guide.pdf<?php echo rc_bmid_param(); ?>" target="_blank">
							<span class="mediaBlockHeader">
								<img src="<?php echo site_url(). '/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif'; ?>" class="lazy lazy-hidden" data-lazy-type="image" data-src="<?php echo get_theme_file_uri( '/images/resources/buyers_guide.jpg' ); ?>" alt="<?php esc_html_e( '#1 cloud communications provider worldwide in revenue and subscriber seats', 'rcconnect_uk_2017' ); ?>">
							</span> 
							<div class="mediaBlockContent">
								<span class="mediaBlockInfoTitle"><b><?php esc_html_e( 'Guide', 'rcconnect_uk_2017' ); ?></b></span> 
								<h3 class="text"><?php esc_html_e( 'VoIP Buyer\'s Guide eBook', 'rcconnect_uk_2017' ); ?></h3>
							</div>
						</a> 
					</div>					
					<div class="col-md-4 mb-20">
						<a class="item" href="" target="_blank" data-toggle="modal" data-target="#videoModalfooter" data-thevideo="https://www.youtube.com/embed/0FoTstucf9U">
							<span class="mediaBlockHeader">
								<span class="play-btn"></span>
								<img src="<?php echo site_url(). '/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif'; ?>" class="lazy lazy-hidden" data-lazy-type="image" data-src="<?php echo get_theme_file_uri( '/images/resources/phone-system-in-the-cloud.jpg' ); ?>" alt="<?php esc_html_e( 'How Does A Phone System in the Cloud Work?', 'rcconnect_uk_2017' ); ?>">
							</span> 
							<div class="mediaBlockContent">
								<span class="mediaBlockInfoTitle"><b><?php esc_html_e( 'Video', 'rcconnect_uk_2017' ); ?></b></span> 
								<h3 class="text"><?php esc_html_e( 'How Does A Phone System in the Cloud Work?', 'rcconnect_uk_2017' ); ?></h3>
							</div>
						</a> 
						<div class="modal fade" id="videoModalfooter" tabindex="-1" role="dialog" aria-labelledby="videoModalfooter" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
							<div class="modal-content">
							  <div class="modal-body">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close X</button>
								<div>
								  <iframe width="100%" height="550" src="" sandbox="allow-scripts allow-same-origin allow-popups allow-presentation"></iframe>
								</div>
							  </div>
							</div>
						  </div>
						</div>
					</div>					
					<div class="col-md-4 mb-20">
						<a class="item" href="https://www.ringcentral.co.uk/lp/gartner-magic-quadrant.html<?php echo rc_bmid_param(); ?>" target="_blank">
							<span class="mediaBlockHeader">
								<img src="<?php echo site_url(). '/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif'; ?>" class="lazy lazy-hidden" data-lazy-type="image" data-src="<?php echo get_theme_file_uri( '/images/resources/MQ_UCAAS.jpg' ); ?>" alt="<?php esc_html_e( 'A Gartner UCaaS Magic Quadrant Leader. Again.', 'rcconnect_uk_2017' ); ?>">
							</span> 
							<div class="mediaBlockContent">
								<span class="mediaBlockInfoTitle"><b><?php esc_html_e( 'Report', 'rcconnect_uk_2017' ); ?></b></span> 
								<h3 class="text"><?php esc_html_e( 'A Gartner UCaaS Magic Quadrant Leader. Again.', 'rcconnect_uk_2017' ); ?></h3>
							</div> 
						</a>
					</div>
				</div>
			</div><!-- .container -->
		</div><!-- .bg-light -->
