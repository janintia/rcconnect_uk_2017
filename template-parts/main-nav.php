<?php 
$is_home = is_home();
$is_front_page = is_front_page();
?>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">	
				<div class="row">
					<div class="col-md-12">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-nav" aria-expanded="false">
							  <span class="sr-only"><?php esc_html_e( 'Toggle navigation', 'rcconnect_uk_2017' ); ?></span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							</button>
							
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="blog-home"><?php esc_html_e( 'Blog Home', 'rcconnect_uk_2017' ); ?></a>
							
							<div class="logo-wrap">
								<?php if ( $is_home && $is_front_page ) { ?>
								<h1>
									<?php } ?>
										<a class="navbar-brand" href="<?php echo rc_gw_url(); ?>" title="RingCentral Virtual PBX, Phone and Internet Fax Service and Software">
											<?php rc_uk_logo(); ?>										
										</a>
										
									<?php if ( $is_home && $is_front_page ) { ?>
								</h1>
								<?php } ?>
							</div><!--Brand-->
							
						</div><!--.navbar-header -->
						
						<div class="collapse navbar-collapse" id="header-nav">
							
							<?php get_search_form(); ?>
						
							<?php wp_nav_menu( array(
									//'menu' => 'Main menu',
									'container' => '',
									'menu_class' => 'nav navbar-nav navbar-right',
									'theme_location' => 'primary',
								) ); 
							?>
						</div><!--/.navbar-collapse -->	
					</div>
				</div>
			</div><!--/.container -->
		</nav>  