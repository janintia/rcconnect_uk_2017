<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rcconnect_uk_2017
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry-box' ); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry_title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php rcconnect_uk_2017_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry_summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry_summary -->

	<footer class="entry-footer">
		<?php rcconnect_uk_2017_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
