<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rcconnect_uk_2017
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
	
	<header class="entry-header _inset-column">
		<?php	
		
		// $categories_list = get_the_category_list();
		// echo $categories_list;
		
		if ( is_singular() ) :
			the_title( '<h1 class="entry_title">', '</h1>' );
		else :
			the_title( '<h2 class="entry_title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php rcconnect_uk_2017_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	
	<?php if ( '' !== get_the_post_thumbnail() ) : ?>
		<div class="post-thumbnail featured-image _inset-column wide">
			<?php the_post_thumbnail( 'rcconnect_uk_2017-post-feature' ); ?>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
	
	<div class="entry-content _inset-column">
		<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'rcconnect_uk_2017' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'rcconnect_uk_2017' ),
				'after'  => '</div>',
			) );
		?>
		
		<?php // get_template_part( 'template-parts/article-subscribe', 'form' ); ?>
		<div class="clearfix"></div>
	</div><!-- .entry-content -->
	
	<footer class="entry-footer _inset-column">
		<?php rcconnect_uk_2017_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->

