<?php
/**
 * Template Name: Infographic Page
 *
 * @package WordPress
 * @subpackage RingCentral UK Theme
 * @since RingCentral UK Theme 1.0
 */

get_header(); ?>

<div id="primary" class="col-md-10 col-md-offset-1">
    <main id="main" class="site-main">

        <?php
        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', 'page' );
            ?>

            <div class="infographic loaded">
                <div class="preloader">
                    <span></span>
                    <span></span>
                    <span></span>
                    <i></i>
                </div>
                <div class="textBlock animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/bubble.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/bubble.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/bubble.gif?1538478729897&quot;);">
                    <h1>Business are saying: <span>1 is better then many</span></h1>
                    <p>More companies than ever are using cloud communications and business apps to support their distributed workforces. But using stand-alone apps independently can actually stifle the productivity gains made by going to the cloud.</p>
                </div>
                <div class="man animate animating" data-delay="4000" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/jumper.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/jumper.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/jumper.gif?1538478729897&quot;);"></div>
                <div class="company"></div>
                <div class="percent percent_1 animate animating" data-delay="1000" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/text_86.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/text_86.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/text_86.gif?1538478729897&quot;);">
                    <p class="p_title">86%</p>
                    <p class="p_text">of enterprises say the most important driver for adopting cloud services is gaining the flexibility to support a mobile workforce.</p>
                </div>
                <div class="window_1 animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_circle.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_color_circle.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_circle.gif?1538478729897&quot;);"></div>
                <div class="window_2 animate animating" data-delay="1000" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_sleep2.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_empty_sleep2.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_sleep2.gif?1538478729897&quot;);"></div>
                <div class="window_3 animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_chair.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_color_chair.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_chair.gif?1538478729897&quot;);"></div>
                <div class="window_4 animate animating" data-delay="1000" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_sleep.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_empty_sleep.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_sleep.gif?1538478729897&quot;);"></div>

                <div class="percent percent_2 animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/text_79.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/text_79.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/text_79.gif?1538478729897&quot;);">
                    <p class="p_title">79%</p>
                    <p class="p_text">of enterprises say integrating voice with business apps is “very important.”</p>
                </div>
                <div class="window_5 animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_sport.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_color_sport.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_sport.gif?1538478729897&quot;);"></div>
                <div class="window_6 animate animating" data-delay="1000" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_3.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_empty_3.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_3.gif?1538478729897&quot;);"></div>
                <div class="window_7 animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_yoga.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_color_yoga.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_yoga.gif?1538478729897&quot;);"></div>
                <div class="window_8 animate animating" data-delay="1000" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_big.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_empty_big.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_empty_big.gif?1538478729897&quot;);"></div>
                <div class="two-as-one animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/text_2_working.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/text_2_working.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/text_2_working.gif?1538478729897&quot;);">
                    <p class="p_title">2 working as 1</p>
                    <p class="p_text">Infusing live communications into your business apps makes disparate applications work more efficiently as a single unit.</p>
                </div>
                <div class="window_9 animate animating" data-delay="0" data-gif="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_big.gif" data-gif2x="https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/2x/window_color_big.gif" style="background-image: url(&quot;https://www.ringcentral.com/content/dam/ringcentral/images/office/business-apps-infographic/window_color_big.gif?1538478729897&quot;);"></div>
                <div class="infographic_cta_block animate animating" data-delay="1500">
                    <p class="p_title">Businesses are recognising that</p>
                    <h2>having apps work together creates greater workflow efficiencies.</h2>
                    <p class="p_text">Isn’t that why you started using cloud services?</p>
                    <p>
                        <a href="http://marketo.ringcentral.com/infonetics-wp.html" target="_blank" class="cta_btn">
                            <span>read white paper:</span>
                            Maximising the Power of the Cloud
                        </a>
                    </p>
                </div>
            </div>

            <div class="download_block" style="">
                <div class="page">
                    <div class="download"><a href="http://netstorage.ringcentral.com/documents/google_infographic.pdf" target="_blank">Download PDF</a> (1.2 MB)</div>
                    <div class="scrollBlock">Scroll down to learn more</div>
                    <div class="clear"></div>
                </div>
            </div>

            <?php
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

        endwhile; // End of the loop.
        ?>
        
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();