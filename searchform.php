<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage rcconnect_uk_2017
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="navbar-form navbar-right search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="<?php echo esc_attr( $unique_id ); ?>" style="display:none;">
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'rcconnect_uk_2017' ); ?></span>
	</label>
	<input type="search" id="<?php echo esc_attr( $unique_id ); ?>" class="form-control search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'rcconnect_uk_2017' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="button--chromeless search-submit">
		<i class="fa fa-fw fa-lg fa-search"></i>
		<span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'rcconnect_uk_2017' ); ?></span>
	</button>
</form>